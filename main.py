import sys

from src.constants import CompilerConstants
from src.controller import Controller


def main():
    if CompilerConstants.ARG_HELP in sys.argv:
        print(f"Compiler Version: {CompilerConstants.VERSION}")
        print("-" * 30)
        print(f"Usage: {CompilerConstants.COMPILER_NAME} <filename>.minion <args*>")
        print("Arguments:")
        print(f"- {CompilerConstants.ARG_PRINT}: Prints the generated code to std::out")
        print(f"- {CompilerConstants.ARG_PRINT_MINION}: Prints the source minion code next to the converted code as comment")
        print(f"- {CompilerConstants.ARG_DEBUG}: Adds a section to the output for printing the model")
        print(f"- {CompilerConstants.ARG_OPTIMIZE}: Removes unused variables from SMT-LIB2 output")
        sys.exit(0)

    controller = Controller()
    data = controller.read_file()
    controller.compile(data)


if __name__ == '__main__':
    main()
