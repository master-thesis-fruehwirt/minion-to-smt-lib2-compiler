# Generated from Minion.g4 by ANTLR 4.12.0
from antlr4 import *
if __name__ is not None and "." in __name__:
    from .MinionParser import MinionParser
else:
    from MinionParser import MinionParser

# This class defines a complete generic visitor for a parse tree produced by MinionParser.

class MinionVisitor(ParseTreeVisitor):

    # Visit a parse tree produced by MinionParser#program.
    def visitProgram(self, ctx:MinionParser.ProgramContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MinionParser#input_section.
    def visitInput_section(self, ctx:MinionParser.Input_sectionContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MinionParser#variablesSection.
    def visitVariablesSection(self, ctx:MinionParser.VariablesSectionContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MinionParser#domain.
    def visitDomain(self, ctx:MinionParser.DomainContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MinionParser#domainSparsebound.
    def visitDomainSparsebound(self, ctx:MinionParser.DomainSparseboundContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MinionParser#listDeclaration.
    def visitListDeclaration(self, ctx:MinionParser.ListDeclarationContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MinionParser#BoolVarDecl.
    def visitBoolVarDecl(self, ctx:MinionParser.BoolVarDeclContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MinionParser#SparseboundVarDecl.
    def visitSparseboundVarDecl(self, ctx:MinionParser.SparseboundVarDeclContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MinionParser#BoundVarDecl.
    def visitBoundVarDecl(self, ctx:MinionParser.BoundVarDeclContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MinionParser#DiscreteVarDecl.
    def visitDiscreteVarDecl(self, ctx:MinionParser.DiscreteVarDeclContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MinionParser#constraintsSection.
    def visitConstraintsSection(self, ctx:MinionParser.ConstraintsSectionContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MinionParser#SimpleConstraint.
    def visitSimpleConstraint(self, ctx:MinionParser.SimpleConstraintContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MinionParser#WatchedConstraint.
    def visitWatchedConstraint(self, ctx:MinionParser.WatchedConstraintContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MinionParser#tableConstraint.
    def visitTableConstraint(self, ctx:MinionParser.TableConstraintContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MinionParser#tableRow.
    def visitTableRow(self, ctx:MinionParser.TableRowContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MinionParser#ArgVariable.
    def visitArgVariable(self, ctx:MinionParser.ArgVariableContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MinionParser#ArgInteger.
    def visitArgInteger(self, ctx:MinionParser.ArgIntegerContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MinionParser#ArgConstraint.
    def visitArgConstraint(self, ctx:MinionParser.ArgConstraintContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MinionParser#ArgList.
    def visitArgList(self, ctx:MinionParser.ArgListContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MinionParser#NotUsedArgListAccess.
    def visitNotUsedArgListAccess(self, ctx:MinionParser.NotUsedArgListAccessContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MinionParser#ArgTableConstraint.
    def visitArgTableConstraint(self, ctx:MinionParser.ArgTableConstraintContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MinionParser#list.
    def visitList(self, ctx:MinionParser.ListContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MinionParser#ListVariable.
    def visitListVariable(self, ctx:MinionParser.ListVariableContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MinionParser#ListInteger.
    def visitListInteger(self, ctx:MinionParser.ListIntegerContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MinionParser#NotUsedListAccess.
    def visitNotUsedListAccess(self, ctx:MinionParser.NotUsedListAccessContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MinionParser#NotUsedList.
    def visitNotUsedList(self, ctx:MinionParser.NotUsedListContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MinionParser#listAccess.
    def visitListAccess(self, ctx:MinionParser.ListAccessContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MinionParser#searchSection.
    def visitSearchSection(self, ctx:MinionParser.SearchSectionContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MinionParser#varValOrdering.
    def visitVarValOrdering(self, ctx:MinionParser.VarValOrderingContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MinionParser#order.
    def visitOrder(self, ctx:MinionParser.OrderContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MinionParser#varOrder.
    def visitVarOrder(self, ctx:MinionParser.VarOrderContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MinionParser#valOrder.
    def visitValOrder(self, ctx:MinionParser.ValOrderContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MinionParser#optimisationFn.
    def visitOptimisationFn(self, ctx:MinionParser.OptimisationFnContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MinionParser#printFormat.
    def visitPrintFormat(self, ctx:MinionParser.PrintFormatContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MinionParser#tuplelistSection.
    def visitTuplelistSection(self, ctx:MinionParser.TuplelistSectionContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MinionParser#tupleList.
    def visitTupleList(self, ctx:MinionParser.TupleListContext):
        return self.visitChildren(ctx)



del MinionParser