# Generated from Minion.g4 by ANTLR 4.12.0
from antlr4 import *
if __name__ is not None and "." in __name__:
    from .MinionParser import MinionParser
else:
    from MinionParser import MinionParser

# This class defines a complete listener for a parse tree produced by MinionParser.
class MinionListener(ParseTreeListener):

    # Enter a parse tree produced by MinionParser#program.
    def enterProgram(self, ctx:MinionParser.ProgramContext):
        pass

    # Exit a parse tree produced by MinionParser#program.
    def exitProgram(self, ctx:MinionParser.ProgramContext):
        pass


    # Enter a parse tree produced by MinionParser#input_section.
    def enterInput_section(self, ctx:MinionParser.Input_sectionContext):
        pass

    # Exit a parse tree produced by MinionParser#input_section.
    def exitInput_section(self, ctx:MinionParser.Input_sectionContext):
        pass


    # Enter a parse tree produced by MinionParser#variablesSection.
    def enterVariablesSection(self, ctx:MinionParser.VariablesSectionContext):
        pass

    # Exit a parse tree produced by MinionParser#variablesSection.
    def exitVariablesSection(self, ctx:MinionParser.VariablesSectionContext):
        pass


    # Enter a parse tree produced by MinionParser#domain.
    def enterDomain(self, ctx:MinionParser.DomainContext):
        pass

    # Exit a parse tree produced by MinionParser#domain.
    def exitDomain(self, ctx:MinionParser.DomainContext):
        pass


    # Enter a parse tree produced by MinionParser#domainSparsebound.
    def enterDomainSparsebound(self, ctx:MinionParser.DomainSparseboundContext):
        pass

    # Exit a parse tree produced by MinionParser#domainSparsebound.
    def exitDomainSparsebound(self, ctx:MinionParser.DomainSparseboundContext):
        pass


    # Enter a parse tree produced by MinionParser#listDeclaration.
    def enterListDeclaration(self, ctx:MinionParser.ListDeclarationContext):
        pass

    # Exit a parse tree produced by MinionParser#listDeclaration.
    def exitListDeclaration(self, ctx:MinionParser.ListDeclarationContext):
        pass


    # Enter a parse tree produced by MinionParser#BoolVarDecl.
    def enterBoolVarDecl(self, ctx:MinionParser.BoolVarDeclContext):
        pass

    # Exit a parse tree produced by MinionParser#BoolVarDecl.
    def exitBoolVarDecl(self, ctx:MinionParser.BoolVarDeclContext):
        pass


    # Enter a parse tree produced by MinionParser#SparseboundVarDecl.
    def enterSparseboundVarDecl(self, ctx:MinionParser.SparseboundVarDeclContext):
        pass

    # Exit a parse tree produced by MinionParser#SparseboundVarDecl.
    def exitSparseboundVarDecl(self, ctx:MinionParser.SparseboundVarDeclContext):
        pass


    # Enter a parse tree produced by MinionParser#BoundVarDecl.
    def enterBoundVarDecl(self, ctx:MinionParser.BoundVarDeclContext):
        pass

    # Exit a parse tree produced by MinionParser#BoundVarDecl.
    def exitBoundVarDecl(self, ctx:MinionParser.BoundVarDeclContext):
        pass


    # Enter a parse tree produced by MinionParser#DiscreteVarDecl.
    def enterDiscreteVarDecl(self, ctx:MinionParser.DiscreteVarDeclContext):
        pass

    # Exit a parse tree produced by MinionParser#DiscreteVarDecl.
    def exitDiscreteVarDecl(self, ctx:MinionParser.DiscreteVarDeclContext):
        pass


    # Enter a parse tree produced by MinionParser#constraintsSection.
    def enterConstraintsSection(self, ctx:MinionParser.ConstraintsSectionContext):
        pass

    # Exit a parse tree produced by MinionParser#constraintsSection.
    def exitConstraintsSection(self, ctx:MinionParser.ConstraintsSectionContext):
        pass


    # Enter a parse tree produced by MinionParser#SimpleConstraint.
    def enterSimpleConstraint(self, ctx:MinionParser.SimpleConstraintContext):
        pass

    # Exit a parse tree produced by MinionParser#SimpleConstraint.
    def exitSimpleConstraint(self, ctx:MinionParser.SimpleConstraintContext):
        pass


    # Enter a parse tree produced by MinionParser#WatchedConstraint.
    def enterWatchedConstraint(self, ctx:MinionParser.WatchedConstraintContext):
        pass

    # Exit a parse tree produced by MinionParser#WatchedConstraint.
    def exitWatchedConstraint(self, ctx:MinionParser.WatchedConstraintContext):
        pass


    # Enter a parse tree produced by MinionParser#tableConstraint.
    def enterTableConstraint(self, ctx:MinionParser.TableConstraintContext):
        pass

    # Exit a parse tree produced by MinionParser#tableConstraint.
    def exitTableConstraint(self, ctx:MinionParser.TableConstraintContext):
        pass


    # Enter a parse tree produced by MinionParser#tableRow.
    def enterTableRow(self, ctx:MinionParser.TableRowContext):
        pass

    # Exit a parse tree produced by MinionParser#tableRow.
    def exitTableRow(self, ctx:MinionParser.TableRowContext):
        pass


    # Enter a parse tree produced by MinionParser#ArgVariable.
    def enterArgVariable(self, ctx:MinionParser.ArgVariableContext):
        pass

    # Exit a parse tree produced by MinionParser#ArgVariable.
    def exitArgVariable(self, ctx:MinionParser.ArgVariableContext):
        pass


    # Enter a parse tree produced by MinionParser#ArgInteger.
    def enterArgInteger(self, ctx:MinionParser.ArgIntegerContext):
        pass

    # Exit a parse tree produced by MinionParser#ArgInteger.
    def exitArgInteger(self, ctx:MinionParser.ArgIntegerContext):
        pass


    # Enter a parse tree produced by MinionParser#ArgConstraint.
    def enterArgConstraint(self, ctx:MinionParser.ArgConstraintContext):
        pass

    # Exit a parse tree produced by MinionParser#ArgConstraint.
    def exitArgConstraint(self, ctx:MinionParser.ArgConstraintContext):
        pass


    # Enter a parse tree produced by MinionParser#ArgList.
    def enterArgList(self, ctx:MinionParser.ArgListContext):
        pass

    # Exit a parse tree produced by MinionParser#ArgList.
    def exitArgList(self, ctx:MinionParser.ArgListContext):
        pass


    # Enter a parse tree produced by MinionParser#NotUsedArgListAccess.
    def enterNotUsedArgListAccess(self, ctx:MinionParser.NotUsedArgListAccessContext):
        pass

    # Exit a parse tree produced by MinionParser#NotUsedArgListAccess.
    def exitNotUsedArgListAccess(self, ctx:MinionParser.NotUsedArgListAccessContext):
        pass


    # Enter a parse tree produced by MinionParser#ArgTableConstraint.
    def enterArgTableConstraint(self, ctx:MinionParser.ArgTableConstraintContext):
        pass

    # Exit a parse tree produced by MinionParser#ArgTableConstraint.
    def exitArgTableConstraint(self, ctx:MinionParser.ArgTableConstraintContext):
        pass


    # Enter a parse tree produced by MinionParser#list.
    def enterList(self, ctx:MinionParser.ListContext):
        pass

    # Exit a parse tree produced by MinionParser#list.
    def exitList(self, ctx:MinionParser.ListContext):
        pass


    # Enter a parse tree produced by MinionParser#ListVariable.
    def enterListVariable(self, ctx:MinionParser.ListVariableContext):
        pass

    # Exit a parse tree produced by MinionParser#ListVariable.
    def exitListVariable(self, ctx:MinionParser.ListVariableContext):
        pass


    # Enter a parse tree produced by MinionParser#ListInteger.
    def enterListInteger(self, ctx:MinionParser.ListIntegerContext):
        pass

    # Exit a parse tree produced by MinionParser#ListInteger.
    def exitListInteger(self, ctx:MinionParser.ListIntegerContext):
        pass


    # Enter a parse tree produced by MinionParser#NotUsedListAccess.
    def enterNotUsedListAccess(self, ctx:MinionParser.NotUsedListAccessContext):
        pass

    # Exit a parse tree produced by MinionParser#NotUsedListAccess.
    def exitNotUsedListAccess(self, ctx:MinionParser.NotUsedListAccessContext):
        pass


    # Enter a parse tree produced by MinionParser#NotUsedList.
    def enterNotUsedList(self, ctx:MinionParser.NotUsedListContext):
        pass

    # Exit a parse tree produced by MinionParser#NotUsedList.
    def exitNotUsedList(self, ctx:MinionParser.NotUsedListContext):
        pass


    # Enter a parse tree produced by MinionParser#listAccess.
    def enterListAccess(self, ctx:MinionParser.ListAccessContext):
        pass

    # Exit a parse tree produced by MinionParser#listAccess.
    def exitListAccess(self, ctx:MinionParser.ListAccessContext):
        pass


    # Enter a parse tree produced by MinionParser#searchSection.
    def enterSearchSection(self, ctx:MinionParser.SearchSectionContext):
        pass

    # Exit a parse tree produced by MinionParser#searchSection.
    def exitSearchSection(self, ctx:MinionParser.SearchSectionContext):
        pass


    # Enter a parse tree produced by MinionParser#varValOrdering.
    def enterVarValOrdering(self, ctx:MinionParser.VarValOrderingContext):
        pass

    # Exit a parse tree produced by MinionParser#varValOrdering.
    def exitVarValOrdering(self, ctx:MinionParser.VarValOrderingContext):
        pass


    # Enter a parse tree produced by MinionParser#order.
    def enterOrder(self, ctx:MinionParser.OrderContext):
        pass

    # Exit a parse tree produced by MinionParser#order.
    def exitOrder(self, ctx:MinionParser.OrderContext):
        pass


    # Enter a parse tree produced by MinionParser#varOrder.
    def enterVarOrder(self, ctx:MinionParser.VarOrderContext):
        pass

    # Exit a parse tree produced by MinionParser#varOrder.
    def exitVarOrder(self, ctx:MinionParser.VarOrderContext):
        pass


    # Enter a parse tree produced by MinionParser#valOrder.
    def enterValOrder(self, ctx:MinionParser.ValOrderContext):
        pass

    # Exit a parse tree produced by MinionParser#valOrder.
    def exitValOrder(self, ctx:MinionParser.ValOrderContext):
        pass


    # Enter a parse tree produced by MinionParser#optimisationFn.
    def enterOptimisationFn(self, ctx:MinionParser.OptimisationFnContext):
        pass

    # Exit a parse tree produced by MinionParser#optimisationFn.
    def exitOptimisationFn(self, ctx:MinionParser.OptimisationFnContext):
        pass


    # Enter a parse tree produced by MinionParser#printFormat.
    def enterPrintFormat(self, ctx:MinionParser.PrintFormatContext):
        pass

    # Exit a parse tree produced by MinionParser#printFormat.
    def exitPrintFormat(self, ctx:MinionParser.PrintFormatContext):
        pass


    # Enter a parse tree produced by MinionParser#tuplelistSection.
    def enterTuplelistSection(self, ctx:MinionParser.TuplelistSectionContext):
        pass

    # Exit a parse tree produced by MinionParser#tuplelistSection.
    def exitTuplelistSection(self, ctx:MinionParser.TuplelistSectionContext):
        pass


    # Enter a parse tree produced by MinionParser#tupleList.
    def enterTupleList(self, ctx:MinionParser.TupleListContext):
        pass

    # Exit a parse tree produced by MinionParser#tupleList.
    def exitTupleList(self, ctx:MinionParser.TupleListContext):
        pass



del MinionParser