# Generated from Minion.g4 by ANTLR 4.12.0
# encoding: utf-8
from antlr4 import *
from io import StringIO
import sys
if sys.version_info[1] > 5:
	from typing import TextIO
else:
	from typing.io import TextIO

def serializedATN():
    return [
        4,1,44,283,2,0,7,0,2,1,7,1,2,2,7,2,2,3,7,3,2,4,7,4,2,5,7,5,2,6,7,
        6,2,7,7,7,2,8,7,8,2,9,7,9,2,10,7,10,2,11,7,11,2,12,7,12,2,13,7,13,
        2,14,7,14,2,15,7,15,2,16,7,16,2,17,7,17,2,18,7,18,2,19,7,19,2,20,
        7,20,2,21,7,21,2,22,7,22,2,23,7,23,1,0,1,0,4,0,51,8,0,11,0,12,0,
        52,1,0,1,0,1,1,1,1,1,1,1,1,3,1,61,8,1,1,2,1,2,5,2,65,8,2,10,2,12,
        2,68,9,2,1,3,1,3,1,3,1,3,1,3,1,3,1,4,1,4,1,4,1,4,5,4,80,8,4,10,4,
        12,4,83,9,4,1,4,1,4,1,5,1,5,1,5,1,5,5,5,91,8,5,10,5,12,5,94,9,5,
        1,5,1,5,1,6,1,6,1,6,3,6,101,8,6,1,6,1,6,1,6,3,6,106,8,6,1,6,1,6,
        1,6,1,6,3,6,112,8,6,1,6,1,6,1,6,1,6,3,6,118,8,6,1,6,3,6,121,8,6,
        1,7,1,7,5,7,125,8,7,10,7,12,7,128,9,7,1,8,1,8,1,8,1,8,1,8,5,8,135,
        8,8,10,8,12,8,138,9,8,1,8,1,8,1,8,1,8,1,8,1,8,1,8,1,8,4,8,148,8,
        8,11,8,12,8,149,1,8,1,8,1,8,3,8,155,8,8,1,9,1,9,1,9,1,9,5,9,161,
        8,9,10,9,12,9,164,9,9,1,9,1,9,1,10,1,10,1,10,1,10,5,10,172,8,10,
        10,10,12,10,175,9,10,1,10,1,10,1,11,3,11,180,8,11,1,11,1,11,3,11,
        184,8,11,1,11,1,11,1,11,1,11,1,11,3,11,191,8,11,1,12,1,12,1,12,1,
        12,5,12,197,8,12,10,12,12,12,200,9,12,1,12,1,12,1,13,3,13,205,8,
        13,1,13,1,13,3,13,209,8,13,1,13,1,13,1,13,3,13,214,8,13,1,14,3,14,
        217,8,14,1,14,1,14,1,14,1,15,1,15,5,15,224,8,15,10,15,12,15,227,
        9,15,1,15,3,15,230,8,15,1,15,3,15,233,8,15,1,16,1,16,3,16,237,8,
        16,1,17,1,17,1,18,1,18,3,18,243,8,18,1,18,3,18,246,8,18,1,18,1,18,
        1,19,1,19,1,19,4,19,253,8,19,11,19,12,19,254,1,19,1,19,1,20,1,20,
        1,20,1,21,1,21,1,21,1,21,3,21,266,8,21,1,22,1,22,5,22,270,8,22,10,
        22,12,22,273,9,22,1,23,1,23,1,23,1,23,4,23,279,8,23,11,23,12,23,
        280,1,23,0,0,24,0,2,4,6,8,10,12,14,16,18,20,22,24,26,28,30,32,34,
        36,38,40,42,44,46,0,3,1,0,29,36,1,0,43,44,1,0,37,38,303,0,48,1,0,
        0,0,2,60,1,0,0,0,4,62,1,0,0,0,6,69,1,0,0,0,8,75,1,0,0,0,10,86,1,
        0,0,0,12,120,1,0,0,0,14,122,1,0,0,0,16,154,1,0,0,0,18,156,1,0,0,
        0,20,167,1,0,0,0,22,190,1,0,0,0,24,192,1,0,0,0,26,213,1,0,0,0,28,
        216,1,0,0,0,30,221,1,0,0,0,32,234,1,0,0,0,34,238,1,0,0,0,36,240,
        1,0,0,0,38,249,1,0,0,0,40,258,1,0,0,0,42,261,1,0,0,0,44,267,1,0,
        0,0,46,274,1,0,0,0,48,50,5,15,0,0,49,51,3,2,1,0,50,49,1,0,0,0,51,
        52,1,0,0,0,52,50,1,0,0,0,52,53,1,0,0,0,53,54,1,0,0,0,54,55,5,16,
        0,0,55,1,1,0,0,0,56,61,3,4,2,0,57,61,3,30,15,0,58,61,3,14,7,0,59,
        61,3,44,22,0,60,56,1,0,0,0,60,57,1,0,0,0,60,58,1,0,0,0,60,59,1,0,
        0,0,61,3,1,0,0,0,62,66,5,17,0,0,63,65,3,12,6,0,64,63,1,0,0,0,65,
        68,1,0,0,0,66,64,1,0,0,0,66,67,1,0,0,0,67,5,1,0,0,0,68,66,1,0,0,
        0,69,70,5,1,0,0,70,71,5,13,0,0,71,72,5,2,0,0,72,73,5,13,0,0,73,74,
        5,3,0,0,74,7,1,0,0,0,75,76,5,1,0,0,76,81,5,13,0,0,77,78,5,4,0,0,
        78,80,5,13,0,0,79,77,1,0,0,0,80,83,1,0,0,0,81,79,1,0,0,0,81,82,1,
        0,0,0,82,84,1,0,0,0,83,81,1,0,0,0,84,85,5,3,0,0,85,9,1,0,0,0,86,
        87,5,5,0,0,87,92,5,13,0,0,88,89,5,4,0,0,89,91,5,13,0,0,90,88,1,0,
        0,0,91,94,1,0,0,0,92,90,1,0,0,0,92,93,1,0,0,0,93,95,1,0,0,0,94,92,
        1,0,0,0,95,96,5,6,0,0,96,11,1,0,0,0,97,98,5,21,0,0,98,100,5,42,0,
        0,99,101,3,10,5,0,100,99,1,0,0,0,100,101,1,0,0,0,101,121,1,0,0,0,
        102,103,5,23,0,0,103,105,5,42,0,0,104,106,3,10,5,0,105,104,1,0,0,
        0,105,106,1,0,0,0,106,107,1,0,0,0,107,121,3,8,4,0,108,109,5,22,0,
        0,109,111,5,42,0,0,110,112,3,10,5,0,111,110,1,0,0,0,111,112,1,0,
        0,0,112,113,1,0,0,0,113,121,3,6,3,0,114,115,5,24,0,0,115,117,5,42,
        0,0,116,118,3,10,5,0,117,116,1,0,0,0,117,118,1,0,0,0,118,119,1,0,
        0,0,119,121,3,6,3,0,120,97,1,0,0,0,120,102,1,0,0,0,120,108,1,0,0,
        0,120,114,1,0,0,0,121,13,1,0,0,0,122,126,5,19,0,0,123,125,3,16,8,
        0,124,123,1,0,0,0,125,128,1,0,0,0,126,124,1,0,0,0,126,127,1,0,0,
        0,127,15,1,0,0,0,128,126,1,0,0,0,129,130,5,42,0,0,130,131,5,7,0,
        0,131,136,3,22,11,0,132,133,5,4,0,0,133,135,3,22,11,0,134,132,1,
        0,0,0,135,138,1,0,0,0,136,134,1,0,0,0,136,137,1,0,0,0,137,139,1,
        0,0,0,138,136,1,0,0,0,139,140,5,8,0,0,140,155,1,0,0,0,141,142,5,
        42,0,0,142,143,5,7,0,0,143,144,5,1,0,0,144,147,3,16,8,0,145,146,
        5,4,0,0,146,148,3,16,8,0,147,145,1,0,0,0,148,149,1,0,0,0,149,147,
        1,0,0,0,149,150,1,0,0,0,150,151,1,0,0,0,151,152,5,3,0,0,152,153,
        5,8,0,0,153,155,1,0,0,0,154,129,1,0,0,0,154,141,1,0,0,0,155,17,1,
        0,0,0,156,157,5,1,0,0,157,162,3,20,10,0,158,159,5,4,0,0,159,161,
        3,20,10,0,160,158,1,0,0,0,161,164,1,0,0,0,162,160,1,0,0,0,162,163,
        1,0,0,0,163,165,1,0,0,0,164,162,1,0,0,0,165,166,5,3,0,0,166,19,1,
        0,0,0,167,168,5,9,0,0,168,173,5,13,0,0,169,170,5,4,0,0,170,172,5,
        13,0,0,171,169,1,0,0,0,172,175,1,0,0,0,173,171,1,0,0,0,173,174,1,
        0,0,0,174,176,1,0,0,0,175,173,1,0,0,0,176,177,5,10,0,0,177,21,1,
        0,0,0,178,180,5,25,0,0,179,178,1,0,0,0,179,180,1,0,0,0,180,181,1,
        0,0,0,181,191,5,42,0,0,182,184,5,25,0,0,183,182,1,0,0,0,183,184,
        1,0,0,0,184,185,1,0,0,0,185,191,5,13,0,0,186,191,3,16,8,0,187,191,
        3,24,12,0,188,191,3,28,14,0,189,191,3,18,9,0,190,179,1,0,0,0,190,
        183,1,0,0,0,190,186,1,0,0,0,190,187,1,0,0,0,190,188,1,0,0,0,190,
        189,1,0,0,0,191,23,1,0,0,0,192,193,5,5,0,0,193,198,3,26,13,0,194,
        195,5,4,0,0,195,197,3,26,13,0,196,194,1,0,0,0,197,200,1,0,0,0,198,
        196,1,0,0,0,198,199,1,0,0,0,199,201,1,0,0,0,200,198,1,0,0,0,201,
        202,5,6,0,0,202,25,1,0,0,0,203,205,5,25,0,0,204,203,1,0,0,0,204,
        205,1,0,0,0,205,206,1,0,0,0,206,214,5,42,0,0,207,209,5,25,0,0,208,
        207,1,0,0,0,208,209,1,0,0,0,209,210,1,0,0,0,210,214,5,13,0,0,211,
        214,3,28,14,0,212,214,3,24,12,0,213,204,1,0,0,0,213,208,1,0,0,0,
        213,211,1,0,0,0,213,212,1,0,0,0,214,27,1,0,0,0,215,217,5,25,0,0,
        216,215,1,0,0,0,216,217,1,0,0,0,217,218,1,0,0,0,218,219,5,42,0,0,
        219,220,3,24,12,0,220,29,1,0,0,0,221,225,5,18,0,0,222,224,3,32,16,
        0,223,222,1,0,0,0,224,227,1,0,0,0,225,223,1,0,0,0,225,226,1,0,0,
        0,226,229,1,0,0,0,227,225,1,0,0,0,228,230,3,40,20,0,229,228,1,0,
        0,0,229,230,1,0,0,0,230,232,1,0,0,0,231,233,3,42,21,0,232,231,1,
        0,0,0,232,233,1,0,0,0,233,31,1,0,0,0,234,236,3,36,18,0,235,237,3,
        38,19,0,236,235,1,0,0,0,236,237,1,0,0,0,237,33,1,0,0,0,238,239,7,
        0,0,0,239,35,1,0,0,0,240,242,5,26,0,0,241,243,5,28,0,0,242,241,1,
        0,0,0,242,243,1,0,0,0,243,245,1,0,0,0,244,246,3,34,17,0,245,244,
        1,0,0,0,245,246,1,0,0,0,246,247,1,0,0,0,247,248,3,24,12,0,248,37,
        1,0,0,0,249,250,5,27,0,0,250,252,5,5,0,0,251,253,7,1,0,0,252,251,
        1,0,0,0,253,254,1,0,0,0,254,252,1,0,0,0,254,255,1,0,0,0,255,256,
        1,0,0,0,256,257,5,6,0,0,257,39,1,0,0,0,258,259,7,2,0,0,259,260,5,
        42,0,0,260,41,1,0,0,0,261,265,5,39,0,0,262,266,3,24,12,0,263,266,
        5,40,0,0,264,266,5,41,0,0,265,262,1,0,0,0,265,263,1,0,0,0,265,264,
        1,0,0,0,266,43,1,0,0,0,267,271,5,20,0,0,268,270,3,46,23,0,269,268,
        1,0,0,0,270,273,1,0,0,0,271,269,1,0,0,0,271,272,1,0,0,0,272,45,1,
        0,0,0,273,271,1,0,0,0,274,275,5,42,0,0,275,276,5,13,0,0,276,278,
        5,13,0,0,277,279,5,13,0,0,278,277,1,0,0,0,279,280,1,0,0,0,280,278,
        1,0,0,0,280,281,1,0,0,0,281,47,1,0,0,0,34,52,60,66,81,92,100,105,
        111,117,120,126,136,149,154,162,173,179,183,190,198,204,208,213,
        216,225,229,232,236,242,245,254,265,271,280
    ]

class MinionParser ( Parser ):

    grammarFileName = "Minion.g4"

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    sharedContextCache = PredictionContextCache()

    literalNames = [ "<INVALID>", "'{'", "'..'", "'}'", "','", "'['", "']'", 
                     "'('", "')'", "'<'", "'>'", "<INVALID>", "<INVALID>", 
                     "<INVALID>", "<INVALID>", "'MINION 3'", "<INVALID>", 
                     "'**VARIABLES**'", "'**SEARCH**'", "'**CONSTRAINTS**'", 
                     "'**TUPLELIST**'", "'BOOL'", "'BOUND'", "'SPARSEBOUND'", 
                     "'DISCRETE'", "'!'", "'VARORDER'", "'VALORDER'", "'AUX'", 
                     "'STATIC'", "'SDF'", "'SRF'", "'LDF'", "'ORIGINAL'", 
                     "'WDEG'", "'CONFLICT'", "'DOMOVERWDEG'", "'MINIMISING'", 
                     "'MAXIMISING'", "'PRINT'", "'ALL'", "'NONE'", "<INVALID>", 
                     "'a'", "'d'" ]

    symbolicNames = [ "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "WS", "COMMENT", 
                      "INT", "BOOL_LIT", "KEY_MINION", "KEY_EOF", "KEY_VARIABLES", 
                      "KEY_SEARCH", "KEY_CONSTRAINTS", "KEY_TUPLELIST", 
                      "KEY_BOOL", "KEY_BOUND", "KEY_SPARSEBOUND", "KEY_DISCRETE", 
                      "KEY_NOT", "KEY_VARORDER", "KEY_VALORDER", "KEY_AUX", 
                      "KEY_ORDER_STATIC", "KEY_ORDER_SDF", "KEY_ORDER_SRF", 
                      "KEY_ORDER_LDF", "KEY_ORDER_ORIGINAL", "KEY_ORDER_WDEG", 
                      "KEY_ORDER_CONFLICT", "KEY_ORDER_DOMOVERWDEG", "KEY_MINIMISING", 
                      "KEY_MAXIMISING", "KEY_PRINT", "KEY_ALL", "KEY_NONE", 
                      "ID", "KEY_ASCENDING", "KEY_DESCENDING" ]

    RULE_program = 0
    RULE_input_section = 1
    RULE_variablesSection = 2
    RULE_domain = 3
    RULE_domainSparsebound = 4
    RULE_listDeclaration = 5
    RULE_varDeclaration = 6
    RULE_constraintsSection = 7
    RULE_constraint = 8
    RULE_tableConstraint = 9
    RULE_tableRow = 10
    RULE_arg = 11
    RULE_list = 12
    RULE_listValue = 13
    RULE_listAccess = 14
    RULE_searchSection = 15
    RULE_varValOrdering = 16
    RULE_order = 17
    RULE_varOrder = 18
    RULE_valOrder = 19
    RULE_optimisationFn = 20
    RULE_printFormat = 21
    RULE_tuplelistSection = 22
    RULE_tupleList = 23

    ruleNames =  [ "program", "input_section", "variablesSection", "domain", 
                   "domainSparsebound", "listDeclaration", "varDeclaration", 
                   "constraintsSection", "constraint", "tableConstraint", 
                   "tableRow", "arg", "list", "listValue", "listAccess", 
                   "searchSection", "varValOrdering", "order", "varOrder", 
                   "valOrder", "optimisationFn", "printFormat", "tuplelistSection", 
                   "tupleList" ]

    EOF = Token.EOF
    T__0=1
    T__1=2
    T__2=3
    T__3=4
    T__4=5
    T__5=6
    T__6=7
    T__7=8
    T__8=9
    T__9=10
    WS=11
    COMMENT=12
    INT=13
    BOOL_LIT=14
    KEY_MINION=15
    KEY_EOF=16
    KEY_VARIABLES=17
    KEY_SEARCH=18
    KEY_CONSTRAINTS=19
    KEY_TUPLELIST=20
    KEY_BOOL=21
    KEY_BOUND=22
    KEY_SPARSEBOUND=23
    KEY_DISCRETE=24
    KEY_NOT=25
    KEY_VARORDER=26
    KEY_VALORDER=27
    KEY_AUX=28
    KEY_ORDER_STATIC=29
    KEY_ORDER_SDF=30
    KEY_ORDER_SRF=31
    KEY_ORDER_LDF=32
    KEY_ORDER_ORIGINAL=33
    KEY_ORDER_WDEG=34
    KEY_ORDER_CONFLICT=35
    KEY_ORDER_DOMOVERWDEG=36
    KEY_MINIMISING=37
    KEY_MAXIMISING=38
    KEY_PRINT=39
    KEY_ALL=40
    KEY_NONE=41
    ID=42
    KEY_ASCENDING=43
    KEY_DESCENDING=44

    def __init__(self, input:TokenStream, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.12.0")
        self._interp = ParserATNSimulator(self, self.atn, self.decisionsToDFA, self.sharedContextCache)
        self._predicates = None




    class ProgramContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def KEY_MINION(self):
            return self.getToken(MinionParser.KEY_MINION, 0)

        def KEY_EOF(self):
            return self.getToken(MinionParser.KEY_EOF, 0)

        def input_section(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(MinionParser.Input_sectionContext)
            else:
                return self.getTypedRuleContext(MinionParser.Input_sectionContext,i)


        def getRuleIndex(self):
            return MinionParser.RULE_program

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterProgram" ):
                listener.enterProgram(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitProgram" ):
                listener.exitProgram(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitProgram" ):
                return visitor.visitProgram(self)
            else:
                return visitor.visitChildren(self)




    def program(self):

        localctx = MinionParser.ProgramContext(self, self._ctx, self.state)
        self.enterRule(localctx, 0, self.RULE_program)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 48
            self.match(MinionParser.KEY_MINION)
            self.state = 50 
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while True:
                self.state = 49
                self.input_section()
                self.state = 52 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if not ((((_la) & ~0x3f) == 0 and ((1 << _la) & 1966080) != 0)):
                    break

            self.state = 54
            self.match(MinionParser.KEY_EOF)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Input_sectionContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def variablesSection(self):
            return self.getTypedRuleContext(MinionParser.VariablesSectionContext,0)


        def searchSection(self):
            return self.getTypedRuleContext(MinionParser.SearchSectionContext,0)


        def constraintsSection(self):
            return self.getTypedRuleContext(MinionParser.ConstraintsSectionContext,0)


        def tuplelistSection(self):
            return self.getTypedRuleContext(MinionParser.TuplelistSectionContext,0)


        def getRuleIndex(self):
            return MinionParser.RULE_input_section

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterInput_section" ):
                listener.enterInput_section(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitInput_section" ):
                listener.exitInput_section(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitInput_section" ):
                return visitor.visitInput_section(self)
            else:
                return visitor.visitChildren(self)




    def input_section(self):

        localctx = MinionParser.Input_sectionContext(self, self._ctx, self.state)
        self.enterRule(localctx, 2, self.RULE_input_section)
        try:
            self.state = 60
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [17]:
                self.enterOuterAlt(localctx, 1)
                self.state = 56
                self.variablesSection()
                pass
            elif token in [18]:
                self.enterOuterAlt(localctx, 2)
                self.state = 57
                self.searchSection()
                pass
            elif token in [19]:
                self.enterOuterAlt(localctx, 3)
                self.state = 58
                self.constraintsSection()
                pass
            elif token in [20]:
                self.enterOuterAlt(localctx, 4)
                self.state = 59
                self.tuplelistSection()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class VariablesSectionContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def KEY_VARIABLES(self):
            return self.getToken(MinionParser.KEY_VARIABLES, 0)

        def varDeclaration(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(MinionParser.VarDeclarationContext)
            else:
                return self.getTypedRuleContext(MinionParser.VarDeclarationContext,i)


        def getRuleIndex(self):
            return MinionParser.RULE_variablesSection

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterVariablesSection" ):
                listener.enterVariablesSection(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitVariablesSection" ):
                listener.exitVariablesSection(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitVariablesSection" ):
                return visitor.visitVariablesSection(self)
            else:
                return visitor.visitChildren(self)




    def variablesSection(self):

        localctx = MinionParser.VariablesSectionContext(self, self._ctx, self.state)
        self.enterRule(localctx, 4, self.RULE_variablesSection)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 62
            self.match(MinionParser.KEY_VARIABLES)
            self.state = 66
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while (((_la) & ~0x3f) == 0 and ((1 << _la) & 31457280) != 0):
                self.state = 63
                self.varDeclaration()
                self.state = 68
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class DomainContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.lower = None # Token
            self.upper = None # Token

        def INT(self, i:int=None):
            if i is None:
                return self.getTokens(MinionParser.INT)
            else:
                return self.getToken(MinionParser.INT, i)

        def getRuleIndex(self):
            return MinionParser.RULE_domain

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterDomain" ):
                listener.enterDomain(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitDomain" ):
                listener.exitDomain(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitDomain" ):
                return visitor.visitDomain(self)
            else:
                return visitor.visitChildren(self)




    def domain(self):

        localctx = MinionParser.DomainContext(self, self._ctx, self.state)
        self.enterRule(localctx, 6, self.RULE_domain)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 69
            self.match(MinionParser.T__0)
            self.state = 70
            localctx.lower = self.match(MinionParser.INT)
            self.state = 71
            self.match(MinionParser.T__1)
            self.state = 72
            localctx.upper = self.match(MinionParser.INT)
            self.state = 73
            self.match(MinionParser.T__2)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class DomainSparseboundContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self._INT = None # Token
            self.items = list() # of Tokens

        def INT(self, i:int=None):
            if i is None:
                return self.getTokens(MinionParser.INT)
            else:
                return self.getToken(MinionParser.INT, i)

        def getRuleIndex(self):
            return MinionParser.RULE_domainSparsebound

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterDomainSparsebound" ):
                listener.enterDomainSparsebound(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitDomainSparsebound" ):
                listener.exitDomainSparsebound(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitDomainSparsebound" ):
                return visitor.visitDomainSparsebound(self)
            else:
                return visitor.visitChildren(self)




    def domainSparsebound(self):

        localctx = MinionParser.DomainSparseboundContext(self, self._ctx, self.state)
        self.enterRule(localctx, 8, self.RULE_domainSparsebound)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 75
            self.match(MinionParser.T__0)
            self.state = 76
            localctx._INT = self.match(MinionParser.INT)
            localctx.items.append(localctx._INT)
            self.state = 81
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==4:
                self.state = 77
                self.match(MinionParser.T__3)
                self.state = 78
                localctx._INT = self.match(MinionParser.INT)
                localctx.items.append(localctx._INT)
                self.state = 83
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 84
            self.match(MinionParser.T__2)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ListDeclarationContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self._INT = None # Token
            self.items = list() # of Tokens

        def INT(self, i:int=None):
            if i is None:
                return self.getTokens(MinionParser.INT)
            else:
                return self.getToken(MinionParser.INT, i)

        def getRuleIndex(self):
            return MinionParser.RULE_listDeclaration

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterListDeclaration" ):
                listener.enterListDeclaration(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitListDeclaration" ):
                listener.exitListDeclaration(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitListDeclaration" ):
                return visitor.visitListDeclaration(self)
            else:
                return visitor.visitChildren(self)




    def listDeclaration(self):

        localctx = MinionParser.ListDeclarationContext(self, self._ctx, self.state)
        self.enterRule(localctx, 10, self.RULE_listDeclaration)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 86
            self.match(MinionParser.T__4)
            self.state = 87
            localctx._INT = self.match(MinionParser.INT)
            localctx.items.append(localctx._INT)
            self.state = 92
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==4:
                self.state = 88
                self.match(MinionParser.T__3)
                self.state = 89
                localctx._INT = self.match(MinionParser.INT)
                localctx.items.append(localctx._INT)
                self.state = 94
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 95
            self.match(MinionParser.T__5)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class VarDeclarationContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return MinionParser.RULE_varDeclaration

     
        def copyFrom(self, ctx:ParserRuleContext):
            super().copyFrom(ctx)



    class DiscreteVarDeclContext(VarDeclarationContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a MinionParser.VarDeclarationContext
            super().__init__(parser)
            self.type_ = None # Token
            self.name = None # Token
            self.copyFrom(ctx)

        def domain(self):
            return self.getTypedRuleContext(MinionParser.DomainContext,0)

        def KEY_DISCRETE(self):
            return self.getToken(MinionParser.KEY_DISCRETE, 0)
        def ID(self):
            return self.getToken(MinionParser.ID, 0)
        def listDeclaration(self):
            return self.getTypedRuleContext(MinionParser.ListDeclarationContext,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterDiscreteVarDecl" ):
                listener.enterDiscreteVarDecl(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitDiscreteVarDecl" ):
                listener.exitDiscreteVarDecl(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitDiscreteVarDecl" ):
                return visitor.visitDiscreteVarDecl(self)
            else:
                return visitor.visitChildren(self)


    class BoolVarDeclContext(VarDeclarationContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a MinionParser.VarDeclarationContext
            super().__init__(parser)
            self.type_ = None # Token
            self.name = None # Token
            self.copyFrom(ctx)

        def KEY_BOOL(self):
            return self.getToken(MinionParser.KEY_BOOL, 0)
        def ID(self):
            return self.getToken(MinionParser.ID, 0)
        def listDeclaration(self):
            return self.getTypedRuleContext(MinionParser.ListDeclarationContext,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterBoolVarDecl" ):
                listener.enterBoolVarDecl(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitBoolVarDecl" ):
                listener.exitBoolVarDecl(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitBoolVarDecl" ):
                return visitor.visitBoolVarDecl(self)
            else:
                return visitor.visitChildren(self)


    class SparseboundVarDeclContext(VarDeclarationContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a MinionParser.VarDeclarationContext
            super().__init__(parser)
            self.type_ = None # Token
            self.name = None # Token
            self.copyFrom(ctx)

        def domainSparsebound(self):
            return self.getTypedRuleContext(MinionParser.DomainSparseboundContext,0)

        def KEY_SPARSEBOUND(self):
            return self.getToken(MinionParser.KEY_SPARSEBOUND, 0)
        def ID(self):
            return self.getToken(MinionParser.ID, 0)
        def listDeclaration(self):
            return self.getTypedRuleContext(MinionParser.ListDeclarationContext,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterSparseboundVarDecl" ):
                listener.enterSparseboundVarDecl(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitSparseboundVarDecl" ):
                listener.exitSparseboundVarDecl(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitSparseboundVarDecl" ):
                return visitor.visitSparseboundVarDecl(self)
            else:
                return visitor.visitChildren(self)


    class BoundVarDeclContext(VarDeclarationContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a MinionParser.VarDeclarationContext
            super().__init__(parser)
            self.type_ = None # Token
            self.name = None # Token
            self.copyFrom(ctx)

        def domain(self):
            return self.getTypedRuleContext(MinionParser.DomainContext,0)

        def KEY_BOUND(self):
            return self.getToken(MinionParser.KEY_BOUND, 0)
        def ID(self):
            return self.getToken(MinionParser.ID, 0)
        def listDeclaration(self):
            return self.getTypedRuleContext(MinionParser.ListDeclarationContext,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterBoundVarDecl" ):
                listener.enterBoundVarDecl(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitBoundVarDecl" ):
                listener.exitBoundVarDecl(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitBoundVarDecl" ):
                return visitor.visitBoundVarDecl(self)
            else:
                return visitor.visitChildren(self)



    def varDeclaration(self):

        localctx = MinionParser.VarDeclarationContext(self, self._ctx, self.state)
        self.enterRule(localctx, 12, self.RULE_varDeclaration)
        self._la = 0 # Token type
        try:
            self.state = 120
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [21]:
                localctx = MinionParser.BoolVarDeclContext(self, localctx)
                self.enterOuterAlt(localctx, 1)
                self.state = 97
                localctx.type_ = self.match(MinionParser.KEY_BOOL)
                self.state = 98
                localctx.name = self.match(MinionParser.ID)
                self.state = 100
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if _la==5:
                    self.state = 99
                    self.listDeclaration()


                pass
            elif token in [23]:
                localctx = MinionParser.SparseboundVarDeclContext(self, localctx)
                self.enterOuterAlt(localctx, 2)
                self.state = 102
                localctx.type_ = self.match(MinionParser.KEY_SPARSEBOUND)
                self.state = 103
                localctx.name = self.match(MinionParser.ID)
                self.state = 105
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if _la==5:
                    self.state = 104
                    self.listDeclaration()


                self.state = 107
                self.domainSparsebound()
                pass
            elif token in [22]:
                localctx = MinionParser.BoundVarDeclContext(self, localctx)
                self.enterOuterAlt(localctx, 3)
                self.state = 108
                localctx.type_ = self.match(MinionParser.KEY_BOUND)
                self.state = 109
                localctx.name = self.match(MinionParser.ID)
                self.state = 111
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if _la==5:
                    self.state = 110
                    self.listDeclaration()


                self.state = 113
                self.domain()
                pass
            elif token in [24]:
                localctx = MinionParser.DiscreteVarDeclContext(self, localctx)
                self.enterOuterAlt(localctx, 4)
                self.state = 114
                localctx.type_ = self.match(MinionParser.KEY_DISCRETE)
                self.state = 115
                localctx.name = self.match(MinionParser.ID)
                self.state = 117
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if _la==5:
                    self.state = 116
                    self.listDeclaration()


                self.state = 119
                self.domain()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ConstraintsSectionContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def KEY_CONSTRAINTS(self):
            return self.getToken(MinionParser.KEY_CONSTRAINTS, 0)

        def constraint(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(MinionParser.ConstraintContext)
            else:
                return self.getTypedRuleContext(MinionParser.ConstraintContext,i)


        def getRuleIndex(self):
            return MinionParser.RULE_constraintsSection

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterConstraintsSection" ):
                listener.enterConstraintsSection(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitConstraintsSection" ):
                listener.exitConstraintsSection(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitConstraintsSection" ):
                return visitor.visitConstraintsSection(self)
            else:
                return visitor.visitChildren(self)




    def constraintsSection(self):

        localctx = MinionParser.ConstraintsSectionContext(self, self._ctx, self.state)
        self.enterRule(localctx, 14, self.RULE_constraintsSection)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 122
            self.match(MinionParser.KEY_CONSTRAINTS)
            self.state = 126
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==42:
                self.state = 123
                self.constraint()
                self.state = 128
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ConstraintContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return MinionParser.RULE_constraint

     
        def copyFrom(self, ctx:ParserRuleContext):
            super().copyFrom(ctx)



    class WatchedConstraintContext(ConstraintContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a MinionParser.ConstraintContext
            super().__init__(parser)
            self.name = None # Token
            self.copyFrom(ctx)

        def constraint(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(MinionParser.ConstraintContext)
            else:
                return self.getTypedRuleContext(MinionParser.ConstraintContext,i)

        def ID(self):
            return self.getToken(MinionParser.ID, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterWatchedConstraint" ):
                listener.enterWatchedConstraint(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitWatchedConstraint" ):
                listener.exitWatchedConstraint(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitWatchedConstraint" ):
                return visitor.visitWatchedConstraint(self)
            else:
                return visitor.visitChildren(self)


    class SimpleConstraintContext(ConstraintContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a MinionParser.ConstraintContext
            super().__init__(parser)
            self.name = None # Token
            self.copyFrom(ctx)

        def arg(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(MinionParser.ArgContext)
            else:
                return self.getTypedRuleContext(MinionParser.ArgContext,i)

        def ID(self):
            return self.getToken(MinionParser.ID, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterSimpleConstraint" ):
                listener.enterSimpleConstraint(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitSimpleConstraint" ):
                listener.exitSimpleConstraint(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitSimpleConstraint" ):
                return visitor.visitSimpleConstraint(self)
            else:
                return visitor.visitChildren(self)



    def constraint(self):

        localctx = MinionParser.ConstraintContext(self, self._ctx, self.state)
        self.enterRule(localctx, 16, self.RULE_constraint)
        self._la = 0 # Token type
        try:
            self.state = 154
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,13,self._ctx)
            if la_ == 1:
                localctx = MinionParser.SimpleConstraintContext(self, localctx)
                self.enterOuterAlt(localctx, 1)
                self.state = 129
                localctx.name = self.match(MinionParser.ID)
                self.state = 130
                self.match(MinionParser.T__6)
                self.state = 131
                self.arg()
                self.state = 136
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==4:
                    self.state = 132
                    self.match(MinionParser.T__3)
                    self.state = 133
                    self.arg()
                    self.state = 138
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 139
                self.match(MinionParser.T__7)
                pass

            elif la_ == 2:
                localctx = MinionParser.WatchedConstraintContext(self, localctx)
                self.enterOuterAlt(localctx, 2)
                self.state = 141
                localctx.name = self.match(MinionParser.ID)
                self.state = 142
                self.match(MinionParser.T__6)
                self.state = 143
                self.match(MinionParser.T__0)
                self.state = 144
                self.constraint()
                self.state = 147 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while True:
                    self.state = 145
                    self.match(MinionParser.T__3)
                    self.state = 146
                    self.constraint()
                    self.state = 149 
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)
                    if not (_la==4):
                        break

                self.state = 151
                self.match(MinionParser.T__2)
                self.state = 152
                self.match(MinionParser.T__7)
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class TableConstraintContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self._tableRow = None # TableRowContext
            self.rows = list() # of TableRowContexts

        def tableRow(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(MinionParser.TableRowContext)
            else:
                return self.getTypedRuleContext(MinionParser.TableRowContext,i)


        def getRuleIndex(self):
            return MinionParser.RULE_tableConstraint

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterTableConstraint" ):
                listener.enterTableConstraint(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitTableConstraint" ):
                listener.exitTableConstraint(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitTableConstraint" ):
                return visitor.visitTableConstraint(self)
            else:
                return visitor.visitChildren(self)




    def tableConstraint(self):

        localctx = MinionParser.TableConstraintContext(self, self._ctx, self.state)
        self.enterRule(localctx, 18, self.RULE_tableConstraint)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 156
            self.match(MinionParser.T__0)
            self.state = 157
            localctx._tableRow = self.tableRow()
            localctx.rows.append(localctx._tableRow)
            self.state = 162
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==4:
                self.state = 158
                self.match(MinionParser.T__3)
                self.state = 159
                localctx._tableRow = self.tableRow()
                localctx.rows.append(localctx._tableRow)
                self.state = 164
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 165
            self.match(MinionParser.T__2)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class TableRowContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self._INT = None # Token
            self.cols = list() # of Tokens

        def INT(self, i:int=None):
            if i is None:
                return self.getTokens(MinionParser.INT)
            else:
                return self.getToken(MinionParser.INT, i)

        def getRuleIndex(self):
            return MinionParser.RULE_tableRow

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterTableRow" ):
                listener.enterTableRow(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitTableRow" ):
                listener.exitTableRow(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitTableRow" ):
                return visitor.visitTableRow(self)
            else:
                return visitor.visitChildren(self)




    def tableRow(self):

        localctx = MinionParser.TableRowContext(self, self._ctx, self.state)
        self.enterRule(localctx, 20, self.RULE_tableRow)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 167
            self.match(MinionParser.T__8)
            self.state = 168
            localctx._INT = self.match(MinionParser.INT)
            localctx.cols.append(localctx._INT)
            self.state = 173
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==4:
                self.state = 169
                self.match(MinionParser.T__3)
                self.state = 170
                localctx._INT = self.match(MinionParser.INT)
                localctx.cols.append(localctx._INT)
                self.state = 175
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 176
            self.match(MinionParser.T__9)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ArgContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return MinionParser.RULE_arg

     
        def copyFrom(self, ctx:ParserRuleContext):
            super().copyFrom(ctx)



    class ArgTableConstraintContext(ArgContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a MinionParser.ArgContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def tableConstraint(self):
            return self.getTypedRuleContext(MinionParser.TableConstraintContext,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterArgTableConstraint" ):
                listener.enterArgTableConstraint(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitArgTableConstraint" ):
                listener.exitArgTableConstraint(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitArgTableConstraint" ):
                return visitor.visitArgTableConstraint(self)
            else:
                return visitor.visitChildren(self)


    class ArgListContext(ArgContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a MinionParser.ArgContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def list_(self):
            return self.getTypedRuleContext(MinionParser.ListContext,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterArgList" ):
                listener.enterArgList(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitArgList" ):
                listener.exitArgList(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitArgList" ):
                return visitor.visitArgList(self)
            else:
                return visitor.visitChildren(self)


    class NotUsedArgListAccessContext(ArgContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a MinionParser.ArgContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def listAccess(self):
            return self.getTypedRuleContext(MinionParser.ListAccessContext,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterNotUsedArgListAccess" ):
                listener.enterNotUsedArgListAccess(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitNotUsedArgListAccess" ):
                listener.exitNotUsedArgListAccess(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitNotUsedArgListAccess" ):
                return visitor.visitNotUsedArgListAccess(self)
            else:
                return visitor.visitChildren(self)


    class ArgIntegerContext(ArgContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a MinionParser.ArgContext
            super().__init__(parser)
            self.value = None # Token
            self.copyFrom(ctx)

        def INT(self):
            return self.getToken(MinionParser.INT, 0)
        def KEY_NOT(self):
            return self.getToken(MinionParser.KEY_NOT, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterArgInteger" ):
                listener.enterArgInteger(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitArgInteger" ):
                listener.exitArgInteger(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitArgInteger" ):
                return visitor.visitArgInteger(self)
            else:
                return visitor.visitChildren(self)


    class ArgConstraintContext(ArgContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a MinionParser.ArgContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def constraint(self):
            return self.getTypedRuleContext(MinionParser.ConstraintContext,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterArgConstraint" ):
                listener.enterArgConstraint(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitArgConstraint" ):
                listener.exitArgConstraint(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitArgConstraint" ):
                return visitor.visitArgConstraint(self)
            else:
                return visitor.visitChildren(self)


    class ArgVariableContext(ArgContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a MinionParser.ArgContext
            super().__init__(parser)
            self.value = None # Token
            self.copyFrom(ctx)

        def ID(self):
            return self.getToken(MinionParser.ID, 0)
        def KEY_NOT(self):
            return self.getToken(MinionParser.KEY_NOT, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterArgVariable" ):
                listener.enterArgVariable(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitArgVariable" ):
                listener.exitArgVariable(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitArgVariable" ):
                return visitor.visitArgVariable(self)
            else:
                return visitor.visitChildren(self)



    def arg(self):

        localctx = MinionParser.ArgContext(self, self._ctx, self.state)
        self.enterRule(localctx, 22, self.RULE_arg)
        self._la = 0 # Token type
        try:
            self.state = 190
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,18,self._ctx)
            if la_ == 1:
                localctx = MinionParser.ArgVariableContext(self, localctx)
                self.enterOuterAlt(localctx, 1)
                self.state = 179
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if _la==25:
                    self.state = 178
                    self.match(MinionParser.KEY_NOT)


                self.state = 181
                localctx.value = self.match(MinionParser.ID)
                pass

            elif la_ == 2:
                localctx = MinionParser.ArgIntegerContext(self, localctx)
                self.enterOuterAlt(localctx, 2)
                self.state = 183
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if _la==25:
                    self.state = 182
                    self.match(MinionParser.KEY_NOT)


                self.state = 185
                localctx.value = self.match(MinionParser.INT)
                pass

            elif la_ == 3:
                localctx = MinionParser.ArgConstraintContext(self, localctx)
                self.enterOuterAlt(localctx, 3)
                self.state = 186
                self.constraint()
                pass

            elif la_ == 4:
                localctx = MinionParser.ArgListContext(self, localctx)
                self.enterOuterAlt(localctx, 4)
                self.state = 187
                self.list_()
                pass

            elif la_ == 5:
                localctx = MinionParser.NotUsedArgListAccessContext(self, localctx)
                self.enterOuterAlt(localctx, 5)
                self.state = 188
                self.listAccess()
                pass

            elif la_ == 6:
                localctx = MinionParser.ArgTableConstraintContext(self, localctx)
                self.enterOuterAlt(localctx, 6)
                self.state = 189
                self.tableConstraint()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ListContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self._listValue = None # ListValueContext
            self.value = list() # of ListValueContexts

        def listValue(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(MinionParser.ListValueContext)
            else:
                return self.getTypedRuleContext(MinionParser.ListValueContext,i)


        def getRuleIndex(self):
            return MinionParser.RULE_list

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterList" ):
                listener.enterList(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitList" ):
                listener.exitList(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitList" ):
                return visitor.visitList(self)
            else:
                return visitor.visitChildren(self)




    def list_(self):

        localctx = MinionParser.ListContext(self, self._ctx, self.state)
        self.enterRule(localctx, 24, self.RULE_list)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 192
            self.match(MinionParser.T__4)
            self.state = 193
            localctx._listValue = self.listValue()
            localctx.value.append(localctx._listValue)
            self.state = 198
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==4:
                self.state = 194
                self.match(MinionParser.T__3)
                self.state = 195
                localctx._listValue = self.listValue()
                localctx.value.append(localctx._listValue)
                self.state = 200
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 201
            self.match(MinionParser.T__5)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ListValueContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return MinionParser.RULE_listValue

     
        def copyFrom(self, ctx:ParserRuleContext):
            super().copyFrom(ctx)



    class ListVariableContext(ListValueContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a MinionParser.ListValueContext
            super().__init__(parser)
            self.value = None # Token
            self.copyFrom(ctx)

        def ID(self):
            return self.getToken(MinionParser.ID, 0)
        def KEY_NOT(self):
            return self.getToken(MinionParser.KEY_NOT, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterListVariable" ):
                listener.enterListVariable(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitListVariable" ):
                listener.exitListVariable(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitListVariable" ):
                return visitor.visitListVariable(self)
            else:
                return visitor.visitChildren(self)


    class NotUsedListContext(ListValueContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a MinionParser.ListValueContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def list_(self):
            return self.getTypedRuleContext(MinionParser.ListContext,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterNotUsedList" ):
                listener.enterNotUsedList(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitNotUsedList" ):
                listener.exitNotUsedList(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitNotUsedList" ):
                return visitor.visitNotUsedList(self)
            else:
                return visitor.visitChildren(self)


    class ListIntegerContext(ListValueContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a MinionParser.ListValueContext
            super().__init__(parser)
            self.value = None # Token
            self.copyFrom(ctx)

        def INT(self):
            return self.getToken(MinionParser.INT, 0)
        def KEY_NOT(self):
            return self.getToken(MinionParser.KEY_NOT, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterListInteger" ):
                listener.enterListInteger(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitListInteger" ):
                listener.exitListInteger(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitListInteger" ):
                return visitor.visitListInteger(self)
            else:
                return visitor.visitChildren(self)


    class NotUsedListAccessContext(ListValueContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a MinionParser.ListValueContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def listAccess(self):
            return self.getTypedRuleContext(MinionParser.ListAccessContext,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterNotUsedListAccess" ):
                listener.enterNotUsedListAccess(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitNotUsedListAccess" ):
                listener.exitNotUsedListAccess(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitNotUsedListAccess" ):
                return visitor.visitNotUsedListAccess(self)
            else:
                return visitor.visitChildren(self)



    def listValue(self):

        localctx = MinionParser.ListValueContext(self, self._ctx, self.state)
        self.enterRule(localctx, 26, self.RULE_listValue)
        self._la = 0 # Token type
        try:
            self.state = 213
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,22,self._ctx)
            if la_ == 1:
                localctx = MinionParser.ListVariableContext(self, localctx)
                self.enterOuterAlt(localctx, 1)
                self.state = 204
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if _la==25:
                    self.state = 203
                    self.match(MinionParser.KEY_NOT)


                self.state = 206
                localctx.value = self.match(MinionParser.ID)
                pass

            elif la_ == 2:
                localctx = MinionParser.ListIntegerContext(self, localctx)
                self.enterOuterAlt(localctx, 2)
                self.state = 208
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if _la==25:
                    self.state = 207
                    self.match(MinionParser.KEY_NOT)


                self.state = 210
                localctx.value = self.match(MinionParser.INT)
                pass

            elif la_ == 3:
                localctx = MinionParser.NotUsedListAccessContext(self, localctx)
                self.enterOuterAlt(localctx, 3)
                self.state = 211
                self.listAccess()
                pass

            elif la_ == 4:
                localctx = MinionParser.NotUsedListContext(self, localctx)
                self.enterOuterAlt(localctx, 4)
                self.state = 212
                self.list_()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ListAccessContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.name = None # Token

        def list_(self):
            return self.getTypedRuleContext(MinionParser.ListContext,0)


        def ID(self):
            return self.getToken(MinionParser.ID, 0)

        def KEY_NOT(self):
            return self.getToken(MinionParser.KEY_NOT, 0)

        def getRuleIndex(self):
            return MinionParser.RULE_listAccess

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterListAccess" ):
                listener.enterListAccess(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitListAccess" ):
                listener.exitListAccess(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitListAccess" ):
                return visitor.visitListAccess(self)
            else:
                return visitor.visitChildren(self)




    def listAccess(self):

        localctx = MinionParser.ListAccessContext(self, self._ctx, self.state)
        self.enterRule(localctx, 28, self.RULE_listAccess)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 216
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==25:
                self.state = 215
                self.match(MinionParser.KEY_NOT)


            self.state = 218
            localctx.name = self.match(MinionParser.ID)
            self.state = 219
            self.list_()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class SearchSectionContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def KEY_SEARCH(self):
            return self.getToken(MinionParser.KEY_SEARCH, 0)

        def varValOrdering(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(MinionParser.VarValOrderingContext)
            else:
                return self.getTypedRuleContext(MinionParser.VarValOrderingContext,i)


        def optimisationFn(self):
            return self.getTypedRuleContext(MinionParser.OptimisationFnContext,0)


        def printFormat(self):
            return self.getTypedRuleContext(MinionParser.PrintFormatContext,0)


        def getRuleIndex(self):
            return MinionParser.RULE_searchSection

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterSearchSection" ):
                listener.enterSearchSection(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitSearchSection" ):
                listener.exitSearchSection(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitSearchSection" ):
                return visitor.visitSearchSection(self)
            else:
                return visitor.visitChildren(self)




    def searchSection(self):

        localctx = MinionParser.SearchSectionContext(self, self._ctx, self.state)
        self.enterRule(localctx, 30, self.RULE_searchSection)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 221
            self.match(MinionParser.KEY_SEARCH)
            self.state = 225
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==26:
                self.state = 222
                self.varValOrdering()
                self.state = 227
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 229
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==37 or _la==38:
                self.state = 228
                self.optimisationFn()


            self.state = 232
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==39:
                self.state = 231
                self.printFormat()


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class VarValOrderingContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def varOrder(self):
            return self.getTypedRuleContext(MinionParser.VarOrderContext,0)


        def valOrder(self):
            return self.getTypedRuleContext(MinionParser.ValOrderContext,0)


        def getRuleIndex(self):
            return MinionParser.RULE_varValOrdering

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterVarValOrdering" ):
                listener.enterVarValOrdering(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitVarValOrdering" ):
                listener.exitVarValOrdering(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitVarValOrdering" ):
                return visitor.visitVarValOrdering(self)
            else:
                return visitor.visitChildren(self)




    def varValOrdering(self):

        localctx = MinionParser.VarValOrderingContext(self, self._ctx, self.state)
        self.enterRule(localctx, 32, self.RULE_varValOrdering)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 234
            self.varOrder()
            self.state = 236
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==27:
                self.state = 235
                self.valOrder()


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class OrderContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def KEY_ORDER_STATIC(self):
            return self.getToken(MinionParser.KEY_ORDER_STATIC, 0)

        def KEY_ORDER_SDF(self):
            return self.getToken(MinionParser.KEY_ORDER_SDF, 0)

        def KEY_ORDER_SRF(self):
            return self.getToken(MinionParser.KEY_ORDER_SRF, 0)

        def KEY_ORDER_LDF(self):
            return self.getToken(MinionParser.KEY_ORDER_LDF, 0)

        def KEY_ORDER_ORIGINAL(self):
            return self.getToken(MinionParser.KEY_ORDER_ORIGINAL, 0)

        def KEY_ORDER_WDEG(self):
            return self.getToken(MinionParser.KEY_ORDER_WDEG, 0)

        def KEY_ORDER_CONFLICT(self):
            return self.getToken(MinionParser.KEY_ORDER_CONFLICT, 0)

        def KEY_ORDER_DOMOVERWDEG(self):
            return self.getToken(MinionParser.KEY_ORDER_DOMOVERWDEG, 0)

        def getRuleIndex(self):
            return MinionParser.RULE_order

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterOrder" ):
                listener.enterOrder(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitOrder" ):
                listener.exitOrder(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitOrder" ):
                return visitor.visitOrder(self)
            else:
                return visitor.visitChildren(self)




    def order(self):

        localctx = MinionParser.OrderContext(self, self._ctx, self.state)
        self.enterRule(localctx, 34, self.RULE_order)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 238
            _la = self._input.LA(1)
            if not((((_la) & ~0x3f) == 0 and ((1 << _la) & 136902082560) != 0)):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class VarOrderContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def KEY_VARORDER(self):
            return self.getToken(MinionParser.KEY_VARORDER, 0)

        def list_(self):
            return self.getTypedRuleContext(MinionParser.ListContext,0)


        def KEY_AUX(self):
            return self.getToken(MinionParser.KEY_AUX, 0)

        def order(self):
            return self.getTypedRuleContext(MinionParser.OrderContext,0)


        def getRuleIndex(self):
            return MinionParser.RULE_varOrder

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterVarOrder" ):
                listener.enterVarOrder(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitVarOrder" ):
                listener.exitVarOrder(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitVarOrder" ):
                return visitor.visitVarOrder(self)
            else:
                return visitor.visitChildren(self)




    def varOrder(self):

        localctx = MinionParser.VarOrderContext(self, self._ctx, self.state)
        self.enterRule(localctx, 36, self.RULE_varOrder)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 240
            self.match(MinionParser.KEY_VARORDER)
            self.state = 242
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==28:
                self.state = 241
                self.match(MinionParser.KEY_AUX)


            self.state = 245
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if (((_la) & ~0x3f) == 0 and ((1 << _la) & 136902082560) != 0):
                self.state = 244
                self.order()


            self.state = 247
            self.list_()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ValOrderContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def KEY_VALORDER(self):
            return self.getToken(MinionParser.KEY_VALORDER, 0)

        def KEY_ASCENDING(self, i:int=None):
            if i is None:
                return self.getTokens(MinionParser.KEY_ASCENDING)
            else:
                return self.getToken(MinionParser.KEY_ASCENDING, i)

        def KEY_DESCENDING(self, i:int=None):
            if i is None:
                return self.getTokens(MinionParser.KEY_DESCENDING)
            else:
                return self.getToken(MinionParser.KEY_DESCENDING, i)

        def getRuleIndex(self):
            return MinionParser.RULE_valOrder

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterValOrder" ):
                listener.enterValOrder(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitValOrder" ):
                listener.exitValOrder(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitValOrder" ):
                return visitor.visitValOrder(self)
            else:
                return visitor.visitChildren(self)




    def valOrder(self):

        localctx = MinionParser.ValOrderContext(self, self._ctx, self.state)
        self.enterRule(localctx, 38, self.RULE_valOrder)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 249
            self.match(MinionParser.KEY_VALORDER)
            self.state = 250
            self.match(MinionParser.T__4)
            self.state = 252 
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while True:
                self.state = 251
                _la = self._input.LA(1)
                if not(_la==43 or _la==44):
                    self._errHandler.recoverInline(self)
                else:
                    self._errHandler.reportMatch(self)
                    self.consume()
                self.state = 254 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if not (_la==43 or _la==44):
                    break

            self.state = 256
            self.match(MinionParser.T__5)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class OptimisationFnContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ID(self):
            return self.getToken(MinionParser.ID, 0)

        def KEY_MINIMISING(self):
            return self.getToken(MinionParser.KEY_MINIMISING, 0)

        def KEY_MAXIMISING(self):
            return self.getToken(MinionParser.KEY_MAXIMISING, 0)

        def getRuleIndex(self):
            return MinionParser.RULE_optimisationFn

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterOptimisationFn" ):
                listener.enterOptimisationFn(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitOptimisationFn" ):
                listener.exitOptimisationFn(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitOptimisationFn" ):
                return visitor.visitOptimisationFn(self)
            else:
                return visitor.visitChildren(self)




    def optimisationFn(self):

        localctx = MinionParser.OptimisationFnContext(self, self._ctx, self.state)
        self.enterRule(localctx, 40, self.RULE_optimisationFn)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 258
            _la = self._input.LA(1)
            if not(_la==37 or _la==38):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
            self.state = 259
            self.match(MinionParser.ID)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class PrintFormatContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def KEY_PRINT(self):
            return self.getToken(MinionParser.KEY_PRINT, 0)

        def list_(self):
            return self.getTypedRuleContext(MinionParser.ListContext,0)


        def KEY_ALL(self):
            return self.getToken(MinionParser.KEY_ALL, 0)

        def KEY_NONE(self):
            return self.getToken(MinionParser.KEY_NONE, 0)

        def getRuleIndex(self):
            return MinionParser.RULE_printFormat

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterPrintFormat" ):
                listener.enterPrintFormat(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitPrintFormat" ):
                listener.exitPrintFormat(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitPrintFormat" ):
                return visitor.visitPrintFormat(self)
            else:
                return visitor.visitChildren(self)




    def printFormat(self):

        localctx = MinionParser.PrintFormatContext(self, self._ctx, self.state)
        self.enterRule(localctx, 42, self.RULE_printFormat)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 261
            self.match(MinionParser.KEY_PRINT)
            self.state = 265
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [5]:
                self.state = 262
                self.list_()
                pass
            elif token in [40]:
                self.state = 263
                self.match(MinionParser.KEY_ALL)
                pass
            elif token in [41]:
                self.state = 264
                self.match(MinionParser.KEY_NONE)
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class TuplelistSectionContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def KEY_TUPLELIST(self):
            return self.getToken(MinionParser.KEY_TUPLELIST, 0)

        def tupleList(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(MinionParser.TupleListContext)
            else:
                return self.getTypedRuleContext(MinionParser.TupleListContext,i)


        def getRuleIndex(self):
            return MinionParser.RULE_tuplelistSection

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterTuplelistSection" ):
                listener.enterTuplelistSection(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitTuplelistSection" ):
                listener.exitTuplelistSection(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitTuplelistSection" ):
                return visitor.visitTuplelistSection(self)
            else:
                return visitor.visitChildren(self)




    def tuplelistSection(self):

        localctx = MinionParser.TuplelistSectionContext(self, self._ctx, self.state)
        self.enterRule(localctx, 44, self.RULE_tuplelistSection)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 267
            self.match(MinionParser.KEY_TUPLELIST)
            self.state = 271
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==42:
                self.state = 268
                self.tupleList()
                self.state = 273
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class TupleListContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.name = None # Token
            self.rows = None # Token
            self.cols = None # Token
            self._INT = None # Token
            self.numbers = list() # of Tokens

        def ID(self):
            return self.getToken(MinionParser.ID, 0)

        def INT(self, i:int=None):
            if i is None:
                return self.getTokens(MinionParser.INT)
            else:
                return self.getToken(MinionParser.INT, i)

        def getRuleIndex(self):
            return MinionParser.RULE_tupleList

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterTupleList" ):
                listener.enterTupleList(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitTupleList" ):
                listener.exitTupleList(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitTupleList" ):
                return visitor.visitTupleList(self)
            else:
                return visitor.visitChildren(self)




    def tupleList(self):

        localctx = MinionParser.TupleListContext(self, self._ctx, self.state)
        self.enterRule(localctx, 46, self.RULE_tupleList)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 274
            localctx.name = self.match(MinionParser.ID)
            self.state = 275
            localctx.rows = self.match(MinionParser.INT)
            self.state = 276
            localctx.cols = self.match(MinionParser.INT)
            self.state = 278 
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while True:
                self.state = 277
                localctx._INT = self.match(MinionParser.INT)
                localctx.numbers.append(localctx._INT)
                self.state = 280 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if not (_la==13):
                    break

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx





