error_list = []


def reset_error_list():
    error_list.clear()


def add_error(error):
    error_list.append(error)


def add_constraint_error(name, args):
    error_list.append(
        f"Constraint '{name}' can not be converted with arguments ({', '.join(str(arg) for arg in args)})")


def add_variable_not_defined_error(name, line, column):
    error_list.append(f"line {line}:{column} Variable '{name}' not defined")


def add_variable_double_declaration(name, line, column):
    error_list.append(f"line {line}:{column} Double declaration of variable '{name}'")


def add_constraint_argument_error(name, line, column, args):
    error_list.append(
        f"line {line}:{column + 1} Constraint '{name}' is not supported with arguments ({', '.join(str(arg) for arg in args)})")
