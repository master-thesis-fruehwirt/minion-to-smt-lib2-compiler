from src.arguments.argument import Argument


class ListAccess(Argument):

    def __init__(self, variable, *accessed_list):
        super().__init__()
        self._variable = variable
        self._accessed_list = accessed_list
        self._type = variable.get_type()

    def __repr__(self):
        return f"{self._variable}[{', '.join(str(i) for i in self._accessed_list)}]"

    def to_smtlib2(self):
        return f"(select {self._variable} {' '.join(str(i) for i in self._accessed_list)})"

    def get_upper_bound(self):
        return self._variable.get_upper_bound()
