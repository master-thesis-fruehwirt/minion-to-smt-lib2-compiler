from src.arguments.argument import Argument
from src.mapper.type import Type
from src.smtlib2_statements.smtlib2_command import DeclareConst


class Table(Argument):

    def __init__(self, name, rows, cols, tuplelist):
        super().__init__()
        self._name = name
        self._rows = rows
        self._cols = cols
        self._tuplelist = tuplelist
        self._type = Type.TYPE_TABLE

    def __repr__(self):
        return self._name

    def get_name(self):
        return self._name

    def to_smtlib2(self):
        return self._name

    def get_rows(self):
        return self._rows

    def get_cols(self):
        return self._cols

    def get_table(self):
        return self._tuplelist

    # for testing purposes
    def get_dimensions(self):
        return [self._rows, self._cols]

    def declaration_to_smtlib2(self):
        return DeclareConst(self._name, 2, "Int", True)

    def to_debug_string(self):
        return f"{{table}} {self._name}[{self._rows}][{self._cols}]"

    def get_smtlib2_model(self):
        return None
