from src.arguments.argument import Argument
from src.mapper.type import Type
from src.smtlib2_statements.smtlib2_command import DeclareConst
from src.smtlib2_statements.smtlib2_function import Select, Eq


class Vector(Argument):

    def __init__(self, *vec):
        super().__init__()
        self._vec = vec
        self._type = Type.TYPE_VECTOR

    def __repr__(self):
        return f"[{', '.join(str(i) for i in self._vec)}]"

    def get_vec(self):
        return self._vec

    def get_upper_bound(self):
        return len(self._vec)

    def to_smtlib2(self):
        constraints = [DeclareConst("A", 1, Type.TYPE_INT)]
        for i, v in enumerate(self._vec):
            constraints.append(Eq(Select("A", i), v.to_smtlib2()))
        return constraints
