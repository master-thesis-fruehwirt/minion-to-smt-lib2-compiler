from src.arguments.argument import Argument
from src.mapper.type import Type
from src.smtlib2_statements.smtlib2_function import Not


class NotVar(Argument):

    def __init__(self, var):
        super().__init__()
        self._var = var
        self._type = Type.TYPE_BOOL

    def __repr__(self):
        return f"!{self._var}"

    def to_smtlib2(self):
        return Not(self._var.to_smtlib2())
