from src.mapper.type import Type


class Argument:

    def __init__(self):
        self._type = Type.TYPE_DEFAULT

    def get_type(self):
        return self._type

    def is_boolean_var(self):
        from src.arguments.variable import Variable
        from src.arguments.not_var import NotVar
        from src.arguments.list_access import ListAccess
        return (type(self) == Variable or type(self) == ListAccess or type(self) == NotVar) and self._type == Type.TYPE_BOOL

    def is_variable(self):
        from src.arguments.variable import Variable
        from src.arguments.list_access import ListAccess
        from src.arguments.not_var import NotVar
        return type(self) == Variable or type(self) == ListAccess or type(self) == NotVar

    def is_const(self):
        return self._type == Type.TYPE_CONST

    def is_vector(self):
        return self._type == Type.TYPE_VECTOR

    def is_variable_or_const(self):
        return self.is_variable() or self.is_const()

    def is_variable_or_vector(self):
        return self.is_variable() or self.is_vector()

    def is_table(self):
        return self._type == Type.TYPE_TABLE
