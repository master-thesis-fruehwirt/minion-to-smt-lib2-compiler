from src.arguments.argument import Argument
from src.mapper.type import Type


class Const(Argument):

    def __init__(self, val):
        super().__init__()
        self._val = val
        self._type = Type.TYPE_CONST

    def __repr__(self):
        return str(self._val)

    def get_val(self):
        return self._val

    def to_smtlib2(self):
        return str(self._val)
