from src.arguments.argument import Argument
from src.constants import Constants
from src.mapper.type import Type
from src.smtlib2_statements.smtlib2_command import DeclareConst
from src.smtlib2_statements.smtlib2_function import Implies, Leq, Select, Or, Eq, Lt
from src.smtlib2_statements.smtlib2_quantifier import Forall
from src.smtlib2_statements.smtlib2_statement import BoundVar


class Variable(Argument):

    def __init__(self, name, tpe=Type.TYPE_DEFAULT, dimensions=None, domain=None, sparsebound=False):
        super().__init__()
        if domain is None:
            domain = []
        if dimensions is None:
            dimensions = []
        self._name = name
        self._type = tpe
        self._dimensions = dimensions
        self._domain = domain
        self._sparsebound = sparsebound

    def to_debug_string(self):
        dim_string = ""
        if self._dimensions:
            dim_string = "[" + ','.join(str(i) for i in self._dimensions) + "]"

        domain_string = ""
        if self._domain:
            if not self._sparsebound:
                domain_string = f" {{{self._domain[0]}..{self._domain[1]}}}"
            else:
                domain_string = f" {{{','.join(str(i) for i in self._domain)}}}"

        return f"{self._name}{dim_string}{domain_string}"

    def __repr__(self):
        return self._name

    def get_name(self):
        return str(self._name)

    def get_dimensions(self):
        return self._dimensions

    def get_upper_bound(self):
        if self._dimensions:
            return self._dimensions[0]
        return 0

    def declaration_to_smtlib2(self):
        if self._dimensions:
            if self._type == Type.TYPE_BOOL:
                return DeclareConst(self._name, len(self._dimensions), "Bool")
            elif self._type == Type.TYPE_INT:
                return DeclareConst(self._name, len(self._dimensions), "Int")
        else:
            if self._type == Type.TYPE_BOOL:
                return DeclareConst(self._name, 0, "Bool")
            elif self._type == Type.TYPE_INT:
                return DeclareConst(self._name, 0, "Int")

    def to_smtlib2(self):
        return self._name

    def create_domain_constraint(self):
        if self._domain:
            if len(self._dimensions) == 1:
                return Forall(BoundVar(Constants.COUNTER1, "Int"),
                              Implies(Leq(0, Constants.COUNTER1, self._dimensions[0] - 1),
                                      Leq(self._domain[0], Select(self._name, Constants.COUNTER1), self._domain[1]))
                              )
            elif len(self._dimensions) == 2:
                return Forall(BoundVar(Constants.COUNTER1, "Int"),
                              Implies(Lt(0, Constants.COUNTER1, self._dimensions[0]),
                                      Forall(BoundVar(Constants.COUNTER2, "Int"),
                                             Implies(Lt(0, Constants.COUNTER2, self._dimensions[1]),
                                                     Leq(self._domain[0],
                                                         Select(self._name, Constants.COUNTER1, Constants.COUNTER2),
                                                         self._domain[1]))
                                             )
                                      )
                              )
            else:
                if self._sparsebound:
                    return Or(*[Eq(self._name, i) for i in self._domain])
                return Leq(self._domain[0], self._name, self._domain[1])
        return ""

    def get_smtlib2_model(self):
        result = ""

        if len(self._dimensions) == 1:
            for i in range(self._dimensions[0]):
                result += f"(get-value ((select {self._name} {i})))\n"
            return result
        if len(self._dimensions) == 2:
            for i in range(self._dimensions[0]):
                for j in range(self._dimensions[1]):
                    result += f"(get-value ((select {self._name} {i} {j})))\n"
            return result

        return f"(get-value ({self._name}))"
