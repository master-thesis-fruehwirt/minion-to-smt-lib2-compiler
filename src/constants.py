
class Constants:
    COUNTER1 = "VAR1"
    COUNTER2 = "VAR2"
    FUNCTION_SUM = "FUN_SUM"
    HELPER_ARRAY = "ARR_HELPER"
    HELPER_TABLE = "TABLE_HELPER"
    HELPER_ACC = "HELPER_ACC"


class CompilerConstants:
    VERSION = "1.1.0"
    COMPILER_NAME = "min2smt"

    ARG_PRINT = "print"
    ARG_DEBUG = "debug"
    ARG_PRINT_MINION = "print-minion"
    ARG_HELP = "help"
    ARG_OPTIMIZE = "optimize"
