from src.constraints.interfaces.constraint import Constraint
from src.smtlib2_statements.smtlib2_function import Leq, Or


class Constraint_w_inintervalset(Constraint):
    def is_valid(self):
        return self.num_args(2) and self._args[0].is_variable_or_const() and self._args[1].is_vector()

    def to_smtlib2(self):
        x = self._args[0].to_smtlib2()
        vec = self._args[1]

        interval = []
        it = iter(vec.get_vec())
        for i in it:
            interval.append(Leq(i.to_smtlib2(), x, next(it).to_smtlib2()))
        return Or(*interval)
