from src.constraints.interfaces.constraint import Constraint
from src.smtlib2_statements.smtlib2_function import And, Distinct


class Constraint_w_notinset(Constraint):

    def is_valid(self):
        return self.num_args(2) and self._args[0].is_variable_or_const() and self._args[1].is_vector()

    def to_smtlib2(self):
        x = self._args[0].to_smtlib2()
        vec = self._args[1]

        return And(*[Distinct(x, v.to_smtlib2()) for v in vec.get_vec()])
