from src.constraints.interfaces.constraint import Constraint
from src.smtlib2_statements.smtlib2_function import And, Or, Eq, Leq, Select


class Constraint_min(Constraint):
    def is_valid(self):
        return self.num_args(2) and self._args[0].is_variable_or_vector() and self._args[1].is_variable_or_const()

    def to_smtlib2(self):
        vec_ = self._args[0]
        vec = vec_.to_smtlib2()
        x = self._args[1].to_smtlib2()

        if vec_.is_vector():
            return And(Or(*[Eq(v.to_smtlib2(), x) for v in vec_.get_vec()]), *[Leq(x, v) for v in vec_.get_vec()])

        else:
            upper = vec_.get_upper_bound()
            return And(Or(*[Eq(x, Select(vec, i)) for i in range(upper)]), *[Leq(x, Select(vec, i)) for i in range(upper)])
