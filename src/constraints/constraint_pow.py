from src.constraints.interfaces.constraint import Constraint
from src.smtlib2_statements.smtlib2_function import Eq, Pow, Ite


class Constraint_pow(Constraint):

    def is_valid(self):
        return self.num_args(3) and self._args[0].is_variable_or_const() and self._args[1].is_variable_or_const() and self._args[2].is_variable_or_const()

    def to_smtlib2(self):
        x = self._args[0].to_smtlib2()
        y = self._args[1].to_smtlib2()
        z = self._args[2].to_smtlib2()

        return Ite(Eq(y, 0), Eq(z, 1), Eq(z, Pow(x, y)))
