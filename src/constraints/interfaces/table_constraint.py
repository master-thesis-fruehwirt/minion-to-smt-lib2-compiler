from src.constraints.interfaces.constraint import Constraint
from src.mapper.type import Type
from src.smtlib2_statements.smtlib2_function import Select, Ite


class TableConstraint(Constraint):

    def __init__(self, name, operator1, operator2, compare_op, *args):
        super().__init__(name, *args)
        self._op1 = operator1
        self._op2 = operator2
        self._compare_op = compare_op

    def is_valid(self):
        return self.num_args(2) and self._args[0].is_variable_or_vector() and self._args[1].is_table()

    def to_smtlib2(self):
        vec_ = self._args[0]
        table_ = self._args[1]

        if vec_.is_variable():
            or_list = []
            for row in table_.get_table():
                and_list = []
                for i, col in enumerate(row):
                    and_list.append(self._compare_op(col, Select(vec_, i)))
                or_list.append(self._op2(*and_list))
            return self._op1(*or_list)
        elif vec_.is_vector():
            or_list = []
            for row in table_.get_table():
                and_list = []
                for v, col in zip(vec_.get_vec(), row):

                    if v.get_type() == Type.TYPE_BOOL:
                        value = Ite(v.to_smtlib2(), 1, 0)
                    else:
                        value = v.to_smtlib2()

                    and_list.append(self._compare_op(col, value))
                or_list.append(self._op2(*and_list))
            return self._op1(*or_list)
