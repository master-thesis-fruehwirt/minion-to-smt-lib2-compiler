from src.constraints.interfaces.constraint import Constraint
from src.mapper.type import Type


class WatchedConstraint(Constraint):

    def __init__(self, name, operator, *args):
        super().__init__(name, *args)
        self._op = operator

    def is_valid(self):
        for arg in self._args:
            if arg.get_type() != Type.TYPE_CONSTRAINT:
                return False
        return len(self._args) > 1

    def to_smtlib2(self):
        smtlib2_constraints = []
        watched_constraints = []
        for constraint in self._args:
            smtlib2_constraint = constraint.to_smtlib2()

            if type(smtlib2_constraint) == list:
                smtlib2_constraints += smtlib2_constraint[:-1]  # add helper functions, arrays... to list
                watched_constraints.append(smtlib2_constraint[-1])  # save watched constraint, which is the last element in the list
            else:
                watched_constraints.append(smtlib2_constraint)

        return smtlib2_constraints + [self._op(*watched_constraints)]
