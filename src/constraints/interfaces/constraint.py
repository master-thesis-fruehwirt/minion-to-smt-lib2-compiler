from src.errors import add_constraint_error
from src.mapper.type import Type


class Constraint:

    def __init__(self, name, *args):
        self._name = name
        self._args = args

    def __repr__(self):
        return f"{self._name}({', '.join(str(i) for i in self._args)})"

    def to_smtlib2(self):
        add_constraint_error(self._name, self._args)
        return self.__repr__()

    def is_valid(self):
        add_constraint_error(self._name, self._args)
        return False

    def num_args(self, expected):
        return len(self._args) == expected

    def get_name(self):
        return self._name

    def get_args(self):
        return self._args

    def get_type(self):
        return Type.TYPE_CONSTRAINT
