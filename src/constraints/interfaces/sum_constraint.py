from src.constraints.interfaces.constraint import Constraint
from src.smtlib2_statements.smtlib2_function import Add, Leq, Eq, Geq, Select


class SumConstraint(Constraint):

    def __init__(self, name, operator, *args):
        super().__init__(name, *args)
        self._op = operator
        self._function_name = ""

    def to_smtlib2(self):
        vec_ = self._args[0]
        vec = vec_.to_smtlib2()
        c = self._args[1].to_smtlib2()

        op = None
        if self._op == '<=':
            op = Leq
        elif self._op == '>=':
            op = Geq
        elif self._op == '=':
            op = Eq

        if vec_.is_vector():
            return op(Add(*[a.to_smtlib2() for a in vec_.get_vec()]), c)
        else:
            upper = vec_.get_upper_bound()

            return op(Add(*[Select(vec, i) for i in range(upper)]), c)
