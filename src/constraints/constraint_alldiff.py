from src.constants import Constants
from src.constraints.interfaces.constraint import Constraint
from src.smtlib2_statements.smtlib2_function import Distinct, Implies, And, Leq, Eq, Select, Lt
from src.smtlib2_statements.smtlib2_quantifier import Forall
from src.smtlib2_statements.smtlib2_statement import BoundVar


class Constraint_alldiff(Constraint):
    def is_valid(self):
        return self.num_args(1) and self._args[0].is_variable_or_vector()

    def to_smtlib2(self):
        arg = self._args[0]
        if arg.is_vector():
            vec = arg.get_vec()
            if len(vec) == 1 and vec[0].is_variable():
                variable = vec[0]
                upper = variable.get_upper_bound()
                return self._build_array_string(upper, variable.to_smtlib2())
            elif len(vec) > 1:
                return Distinct(*[v.to_smtlib2() for v in vec])
        elif arg.is_variable():
            upper = arg.get_upper_bound()
            return self._build_array_string(upper, arg.to_smtlib2())

    def _build_array_string(self, upper, arr_name):
        return Forall(BoundVar(Constants.COUNTER1, "Int"), BoundVar(Constants.COUNTER2, "Int"),
                      Implies(And(Lt(0, Constants.COUNTER1, upper),
                                  Lt(0, Constants.COUNTER2, upper),
                                  Eq(Select(arr_name, Constants.COUNTER1),
                                     Select(arr_name, Constants.COUNTER2))),
                              Eq(Constants.COUNTER1, Constants.COUNTER2)
                              )
                      )
