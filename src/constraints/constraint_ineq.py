from src.constraints.interfaces.constraint import Constraint
from src.smtlib2_statements.smtlib2_function import Lt, Add, Leq


class Constraint_ineq(Constraint):

    def is_valid(self):
        return self.num_args(3) and self._args[0].is_variable_or_const() and self._args[1].is_variable_or_const() and self._args[2].is_const()

    def to_smtlib2(self):
        x = self._args[0].to_smtlib2()
        y = self._args[1].to_smtlib2()
        z_ = self._args[2]
        z = z_.to_smtlib2()

        if z_.is_const() and z_.get_val() == -1:
            return Lt(x, y)
        return Leq(x, Add(y, z))
