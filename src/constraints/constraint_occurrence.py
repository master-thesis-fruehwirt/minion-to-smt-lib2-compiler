from src.constraints.interfaces.constraint import Constraint
from src.smtlib2_statements.smtlib2_function import Eq, Add, Ite, Select


class Constraint_occurrence(Constraint):
    def is_valid(self):
        return self.num_args(3) and self._args[0].is_variable_or_vector() and self._args[1].is_const() and self._args[
            2].is_variable_or_const()

    def to_smtlib2(self):
        vec_ = self._args[0]
        vec = vec_.to_smtlib2()
        elem = self._args[1].to_smtlib2()
        count = self._args[2].to_smtlib2()

        if vec_.is_vector():
            return Eq(Add(*[Ite(Eq(v.to_smtlib2(), elem), 1, 0) for v in vec_.get_vec()]), count)
        elif vec_.is_variable():
            upper = vec_.get_upper_bound()
            return Eq(count, Add(*[Ite(Eq(Select(vec, i), elem), 1, 0) for i in range(upper)]))
