from src.constraints.interfaces.constraint import Constraint
from src.smtlib2_statements.smtlib2_function import And, Select, Leq, Not, Eq, Or, Lt


class Constraint_element(Constraint):
    def is_valid(self):
        return self.num_args(3) and self._args[0].is_variable_or_vector() and self._args[1].is_variable_or_const() and self._args[2].is_variable_or_const()

    def to_smtlib2(self):
        vec_ = self._args[0]
        i_ = self._args[1]
        e_ = self._args[2]
        vec = vec_.to_smtlib2()
        i = i_.to_smtlib2()
        e = e_.to_smtlib2()

        upper = vec_.get_upper_bound()
        if vec_.is_boolean_var():
            if e_.is_const():
                if e_.get_val() == 1:
                    return And(Select(vec, i), Lt(i, upper))
                elif e_.get_val() == 0:
                    return And(Not(Select(vec, i)), Lt(i, upper))
            else:
                return And(Eq(Select(vec, i), e), Lt(i, upper))
        elif vec_.is_variable():
            return And(Eq(Select(vec, i), e), Lt(i, upper))
        elif vec_.is_vector():
            if i_.is_const():
                if i_.get_val() >= upper:
                    return Lt(i, upper)
                else:
                    return And(Eq(vec_.get_vec()[i_.get_val()], e), Lt(i, upper))
            else:
                return And(Or(*[Eq(elem.to_smtlib2(), e) for elem in vec_.get_vec()]), Lt(i, upper))
