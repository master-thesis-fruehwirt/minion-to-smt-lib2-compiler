from src.constraints.interfaces.constraint import Constraint
from src.smtlib2_statements.smtlib2_function import Eq, And, Mul


class Constraint_product(Constraint):

    def is_valid(self):
        return self.num_args(3) and self._args[0].is_variable_or_const() and self._args[1].is_variable_or_const() and self._args[2].is_variable_or_const()

    def to_smtlib2(self):
        x_ = self._args[0]
        y_ = self._args[1]
        z_ = self._args[2]
        x = x_.to_smtlib2()
        y = y_.to_smtlib2()
        z = z_.to_smtlib2()

        if x_.is_boolean_var() or y_.is_boolean_var() or z_.is_boolean_var():
            return Eq(z, And(x, y))
        return Eq(z, Mul(x, y))
