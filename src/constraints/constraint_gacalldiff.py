from src.constraints.constraint_alldiff import Constraint_alldiff
from src.constraints.interfaces.constraint import Constraint


class Constraint_gacalldiff(Constraint):
    def is_valid(self):
        return self.num_args(1) and self._args[0].is_variable_or_vector()

    def to_smtlib2(self):
        constraint = Constraint_alldiff(self._name, *self._args)
        return constraint.to_smtlib2()
