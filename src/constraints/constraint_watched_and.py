from src.constraints.interfaces.watched_constraint import WatchedConstraint
from src.smtlib2_statements.smtlib2_function import And


class Constraint_watched_and(WatchedConstraint):

    def __init__(self, name, *args):
        super().__init__(name, And, *args)
