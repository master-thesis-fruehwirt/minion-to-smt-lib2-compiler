from src.constraints.interfaces.constraint import Constraint
from src.smtlib2_statements.smtlib2_function import Or, Lt, Gt


class Constraint_w_notinrange(Constraint):

    def is_valid(self):
        return self.num_args(2) and self._args[0].is_variable_or_const() and self._args[1].is_vector()

    def to_smtlib2(self):
        x = self._args[0].to_smtlib2()
        vec = self._args[1]

        return Or(Lt(x, vec.get_vec()[0].to_smtlib2()), Gt(x, vec.get_vec()[1].to_smtlib2()))
