from src.constants import Constants
from src.constraints.interfaces.constraint import Constraint
from src.globals import get_global_counter
from src.smtlib2_statements.smtlib2_command import DefineFunRec
from src.smtlib2_statements.smtlib2_function import And, Eq, Select, Add, Ite, Geq
from src.smtlib2_statements.smtlib2_statement import FunctionCall


class Constraint_alldiffmatrix(Constraint):
    def is_valid(self):
        return self.num_args(2) and self._args[0].is_variable() and self._args[1].is_const()

    def to_smtlib2(self):
        vec_ = self._args[0]
        val_ = self._args[1]
        val = val_.to_smtlib2()

        counter = get_global_counter()

        rows, cols = vec_.get_dimensions()

        function_name = f"fun_alldiffmatrix_{counter}"
        i = Constants.COUNTER1
        j = Constants.COUNTER2
        acc = Constants.HELPER_ACC

        return [
            DefineFunRec(function_name, [f"{i} Int", f"{j} Int", f"{acc} Int"], "Bool",
                         Ite(Geq(j, cols),
                             Eq(acc, 1),
                             Ite(Eq(Select(vec_, i, j), val),
                                 Ite(Eq(acc, 1),
                                     "false",
                                     FunctionCall(function_name, i, Add(j, 1), 1)
                                     ),
                                 FunctionCall(function_name, i, Add(j, 1), acc)
                                 )
                             )
                         ),
            And(*[FunctionCall(function_name, row, 0, 0) for row in range(rows)])
        ]
