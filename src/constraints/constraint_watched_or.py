from src.constraints.interfaces.watched_constraint import WatchedConstraint
from src.smtlib2_statements.smtlib2_function import Or


class Constraint_watched_or(WatchedConstraint):

    def __init__(self, name, *args):
        super().__init__(name, Or, *args)
