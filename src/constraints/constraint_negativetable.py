from src.constraints.interfaces.table_constraint import TableConstraint
from src.smtlib2_statements.smtlib2_function import Distinct, And, Or


class Constraint_negativetable(TableConstraint):

    def __init__(self, name, *args):
        super().__init__(name, And, Or, Distinct, *args)
