from src.constraints.interfaces.sum_constraint import SumConstraint


class Constraint_sumleq(SumConstraint):
    def is_valid(self):
        return self.num_args(2) and self._args[0].is_variable_or_vector() and self._args[1].is_variable_or_const()

    def __init__(self, name, *args):
        super().__init__(name, "<=", *args)
