from src.constraints.interfaces.constraint import Constraint
from src.smtlib2_statements.smtlib2_function import Geq, Add, Ite, Distinct, Select


class Constraint_hamming(Constraint):
    def is_valid(self):
        return self.num_args(3) and self._args[0].is_variable_or_vector() and self._args[1].is_variable_or_vector() and \
            self._args[2].is_const()

    def to_smtlib2(self):
        vec1_ = self._args[0]
        vec2_ = self._args[1]
        vec1 = vec1_.to_smtlib2()
        vec2 = vec2_.to_smtlib2()
        c = self._args[2].to_smtlib2()

        if vec1_.is_vector():
            if vec2_.is_vector():
                return Geq(Add(*[Ite(Distinct(v1.to_smtlib2(), v2.to_smtlib2()), 1, 0) for v1, v2 in zip(vec1_.get_vec(), vec2_.get_vec())]), c)
            elif vec2_.is_variable():
                return Geq(Add(*[Ite(Distinct(v1.to_smtlib2(), Select(vec2, i)), 1, 0) for i, v1 in enumerate(vec1_.get_vec())]), c)
        elif vec1_.is_variable():
            if vec2_.is_vector():
                return Geq(Add(*[Ite(Distinct(Select(vec1, i), v2.to_smtlib2()), 1, 0) for i, v2 in enumerate(vec2_.get_vec())]), c)
            elif vec2_.is_variable():
                upper = vec1_.get_upper_bound()
                return Geq(Add(*[Ite(Distinct(Select(vec1, i), Select(vec2, i)), 1, 0) for i in range(upper)]), c)
