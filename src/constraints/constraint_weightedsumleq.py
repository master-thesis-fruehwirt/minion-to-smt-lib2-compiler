from src.constraints.interfaces.constraint import Constraint
from src.smtlib2_statements.smtlib2_function import Leq, Add, Mul, Select


class Constraint_weightedsumleq(Constraint):
    def is_valid(self):
        return self.num_args(3) and self._args[0].is_vector() and self._args[1].is_variable_or_vector() and self._args[2].is_variable_or_const()

    def to_smtlib2(self):
        vec1_ = self._args[0]
        vec2_ = self._args[1]
        vec2 = vec2_.to_smtlib2()
        total = self._args[2].to_smtlib2()

        if vec2_.is_vector():
            return Leq(Add(*[Mul(v1.to_smtlib2(), v2.to_smtlib2()) for v1, v2 in zip(vec1_.get_vec(), vec2_.get_vec())]), total)
        elif vec2_.is_variable():
            return Leq(Add(*[Mul(v1.to_smtlib2(), Select(vec2, i)) for i, v1 in enumerate(vec1_.get_vec())]), total)

