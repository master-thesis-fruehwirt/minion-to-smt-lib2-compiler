from src.constraints.constraint_element import Constraint_element
from src.constraints.interfaces.constraint import Constraint


class Constraint_watchelement(Constraint):

    def is_valid(self):
        constraint = Constraint_element(self._name, *self._args)
        return constraint.is_valid()

    def to_smtlib2(self):
        constraint = Constraint_element(self._name, *self._args)
        return constraint.to_smtlib2()
