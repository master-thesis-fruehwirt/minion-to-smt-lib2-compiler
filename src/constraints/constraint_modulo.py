from src.constraints.interfaces.constraint import Constraint
from src.smtlib2_statements.smtlib2_function import And, Distinct, Eq, Mod, Ite, Lt, Gt, Or
from src.smtlib2_statements.smtlib2_statement import Neg


class Constraint_modulo(Constraint):

    def is_valid(self):
        return self.num_args(3) and self._args[0].is_variable_or_const() and self._args[1].is_variable_or_const() and \
            self._args[2].is_variable_or_const()

    def to_smtlib2(self):
        x = self._args[0].to_smtlib2()
        y = self._args[1].to_smtlib2()
        z = self._args[2].to_smtlib2()

        return And(Distinct(y, 0),
                   Ite(Or(And(Gt(x, 0), Gt(y, 0)), And(Lt(x, 0), Gt(y, 0))),
                       Eq(Mod(x, y), z),
                       Ite(And(Gt(x, 0), Lt(y, 0)),
                           Eq(Mod(Neg(x), Neg(y)), Neg(z)),
                           Eq(Mod(Neg(x), y), Neg(z)),
                           )
                       )
                   )
