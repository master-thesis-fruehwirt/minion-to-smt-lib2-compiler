from src.constraints.constraint_element_one import Constraint_element_one
from src.constraints.interfaces.constraint import Constraint


class Constraint_watchelement_one(Constraint):

    def is_valid(self):
        constraint = Constraint_element_one(self._name, *self._args)
        return constraint.is_valid()

    def to_smtlib2(self):
        constraint = Constraint_element_one(self._name, *self._args)
        return constraint.to_smtlib2()
