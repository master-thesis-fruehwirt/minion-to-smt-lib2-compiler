from src.constraints.interfaces.constraint import Constraint
from src.smtlib2_statements.smtlib2_function import Implies


class Constraint_reifyimply(Constraint):

    def is_valid(self):
        return self.num_args(2) and (self._args[1].is_boolean_var() or self._args[1].is_const())

    def to_smtlib2(self):
        constraint_ = self._args[0]
        smtlib2_constraint = constraint_.to_smtlib2()
        r_ = self._args[1]
        r = r_.to_smtlib2()

        smtlib2_constraints = []

        if type(smtlib2_constraint) == list:  # smtlib2_constraint could be a list of smtlib2 constraints
            smtlib2_constraints += smtlib2_constraint[:-1]  # add helper functions, arrays... to list
            constraint = smtlib2_constraint[-1]
        else:  # it is a single smtlib2 constraint
            constraint = smtlib2_constraint

        if r_.is_boolean_var():
            smtlib2_constraints.append(Implies(r, constraint))
        elif r_.is_const():
            if r_.get_val() == 1:
                smtlib2_constraints.append(Implies("true", constraint))
            elif r_.get_val() == 0:
                smtlib2_constraints.append(Implies("false", constraint))
        return smtlib2_constraints
