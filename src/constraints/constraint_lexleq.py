from src.arguments.list_access import ListAccess
from src.arguments.variable import Variable
from src.constants import Constants
from src.constraints.constraint_eq import Constraint_eq
from src.constraints.interfaces.constraint import Constraint
from src.globals import get_global_counter
from src.mapper.type import Type
from src.smtlib2_statements.smtlib2_command import DefineFunRec
from src.smtlib2_statements.smtlib2_function import Lt, Select, Ite, Gt, Geq
from src.smtlib2_statements.smtlib2_statement import FunctionCall


class Constraint_lexleq(Constraint):
    def is_valid(self):
        return self.num_args(2) and self._args[0].is_variable_or_vector() and self._args[1].is_variable_or_vector()

    def to_smtlib2(self):
        vec1_ = self._args[0]
        vec2_ = self._args[1]
        vec1 = vec1_.to_smtlib2()
        vec2 = vec2_.to_smtlib2()

        counter = get_global_counter()

        constraints = []
        if vec1_.is_vector():
            vec1 = f"{Constants.HELPER_ARRAY}_1_{counter}"
            variable = Variable(vec1, Type.TYPE_INT, dimensions=[1])
            constraints.append(variable.declaration_to_smtlib2())
            for i, var in enumerate(vec1_.get_vec()):
                constraints.append(Constraint_eq("eq", var, ListAccess(variable, i)).to_smtlib2())

        if vec2_.is_vector():
            vec2 = f"{Constants.HELPER_ARRAY}_2_{counter}"
            variable = Variable(vec2, Type.TYPE_INT, dimensions=[1])
            constraints.append(variable.declaration_to_smtlib2())
            for i, var in enumerate(vec2_.get_vec()):
                constraints.append(Constraint_eq("eq", var, ListAccess(variable, i)).to_smtlib2())

        if vec1_.is_variable():
            upper = vec1_.get_upper_bound()
        else:
            upper = len(vec1_.get_vec())

        function_name = f"fun_lexleq_{counter}"
        i = Constants.COUNTER1

        return constraints + [
            DefineFunRec(function_name, [f"{i} Int"], "Bool",
                      Ite(Geq(i, upper), "true",
                          Ite(Gt(Select(vec1, i), Select(vec2, i)),
                              "false",
                              Ite(Lt(Select(vec1, i), Select(vec2, i)),
                                  "true",
                                  f"({function_name} (+ {i} 1))"
                                  )
                              ))
                      ),
            FunctionCall(function_name, 0)
        ]
