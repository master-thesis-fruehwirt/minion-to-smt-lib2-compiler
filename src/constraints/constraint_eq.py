from src.constraints.interfaces.constraint import Constraint
from src.smtlib2_statements.smtlib2_function import Eq, Not


class Constraint_eq(Constraint):

    def is_valid(self):
        return self.num_args(2) and self._args[0].is_variable_or_const() and self._args[1].is_variable_or_const()

    def to_smtlib2(self):
        x_ = self._args[0]
        y_ = self._args[1]
        x = x_.to_smtlib2()
        y = y_.to_smtlib2()

        if x_.is_boolean_var():
            if y_.is_const():
                if y_.get_val() == 1:
                    return Eq(x, "true")
                else:
                    return Eq(x, "false")
        elif y_.is_boolean_var():
            if x_.is_const():
                if x_.get_val() == 1:
                    return Eq(y, "true")
                else:
                    return Eq(y, "false")

        return Eq(x, y)
