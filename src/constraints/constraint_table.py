from src.constraints.interfaces.table_constraint import TableConstraint
from src.smtlib2_statements.smtlib2_function import Eq, Or, And


class Constraint_table(TableConstraint):

    def __init__(self, name, *args):
        super().__init__(name, Or, And, Eq, *args)
