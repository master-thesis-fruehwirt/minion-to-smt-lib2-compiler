from src.constants import CompilerConstants
from src.constraints.constraint_sumgeq import Constraint_sumgeq
from src.constraints.constraint_sumleq import Constraint_sumleq
from src.constraints.constraint_watchsumgeq import Constraint_watchsumgeq
from src.constraints.constraint_watchsumleq import Constraint_watchsumleq
from src.constraints.constraint_weightedsumgeq import Constraint_weightedsumgeq
from src.constraints.constraint_weightedsumleq import Constraint_weightedsumleq
from src.constraints.custom_constraint_sumeq import Constraint_sumeq
from src.constraints.custom_constraint_watchsumeq import Constraint_watchsumeq
from src.constraints.custom_constraint_weightedsumeq import Constraint_weightedsumeq


class MinionProgramRepresentation:

    def __init__(self):
        self._var_declarations = []
        self._constraints = []
        self._var_accesses = []

    def add_var_decl(self, var_decl):
        self._var_declarations.append(var_decl)

    def get_var_decls(self):
        return self._var_declarations

    def add_constraint(self, constraint):
        self._constraints.append(constraint)

    def add_var_access(self, var_access):
        self._var_accesses.append(var_access)

    def _is_used(self, var_decl):
        accessed_variables = set(self._var_accesses)
        for accessed_var in accessed_variables:
            if accessed_var.get_name() == var_decl.get_name():
                return True
        return False

    def _are_equivalent(self, c1, c2):
        if not (type(c1) == Constraint_sumgeq and type(c2) == Constraint_sumleq or
                type(c1) == Constraint_watchsumgeq and type(c2) == Constraint_watchsumleq or
                type(c1) == Constraint_weightedsumgeq and type(c2) == Constraint_weightedsumleq):
            return False

        if len(c1.get_args()) != len(c2.get_args()):
            return False

        for arg1, arg2 in zip(c1.get_args(), c2.get_args()):
            if arg1.get_type() != arg2.get_type():
                return False
            if arg1.is_vector() and arg2.is_vector():
                for v1, v2 in zip(arg1.get_vec(), arg2.get_vec()):
                    if str(v1) != str(v2):
                        return False
            else:
                if str(arg1) != str(arg2):
                    return False
        return True

    def _optimize_constraints(self):
        indices_to_skip = []
        indices_to_change = []
        optimized_constraints = []

        for i in range(len(self._constraints)):
            if i in indices_to_skip:
                continue  # do not consider already found matches

            c1 = self._constraints[i]
            for j in range(len(self._constraints)):
                c2 = self._constraints[j]
                if self._are_equivalent(c1, c2):
                    indices_to_change.append(i)
                    indices_to_skip.append(j)
                    break

        for i in range(len(self._constraints)):
            c1 = self._constraints[i]

            if i not in indices_to_change + indices_to_skip:
                optimized_constraints.append(c1)
            elif i in indices_to_change:
                if type(c1) == Constraint_sumgeq:
                    optimized_constraints.append(Constraint_sumeq("sumeq {custom}", *c1.get_args()))
                elif type(c1) == Constraint_watchsumgeq:
                    optimized_constraints.append(Constraint_watchsumeq("watchsumeq {custom}", *c1.get_args()))
                elif type(c1) == Constraint_weightedsumgeq:
                    optimized_constraints.append(Constraint_weightedsumeq("weightedsumeq {custom}", *c1.get_args()))
        return optimized_constraints

    def to_smtlib2(self, cmd_args):
        smtlib2_var_declarations = []
        smtlib2_constraints = []
        smtlib2_model = []

        for var_decl in self._var_declarations:
            if self._is_used(var_decl) or CompilerConstants.ARG_OPTIMIZE not in cmd_args:
                smtlib2_var_decl = var_decl.declaration_to_smtlib2()
                if not var_decl.is_table():
                    domain_constraint = var_decl.create_domain_constraint()

                    if smtlib2_var_decl:
                        smtlib2_var_declarations.append(smtlib2_var_decl.to_code())

                    if domain_constraint:
                        if CompilerConstants.ARG_PRINT_MINION in cmd_args:
                            smtlib2_constraints.append(f"; {var_decl.to_debug_string()}")
                        if type(domain_constraint) == list:
                            for constraint in domain_constraint:
                                smtlib2_constraints.append(constraint.to_code())
                        else:
                            smtlib2_constraints.append(domain_constraint.to_code())

                model = var_decl.get_smtlib2_model()
                if model:
                    smtlib2_model.append(model)

        optimized_constraints = self._optimize_constraints()

        for constraint in optimized_constraints:
            smtlib2_constraint = constraint.to_smtlib2()
            if smtlib2_constraint:
                if CompilerConstants.ARG_PRINT_MINION in cmd_args:
                    smtlib2_constraints.append(f"; {str(constraint)}")

                if type(smtlib2_constraint) == list:
                    for elem in smtlib2_constraint:
                        smtlib2_constraints.append(elem.to_code())
                else:
                    smtlib2_constraints.append(smtlib2_constraint.to_code())

        result = smtlib2_var_declarations + \
                 [""] + \
                 smtlib2_constraints + \
                 [""] + \
                 ["(check-sat)"]

        if CompilerConstants.ARG_DEBUG in cmd_args:
            result += ["(get-model)"] + smtlib2_model

        return result
