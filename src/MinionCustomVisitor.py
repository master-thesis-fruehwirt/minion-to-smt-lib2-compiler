from gen.MinionParser import MinionParser
from gen.MinionVisitor import MinionVisitor
from src.arguments.const import Const
from src.arguments.list_access import ListAccess
from src.arguments.not_var import NotVar
from src.arguments.table import Table
from src.arguments.variable import Variable
from src.arguments.vector import Vector
from src.constants import Constants
from src.errors import add_constraint_argument_error
from src.globals import get_global_counter
from src.mapper.constraint_mapper import ConstraintMapper
from src.mapper.type import Type
from src.mapper.variable_mapper import VariableMapper


class MinionCustomVisitor(MinionVisitor):

    def __init__(self, program):
        self._program = program
        self._constraint_mapper = ConstraintMapper()
        self._variable_mapper = VariableMapper()
        self._constraint_depth = 0

    def visitDiscreteVarDecl(self, ctx:MinionParser.DiscreteVarDeclContext):
        dimensions = []
        domain = []
        if ctx.listDeclaration():
            dimensions = self.visit(ctx.listDeclaration())

        if ctx.domain():
            domain = self.visit(ctx.domain())

        variable = Variable(ctx.name.text, Type.TYPE_INT, dimensions, domain)
        self._program.add_var_decl(variable)
        self._variable_mapper.add_mapping(variable, ctx.start.line, ctx.start.column)

    def visitBoolVarDecl(self, ctx:MinionParser.BoolVarDeclContext):
        dimensions = []
        if ctx.listDeclaration():
            dimensions = self.visit(ctx.listDeclaration())

        variable = Variable(ctx.name.text, Type.TYPE_BOOL, dimensions)
        self._program.add_var_decl(variable)
        self._variable_mapper.add_mapping(variable, ctx.start.line, ctx.start.column)

    def visitSparseboundVarDecl(self, ctx:MinionParser.SparseboundVarDeclContext):
        dimensions = []
        if ctx.listDeclaration():
            dimensions = self.visit(ctx.listDeclaration())
        domain = self.visit(ctx.domainSparsebound())

        variable = Variable(ctx.name.text, Type.TYPE_INT, dimensions, domain, True)
        self._program.add_var_decl(variable)
        self._variable_mapper.add_mapping(variable, ctx.start.line, ctx.start.column)

    def visitBoundVarDecl(self, ctx:MinionParser.BoundVarDeclContext):
        dimensions = []
        if ctx.listDeclaration():
            dimensions = self.visit(ctx.listDeclaration())
        domain = self.visit(ctx.domain())

        variable = Variable(ctx.name.text, Type.TYPE_INT, dimensions, domain)
        self._program.add_var_decl(variable)
        self._variable_mapper.add_mapping(variable, ctx.start.line, ctx.start.column)

    def visitListDeclaration(self, ctx:MinionParser.ListDeclarationContext):
        items = []
        for item in ctx.items:
            items.append(int(item.text))
        return items

    def visitDomain(self, ctx:MinionParser.DomainContext):
        lower = 0
        upper = 0
        if ctx.lower and ctx.upper:
            lower = int(ctx.lower.text)
            upper = int(ctx.upper.text)
        return [lower, upper]

    def visitDomainSparsebound(self, ctx:MinionParser.DomainSparseboundContext):
        items = []
        for item in ctx.items:
            items.append(int(item.text))
        return items

    def visitSimpleConstraint(self, ctx:MinionParser.SimpleConstraintContext):
        self._constraint_depth += 1
        name = ctx.name.text

        args = []
        for arg in ctx.arg():
            result = self.visit(arg)
            args.append(result)

        constraint = self._constraint_mapper.map(name, *args)
        if self._constraint_depth == 1:
            self._program.add_constraint(constraint)

        self._constraint_depth -= 1
        if not constraint.is_valid():
            add_constraint_argument_error(constraint.get_name(), ctx.start.line, ctx.start.column, constraint.get_args())
        return constraint

    def visitArgVariable(self, ctx:MinionParser.ArgVariableContext):
        mapping = self._variable_mapper.map(ctx.value.text, ctx.start.line, ctx.start.column)
        self._program.add_var_access(mapping)
        if ctx.KEY_NOT():
            return NotVar(mapping)
        return mapping

    def visitArgInteger(self, ctx:MinionParser.ArgIntegerContext):
        return Const(int(ctx.value.text))

    def visitArgList(self, ctx:MinionParser.ArgListContext):
        return Vector(*self.visit(ctx.list_()))

    def visitList(self, ctx:MinionParser.ListContext):
        list_values = []
        for value in ctx.value:
            result = self.visit(value)
            list_values.append(result)
        return list_values

    def visitListVariable(self, ctx:MinionParser.ListVariableContext):
        mapping = self._variable_mapper.map(ctx.value.text, ctx.start.line, ctx.start.column)
        self._program.add_var_access(mapping)
        if ctx.KEY_NOT():
            return NotVar(mapping)
        return mapping

    def visitListInteger(self, ctx:MinionParser.ListIntegerContext):
        return Const(int(ctx.value.text))

    def visitListAccess(self, ctx:MinionParser.ListAccessContext):
        result = self.visit(ctx.list_())
        mapping = self._variable_mapper.map(ctx.name.text, ctx.start.line, ctx.start.column)
        self._program.add_var_access(mapping)
        la = ListAccess(mapping, *result)
        if ctx.KEY_NOT():
            return NotVar(la)
        return la

    def visitWatchedConstraint(self, ctx:MinionParser.WatchedConstraintContext):
        self._constraint_depth += 1

        constraints = []
        for constraint in ctx.constraint():
            result = self.visit(constraint)
            constraints.append(result)

        constraint = self._constraint_mapper.map(ctx.name.text, *constraints)
        self._program.add_constraint(constraint)

        self._constraint_depth -= 1
        return constraints

    def visitTableConstraint(self, ctx:MinionParser.TableConstraintContext):
        rows = []
        for row in ctx.rows:
            rows.append(self.visit(row))

        counter = get_global_counter()
        table_name = f"{Constants.HELPER_TABLE}_{counter}"
        table = Table(table_name, len(rows), len(rows[0]), rows)

        self._program.add_var_decl(table)
        self._variable_mapper.add_mapping(table)
        return table

    def visitTableRow(self, ctx:MinionParser.TableRowContext):
        cols = []
        for col in ctx.cols:
            cols.append(int(col.text))
        return cols

    def visitTupleList(self, ctx:MinionParser.TupleListContext):
        name = ctx.name.text
        rows = int(ctx.rows.text)
        cols = int(ctx.cols.text)
        numbers = ctx.numbers

        tuplelist = []

        for i in range(0, len(numbers), cols):
            tuplelist.append([int(x.text) for x in numbers[i:i + cols]])

        table = Table(name, rows, cols, tuplelist)

        self._program.add_var_decl(table)
        self._variable_mapper.add_mapping(table)
