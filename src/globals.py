
GLOBAL_COUNTER = 0


def get_global_counter():
    global GLOBAL_COUNTER
    counter = GLOBAL_COUNTER
    GLOBAL_COUNTER += 1
    return counter


def reset_global_counter():
    global GLOBAL_COUNTER
    GLOBAL_COUNTER = 0
