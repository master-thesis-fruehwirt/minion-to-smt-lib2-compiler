from src.constraints.constraint_abs import Constraint_abs
from src.constraints.constraint_alldiff import Constraint_alldiff
from src.constraints.constraint_alldiffmatrix import Constraint_alldiffmatrix
from src.constraints.constraint_difference import Constraint_difference
from src.constraints.constraint_diseq import Constraint_diseq
from src.constraints.constraint_div import Constraint_div
from src.constraints.constraint_element import Constraint_element
from src.constraints.constraint_element_one import Constraint_element_one
from src.constraints.constraint_eq import Constraint_eq
from src.constraints.constraint_gacalldiff import Constraint_gacalldiff
from src.constraints.not_implemented.constraint_gacschema import Constraint_gacschema
from src.constraints.not_implemented.constraint_gcc import Constraint_gcc
from src.constraints.not_implemented.constraint_gccweak import Constraint_gccweak
from src.constraints.not_implemented.constraint_haggisgac import Constraint_haggisgac
from src.constraints.not_implemented.constraint_haggisgac_stable import Constraint_haggisgac_stable
from src.constraints.constraint_hamming import Constraint_hamming
from src.constraints.constraint_ineq import Constraint_ineq
from src.constraints.constraint_lexleq import Constraint_lexleq
from src.constraints.constraint_lexless import Constraint_lexless
from src.constraints.not_implemented.constraint_lighttable import Constraint_lighttable
from src.constraints.constraint_litsumgeq import Constraint_litsumgeq
from src.constraints.constraint_max import Constraint_max
from src.constraints.not_implemented.constraint_mddc import Constraint_mddc
from src.constraints.constraint_min import Constraint_min
from src.constraints.constraint_minuseq import Constraint_minuseq
from src.constraints.constraint_mod_undefzero import Constraint_mod_undefzero
from src.constraints.constraint_modulo import Constraint_modulo
from src.constraints.not_implemented.constraint_negativemddc import Constraint_negativemddc
from src.constraints.constraint_negativetable import Constraint_negativetable
from src.constraints.constraint_occurrence import Constraint_occurrence
from src.constraints.constraint_occurrencegeq import Constraint_occurrencegeq
from src.constraints.constraint_occurrenceleq import Constraint_occurrenceleq
from src.constraints.constraint_pow import Constraint_pow
from src.constraints.constraint_product import Constraint_product
from src.constraints.constraint_reify import Constraint_reify
from src.constraints.constraint_reifyimply import Constraint_reifyimply
from src.constraints.not_implemented.constraint_shortstr2 import Constraint_shortstr2
from src.constraints.not_implemented.constraint_str2plus import Constraint_str2plus
from src.constraints.constraint_sumgeq import Constraint_sumgeq
from src.constraints.constraint_sumleq import Constraint_sumleq
from src.constraints.constraint_table import Constraint_table
from src.constraints.constraint_w_inintervalset import Constraint_w_inintervalset
from src.constraints.constraint_w_inrange import Constraint_w_inrange
from src.constraints.constraint_w_inset import Constraint_w_inset
from src.constraints.constraint_w_literal import Constraint_w_literal
from src.constraints.constraint_w_notinrange import Constraint_w_notinrange
from src.constraints.constraint_w_notinset import Constraint_w_notinset
from src.constraints.constraint_w_notliteral import Constraint_w_notliteral
from src.constraints.constraint_watched_and import Constraint_watched_and
from src.constraints.constraint_watched_or import Constraint_watched_or
from src.constraints.constraint_watchelement import Constraint_watchelement
from src.constraints.constraint_watchelement_one import Constraint_watchelement_one
from src.constraints.constraint_watchelement_undefzero import Constraint_watchelement_undefzero
from src.constraints.constraint_watchless import Constraint_watchless
from src.constraints.constraint_watchsumgeq import Constraint_watchsumgeq
from src.constraints.constraint_watchsumleq import Constraint_watchsumleq
from src.constraints.constraint_watchvecneq import Constraint_watchvecneq
from src.constraints.constraint_weightedsumgeq import Constraint_weightedsumgeq
from src.constraints.constraint_weightedsumleq import Constraint_weightedsumleq
from src.errors import add_error


class ConstraintMapper:

    def __init__(self):
        pass

    def map(self, name, *args):
        if name == "abs":
            return Constraint_abs(name, *args)
        elif name == "alldiff":
            return Constraint_alldiff(name, *args)
        elif name == "alldiffmatrix":
            return Constraint_alldiffmatrix(name, *args)
        elif name == "difference":
            return Constraint_difference(name, *args)
        elif name == "diseq":
            return Constraint_diseq(name, *args)
        elif name == "div":
            return Constraint_div(name, *args)
        elif name == "element":
            return Constraint_element(name, *args)
        elif name == "element_one":
            return Constraint_element_one(name, *args)
        elif name == "eq":
            return Constraint_eq(name, *args)
        elif name == "gacalldiff":
            return Constraint_gacalldiff(name, *args)
        elif name == "gacschema":
            return Constraint_gacschema(name, *args)
        elif name == "gcc":
            return Constraint_gcc(name, *args)
        elif name == "gccweak":
            return Constraint_gccweak(name, *args)
        elif name == "haggisgac":
            return Constraint_haggisgac(name, *args)
        elif name == "haggisgac-stable":
            return Constraint_haggisgac_stable(name, *args)
        elif name == "hamming":
            return Constraint_hamming(name, *args)
        elif name == "ineq":
            return Constraint_ineq(name, *args)
        elif name == "lexleq":
            return Constraint_lexleq(name, *args)
        elif name == "lexless":
            return Constraint_lexless(name, *args)
        elif name == "lighttable":
            return Constraint_lighttable(name, *args)
        elif name == "litsumgeq":
            return Constraint_litsumgeq(name, *args)
        elif name == "max":
            return Constraint_max(name, *args)
        elif name == "mddc":
            return Constraint_mddc(name, *args)
        elif name == "min":
            return Constraint_min(name, *args)
        elif name == "minuseq":
            return Constraint_minuseq(name, *args)
        elif name == "mod_undefzero":
            return Constraint_mod_undefzero(name, *args)
        elif name == "modulo":
            return Constraint_modulo(name, *args)
        elif name == "negativemddc":
            return Constraint_negativemddc(name, *args)
        elif name == "negativetable":
            return Constraint_negativetable(name, *args)
        elif name == "occurrence":
            return Constraint_occurrence(name, *args)
        elif name == "occurrencegeq":
            return Constraint_occurrencegeq(name, *args)
        elif name == "occurrenceleq":
            return Constraint_occurrenceleq(name, *args)
        elif name == "pow":
            return Constraint_pow(name, *args)
        elif name == "product":
            return Constraint_product(name, *args)
        elif name == "reify":
            return Constraint_reify(name, *args)
        elif name == "reifyimply":
            return Constraint_reifyimply(name, *args)
        elif name == "shortstr2":
            return Constraint_shortstr2(name, *args)
        elif name == "str2plus":
            return Constraint_str2plus(name, *args)
        elif name == "sumgeq":
            return Constraint_sumgeq(name, *args)
        elif name == "sumleq":
            return Constraint_sumleq(name, *args)
        elif name == "table":
            return Constraint_table(name, *args)
        elif name == "w-inintervalset":
            return Constraint_w_inintervalset(name, *args)
        elif name == "w-inrange":
            return Constraint_w_inrange(name, *args)
        elif name == "w-inset":
            return Constraint_w_inset(name, *args)
        elif name == "w-literal":
            return Constraint_w_literal(name, *args)
        elif name == "w-notinrange":
            return Constraint_w_notinrange(name, *args)
        elif name == "w-notinset":
            return Constraint_w_notinset(name, *args)
        elif name == "w-notliteral":
            return Constraint_w_notliteral(name, *args)
        elif name == "watched-and":
            return Constraint_watched_and(name, *args)
        elif name == "watched-or":
            return Constraint_watched_or(name, *args)
        elif name == "watchelement":
            return Constraint_watchelement(name, *args)
        elif name == "watchelement_one":
            return Constraint_watchelement_one(name, *args)
        elif name == "watchelement_undefzero":
            return Constraint_watchelement_undefzero(name, *args)
        elif name == "watchless":
            return Constraint_watchless(name, *args)
        elif name == "watchsumgeq":
            return Constraint_watchsumgeq(name, *args)
        elif name == "watchsumleq":
            return Constraint_watchsumleq(name, *args)
        elif name == "watchvecneq":
            return Constraint_watchvecneq(name, *args)
        elif name == "weightedsumgeq":
            return Constraint_weightedsumgeq(name, *args)
        elif name == "weightedsumleq":
            return Constraint_weightedsumleq(name, *args)
        else:
            add_error(f"Unkown constraint: {name}")
