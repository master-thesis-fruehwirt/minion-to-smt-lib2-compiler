import enum


class Type(enum.IntEnum):
    TYPE_DEFAULT = 0
    TYPE_INT = 1
    TYPE_BOOL = 2
    TYPE_VECTOR = 3
    TYPE_CONST = 4
    TYPE_CONSTRAINT = 5
    TYPE_TABLE = 6
