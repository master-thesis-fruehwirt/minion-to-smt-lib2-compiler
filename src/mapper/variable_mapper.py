from src.arguments.variable import Variable
from src.errors import add_variable_double_declaration, add_variable_not_defined_error


class VariableMapper:

    def __init__(self):
        self._type_map = {}

    def add_mapping(self, variable, line=0, column=0):
        if variable.get_name() in self._type_map:
            add_variable_double_declaration(variable, line, column)
        else:
            self._type_map[variable.get_name()] = variable

    def map(self, name, line, column):
        try:
            var = self._type_map[name]
        except:
            var = Variable(name)
            if name != '_':
                add_variable_not_defined_error(name, line, column)

        return var
