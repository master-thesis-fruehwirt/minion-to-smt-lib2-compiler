from src.smtlib2_statements.smtlib2_statement import Smtlib2Statement


class Smtlib2Function(Smtlib2Statement):

    def __init__(self, name, *args):
        self._name = name
        self._args = args
        self._assertable = True

    def __str__(self):
        return f"({self._name} {' '.join(str(a) for a in self._args)})"


class Abs(Smtlib2Function):

    def __init__(self, *args):
        super().__init__("abs", *args)


class Add(Smtlib2Function):

    def __init__(self, *args):
        super().__init__("+", *args)


class And(Smtlib2Function):

    def __init__(self, *args):
        super().__init__("and", *args)


class Distinct(Smtlib2Function):

    def __init__(self, *args):
        super().__init__("distinct", *args)


class Div(Smtlib2Function):

    def __init__(self, *args):
        super().__init__("div", *args)


class Eq(Smtlib2Function):

    def __init__(self, *args):
        super().__init__("=", *args)


class Geq(Smtlib2Function):

    def __init__(self, *args):
        super().__init__(">=", *args)


class Gt(Smtlib2Function):

    def __init__(self, *args):
        super().__init__(">", *args)


class Implies(Smtlib2Function):

    def __init__(self, *args):
        super().__init__("=>", *args)


class Ite(Smtlib2Function):

    def __init__(self, *args):
        super().__init__("ite", *args)


class Leq(Smtlib2Function):

    def __init__(self, *args):
        super().__init__("<=", *args)


class Lt(Smtlib2Function):

    def __init__(self, *args):
        super().__init__("<", *args)


class Mod(Smtlib2Function):

    def __init__(self, *args):
        super().__init__("mod", *args)


class Mul(Smtlib2Function):

    def __init__(self, *args):
        super().__init__("*", *args)


class Not(Smtlib2Function):

    def __init__(self, *args):
        super().__init__("not", *args)


class Or(Smtlib2Function):

    def __init__(self, *args):
        super().__init__("or", *args)


class Pow(Smtlib2Function):

    def __init__(self, *args):
        super().__init__("^", *args)


class Select(Smtlib2Function):

    def __init__(self, *args):
        super().__init__("select", *args)


class Sub(Smtlib2Function):

    def __init__(self, *args):
        super().__init__("-", *args)
