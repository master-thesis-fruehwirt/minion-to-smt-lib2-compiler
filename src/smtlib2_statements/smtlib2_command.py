from src.smtlib2_statements.smtlib2_statement import Smtlib2Statement


class Smtlib2Command(Smtlib2Statement):

    def __init__(self):
        self._assertable = False


class DeclareConst(Smtlib2Command):

    def __init__(self, name, dimensions, tpe, is_table=False):
        super().__init__()
        self._name = name
        self._dimensions = dimensions
        self._tpe = tpe
        self._is_table = is_table

    def __str__(self):
        if self._is_table:
            return f"(declare-const {self._name} (Array Int (Array Int Int)))"

        if self._dimensions:
            return f"(declare-const {self._name} (Array{' Int' * self._dimensions} {self._tpe}))"
        else:
            return f"(declare-const {self._name} {self._tpe})"


class DefineFunRec(Smtlib2Command):

    def __init__(self, name, params, ret_type, expr):
        super().__init__()
        self._name = name
        self._ret_type = ret_type
        self._params = params
        self._expr = expr

    def __str__(self):
        return f"(define-fun-rec {self._name} ({' '.join(f'({param})' for param in self._params)}) {self._ret_type} {self._expr})"
