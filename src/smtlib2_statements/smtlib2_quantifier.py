from src.smtlib2_statements.smtlib2_statement import Smtlib2Statement


class Smtlib2Quantifier(Smtlib2Statement):

    def __init__(self, name, *args):
        self._name = name
        self._args = args
        self._assertable = True

    def __str__(self):
        return f"({self._name} ({' '.join(str(a) for a in self._args[:-1])}) {str(self._args[-1])})"


class Exists(Smtlib2Quantifier):

    def __init__(self, *args):
        super().__init__("exists", *args)


class Forall(Smtlib2Quantifier):

    def __init__(self, *args):
        super().__init__("forall", *args)
