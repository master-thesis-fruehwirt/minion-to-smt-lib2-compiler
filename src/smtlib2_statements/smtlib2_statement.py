

class Smtlib2Statement:

    def __int__(self):
        self._assertable = True

    def to_repr(self):
        return self.__str__()

    def to_code(self):
        if self._assertable:
            return f"(assert {self.__str__()})"
        else:
            return self.__str__()


class BoundVar(Smtlib2Statement):

    def __init__(self, symbol, sort="Int"):
        self._symbol = symbol
        self._sort = sort
        self._assertable = True

    def __str__(self):
        return f"({self._symbol} {self._sort})"


class FunctionCall(Smtlib2Statement):

    def __init__(self, name, *args):
        self._name = name
        self._args = args
        self._assertable = True

    def __str__(self):
        return f"({self._name} {' '.join(str(arg) for arg in self._args)})"


class Neg(Smtlib2Statement):

    def __init__(self, var):
        self._var = var
        self._assertable = False

    def __str__(self):
        return f"(- {self._var})"
