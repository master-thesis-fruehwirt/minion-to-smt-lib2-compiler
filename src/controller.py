import os
import sys

import antlr4
from antlr4 import InputStream

from gen.MinionLexer import MinionLexer
from gen.MinionParser import MinionParser
from src.MinionCustomVisitor import MinionCustomVisitor
from src.constants import CompilerConstants
from src.errors import error_list, reset_error_list
from src.globals import reset_global_counter
from src.minion_program_representation import MinionProgramRepresentation


class Controller:

    def __init__(self):
        self._program = MinionProgramRepresentation()
        reset_global_counter()
        reset_error_list()

    def init_lexer(self, data):
        code_stream = InputStream(data)
        lexer = MinionLexer(code_stream)
        return lexer

    def init_parser(self, lexer):
        token_stream = antlr4.CommonTokenStream(lexer)
        parser = MinionParser(token_stream)
        return parser

    def compile(self, data):
        lexer = self.init_lexer(data)
        parser = self.init_parser(lexer)
        tree = parser.program()
        visitor = MinionCustomVisitor(self._program)

        visitor.visit(tree)

        if parser.getNumberOfSyntaxErrors() == 0:
            if len(error_list) > 0:
                self._print_errors()
            else:
                smtlib_program = self._program.to_smtlib2(sys.argv)
                self._print_program(smtlib_program)
                self._write_file(smtlib_program)

    def get_var_decls(self):
        return self._program.get_var_decls()

    def _print_program(self, smtlib_program):
        if CompilerConstants.ARG_PRINT in sys.argv:
            for l in smtlib_program:
                print(l)

    def _print_errors(self):
        print(f"Number of syntax errors: {len(error_list)}")
        for i, error in enumerate(error_list):
            print(f"#{i + 1}: {error}")

    def read_file(self):
        if len(sys.argv) == 1:
            print(f"Missing input file! Type {CompilerConstants.COMPILER_NAME} {CompilerConstants.ARG_HELP} for further information.")
            sys.exit(-1)

        filename = sys.argv[1]

        if not os.path.exists(filename):
            print(f"File '{filename}' does not exist! Please provide a correct input file.")
            sys.exit(-1)

        with open(filename, 'r') as f:
            data = f.read()

        return data

    def _write_file(self, lines):
        filename = sys.argv[1].replace(".minion", ".smtlib2")
        with open(filename, 'w') as f:
            f.writelines(l + '\n' for l in lines)
