grammar Minion;

WS : [ \r\n\t] + -> channel (HIDDEN);

COMMENT : '#' ~[\n\r]* -> channel(HIDDEN);

fragment NON_ZERO_DIGIT: '1'..'9';
fragment DIGIT: '0'..'9';
fragment LETTER : LOWERCASE | UPPERCASE;
fragment UPPERCASE : 'A'..'Z';
fragment LOWERCASE : 'a'..'z';

INT : '0' | '-'? NON_ZERO_DIGIT DIGIT*;
BOOL_LIT : 'true' | 'false';

// different sections start keywords
KEY_MINION: 'MINION 3';
KEY_EOF: '**EOF**' .*?;
KEY_VARIABLES: '**VARIABLES**';
KEY_SEARCH: '**SEARCH**';
KEY_CONSTRAINTS: '**CONSTRAINTS**';
KEY_TUPLELIST: '**TUPLELIST**';

// VARIABLES section keywords
KEY_BOOL: 'BOOL';
KEY_BOUND: 'BOUND';
KEY_SPARSEBOUND: 'SPARSEBOUND';
KEY_DISCRETE: 'DISCRETE';
KEY_NOT: '!';

// SEARCH section keywords
KEY_VARORDER: 'VARORDER';
KEY_VALORDER: 'VALORDER';
KEY_AUX: 'AUX';

KEY_ORDER_STATIC: 'STATIC';
KEY_ORDER_SDF: 'SDF';
KEY_ORDER_SRF: 'SRF';
KEY_ORDER_LDF: 'LDF';
KEY_ORDER_ORIGINAL: 'ORIGINAL';
KEY_ORDER_WDEG: 'WDEG';
KEY_ORDER_CONFLICT: 'CONFLICT';
KEY_ORDER_DOMOVERWDEG: 'DOMOVERWDEG';
KEY_MINIMISING: 'MINIMISING';
KEY_MAXIMISING: 'MAXIMISING';
KEY_PRINT: 'PRINT';
KEY_ALL: 'ALL';
KEY_NONE: 'NONE';

ID : (LETTER | DIGIT | '_' | '-')+;
KEY_ASCENDING: 'a';
KEY_DESCENDING: 'd';


program : KEY_MINION input_section+ KEY_EOF;

input_section :
               variablesSection
              | searchSection
              | constraintsSection
              | tuplelistSection;
//              | short_tuplelist_section;

// VARIABLES section
variablesSection: KEY_VARIABLES varDeclaration*;

domain: '{' lower=INT '..' upper=INT '}';
domainSparsebound: '{' items+=INT (',' items+=INT)* '}';

listDeclaration: '[' items+=INT (',' items+=INT)* ']';

varDeclaration: type=KEY_BOOL name=ID listDeclaration? #BoolVarDecl
               | type=KEY_SPARSEBOUND name=ID listDeclaration? domainSparsebound #SparseboundVarDecl
               | type=KEY_BOUND name=ID listDeclaration? domain #BoundVarDecl
               | type=KEY_DISCRETE name=ID listDeclaration? domain #DiscreteVarDecl
               ;

// CONSTRAINTS section

constraintsSection: KEY_CONSTRAINTS constraint*;

constraint: name=ID '(' arg (',' arg)* ')' #SimpleConstraint
          | name=ID '(' '{' constraint (',' constraint)+ '}' ')' #WatchedConstraint
          ;

tableConstraint: '{' rows+=tableRow (',' rows+=tableRow)* '}';
tableRow: '<' cols+=INT (',' cols+=INT)* '>';

arg: KEY_NOT? value=ID #ArgVariable
   | KEY_NOT? value=INT #ArgInteger
   | constraint #ArgConstraint
//   | BOOL_LIT #ArgBool // not used
   | list #ArgList
   | listAccess #NotUsedArgListAccess
   | tableConstraint #ArgTableConstraint
   ;

list: '[' value+=listValue (',' value+=listValue)* ']';
listValue: KEY_NOT? value=ID #ListVariable
         | KEY_NOT? value=INT #ListInteger
         | listAccess #NotUsedListAccess
         | list #NotUsedList
         ;
listAccess: KEY_NOT? name=ID list;

// SEARCH section

searchSection: KEY_SEARCH varValOrdering* optimisationFn? printFormat?;

varValOrdering: varOrder valOrder?;

order: KEY_ORDER_STATIC
     | KEY_ORDER_SDF
     | KEY_ORDER_SRF
     | KEY_ORDER_LDF
     | KEY_ORDER_ORIGINAL
     | KEY_ORDER_WDEG
     | KEY_ORDER_CONFLICT
     | KEY_ORDER_DOMOVERWDEG;

varOrder: KEY_VARORDER KEY_AUX? order? list;

valOrder: KEY_VALORDER '[' (KEY_ASCENDING | KEY_DESCENDING)+ ']';

optimisationFn: (KEY_MINIMISING | KEY_MAXIMISING) ID;

printFormat: KEY_PRINT (list | KEY_ALL | KEY_NONE);

tuplelistSection: KEY_TUPLELIST tupleList*;
tupleList: name=ID rows=INT cols=INT numbers+=INT+;

//short_tuple: '[' tuple* ',' ']';
//tuple: '(' INT ',' INT ')';

//ID : (LETTER | DIGIT | '_' | '-')+;
