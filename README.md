# Min2Smt Compiler - The MINION to SMT-LIB2 compiler

This repository provides the source code for the *Min2Smt* compiler described in the Master's Thesis *A practical approach to the compilation of MINION constraint models into SMT-LIB2 constraint models* written by Stephan Frühwirt 2023 at Graz University of Technology ([Institute of Software Technology](https://www.tugraz.at/institute/ist/home)) and supervised by **Univ.-Prof. Dipl.-Ing. Dr.techn. Franz Wotawa**. The thesis can be found [here](https://online.tugraz.at/tug_online/wbAbs.showThesis?pThesisNr=84906&pOrgNr=14250).

## Abstract
Compilers are great tools for translating source code written in one programming language into another. This allows the execution of programs on different platforms, utilizing features of one problem-solving technique in other problem-solving systems, optimizing computer architectures, and more. One concrete application of compilers is the translation of Constraint Satisfiability Problems (CSP) into Boolean Satisfiability Problems (SAT) or Satisfiability Modulo Theories (SMT). In this master’s thesis, we present a new compiler called Min2Smt, written in Python and ANTLR. It compiles MINION Input Language to the SMT-LIB2 language. The MINION Input Language is used to model CSPs, and the SMT-LIB2 language is used to express logical formulas and constraints in SMT solvers. The compiler aims to provide a more accessible way of defining and solving optimization problems using the simple syntax of MINION and to integrate these problems into larger systems that use SMT solvers. In order to be able to check the correctness of the translation, the term 'equivalence' must be defined. A test framework explicitly developed for Min2Smt uses a large number of test files to demonstrate this equivalence. Optimizations of the compiler ultimately lead to performance improvements of the generated code, thus forming the compiler into a good tool that can translate CSPs in SMT. 

## Testing and test data
The test data located in `/test_files`  and are structured as follows:
* `/ISCAS85`: These input files were used in [Jef+14]. They mainly utilize the following constraints: `diseq`, `reify`, `reifyimply`,
`eq`, `max`, `min`, `sumgeq` and `sumleq. In addition, a larger set of variables (between 300-4500) and one large vector (up to 2300 elements) are used.
* `/minion_java_examples`: These are Minion files taken from [Hof14]
and include various complex CSPs or logic puzzles such as *The farmers problem*, *The Zebra Puzzle* or the *N-Queens problem*.
* `/spreadsheet_examples`: These Minion files were generated automatically from spreadsheets. Hofer et al. [Hof+] utilized the files for fault localization in spreadsheets. The evaluation data of their experiments are located at the corresponding [Git repository](https://github.com/bhoferTU/Abstraction-Levels-for-Model-based-Software-Debugging).

The test framework utilizes these files for verification of the equivalency of the input and output. For this the workflow of the integration test framework distinguishes between to cases:

### Case UNSAT
1. The Minion code is run by the Minion solver. No solution can be found; the result is UNSAT.
2. The Minion code gets translated by the Min2Smt compiler.
3. The resulting Smt-Lib2 file is run by the Z3 solver. The result must  also be UNSAT.

### Case SAT
1. The Minion code is run by the Minion solver, using the `-findallsols -sollimit 5` flags. If these flags are set, Minion tries to find as much solutions as possible, but at most 5.
2. The integration test interface parses the Minion output.
3. The Minion code gets translated by the Min2Smt compiler. As a by-product, the compiler passes the symbol table of variables to the test interface.
4. After merging the solutions from step 2 with the variable meta-information from step 3, the test interface injects the resulting data into the SMT-LIB2 code.
5. The resulting SMT-LIB2 file is run by the Z3 solver. The result must also be SAT.
6. The process outlined in steps 4 and 5 is repeated for each resulting
solution.

For the test framework to provide a correct output, the entire **\*\*SEARCH**** section must be removed from the Minion code. Otherwise, automatically mapping the variables with the corresponding values would not work, as this section defines which variables have to be output and in which order.

## Execution
The compiler can be executed by typing `min2smt <filename>.minion <args*>`.

The following arguments are supported:
* **print**: Prints the generated code to std::out
* **print-minion**: Prints the source minion code next to the converted code as comment
* **debug**: Adds a section to the output for printing the model
* **optimize**: Removes unused variables from SMT-LIB2 output
* **help**: Prints compiler version and usage information

## Building an executable
The whole compiler was developed using Python and ANTLR. After creating a virtual environment and installing the necessary libraries defined in `requirements.txt`, the Python code can be executed. However, there also exist two scripts that can be used for building a standalone executable including all dependencies:
* `/scripts/build.sh`: Build script for Linux and Windows
* `/scripts/build_mac.sh`: Build script for MacOS

## Acknowledgement
This compiler as well as the Thesis were written with knowledge gained mainly during the following lectures:
* **Compiler Construction** (Franz Wotawa, Birgit Hofer / Roxane Koitz-Hristov)
* **Software Maintenance** (Franz Wotawa, Birgit Hofer / Roxane Koitz-Hristov)
* **Selected Topics Softwaretechnology 3** (Birgit Hofer)

I would like to thank my thesis advisor Univ.-Prof. Dipl.-Ing. Dr.techn. Franz Wotawa. He inspired my interest in this topic a few years ago with the Compiler Construction lecture. That is why I chose him as my supervisor. The meetings with him were always productive and informative. He provided me with optimal support and important tips in the preparation, practical implementation, and, finally, for the written work.

## References
[Jef+14] **Christopher Jefferson et al.** *The Minion Manual*. July 2014

[Hof14] **Birgit Hofer**. *“Spectrum-Based Fault Localization for Spreadsheets: Influence of Correct Output Cells on the Fault Localization Quality.”* In: 2014 IEEE International Symposium on Software Reliability Engineering Workshops. 2014, pp. 263–268. doi: 10.1109/ISSREW.2014.100

[Hof+] **Birgit Hofer et al.** “Choosing Abstraction Levels for Model-based Software Debugging: A Theoretical and Empirical Analysis for Spreadsheet Programs.” unpublished.