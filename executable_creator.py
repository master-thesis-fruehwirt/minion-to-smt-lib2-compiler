import PyInstaller.__main__
import os

from src.constants import CompilerConstants

DIST_FOLDER = "distribution"


def create_executable(main_file, name):
    PyInstaller.__main__.run([
        '--name=%s' % name,
        '--onefile',
        '--noconsole',
        '--distpath=%s' % os.path.join(DIST_FOLDER),
        os.path.join(main_file),
    ], )


if __name__ == '__main__':
    create_executable("main.py", CompilerConstants.COMPILER_NAME)
