import os


def main():
    dirs = [
        # "ISCAS85",
        # "minion_java_examples/MINIONDivATC",
        # "minion_java_examples/MINIONgcDATC",
        # "minion_java_examples/MINIONmultATC",
        # "minion_java_examples/MINIONmultV2ATC",
        # "minion_java_examples/MINIONRandomATC",
        # "minion_java_examples/MINIONsumATC",
        "spreadsheet_examples"
    ]

    for dir in dirs:
        files = [filename for filename in os.listdir(dir) if filename.endswith(".minion")]
        for filename in files:
            with open(f"{dir}/{filename}") as file:
                lines = file.readlines()
            search_index = -1
            constraints_index = -1

            for i in range(len(lines)):
                if "**SEARCH**" in lines[i]:
                    search_index = i
                if "**CONSTRAINTS**" in lines[i]:
                    constraints_index = i

            if search_index != -1:
                new_code = lines[:search_index] + lines[constraints_index:]
                with open(f"{dir}/{filename}", 'w') as file:
                    file.writelines(new_code)


if __name__ == '__main__':
    main()
