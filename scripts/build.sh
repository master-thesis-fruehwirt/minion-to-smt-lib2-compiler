python -m virtualenv venv
source ./venv/Scripts/activate
pip install -r requirements.txt

python -m nose --traverse-namespace --verbose testing/unit_tests/

python executable_creator.py

deactivate