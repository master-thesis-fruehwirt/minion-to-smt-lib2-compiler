python3 -m virtualenv venv
source venv/bin/activate
pip install -r requirements.txt

python -m nose --traverse-namespace --verbose testing/unit_tests/

python3 executable_creator.py

deactivate