import os

header = """from unittest import TestCase

from test_integration.interface.integrationtest_interface import IntegrationtestInterface


"""

setup = """
    def setUp(self) -> None:
        self._interface = IntegrationtestInterface()

"""


def get_filenames(path):
    l = os.listdir(path)
    files = [filename.split('.')[0] for filename in l if filename.endswith(".minion")]
    files.sort()
    return files


def main():
    # ------------------------------------------------------------------------------------------------------------------
    # ------------------------------------------------------------------------------------------------------------------
    # CHANGE THESE PARAMETERS
    # ------------------------------------------------------------------------------------------------------------------
    test_dirs = [
        ("ISCAS85", "test_files/testcases_advanced/ISCAS85"),
        ("ISCAS85_long", "test_files/testcases_advanced/ISCAS85/ISCAS85_long"),
        ("MINIONDivATC", "test_files/testcases_advanced/minion_java_examples/MINIONDivATC"),
        ("MINIONgcDATC", "test_files/testcases_advanced/minion_java_examples/MINIONgcDATC"),
        ("MINIONmultATC", "test_files/testcases_advanced/minion_java_examples/MINIONmultATC"),
        ("MINIONmultV2ATC", "test_files/testcases_advanced/minion_java_examples/MINIONmultV2ATC"),
        ("MINIONRandomATC", "test_files/testcases_advanced/minion_java_examples/MINIONRandomATC"),
        ("MINIONsumATC", "test_files/testcases_advanced/minion_java_examples/MINIONsumATC"),
        ("testcases_constraints", "test_files/testcases_constraints"),
        ("testcases_minion_doc", "test_files/testcases_minion_doc"),
        ("spreadsheet_examples", "test_files/testcases_advanced/spreadsheet_examples"),
    ]
    # ------------------------------------------------------------------------------------------------------------------

    num_files = 0

    for object_under_test, test_path in test_dirs:
        classname = f"Test{object_under_test}"
        file_name = f"test_{object_under_test}"
        test_file_path = "test_integration"

        files = get_filenames(test_path)
        num_files += len(files)
        string = f"class {classname}(TestCase):\n" + setup
        for file in files:
            str1 = f"    def test_{file.replace('-', '_')}(self):\n"
            str2 = f"        self.assertTrue(self._interface.run_file('{test_path}/{file}'))\n\n"
            string += str1 + str2

        with open(f"{test_file_path}/{file_name}.py", 'w') as file:
            file.writelines(header + string)

        print(f"{object_under_test}: {len(files)}")

    print("-" * 60)
    print(f"Generated {num_files} Unit/Integration Tests")


if __name__ == '__main__':
    main()
