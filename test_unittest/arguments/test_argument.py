
from unittest import TestCase

from src.arguments.argument import Argument
from src.arguments.const import Const
from src.arguments.list_access import ListAccess
from src.arguments.not_var import NotVar
from src.arguments.table import Table
from src.arguments.variable import Variable
from src.arguments.vector import Vector
from src.mapper.type import Type


class TestArgument(TestCase):

    def setUp(self) -> None:
        self._var = Variable("name", Type.TYPE_INT)
        self._la = ListAccess(Variable("name", Type.TYPE_INT), 1)
        self._const = Const(1)
        self._vec = Vector(Const(1), Const(2), Const(3))
        self._nv = NotVar(Variable("name"))
        self._table = Table("name", 3, 3, [])

    def test_get_type(self):
        arg = Argument()
        self.assertEqual(Type.TYPE_DEFAULT, arg.get_type())

        var = Variable("name", Type.TYPE_INT)
        self.assertEqual(Type.TYPE_INT, var.get_type())

    def test_is_boolean_var(self):
        var = Variable("name", Type.TYPE_BOOL)
        self.assertTrue(var.is_boolean_var())

        la = ListAccess(var, 1)
        self.assertTrue(la.is_boolean_var())

        self.assertFalse(self._var.is_boolean_var())
        self.assertFalse(self._vec.is_boolean_var())
        self.assertTrue(self._nv.is_boolean_var())
        self.assertFalse(self._table.is_boolean_var())

    def test_is_variable(self):
        self.assertTrue(self._var.is_variable())
        self.assertTrue(self._la.is_variable())
        self.assertFalse(self._const.is_variable())
        self.assertFalse(self._vec.is_variable())
        self.assertFalse(self._table.is_variable())
        self.assertTrue(self._nv.is_boolean_var())

    def test_is_const(self):
        self.assertFalse(self._var.is_const())
        self.assertFalse(self._la.is_const())
        self.assertFalse(self._vec.is_const())
        self.assertFalse(self._table.is_const())
        self.assertTrue(self._const.is_const())

    def test_is_vector(self):
        self.assertFalse(self._var.is_vector())
        self.assertFalse(self._la.is_vector())
        self.assertTrue(self._vec.is_vector())
        self.assertFalse(self._const.is_vector())
        self.assertFalse(self._table.is_vector())

    def test_is_variable_or_const(self):
        self.assertTrue(self._var.is_variable_or_const())
        self.assertTrue(self._la.is_variable_or_const())
        self.assertFalse(self._vec.is_variable_or_const())
        self.assertTrue(self._const.is_variable_or_const())
        self.assertFalse(self._table.is_variable_or_const())

    def test_is_variable_or_vector(self):
        self.assertTrue(self._var.is_variable_or_vector())
        self.assertTrue(self._la.is_variable_or_vector())
        self.assertTrue(self._vec.is_variable_or_vector())
        self.assertFalse(self._const.is_variable_or_vector())
        self.assertFalse(self._table.is_variable_or_vector())

    def test_is_table(self):
        self.assertTrue(self._table.is_table())
