from unittest import TestCase

from src.arguments.const import Const
from src.arguments.vector import Vector


class TestConst(TestCase):

    def test_repr(self):
        const = Const(1)
        self.assertEqual("1", const.__repr__())

    def test_get_val(self):
        const = Const(1)
        self.assertEqual(1, const.get_val())

    def test_to_smtlib2(self):
        const = Const(1)
        self.assertEqual("1", const.to_smtlib2())
