from unittest import TestCase

from src.arguments.const import Const
from src.arguments.vector import Vector


class TestVector(TestCase):

    def test_get_vec(self):
        vec = Vector(Const(1), Const(2), Const(3))
        self.assertEqual([str(v) for v in [1, 2, 3]], [v.to_smtlib2() for v in vec.get_vec()])

    def test_get_upper_bound(self):
        vec = Vector(Const(1), Const(2), Const(3))
        self.assertEqual(3, vec.get_upper_bound())
