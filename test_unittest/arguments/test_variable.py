from unittest import TestCase

from src.arguments.variable import Variable
from src.constants import Constants
from src.mapper.type import Type


class TestVariable(TestCase):

    def test_to_smtlib2(self):
        # Boolean variable
        var = Variable("name", Type.TYPE_BOOL)
        self.assertEqual("(declare-const name Bool)", var.declaration_to_smtlib2().to_repr())

        var = Variable("name", Type.TYPE_BOOL, [2])
        self.assertEqual("(declare-const name (Array Int Bool))", var.declaration_to_smtlib2().to_repr())

        var = Variable("name", Type.TYPE_BOOL, [2, 2])
        self.assertEqual("(declare-const name (Array Int Int Bool))", var.declaration_to_smtlib2().to_repr())

        # integer variable
        var = Variable("name", Type.TYPE_INT, [], [1, 5])
        self.assertEqual("(declare-const name Int)", var.declaration_to_smtlib2().to_repr())

        var = Variable("name", Type.TYPE_INT, [4], [1, 5])
        self.assertEqual("(declare-const name (Array Int Int))", var.declaration_to_smtlib2().to_repr())

        var = Variable("name", Type.TYPE_INT, [2, 2], [1, 5])
        self.assertEqual("(declare-const name (Array Int Int Int))", var.declaration_to_smtlib2().to_repr())

    def test_create_domain_constraint(self):
        var_declaration = Variable("var", [], [])
        self.assertEqual("", var_declaration.create_domain_constraint())

        var_declaration = Variable("var", dimensions=[3], domain=[1, 2])
        self.assertEqual(
            f"(forall (({Constants.COUNTER1} Int)) (=> (<= 0 {Constants.COUNTER1} 2) (<= 1 (select var {Constants.COUNTER1}) 2)))",
            var_declaration.create_domain_constraint().to_repr())

        var_declaration = Variable("var", domain=[1, 3])
        self.assertEqual("(<= 1 var 3)", var_declaration.create_domain_constraint().to_repr())

        var_declaration = Variable("var", domain=[1, 2], sparsebound=True)
        self.assertEqual("(or (= var 1) (= var 2))", var_declaration.create_domain_constraint().to_repr())

    def test_get_upper_bound(self):
        var_declaration = Variable("var", dimensions=[2])
        self.assertEqual(2, var_declaration.get_upper_bound())
