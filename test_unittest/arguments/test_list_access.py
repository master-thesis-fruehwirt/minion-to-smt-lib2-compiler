from unittest import TestCase

from src.arguments.list_access import ListAccess
from src.arguments.variable import Variable


class TestListAccess(TestCase):

    def test_to_smtlib2(self):
        la = ListAccess(Variable("var"), 1, 2)
        self.assertEqual("(select var 1 2)", la.to_smtlib2())

    def test_get_upper_bound(self):
        la = ListAccess(Variable("var"), 1, 2)
        self.assertEqual(0, la.get_upper_bound())

        la = ListAccess(Variable("var", dimensions=[5]), 1, 2)
        self.assertEqual(5, la.get_upper_bound())
