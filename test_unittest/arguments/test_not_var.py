from unittest import TestCase

from src.arguments.not_var import NotVar
from src.arguments.variable import Variable


class TestNotVar(TestCase):

    def test_repr(self):
        notvar = NotVar(Variable("a"))
        self.assertEqual("!a", notvar.__repr__())

    def test_to_smtlib2(self):
        notvar = NotVar(Variable("a"))
        self.assertEqual("(not a)", notvar.to_smtlib2().to_repr())
