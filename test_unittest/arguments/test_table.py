from unittest import TestCase

from src.arguments.table import Table


class TestTable(TestCase):

    def setUp(self) -> None:
        self._table = Table("name", 3, 4, [])

    def test_repr(self):
        self.assertEqual("name", self._table.__repr__())

    def test_get_name(self):
        self.assertEqual("name", self._table.get_name())

    def test_to_smtlib2(self):
        self.assertEqual("name", self._table.to_smtlib2())

    def test_get_rows(self):
        self.assertEqual(3, self._table.get_rows())

    def test_get_cols(self):
        self.assertEqual(4, self._table.get_cols())

    def test_get_dimensions(self):
        self.assertEqual([3, 4], self._table.get_dimensions())

    def test_declaration_to_smtlib2(self):
        self.assertEqual("(declare-const name (Array Int (Array Int Int)))",
                         self._table.declaration_to_smtlib2().to_repr())

    def test_get_table(self):
        l = [[1, 2], [3, 4]]
        table = Table("name", 2, 2, [[1, 2], [3, 4]])
        self.assertEqual(l, table.get_table())

    def test_to_debug_string(self):
        self.assertEqual("{table} name[3][4]", self._table.to_debug_string())

    def test_get_smtlib2_model(self):
        self.assertIsNone(self._table.get_smtlib2_model())
