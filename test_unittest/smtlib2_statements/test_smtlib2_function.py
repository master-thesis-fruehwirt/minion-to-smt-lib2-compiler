from unittest import TestCase

from src.smtlib2_statements.smtlib2_function import Abs, Add, And, Distinct, Div, Eq, Geq, Gt, Implies, Ite, Leq, Lt, \
    Mod, Mul, Not, Or, Pow, Select, Sub


class TestSmtlib2Function(TestCase):

    def test_abs(self):
        fun = Abs(1)
        self.assertEqual("(abs 1)", str(fun))

    def test_add(self):
        fun = Add(1, 2, 3)
        self.assertEqual("(+ 1 2 3)", str(fun))

    def test_and(self):
        fun = And(1, 2)
        self.assertEqual("(and 1 2)", str(fun))

    def test_distinct(self):
        fun = Distinct(1, 2, 3)
        self.assertEqual("(distinct 1 2 3)", str(fun))

    def test_div(self):
        fun = Div(1, 2)
        self.assertEqual("(div 1 2)", str(fun))

    def test_eq(self):
        fun = Eq(1, 2, 3)
        self.assertEqual("(= 1 2 3)", str(fun))

    def test_geq(self):
        fun = Geq(1, 2, 3)
        self.assertEqual("(>= 1 2 3)", str(fun))

    def test_gt(self):
        fun = Gt(1, 2, 3)
        self.assertEqual("(> 1 2 3)", str(fun))

    def test_implies(self):
        fun = Implies(1, 2, 3)
        self.assertEqual("(=> 1 2 3)", str(fun))

    def test_ite(self):
        fun = Ite(1, 2, 3)
        self.assertEqual("(ite 1 2 3)", str(fun))

    def test_leq(self):
        fun = Leq(1, 2, 3)
        self.assertEqual("(<= 1 2 3)", str(fun))

    def test_lt(self):
        fun = Lt(1, 2, 3)
        self.assertEqual("(< 1 2 3)", str(fun))

    def test_mod(self):
        fun = Mod(1, 2)
        self.assertEqual("(mod 1 2)", str(fun))

    def test_mul(self):
        fun = Mul(1, 2, 3)
        self.assertEqual("(* 1 2 3)", str(fun))

    def test_not(self):
        fun = Not(1)
        self.assertEqual("(not 1)", str(fun))

    def test_or(self):
        fun = Or(1, 2, 3)
        self.assertEqual("(or 1 2 3)", str(fun))

    def test_pow(self):
        fun = Pow(1, 2)
        self.assertEqual("(^ 1 2)", str(fun))

    def test_select(self):
        fun = Select("A", 1)
        self.assertEqual("(select A 1)", str(fun))

    def test_sub(self):
        fun = Sub(1, 2, 3)
        self.assertEqual("(- 1 2 3)", str(fun))
