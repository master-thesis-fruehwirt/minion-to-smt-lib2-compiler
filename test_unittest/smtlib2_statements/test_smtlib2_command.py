from unittest import TestCase

from src.constants import Constants
from src.smtlib2_statements.smtlib2_command import DeclareConst, DefineFunRec
from src.smtlib2_statements.smtlib2_function import Eq


class TestSmtlib2Command(TestCase):

    def test_declare_const(self):
        dc = DeclareConst("a", 0, "Int")
        self.assertEqual("(declare-const a Int)", str(dc))

        dc = DeclareConst("A", 1, "Int")
        self.assertEqual("(declare-const A (Array Int Int))", str(dc))

        dc = DeclareConst("A", 2, "Int")
        self.assertEqual("(declare-const A (Array Int Int Int))", str(dc))

        dc = DeclareConst("A", 3, "Int")
        self.assertEqual("(declare-const A (Array Int Int Int Int))", str(dc))

    def test_define_fun_rec(self):
        dfr = DefineFunRec("name", ["i Int", "j Int"], "Int", Eq("i", "j"))
        self.assertEqual("(define-fun-rec name ((i Int) (j Int)) Int (= i j))", str(dfr))
