from unittest import TestCase

from src.smtlib2_statements.smtlib2_statement import BoundVar, FunctionCall, Neg


class TestSmtlib2Statement(TestCase):

    def setUp(self) -> None:
        self._bv = BoundVar("A", "Int")
        self._fc = FunctionCall("sum", 1, 0)
        self._neg = Neg(1)

    def test_to_repr(self):
        self.assertEqual("(A Int)", self._bv.to_repr())
        self.assertEqual("(sum 1 0)", self._fc.to_repr())
        self.assertEqual("(- 1)", self._neg.to_repr())

    def test_to_code(self):
        self.assertEqual("(assert (A Int))", self._bv.to_code())
        self.assertEqual("(assert (sum 1 0))", self._fc.to_code())
        self.assertEqual("(- 1)", self._neg.to_code())

    def test_bound_var(self):
        self.assertEqual("(A Int)", str(self._bv))

    def test_function_call(self):
        self.assertEqual("(sum 1 0)", str(self._fc))

    def test_neg(self):
        self.assertEqual("(- 1)", str(self._neg))
