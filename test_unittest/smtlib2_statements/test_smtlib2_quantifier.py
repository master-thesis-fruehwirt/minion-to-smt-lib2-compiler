from unittest import TestCase

from src.smtlib2_statements.smtlib2_quantifier import Exists, Forall


class TestSmtlib2Quantifier(TestCase):

    def test_exists(self):
        exists = Exists("a", "b", "c")
        self.assertEqual("(exists (a b) c)", str(exists))
        self.assertEqual("(assert (exists (a b) c))", exists.to_code())

    def test_forall(self):
        forall = Forall("a", "b", "c")
        self.assertEqual("(forall (a b) c)", str(forall))
        self.assertEqual("(assert (forall (a b) c))", forall.to_code())
