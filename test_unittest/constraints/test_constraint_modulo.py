from unittest import TestCase

from src.arguments.const import Const
from src.arguments.variable import Variable
from src.constraints.constraint_modulo import Constraint_modulo


class TestConstraint_modulo(TestCase):
    def test_to_smtlib2(self):
        constraint = Constraint_modulo("", Const(1), Const(2), Const(3))
        self.assertEqual("(and (distinct 2 0) "
                         "(ite (or (and (> 1 0) (> 2 0)) (and (< 1 0) (> 2 0))) "
                         "(= (mod 1 2) 3) "
                         "(ite (and (> 1 0) (< 2 0)) "
                         "(= (mod (- 1) (- 2)) (- 3)) "
                         "(= (mod (- 1) 2) (- 3)))))", constraint.to_smtlib2().to_repr())
        self.assertTrue(constraint.is_valid())

        constraint = Constraint_modulo("", Variable("x"), Variable("y"), Variable("z"))
        self.assertEqual("(and (distinct y 0) "
                         "(ite (or (and (> x 0) (> y 0)) (and (< x 0) (> y 0))) "
                         "(= (mod x y) z) "
                         "(ite (and (> x 0) (< y 0)) "
                         "(= (mod (- x) (- y)) (- z)) "
                         "(= (mod (- x) y) (- z)))))", constraint.to_smtlib2().to_repr())
        self.assertTrue(constraint.is_valid())
