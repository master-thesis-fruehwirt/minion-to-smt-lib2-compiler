from unittest import TestCase

from src.arguments.const import Const
from src.arguments.variable import Variable
from src.constraints.constraint_w_literal import Constraint_w_literal


class TestConstraint_w_literal(TestCase):
    def test_to_smtlib2(self):
        constraint = Constraint_w_literal("", Variable("x"), Const(1))
        self.assertEqual("(= x 1)", constraint.to_smtlib2().to_repr())
        self.assertTrue(constraint.is_valid())

        constraint = Constraint_w_literal("", Const(1), Const(1))
        self.assertEqual("(= 1 1)", constraint.to_smtlib2().to_repr())
        self.assertTrue(constraint.is_valid())
