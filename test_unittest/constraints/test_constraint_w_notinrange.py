from unittest import TestCase

from src.arguments.const import Const
from src.arguments.variable import Variable
from src.arguments.vector import Vector
from src.constraints.constraint_w_notinrange import Constraint_w_notinrange


class TestConstraint_w_notinrange(TestCase):
    def test_to_smtlib2(self):
        constraint = Constraint_w_notinrange("", Const(2), Vector(Const(1), Const(3)))
        self.assertEqual("(or (< 2 1) (> 2 3))", constraint.to_smtlib2().to_repr())
        self.assertTrue(constraint.is_valid())

        constraint = Constraint_w_notinrange("", Const(1), Variable("A"))
        self.assertFalse(constraint.is_valid())
