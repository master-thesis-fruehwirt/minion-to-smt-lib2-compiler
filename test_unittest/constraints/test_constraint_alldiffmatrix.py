from unittest import TestCase

from src.arguments.const import Const
from src.arguments.variable import Variable
from src.constants import Constants
from src.constraints.constraint_alldiffmatrix import Constraint_alldiffmatrix
from src.globals import get_global_counter
from src.mapper.type import Type


class TestConstraint_alldiffmatrix(TestCase):
    def test_to_smtlib2(self):
        constraint = Constraint_alldiffmatrix("", Variable("A", Type.TYPE_INT, [3, 3]), Const(1))
        smtlib2_constraints = constraint.to_smtlib2()
        self.assertEqual(2, len(smtlib2_constraints))

        counter = get_global_counter() - 1
        function_name = f"fun_alldiffmatrix_{counter}"
        i = Constants.COUNTER1
        j = Constants.COUNTER2
        acc = Constants.HELPER_ACC

        self.assertEqual(f"(define-fun-rec {function_name} (({i} Int) ({j} Int) ({acc} Int)) Bool "
                         f"(ite (>= {j} 3) (= {acc} 1) "
                         f"(ite (= (select A {i} {j}) 1) "
                         f"(ite (= {acc} 1) "
                         f"false "
                         f"({function_name} {i} (+ {j} 1) 1)) "
                         f"({function_name} {i} (+ {j} 1) {acc}))))", smtlib2_constraints[0].to_repr())
        self.assertTrue(constraint.is_valid())
