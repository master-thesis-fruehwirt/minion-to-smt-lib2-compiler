from unittest import TestCase

from src.arguments.const import Const
from src.arguments.variable import Variable
from src.constraints.constraint_ineq import Constraint_ineq


class TestConstraint_ineq(TestCase):
    def test_to_smtlib2(self):
        constraint = Constraint_ineq("", Const(1), Const(2), Const(3))
        self.assertEqual("(<= 1 (+ 2 3))", constraint.to_smtlib2().to_repr())
        self.assertTrue(constraint.is_valid())

        constraint = Constraint_ineq("", Const(1), Const(2), Const(-1))
        self.assertEqual("(< 1 2)", constraint.to_smtlib2().to_repr())
        self.assertTrue(constraint.is_valid())

        constraint = Constraint_ineq("", Variable("x"), Variable("y"), Variable("z"))
        self.assertFalse(constraint.is_valid())

        constraint = Constraint_ineq("", Variable("x"), Variable("y"), Const(-1))
        self.assertEqual("(< x y)", constraint.to_smtlib2().to_repr())
        self.assertTrue(constraint.is_valid())
