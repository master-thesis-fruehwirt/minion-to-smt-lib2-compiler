from unittest import TestCase

from src.arguments.const import Const
from src.constraints.constraint_eq import Constraint_eq
from src.constraints.constraint_watched_or import Constraint_watched_or


class TestConstraint_watched_or(TestCase):
    def test_to_smtlib2(self):
        constraint = Constraint_watched_or("", Constraint_eq("", Const(1), Const(2)), Constraint_eq("", Const(2), Const(3)))
        smtlib2_result = constraint.to_smtlib2()
        self.assertIsInstance(smtlib2_result, list)
        self.assertEqual(1, len(smtlib2_result))
        self.assertEqual("(or (= 1 2) (= 2 3))", smtlib2_result[0].to_repr())
        self.assertTrue(constraint.is_valid())

        constraint = Constraint_watched_or("", Constraint_eq("", Const(1), Const(2)), Const(1))
        self.assertFalse(constraint.is_valid())
        