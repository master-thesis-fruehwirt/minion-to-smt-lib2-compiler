from unittest import TestCase

from src.arguments.const import Const
from src.arguments.variable import Variable
from src.arguments.vector import Vector
from src.constants import Constants
from src.constraints.constraint_occurrenceleq import Constraint_occurrenceleq
from src.globals import get_global_counter


class TestConstraint_occurrenceleq(TestCase):
    def test_to_smtlib2(self):
        # vec1 = vector
        constraint = Constraint_occurrenceleq("", Vector(Const(3), Const(1), Const(3)), Const(3), Const(2))
        self.assertEqual("(<= (+ (ite (= 3 3) 1 0) (ite (= 1 3) 1 0) (ite (= 3 3) 1 0)) 2)", constraint.to_smtlib2().to_repr())
        self.assertTrue(constraint.is_valid())

        # vec1 = variable
        constraint = Constraint_occurrenceleq("", Variable("A", dimensions=[4]), Const(3), Const(2))
        self.assertEqual("(<= 2 (+ (ite (= (select A 0) 3) 1 0) (ite (= (select A 1) 3) 1 0) (ite (= (select A 2) 3) 1 0) (ite (= (select A 3) 3) 1 0)))", constraint.to_smtlib2().to_repr())
        self.assertTrue(constraint.is_valid())
