from unittest import TestCase

from src.arguments.const import Const
from src.constraints.constraint_product import Constraint_product
from src.arguments.variable import Variable
from src.mapper.type import Type


class TestConstraint_product(TestCase):
    def test_to_smtlib2(self):
        constraint = Constraint_product("", Const(1), Const(2), Const(3))
        self.assertEqual("(= 3 (* 1 2))", constraint.to_smtlib2().to_repr())
        self.assertTrue(constraint.is_valid())

        constraint = Constraint_product("", Variable("x"), Variable("y"), Variable("z"))
        self.assertEqual("(= z (* x y))", constraint.to_smtlib2().to_repr())
        self.assertTrue(constraint.is_valid())

        constraint = Constraint_product("", Variable("x", Type.TYPE_BOOL), Variable("y"), Variable("z"))
        self.assertEqual("(= z (and x y))", constraint.to_smtlib2().to_repr())
        self.assertTrue(constraint.is_valid())

        constraint = Constraint_product("", Variable("x"), Variable("y", Type.TYPE_BOOL), Variable("z"))
        self.assertEqual("(= z (and x y))", constraint.to_smtlib2().to_repr())
        self.assertTrue(constraint.is_valid())

        constraint = Constraint_product("", Variable("x"), Variable("y"), Variable("z", Type.TYPE_BOOL))
        self.assertEqual("(= z (and x y))", constraint.to_smtlib2().to_repr())
        self.assertTrue(constraint.is_valid())
