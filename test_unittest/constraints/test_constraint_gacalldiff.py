from unittest import TestCase

from src.arguments.variable import Variable
from src.arguments.vector import Vector
from src.constraints.constraint_alldiff import Constraint_alldiff
from src.constraints.constraint_gacalldiff import Constraint_gacalldiff


class TestConstraint_gacalldiff(TestCase):
    def test_to_smtlib2(self):
        test_cases = [
            Variable("A", dimensions=[3]),
            Vector(Variable("A", dimensions=[3])),
            Vector(Variable("a"), Variable("b"), Variable("c"))
        ]

        for arg in test_cases:
            constraint_alldiff = Constraint_alldiff("", arg)
            constraint_gacalldiff = Constraint_gacalldiff("", arg)
            self.assertEqual(constraint_alldiff.to_smtlib2().to_repr(), constraint_gacalldiff.to_smtlib2().to_repr())
            self.assertTrue(constraint_gacalldiff.is_valid())

