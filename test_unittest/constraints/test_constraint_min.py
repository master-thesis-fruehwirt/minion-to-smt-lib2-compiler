from unittest import TestCase

from src.arguments.const import Const
from src.arguments.variable import Variable
from src.arguments.vector import Vector
from src.constants import Constants
from src.constraints.constraint_min import Constraint_min


class TestConstraint_min(TestCase):
    def test_to_smtlib2(self):
        constraint = Constraint_min("", Vector(Variable("a"), Variable("b"), Variable("c")), Const(5))
        self.assertEqual("(and (or (= a 5) (= b 5) (= c 5)) (<= 5 a) (<= 5 b) (<= 5 c))", constraint.to_smtlib2().to_repr())
        self.assertTrue(constraint.is_valid())

        constraint = Constraint_min("", Variable("A", dimensions=[3]), Const(0))
        self.assertEqual("(and (or (= 0 (select A 0)) (= 0 (select A 1)) (= 0 (select A 2))) (<= 0 (select A 0)) (<= 0 (select A 1)) (<= 0 (select A 2)))", constraint.to_smtlib2().to_repr())
        self.assertTrue(constraint.is_valid())
