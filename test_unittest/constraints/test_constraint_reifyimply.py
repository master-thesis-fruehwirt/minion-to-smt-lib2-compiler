from unittest import TestCase

from src.arguments.const import Const
from src.arguments.variable import Variable
from src.constraints.constraint_eq import Constraint_eq
from src.constraints.constraint_reifyimply import Constraint_reifyimply
from src.mapper.type import Type


class TestConstraint_reifyimply(TestCase):
    def test_to_smtlib2(self):
        constraint = Constraint_reifyimply("", Constraint_eq("", Variable("a"), Const(1)), Variable("b", Type.TYPE_BOOL))
        smtlib2_result = constraint.to_smtlib2()
        self.assertIsInstance(smtlib2_result, list)
        self.assertEqual(1, len(smtlib2_result))
        self.assertEqual("(=> b (= a 1))", smtlib2_result[0].to_repr())
        self.assertTrue(constraint.is_valid())

        constraint = Constraint_reifyimply("", Constraint_eq("", Variable("a"), Const(1)), Const(1))
        smtlib2_result = constraint.to_smtlib2()
        self.assertEqual("(=> true (= a 1))", smtlib2_result[0].to_repr())
        self.assertTrue(constraint.is_valid())

        constraint = Constraint_reifyimply("", Constraint_eq("", Variable("a"), Const(1)), Const(0))
        smtlib2_result = constraint.to_smtlib2()
        self.assertEqual("(=> false (= a 1))", smtlib2_result[0].to_repr())
        self.assertTrue(constraint.is_valid())
