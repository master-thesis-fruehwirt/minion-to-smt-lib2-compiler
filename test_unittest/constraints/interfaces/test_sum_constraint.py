from unittest import TestCase

from src.arguments.const import Const
from src.arguments.variable import Variable
from src.arguments.vector import Vector
from src.constants import Constants
from src.constraints.interfaces.sum_constraint import SumConstraint
from src.globals import get_global_counter


class TestSumConstraint(TestCase):

    def test_to_smtlib2(self):
        constraint = SumConstraint("", ">=", Vector(Const(1), Const(2), Const(3)), Const(6))
        self.assertEqual("(>= (+ 1 2 3) 6)", constraint.to_smtlib2().to_repr())

        constraint = SumConstraint("", "<=", Vector(Const(1), Const(2), Const(3)), Const(6))
        self.assertEqual("(<= (+ 1 2 3) 6)", constraint.to_smtlib2().to_repr())

        constraint = SumConstraint("",  ">=", Vector(Variable("a"), Const(2), Const(3)), Variable("b"))
        self.assertEqual("(>= (+ a 2 3) b)", constraint.to_smtlib2().to_repr())

        constraint = SumConstraint("", ">=", Variable("A", dimensions=[5]), Const(5))
        self.assertEqual("(>= (+ (select A 0) (select A 1) (select A 2) (select A 3) (select A 4)) 5)", constraint.to_smtlib2().to_repr())
