from unittest import TestCase

from src.constraints.interfaces.constraint import Constraint
from src.errors import error_list


class TestConstraint(TestCase):
    def test_to_smtlib2(self):
        constraint = Constraint("constraint", 1, 2, 3)
        self.assertEqual("constraint(1, 2, 3)", constraint.to_smtlib2())
        self.assertEqual(1, len(error_list))
