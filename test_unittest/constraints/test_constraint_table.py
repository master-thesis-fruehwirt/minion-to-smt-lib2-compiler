from unittest import TestCase

from src.arguments.const import Const
from src.arguments.table import Table
from src.arguments.variable import Variable
from src.arguments.vector import Vector
from src.constraints.constraint_table import Constraint_table
from src.mapper.type import Type


class TestConstraint_table(TestCase):
    def test_to_smtlib2(self):
        table = Table("table", 2, 2, [[1, 2], [3, 4]])

        constraint = Constraint_table("", Variable("var", Type.TYPE_INT, dimensions=[2], domain=[0, 5]), table)
        self.assertEqual(f"(or (and (= 1 (select var 0)) (= 2 (select var 1))) (and (= 3 (select var 0)) (= 4 (select var 1))))", constraint.to_smtlib2().to_repr())
        self.assertTrue(constraint.is_valid())

        constraint = Constraint_table("", Vector(Const(1), Const(2)), table)
        self.assertEqual(f"(or (and (= 1 1) (= 2 2)) (and (= 3 1) (= 4 2)))", constraint.to_smtlib2().to_repr())
        self.assertTrue(constraint.is_valid())

        constraint = Constraint_table("", Vector(Variable("a", Type.TYPE_BOOL), Const(2)), table)
        self.assertEqual(f"(or (and (= 1 (ite a 1 0)) (= 2 2)) (and (= 3 (ite a 1 0)) (= 4 2)))", constraint.to_smtlib2().to_repr())
        self.assertTrue(constraint.is_valid())
