from unittest import TestCase

from src.arguments.const import Const
from src.arguments.variable import Variable
from src.constraints.constraint_difference import Constraint_difference


class TestConstraint_difference(TestCase):
    def test_to_smtlib2(self):
        constraint = Constraint_difference("", Const(1), Const(2), Const(3))
        self.assertEqual("(= (abs (- 1 2)) 3)", constraint.to_smtlib2().to_repr())
        self.assertTrue(constraint.is_valid())

        constraint = Constraint_difference("", Variable("x"), Variable("y"), Variable("z"))
        self.assertEqual("(= (abs (- x y)) z)", constraint.to_smtlib2().to_repr())
        self.assertTrue(constraint.is_valid())

