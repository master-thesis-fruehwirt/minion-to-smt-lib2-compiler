from unittest import TestCase

from src.arguments.const import Const
from src.arguments.variable import Variable
from src.arguments.vector import Vector
from src.constraints.constraint_w_inintervalset import Constraint_w_inintervalset


class TestConstraint_w_inintervalset(TestCase):
    def test_to_smtlib2(self):
        constraint = Constraint_w_inintervalset("", Const(4), Vector(Const(1), Const(5), Const(7), Const(9)))
        self.assertEqual("(or (<= 1 4 5) (<= 7 4 9))", constraint.to_smtlib2().to_repr())
        self.assertTrue(constraint.is_valid())

        constraint = Constraint_w_inintervalset("", Variable("a"), Vector(Const(1), Const(5), Const(7), Const(9)))
        self.assertEqual("(or (<= 1 a 5) (<= 7 a 9))", constraint.to_smtlib2().to_repr())
        self.assertTrue(constraint.is_valid())

        constraint = Constraint_w_inintervalset("", Variable("a"), Variable("A", dimensions=[5]))
        self.assertFalse(constraint.is_valid())
