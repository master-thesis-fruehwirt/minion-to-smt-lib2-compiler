from unittest import TestCase

from src.arguments.const import Const
from src.arguments.variable import Variable
from src.constraints.constraint_minuseq import Constraint_minuseq


class TestConstraint_minuseq(TestCase):
    def test_to_smtlib2(self):
        constraint = Constraint_minuseq("", Const(1), Const(2))
        self.assertEqual("(= 1 (- 2))", constraint.to_smtlib2().to_repr())
        self.assertTrue(constraint.is_valid())

        constraint = Constraint_minuseq("", Variable("x"), Variable("y"))
        self.assertEqual("(= x (- y))", constraint.to_smtlib2().to_repr())
        self.assertTrue(constraint.is_valid())
