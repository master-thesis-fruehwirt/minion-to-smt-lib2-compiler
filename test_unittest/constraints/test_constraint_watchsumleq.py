from unittest import TestCase

from src.constraints.constraint_watchsumleq import Constraint_watchsumleq
from src.constraints.interfaces.sum_constraint import SumConstraint


class TestConstraint_watchsumleq(TestCase):
    def test_constructor(self):
        constraint = Constraint_watchsumleq("")
        self.assertIsInstance(constraint, SumConstraint)
