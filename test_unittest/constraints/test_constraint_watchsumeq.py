from unittest import TestCase

from src.constraints.custom_constraint_watchsumeq import Constraint_watchsumeq
from src.constraints.interfaces.sum_constraint import SumConstraint


class TestConstraint_watchsumeq(TestCase):
    def test_constructor(self):
        constraint = Constraint_watchsumeq("")
        self.assertIsInstance(constraint, SumConstraint)
