from unittest import TestCase

from src.arguments.const import Const
from src.arguments.variable import Variable
from src.constraints.constraint_diseq import Constraint_diseq


class TestConstraint_diseq(TestCase):
    def test_to_smtlib2(self):
        constraint = Constraint_diseq("", Const(1), Const(2))
        self.assertEqual("(distinct 1 2)", constraint.to_smtlib2().to_repr())
        self.assertTrue(constraint.is_valid())

        constraint = Constraint_diseq("", Variable("x"), Variable("y"))
        self.assertEqual("(distinct x y)", constraint.to_smtlib2().to_repr())
        self.assertTrue(constraint.is_valid())
