from unittest import TestCase

from src.arguments.const import Const
from src.arguments.variable import Variable
from src.arguments.vector import Vector
from src.constraints.constraint_w_notinset import Constraint_w_notinset


class TestConstraint_w_notinset(TestCase):
    def test_to_smtlib2(self):
        constraint = Constraint_w_notinset("", Const(1), Vector(Const(1), Const(3)))
        self.assertEqual("(and (distinct 1 1) (distinct 1 3))", constraint.to_smtlib2().to_repr())
        self.assertTrue(constraint.is_valid())

        constraint = Constraint_w_notinset("", Const(1), Variable("A", dimensions=[5]))
        self.assertFalse(constraint.is_valid())
