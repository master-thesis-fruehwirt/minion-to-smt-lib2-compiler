from unittest import TestCase

from src.constraints.custom_constraint_sumeq import Constraint_sumeq
from src.constraints.interfaces.sum_constraint import SumConstraint


class TestConstraint_sumeq(TestCase):
    def test_constructor(self):
        constraint = Constraint_sumeq("")
        self.assertIsInstance(constraint, SumConstraint)
