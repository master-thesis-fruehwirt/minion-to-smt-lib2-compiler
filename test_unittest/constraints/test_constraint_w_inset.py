from unittest import TestCase

from src.arguments.const import Const
from src.arguments.variable import Variable
from src.arguments.vector import Vector
from src.constraints.constraint_w_inset import Constraint_w_inset


class TestConstraint_w_inset(TestCase):
    def test_to_smtlib2(self):
        constraint = Constraint_w_inset("", Const(1), Vector(Const(1), Const(3)))
        self.assertEqual("(or (= 1 1) (= 1 3))", constraint.to_smtlib2().to_repr())
        self.assertTrue(constraint.is_valid())

        constraint = Constraint_w_inset("", Const(5), Variable("A"))
        self.assertFalse(constraint.is_valid())
