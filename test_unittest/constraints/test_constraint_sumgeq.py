from unittest import TestCase

from src.constraints.constraint_sumgeq import Constraint_sumgeq
from src.constraints.interfaces.sum_constraint import SumConstraint


class TestConstraint_sumgeq(TestCase):
    def test_constructor(self):
        constraint = Constraint_sumgeq("")
        self.assertIsInstance(constraint, SumConstraint)
