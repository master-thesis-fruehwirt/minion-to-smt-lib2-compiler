from unittest import TestCase

from src.arguments.const import Const
from src.arguments.variable import Variable
from src.arguments.vector import Vector
from src.constraints.constraint_watchvecneq import Constraint_watchvecneq


class TestConstraint_watchvecneq(TestCase):
    def test_to_smtlib2(self):
        # vec1 = vector, vec2 = vector
        constraint = Constraint_watchvecneq("",
                                            Vector(Const(1), Const(2), Const(3)), Vector(Const(1), Const(2), Const(3)))
        self.assertEqual("(or (distinct 1 1) (distinct 2 2) (distinct 3 3))", constraint.to_smtlib2().to_repr())
        self.assertTrue(constraint.is_valid())

        # vec1 = vector, vec2 = vector
        constraint = Constraint_watchvecneq("", Vector(Variable("a"), Variable("b")),
                                                 Vector(Variable("b"), Variable("c")))
        self.assertEqual("(or (distinct a b) (distinct b c))", constraint.to_smtlib2().to_repr())
        self.assertTrue(constraint.is_valid())

        # vec1 = variable, vec2 = variable
        constraint = Constraint_watchvecneq("", Variable("A", dimensions=[3]), Variable("B"))
        self.assertEqual("(or (distinct (select A 0) (select B 0)) (distinct (select A 1) (select B 1)) (distinct (select A 2) (select B 2)))",
            constraint.to_smtlib2().to_repr())
        self.assertTrue(constraint.is_valid())

        # vec1 = vector, vec2 = variable
        constraint = Constraint_watchvecneq("", Vector(Const(1), Const(2), Const(3)), Variable("B"))
        self.assertEqual("(or (distinct 1 (select B 0)) (distinct 2 (select B 1)) (distinct 3 (select B 2)))", constraint.to_smtlib2().to_repr())
        self.assertTrue(constraint.is_valid())

        # vec1 = variable, vec2 = vector
        constraint = Constraint_watchvecneq("", Variable("A"), Vector(Const(1), Const(2), Const(3)))
        self.assertEqual("(or (distinct (select A 0) 1) (distinct (select A 1) 2) (distinct (select A 2) 3))", constraint.to_smtlib2().to_repr())
        self.assertTrue(constraint.is_valid())
