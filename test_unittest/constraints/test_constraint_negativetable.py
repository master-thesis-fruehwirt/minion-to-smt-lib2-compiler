from unittest import TestCase

from src.arguments.const import Const
from src.arguments.table import Table
from src.arguments.variable import Variable
from src.arguments.vector import Vector
from src.constraints.constraint_negativetable import Constraint_negativetable
from src.mapper.type import Type


class TestConstraint_negativetable(TestCase):
    def test_to_smtlib2(self):
        table = Table("table", 2, 2, [[1, 2], [3, 4]])

        constraint = Constraint_negativetable("", Variable("var", Type.TYPE_INT, dimensions=[2], domain=[0, 5]), table)
        self.assertEqual(f"(and (or (distinct 1 (select var 0)) (distinct 2 (select var 1))) (or (distinct 3 (select var 0)) (distinct 4 (select var 1))))", constraint.to_smtlib2().to_repr())
        self.assertTrue(constraint.is_valid())

        constraint = Constraint_negativetable("", Vector(Const(1), Const(2)), table)
        self.assertEqual(f"(and (or (distinct 1 1) (distinct 2 2)) (or (distinct 3 1) (distinct 4 2)))", constraint.to_smtlib2().to_repr())
        self.assertTrue(constraint.is_valid())

        constraint = Constraint_negativetable("", Vector(Variable("a", Type.TYPE_BOOL), Const(2)), table)
        self.assertEqual(f"(and (or (distinct 1 (ite a 1 0)) (distinct 2 2)) (or (distinct 3 (ite a 1 0)) (distinct 4 2)))", constraint.to_smtlib2().to_repr())
        self.assertTrue(constraint.is_valid())
