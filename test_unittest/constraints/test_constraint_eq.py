from unittest import TestCase

from src.arguments.const import Const
from src.constraints.constraint_eq import Constraint_eq
from src.arguments.variable import Variable
from src.mapper.type import Type


class TestConstraint_eq(TestCase):
    def test_to_smtlib2(self):
        constraint = Constraint_eq("", Const(1), Const(2))
        self.assertEqual("(= 1 2)", constraint.to_smtlib2().to_repr())
        self.assertTrue(constraint.is_valid())

        constraint = Constraint_eq("", Variable("x"), Variable("y"))
        self.assertEqual("(= x y)", constraint.to_smtlib2().to_repr())
        self.assertTrue(constraint.is_valid())

        constraint = Constraint_eq("", Variable("x", Type.TYPE_BOOL), Const(1))
        self.assertEqual("(= x true)", constraint.to_smtlib2().to_repr())
        self.assertTrue(constraint.is_valid())

        constraint = Constraint_eq("", Variable("x", Type.TYPE_BOOL), Const(0))
        self.assertEqual("(= x false)", constraint.to_smtlib2().to_repr())
        self.assertTrue(constraint.is_valid())

        constraint = Constraint_eq("", Const(1), Variable("y", Type.TYPE_BOOL))
        self.assertEqual("(= y true)", constraint.to_smtlib2().to_repr())
        self.assertTrue(constraint.is_valid())

        constraint = Constraint_eq("", Const(0), Variable("y", Type.TYPE_BOOL))
        self.assertEqual("(= y false)", constraint.to_smtlib2().to_repr())
        self.assertTrue(constraint.is_valid())
