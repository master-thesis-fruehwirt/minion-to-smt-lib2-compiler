from unittest import TestCase

from src.arguments.const import Const
from src.arguments.variable import Variable
from src.arguments.vector import Vector
from src.constraints.constraint_lexless import Constraint_lexless
from src.globals import get_global_counter


class TestConstraint_lexless(TestCase):
    def test_to_smtlib2(self):
        # vec1 = vector, vec2 = vector
        constraint = Constraint_lexless("", Vector(Const(1), Const(2), Const(3)), Vector(Const(1), Const(2), Const(3)))
        constraints = constraint.to_smtlib2()
        self.assertEqual(10, len(constraints))
        self.assertTrue(constraint.is_valid())

        # vec1 = vector, vec2 = vector
        constraint = Constraint_lexless("", Vector(Variable("a"), Variable("b")), Vector(Variable("b"), Variable("c")))
        constraints = constraint.to_smtlib2()
        self.assertEqual(8, len(constraints))
        self.assertTrue(constraint.is_valid())

        # vec1 = vector, vec2 = variable
        constraint = Constraint_lexless("", Vector(Const(1), Const(2), Const(3)), Variable("B"))
        constraints = constraint.to_smtlib2()
        self.assertEqual(6, len(constraints))
        self.assertTrue(constraint.is_valid())

        # vec1 = variable, vec2 = vector
        constraint = Constraint_lexless("", Variable("A"), Vector(Const(1), Const(2), Const(3)))
        constraints = constraint.to_smtlib2()
        self.assertEqual(6, len(constraints))
        self.assertTrue(constraint.is_valid())

        # vec1 = variable, vec2 = variable
        constraint = Constraint_lexless("", Variable("A", dimensions=[5]), Variable("B"))
        constraints = constraint.to_smtlib2()
        self.assertEqual(2, len(constraints))
        self.assertTrue(constraint.is_valid())

        counter = get_global_counter() - 1
        function_name = f"fun_lexless_{counter}"

        self.assertEqual(
            f"(define-fun-rec {function_name} ((VAR1 Int)) Bool (ite (>= VAR1 5) false (ite (> (select A VAR1) (select B VAR1)) false (ite (< (select A VAR1) (select B VAR1)) true ({function_name} (+ VAR1 1))))))",
            constraints[0].to_repr())
