from unittest import TestCase

from src.arguments.const import Const
from src.arguments.variable import Variable
from src.arguments.vector import Vector
from src.constants import Constants
from src.constraints.constraint_max import Constraint_max


class TestConstraint_max(TestCase):
    def test_to_smtlib2(self):
        constraint = Constraint_max("", Vector(Variable("a"), Variable("b"), Variable("c")), Const(5))
        self.assertEqual("(and (or (= a 5) (= b 5) (= c 5)) (>= 5 a) (>= 5 b) (>= 5 c))", constraint.to_smtlib2().to_repr())
        self.assertTrue(constraint.is_valid())

        constraint = Constraint_max("", Variable("A", dimensions=[3]), Const(5))
        self.assertEqual("(and (or (= 5 (select A 0)) (= 5 (select A 1)) (= 5 (select A 2))) (>= 5 (select A 0)) (>= 5 (select A 1)) (>= 5 (select A 2)))", constraint.to_smtlib2().to_repr())
        self.assertTrue(constraint.is_valid())
