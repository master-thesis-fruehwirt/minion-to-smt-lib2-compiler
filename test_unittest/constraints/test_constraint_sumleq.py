from unittest import TestCase

from src.constraints.constraint_sumleq import Constraint_sumleq
from src.constraints.interfaces.sum_constraint import SumConstraint


class TestConstraint_sumleq(TestCase):
    def test_constructor(self):
        constraint = Constraint_sumleq("")
        self.assertIsInstance(constraint, SumConstraint)
