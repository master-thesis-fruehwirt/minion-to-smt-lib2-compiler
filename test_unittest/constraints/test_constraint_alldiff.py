from unittest import TestCase

from src.arguments.list_access import ListAccess
from src.arguments.variable import Variable
from src.arguments.vector import Vector
from src.constants import Constants
from src.constraints.constraint_alldiff import Constraint_alldiff


class TestConstraint_alldiff(TestCase):
    def test_to_smtlib2(self):
        constraint = Constraint_alldiff("", Variable("A", dimensions=[3]))
        self.assertEqual(
            f"(forall (({Constants.COUNTER1} Int) ({Constants.COUNTER2} Int)) "
            f"(=> (and (< 0 {Constants.COUNTER1} 3) (< 0 {Constants.COUNTER2} 3) (= (select A {Constants.COUNTER1}) (select A {Constants.COUNTER2}))) (= {Constants.COUNTER1} {Constants.COUNTER2})))",
            constraint.to_smtlib2().to_repr())
        self.assertTrue(constraint.is_valid())

        constraint = Constraint_alldiff("", Vector(Variable("A", dimensions=[3])))
        self.assertEqual(
           f"(forall (({Constants.COUNTER1} Int) ({Constants.COUNTER2} Int)) (=> (and (< 0 {Constants.COUNTER1} 3) (< 0 {Constants.COUNTER2} 3) (= (select A {Constants.COUNTER1}) (select A {Constants.COUNTER2}))) (= {Constants.COUNTER1} {Constants.COUNTER2})))",
            constraint.to_smtlib2().to_repr())
        self.assertTrue(constraint.is_valid())

        constraint = Constraint_alldiff("", Vector(Variable("a"), Variable("b"), Variable("c")))
        self.assertEqual("(distinct a b c)", constraint.to_smtlib2().to_repr())
        self.assertTrue(constraint.is_valid())

        constraint = Constraint_alldiff("", Vector(ListAccess(Variable("a"), 0), Variable("b"), Variable("c")))
        self.assertEqual("(distinct (select a 0) b c)", constraint.to_smtlib2().to_repr())
        self.assertTrue(constraint.is_valid())


    def test__build_array_string(self):
        constraint = Constraint_alldiff("", Vector(Variable("A", dimensions=[3])))
        self.assertEqual(
            f"(forall (({Constants.COUNTER1} Int) ({Constants.COUNTER2} Int)) (=> (and (< 0 {Constants.COUNTER1} 3) (< 0 {Constants.COUNTER2} 3) (= (select A {Constants.COUNTER1}) (select A {Constants.COUNTER2}))) (= {Constants.COUNTER1} {Constants.COUNTER2})))",
            str(constraint._build_array_string(3, "A")))
