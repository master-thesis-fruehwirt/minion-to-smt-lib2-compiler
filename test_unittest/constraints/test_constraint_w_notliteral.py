from unittest import TestCase

from src.arguments.const import Const
from src.arguments.variable import Variable
from src.constraints.constraint_w_notliteral import Constraint_w_notliteral


class TestConstraint_w_notliteral(TestCase):
    def test_to_smtlib2(self):
        constraint = Constraint_w_notliteral("", Variable("x"), Const(1))
        self.assertEqual("(distinct x 1)", constraint.to_smtlib2().to_repr())
        self.assertTrue(constraint.is_valid())

        constraint = Constraint_w_notliteral("", Const(1), Const(1))
        self.assertEqual("(distinct 1 1)", constraint.to_smtlib2().to_repr())
        self.assertTrue(constraint.is_valid())
