from unittest import TestCase

from src.arguments.const import Const
from src.arguments.variable import Variable
from src.arguments.vector import Vector
from src.constraints.constraint_watchelement_undefzero import Constraint_watchelement_undefzero
from src.mapper.type import Type


class TestConstraint_watchelement_undefzero(TestCase):
    def test_to_smtlib2(self):
        # vec = variable, i = constant, e = constant
        constraint = Constraint_watchelement_undefzero("", Variable("vec", dimensions=[5]), Const(1), Const(2))
        self.assertEqual("(or (= (select vec 1) 2) (and (>= 1 5) (= 2 0)))", constraint.to_smtlib2().to_repr())
        self.assertTrue(constraint.is_valid())

        # vec = boolean variable, e = 1
        constraint = Constraint_watchelement_undefzero("", Variable("vec", Type.TYPE_BOOL, dimensions=[5]), Const(1), Const(1))
        self.assertEqual("(or (select vec 1) (and (>= 1 5) (= 1 0)))", constraint.to_smtlib2().to_repr())
        self.assertTrue(constraint.is_valid())

        # vec = boolean variable, e = 0
        constraint = Constraint_watchelement_undefzero("", Variable("vec", Type.TYPE_BOOL, dimensions=[5]), Const(1), Const(0))
        self.assertEqual("(or (not (select vec 1)) (and (>= 1 5) (= 0 0)))", constraint.to_smtlib2().to_repr())
        self.assertTrue(constraint.is_valid())

        # vec = boolean variable, e = variable
        constraint = Constraint_watchelement_undefzero("", Variable("vec", Type.TYPE_BOOL, dimensions=[5]), Const(1), Variable("e"))
        self.assertEqual("(or (= (select vec 1) e) (and (>= 1 5) (= e 0)))", constraint.to_smtlib2().to_repr())
        self.assertTrue(constraint.is_valid())

        # vec = vector, i > len
        constraint = Constraint_watchelement_undefzero("", Vector(Const(1), Const(2), Const(3)), Const(4), Variable("e"))
        self.assertEqual("(and (>= 4 3) (= e 0))", constraint.to_smtlib2().to_repr())
        self.assertTrue(constraint.is_valid())

        # vec = vector, i <= len
        constraint = Constraint_watchelement_undefzero("", Vector(Const(1), Const(2), Const(3)), Const(2), Variable("e"))
        self.assertEqual("(or (= 3 e) (and (>= 2 3) (= e 0)))", constraint.to_smtlib2().to_repr())
        self.assertTrue(constraint.is_valid())

        # vec = vector, i = variable, e = variable
        constraint = Constraint_watchelement_undefzero("", Vector(Const(1), Const(2), Const(3)), Variable("i"), Variable("e"))
        self.assertEqual("(or (= 1 e) (= 2 e) (= 3 e) (and (>= i 3) (= e 0)))", constraint.to_smtlib2().to_repr())
        self.assertTrue(constraint.is_valid())
