from unittest import TestCase

from src.arguments.const import Const
from src.arguments.list_access import ListAccess
from src.arguments.variable import Variable
from src.arguments.vector import Vector
from src.constraints.custom_constraint_weightedsumeq import Constraint_weightedsumeq


class TestConstraint_weightedsumeq(TestCase):
    def test_to_smtlib2(self):
        # vec2 = vector
        constraint = Constraint_weightedsumeq("", Vector(Const(1), Const(2)), Vector(Const(3), Const(4)), Const(5))
        self.assertEqual("(= (+ (* 1 3) (* 2 4)) 5)", constraint.to_smtlib2().to_repr())
        self.assertTrue(constraint.is_valid())

        # vec2 = variable
        constraint = Constraint_weightedsumeq("", Vector(Const(1), Const(2)), Variable("A"), Const(5))
        self.assertEqual("(= (+ (* 1 (select A 0)) (* 2 (select A 1))) 5)", constraint.to_smtlib2().to_repr())
        self.assertTrue(constraint.is_valid())

        constraint = Constraint_weightedsumeq("", Vector(Const(1), Const(2)), ListAccess(Variable("A"), 1), Const(5))
        self.assertEqual("(= (+ (* 1 (select (select A 1) 0)) (* 2 (select (select A 1) 1))) 5)", constraint.to_smtlib2().to_repr())
        self.assertTrue(constraint.is_valid())
