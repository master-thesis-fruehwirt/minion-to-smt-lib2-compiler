from unittest import TestCase

from src.arguments.const import Const
from src.arguments.variable import Variable
from src.constraints.constraint_abs import Constraint_abs


class TestConstraint_abs(TestCase):
    def test_to_smtlib2(self):
        constraint = Constraint_abs("", Const(1), Const(2))
        self.assertEqual("(= 1 (abs 2))", constraint.to_smtlib2().to_repr())
        self.assertTrue(constraint.is_valid())

        constraint = Constraint_abs("", Variable("x"), Variable("y"))
        self.assertEqual("(= x (abs y))", constraint.to_smtlib2().to_repr())
        self.assertTrue(constraint.is_valid())
