from unittest import TestCase

from src.arguments.const import Const
from src.arguments.variable import Variable
from src.constraints.constraint_watchless import Constraint_watchless


class TestConstraint_watchless(TestCase):
    def test_to_smtlib2(self):
        constraint = Constraint_watchless("", Const(4), Const(5))
        self.assertEqual("(< 4 5)", constraint.to_smtlib2().to_repr())
        self.assertTrue(constraint.is_valid())

        constraint = Constraint_watchless("", Variable("a"), Const(5))
        self.assertEqual("(< a 5)", constraint.to_smtlib2().to_repr())
        self.assertTrue(constraint.is_valid())

        constraint = Constraint_watchless("", Variable("a"), Variable("b"))
        self.assertEqual("(< a b)", constraint.to_smtlib2().to_repr())
        self.assertTrue(constraint.is_valid())
