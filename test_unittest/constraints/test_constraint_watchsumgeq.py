from unittest import TestCase

from src.constraints.constraint_watchsumgeq import Constraint_watchsumgeq
from src.constraints.interfaces.sum_constraint import SumConstraint


class TestConstraint_watchsumgeq(TestCase):
    def test_constructor(self):
        constraint = Constraint_watchsumgeq("")
        self.assertIsInstance(constraint, SumConstraint)
