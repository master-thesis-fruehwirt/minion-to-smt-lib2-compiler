from unittest import TestCase

from src.arguments.const import Const
from src.arguments.vector import Vector
from src.constraints.constraint_element import Constraint_element
from src.arguments.variable import Variable
from src.mapper.type import Type


class TestConstraint_element(TestCase):
    def test_to_smtlib2(self):
        # vec = variable, i = constant, e = constant
        constraint = Constraint_element("", Variable("vec", dimensions=[5]), Const(1), Const(2))
        self.assertEqual("(and (= (select vec 1) 2) (< 1 5))", constraint.to_smtlib2().to_repr())
        self.assertTrue(constraint.is_valid())

        # vec = boolean variable, e = 1
        constraint = Constraint_element("", Variable("vec", Type.TYPE_BOOL, dimensions=[5]), Const(1), Const(1))
        self.assertEqual("(and (select vec 1) (< 1 5))", constraint.to_smtlib2().to_repr())
        self.assertTrue(constraint.is_valid())

        # vec = boolean variable, e = 0
        constraint = Constraint_element("", Variable("vec", Type.TYPE_BOOL, dimensions=[5]), Const(1), Const(0))
        self.assertEqual("(and (not (select vec 1)) (< 1 5))", constraint.to_smtlib2().to_repr())
        self.assertTrue(constraint.is_valid())

        # vec = boolean variable, e = variable
        constraint = Constraint_element("", Variable("vec", Type.TYPE_BOOL, dimensions=[5]), Const(1), Variable("e"))
        self.assertEqual("(and (= (select vec 1) e) (< 1 5))", constraint.to_smtlib2().to_repr())
        self.assertTrue(constraint.is_valid())

        # vec = vector, i > len
        constraint = Constraint_element("", Vector(Const(1), Const(2), Const(3)), Const(3), Variable("e"))
        self.assertEqual("(< 3 3)", constraint.to_smtlib2().to_repr())
        self.assertTrue(constraint.is_valid())

        # vec = vector, i <= len
        constraint = Constraint_element("", Vector(Const(1), Const(2), Const(3)), Const(2), Variable("e"))
        self.assertEqual("(and (= 3 e) (< 2 3))", constraint.to_smtlib2().to_repr())
        self.assertTrue(constraint.is_valid())

        # vec = vector, i = variable, e = variable
        constraint = Constraint_element("", Vector(Const(1), Const(2), Const(3)), Variable("i"), Variable("e"))
        self.assertEqual("(and (or (= 1 e) (= 2 e) (= 3 e)) (< i 3))", constraint.to_smtlib2().to_repr())
        self.assertTrue(constraint.is_valid())
