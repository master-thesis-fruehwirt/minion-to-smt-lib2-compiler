from unittest import TestCase

from src.arguments.const import Const
from src.arguments.variable import Variable
from src.arguments.vector import Vector
from src.constraints.constraint_element import Constraint_element
from src.constraints.constraint_watchelement import Constraint_watchelement
from src.mapper.type import Type


class TestConstraint_watchelement(TestCase):
    def test_to_smtlib2(self):
        test_cases = [
            [Variable("vec"), Const(1), Const(2)],
            [Variable("vec", Type.TYPE_BOOL), Const(1), Const(1)],
            [Variable("vec", Type.TYPE_BOOL), Const(1), Const(0)],
            [Vector(Const(1), Const(2), Const(3)), Variable("i"), Variable("e")]
        ]

        for arg in test_cases:
            constraint_element = Constraint_element("", *arg)
            constraint_watchelement = Constraint_watchelement("", *arg)
            self.assertEqual(constraint_element.to_smtlib2().to_repr(), constraint_watchelement.to_smtlib2().to_repr())
            self.assertTrue(constraint_watchelement.is_valid())
