from unittest import TestCase

from src.arguments.const import Const
from src.arguments.variable import Variable
from src.constraints.constraint_pow import Constraint_pow


class TestConstraint_pow(TestCase):
    def test_to_smtlib2(self):
        constraint = Constraint_pow("", Const(1), Const(2), Const(3))
        self.assertEqual("(ite (= 2 0) (= 3 1) (= 3 (^ 1 2)))", constraint.to_smtlib2().to_repr())
        self.assertTrue(constraint.is_valid())

        constraint = Constraint_pow("", Variable("x"), Variable("y"), Variable("z"))
        self.assertEqual("(ite (= y 0) (= z 1) (= z (^ x y)))", constraint.to_smtlib2().to_repr())
        self.assertTrue(constraint.is_valid())
