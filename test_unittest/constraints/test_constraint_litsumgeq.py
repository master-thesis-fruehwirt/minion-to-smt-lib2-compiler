from unittest import TestCase

from src.arguments.const import Const
from src.arguments.variable import Variable
from src.arguments.vector import Vector
from src.constraints.constraint_litsumgeq import Constraint_litsumgeq


class TestConstraint_litsumgeq(TestCase):
    def test_to_smtlib2(self):
        # vec1 = vector
        constraint = Constraint_litsumgeq("", Vector(Const(1), Const(2), Const(3)), Vector(Const(1), Const(2), Const(3)), Const(1))
        self.assertEqual("(>= (+ (ite (= 1 1) 1 0) (ite (= 2 2) 1 0) (ite (= 3 3) 1 0)) 1)", constraint.to_smtlib2().to_repr())
        self.assertTrue(constraint.is_valid())

        # vec1 = variable
        constraint = Constraint_litsumgeq("", Variable("A"), Vector(Const(1), Const(2), Const(3)), Const(1))
        self.assertEqual("(>= (+ (ite (= (select A 0) 1) 1 0) (ite (= (select A 1) 2) 1 0) (ite (= (select A 2) 3) 1 0)) 1)", constraint.to_smtlib2().to_repr())
        self.assertTrue(constraint.is_valid())
