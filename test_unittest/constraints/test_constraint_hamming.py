from unittest import TestCase

from src.arguments.const import Const
from src.arguments.variable import Variable
from src.arguments.vector import Vector
from src.constants import Constants
from src.constraints.constraint_hamming import Constraint_hamming
from src.globals import get_global_counter


class TestConstraint_hamming(TestCase):
    def test_to_smtlib2(self):
        # vec1 = vector, vec2 = vector
        constraint = Constraint_hamming("", Vector(Const(1), Const(3)), Vector(Const(2), Const(4)), Const(2))
        self.assertEqual("(>= (+ (ite (distinct 1 2) 1 0) (ite (distinct 3 4) 1 0)) 2)", constraint.to_smtlib2().to_repr())
        self.assertTrue(constraint.is_valid())

        # vec1 = vector, vec2 = variable
        constraint = Constraint_hamming("", Vector(Const(1), Const(3)), Variable("B"), Const(2))
        self.assertEqual("(>= (+ (ite (distinct 1 (select B 0)) 1 0) (ite (distinct 3 (select B 1)) 1 0)) 2)", constraint.to_smtlib2().to_repr())
        self.assertTrue(constraint.is_valid())

        # vec1 = variable, vec2 = vector
        constraint = Constraint_hamming("", Variable("A"), Vector(Const(1), Const(3)), Const(2))
        self.assertEqual("(>= (+ (ite (distinct (select A 0) 1) 1 0) (ite (distinct (select A 1) 3) 1 0)) 2)", constraint.to_smtlib2().to_repr())
        self.assertTrue(constraint.is_valid())

        # vec1 = variable, vec2 = variable
        constraint = Constraint_hamming("", Variable("A", dimensions=[4]), Variable("B"), Const(2))
        self.assertEqual("(>= (+ (ite (distinct (select A 0) (select B 0)) 1 0) (ite (distinct (select A 1) (select B 1)) 1 0) (ite (distinct (select A 2) (select B 2)) 1 0) (ite (distinct (select A 3) (select B 3)) 1 0)) 2)", constraint.to_smtlib2().to_repr())
        self.assertTrue(constraint.is_valid())
