from unittest import TestCase

from src.arguments.const import Const
from src.arguments.variable import Variable
from src.arguments.vector import Vector
from src.constraints.constraint_sumgeq import Constraint_sumgeq
from src.constraints.constraint_sumleq import Constraint_sumleq
from src.constraints.constraint_watchsumgeq import Constraint_watchsumgeq
from src.constraints.constraint_watchsumleq import Constraint_watchsumleq
from src.constraints.constraint_weightedsumgeq import Constraint_weightedsumgeq
from src.constraints.constraint_weightedsumleq import Constraint_weightedsumleq
from src.constraints.custom_constraint_sumeq import Constraint_sumeq
from src.constraints.custom_constraint_watchsumeq import Constraint_watchsumeq
from src.constraints.custom_constraint_weightedsumeq import Constraint_weightedsumeq
from src.mapper.type import Type
from src.minion_program_representation import MinionProgramRepresentation


class TestMinionProgramRepresentation(TestCase):
    def test_add_var_decl(self):
        prog = MinionProgramRepresentation()
        prog.add_var_decl(1)
        self.assertEqual(1, len(prog._var_declarations))

    def test_add_constraint(self):
        prog = MinionProgramRepresentation()
        prog.add_constraint(1)
        self.assertEqual(1, len(prog._constraints))

    def test_add_var_access(self):
        prog = MinionProgramRepresentation()
        prog.add_var_access(1)
        self.assertEqual(1, len(prog._var_accesses))

    def test__is_used(self):
        prog = MinionProgramRepresentation()
        var = Variable("var")
        prog.add_var_access(var)
        self.assertTrue(prog._is_used(var))

        var = Variable("var1")
        self.assertFalse(prog._is_used(var))

    def test__are_equivalent(self):
        prog = MinionProgramRepresentation()

        c1 = Constraint_sumgeq("sumgeq")
        c2 = Constraint_sumleq("sumleq")
        self.assertTrue(prog._are_equivalent(c1, c2))

        c1 = Constraint_watchsumgeq("watchsumgeq")
        c2 = Constraint_watchsumleq("watchsumleq")
        self.assertTrue(prog._are_equivalent(c1, c2))

        c1 = Constraint_weightedsumgeq("weightedsumgeq")
        c2 = Constraint_weightedsumleq("weightedsumleq")
        self.assertTrue(prog._are_equivalent(c1, c2))

        var1 = Variable("a")
        var2 = Variable("b")
        c1 = Constraint_sumgeq("sumgeq", var1)
        c2 = Constraint_sumleq("sumleq", var1)
        self.assertTrue(prog._are_equivalent(c1, c2))

        c1 = Constraint_sumgeq("sumgeq", var1, var1)
        c2 = Constraint_sumleq("sumleq", var1)
        self.assertFalse(prog._are_equivalent(c1, c2))

        c1 = Constraint_sumgeq("sumgeq", var1)
        c2 = Constraint_sumleq("sumleq", var2)
        self.assertFalse(prog._are_equivalent(c1, c2))

        vec1 = Vector(var1)
        vec2 = Vector(var2)
        c1 = Constraint_sumgeq("sumgeq", vec1)
        c2 = Constraint_sumleq("sumleq", vec1)
        self.assertTrue(prog._are_equivalent(c1, c2))

        c1 = Constraint_sumgeq("sumgeq", vec1)
        c2 = Constraint_sumleq("sumleq", vec2)
        self.assertFalse(prog._are_equivalent(c1, c2))

        var1 = Variable("a", tpe=Type.TYPE_BOOL)
        var2 = Variable("a", tpe=Type.TYPE_INT)
        c1 = Constraint_sumgeq("sumgeq", var1)
        c2 = Constraint_sumleq("sumleq", var2)
        self.assertFalse(prog._are_equivalent(c1, c2))

    def test__optimize_constraints(self):
        def helper_function(c1, c2):
            prog = MinionProgramRepresentation()
            prog.add_constraint(c1)
            prog.add_constraint(c2)
            return prog._optimize_constraints()

        args = [Variable("a"), Const(1)]

        opt = helper_function(Constraint_sumgeq("sumgeq", *args), Constraint_sumleq("sumleq", *args))
        self.assertEqual(1, len(opt))
        self.assertIsInstance(opt[0], Constraint_sumeq)

        opt = helper_function(Constraint_watchsumgeq("watchsumgeq", *args), Constraint_watchsumleq("watchsumleq", *args))
        self.assertEqual(1, len(opt))
        self.assertIsInstance(opt[0], Constraint_watchsumeq)

        opt = helper_function(Constraint_weightedsumgeq("weightedsumgeq", *args), Constraint_weightedsumleq("weightedsumleq", *args))
        self.assertEqual(1, len(opt))
        self.assertIsInstance(opt[0], Constraint_weightedsumeq)

        opt = helper_function(Constraint_sumgeq("sumgeq", *args), Constraint_watchsumleq("watchsumleq", *args))
        self.assertEqual(2, len(opt))
        self.assertIsInstance(opt[0], Constraint_sumgeq)
        self.assertIsInstance(opt[1], Constraint_watchsumleq)
