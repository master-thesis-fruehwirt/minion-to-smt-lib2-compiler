OUTFILE=documentation.pdf

rm -rf $OUTFILE latex html

doxygen
cd latex
make
pdflatex refman.tex
pdflatex refman.tex
mv refman.pdf "../$OUTFILE"
