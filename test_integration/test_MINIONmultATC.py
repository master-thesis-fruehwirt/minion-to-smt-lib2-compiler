from unittest import TestCase

from test_integration.interface.integrationtest_interface import IntegrationtestInterface


class TestMINIONmultATC(TestCase):

    def setUp(self) -> None:
        self._interface = IntegrationtestInterface()

    def test_2MINIONmultATC(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/minion_java_examples/MINIONmultATC/2MINIONmultATC'))

    def test_2MINIONmultATC_V1(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/minion_java_examples/MINIONmultATC/2MINIONmultATC_V1'))

    def test_2MINIONmultATC_V2(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/minion_java_examples/MINIONmultATC/2MINIONmultATC_V2'))

    def test_2MINIONmultATC_V3(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/minion_java_examples/MINIONmultATC/2MINIONmultATC_V3'))

    def test_2MINIONmultATC_V4(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/minion_java_examples/MINIONmultATC/2MINIONmultATC_V4'))

    def test_4MINIONmultATC(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/minion_java_examples/MINIONmultATC/4MINIONmultATC'))

    def test_4MINIONmultATC_V1(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/minion_java_examples/MINIONmultATC/4MINIONmultATC_V1'))

    def test_4MINIONmultATC_V2(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/minion_java_examples/MINIONmultATC/4MINIONmultATC_V2'))

    def test_4MINIONmultATC_V3(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/minion_java_examples/MINIONmultATC/4MINIONmultATC_V3'))

    def test_4MINIONmultATC_V4(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/minion_java_examples/MINIONmultATC/4MINIONmultATC_V4'))

    def test_7MINIONmultATC(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/minion_java_examples/MINIONmultATC/7MINIONmultATC'))

    def test_7MINIONmultATC_V1(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/minion_java_examples/MINIONmultATC/7MINIONmultATC_V1'))

    def test_7MINIONmultATC_V2(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/minion_java_examples/MINIONmultATC/7MINIONmultATC_V2'))

    def test_7MINIONmultATC_V3(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/minion_java_examples/MINIONmultATC/7MINIONmultATC_V3'))

    def test_7MINIONmultATC_V4(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/minion_java_examples/MINIONmultATC/7MINIONmultATC_V4'))

    def test_TG_V1a(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/minion_java_examples/MINIONmultATC/TG_V1a'))

