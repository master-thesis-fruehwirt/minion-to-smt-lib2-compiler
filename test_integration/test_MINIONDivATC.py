from unittest import TestCase

from test_integration.interface.integrationtest_interface import IntegrationtestInterface


class TestMINIONDivATC(TestCase):

    def setUp(self) -> None:
        self._interface = IntegrationtestInterface()

    def test_2MINIONDivATC(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/minion_java_examples/MINIONDivATC/2MINIONDivATC'))

    def test_2MINIONDivATC_V1(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/minion_java_examples/MINIONDivATC/2MINIONDivATC_V1'))

    def test_2MINIONDivATC_V2(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/minion_java_examples/MINIONDivATC/2MINIONDivATC_V2'))

    def test_2MINIONDivATC_V3(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/minion_java_examples/MINIONDivATC/2MINIONDivATC_V3'))

    def test_2MINIONDivATC_V4(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/minion_java_examples/MINIONDivATC/2MINIONDivATC_V4'))

    def test_4MINIONDivATC(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/minion_java_examples/MINIONDivATC/4MINIONDivATC'))

    def test_4MINIONDivATC_V1(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/minion_java_examples/MINIONDivATC/4MINIONDivATC_V1'))

    def test_4MINIONDivATC_V2(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/minion_java_examples/MINIONDivATC/4MINIONDivATC_V2'))

    def test_4MINIONDivATC_V3(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/minion_java_examples/MINIONDivATC/4MINIONDivATC_V3'))

    def test_4MINIONDivATC_V4(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/minion_java_examples/MINIONDivATC/4MINIONDivATC_V4'))

    def test_7MINIONDivATC(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/minion_java_examples/MINIONDivATC/7MINIONDivATC'))

    def test_7MINIONDivATC_V1(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/minion_java_examples/MINIONDivATC/7MINIONDivATC_V1'))

    def test_7MINIONDivATC_V2(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/minion_java_examples/MINIONDivATC/7MINIONDivATC_V2'))

    def test_7MINIONDivATC_V3(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/minion_java_examples/MINIONDivATC/7MINIONDivATC_V3'))

    def test_7MINIONDivATC_V4(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/minion_java_examples/MINIONDivATC/7MINIONDivATC_V4'))

