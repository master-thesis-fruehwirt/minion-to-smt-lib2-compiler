from unittest import TestCase

from test_integration.interface.integrationtest_interface import IntegrationtestInterface


class TestMINIONmultV2ATC(TestCase):

    def setUp(self) -> None:
        self._interface = IntegrationtestInterface()

    def test_2MINIONMultV2ATC(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/minion_java_examples/MINIONmultV2ATC/2MINIONMultV2ATC'))

    def test_2MINIONMultV2ATC_V1(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/minion_java_examples/MINIONmultV2ATC/2MINIONMultV2ATC_V1'))

    def test_2MINIONMultV2ATC_V2(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/minion_java_examples/MINIONmultV2ATC/2MINIONMultV2ATC_V2'))

    def test_2MINIONMultV2ATC_V3(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/minion_java_examples/MINIONmultV2ATC/2MINIONMultV2ATC_V3'))

    def test_2MINIONMultV2ATC_V4(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/minion_java_examples/MINIONmultV2ATC/2MINIONMultV2ATC_V4'))

    def test_4MINIONMultV2ATC(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/minion_java_examples/MINIONmultV2ATC/4MINIONMultV2ATC'))

    def test_4MINIONMultV2ATC_V1(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/minion_java_examples/MINIONmultV2ATC/4MINIONMultV2ATC_V1'))

    def test_4MINIONMultV2ATC_V2(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/minion_java_examples/MINIONmultV2ATC/4MINIONMultV2ATC_V2'))

    def test_4MINIONMultV2ATC_V3(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/minion_java_examples/MINIONmultV2ATC/4MINIONMultV2ATC_V3'))

    def test_4MINIONMultV2ATC_V4(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/minion_java_examples/MINIONmultV2ATC/4MINIONMultV2ATC_V4'))

    def test_7MINIONMultV2ATC(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/minion_java_examples/MINIONmultV2ATC/7MINIONMultV2ATC'))

    def test_7MINIONMultV2ATC_V1(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/minion_java_examples/MINIONmultV2ATC/7MINIONMultV2ATC_V1'))

    def test_7MINIONMultV2ATC_V2(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/minion_java_examples/MINIONmultV2ATC/7MINIONMultV2ATC_V2'))

    def test_7MINIONMultV2ATC_V3(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/minion_java_examples/MINIONmultV2ATC/7MINIONMultV2ATC_V3'))

    def test_7MINIONMultV2ATC_V4(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/minion_java_examples/MINIONmultV2ATC/7MINIONMultV2ATC_V4'))

    def test_TG_V1b(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/minion_java_examples/MINIONmultV2ATC/TG_V1b'))

