from unittest import TestCase

from test_integration.interface.integrationtest_interface import IntegrationtestInterface


class Testspreadsheet_examples(TestCase):

    def setUp(self) -> None:
        self._interface = IntegrationtestInterface()

    def test_AFW_amortization_1Faults_Fault1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_amortization_1Faults_Fault1_comparison'))

    def test_AFW_amortization_1Faults_Fault1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_amortization_1Faults_Fault1_dependency'))

    def test_AFW_amortization_1Faults_Fault1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_amortization_1Faults_Fault1_value'))

    def test_AFW_amortization_2Faults_Fault1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_amortization_2Faults_Fault1_comparison'))

    def test_AFW_amortization_2Faults_Fault1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_amortization_2Faults_Fault1_dependency'))

    def test_AFW_amortization_2Faults_Fault1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_amortization_2Faults_Fault1_value'))

    def test_AFW_amortization_2Faults_Fault2_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_amortization_2Faults_Fault2_comparison'))

    def test_AFW_amortization_2Faults_Fault2_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_amortization_2Faults_Fault2_dependency'))

    def test_AFW_amortization_2Faults_Fault2_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_amortization_2Faults_Fault2_value'))

    def test_AFW_amortization_2Faults_Fault3_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_amortization_2Faults_Fault3_comparison'))

    def test_AFW_amortization_2Faults_Fault3_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_amortization_2Faults_Fault3_dependency'))

    def test_AFW_amortization_2Faults_Fault3_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_amortization_2Faults_Fault3_value'))

    def test_AFW_amortization_3Faults_Fault1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_amortization_3Faults_Fault1_comparison'))

    def test_AFW_amortization_3Faults_Fault1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_amortization_3Faults_Fault1_dependency'))

    def test_AFW_amortization_3Faults_Fault1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_amortization_3Faults_Fault1_value'))

    def test_AFW_area_2Faults_Fault1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_area_2Faults_Fault1_comparison'))

    def test_AFW_area_2Faults_Fault1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_area_2Faults_Fault1_dependency'))

    def test_AFW_area_2Faults_Fault1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_area_2Faults_Fault1_value'))

    def test_AFW_area_2Faults_Fault2_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_area_2Faults_Fault2_comparison'))

    def test_AFW_area_2Faults_Fault2_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_area_2Faults_Fault2_dependency'))

    def test_AFW_area_2Faults_Fault2_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_area_2Faults_Fault2_value'))

    def test_AFW_area_2Faults_Fault3_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_area_2Faults_Fault3_comparison'))

    def test_AFW_area_2Faults_Fault3_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_area_2Faults_Fault3_dependency'))

    def test_AFW_area_2Faults_Fault3_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_area_2Faults_Fault3_value'))

    def test_AFW_area_3Faults_Fault1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_area_3Faults_Fault1_comparison'))

    def test_AFW_area_3Faults_Fault1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_area_3Faults_Fault1_dependency'))

    def test_AFW_area_3Faults_Fault1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_area_3Faults_Fault1_value'))

    def test_AFW_arithmetics00_1Faults_Fault1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_arithmetics00_1Faults_Fault1_comparison'))

    def test_AFW_arithmetics00_1Faults_Fault1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_arithmetics00_1Faults_Fault1_dependency'))

    def test_AFW_arithmetics00_1Faults_Fault1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_arithmetics00_1Faults_Fault1_value'))

    def test_AFW_arithmetics00_1Faults_Fault2_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_arithmetics00_1Faults_Fault2_comparison'))

    def test_AFW_arithmetics00_1Faults_Fault2_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_arithmetics00_1Faults_Fault2_dependency'))

    def test_AFW_arithmetics00_1Faults_Fault2_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_arithmetics00_1Faults_Fault2_value'))

    def test_AFW_arithmetics00_1Faults_Fault3_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_arithmetics00_1Faults_Fault3_comparison'))

    def test_AFW_arithmetics00_1Faults_Fault3_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_arithmetics00_1Faults_Fault3_dependency'))

    def test_AFW_arithmetics00_1Faults_Fault3_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_arithmetics00_1Faults_Fault3_value'))

    def test_AFW_arithmetics00_2Faults_Fault1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_arithmetics00_2Faults_Fault1_comparison'))

    def test_AFW_arithmetics00_2Faults_Fault1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_arithmetics00_2Faults_Fault1_dependency'))

    def test_AFW_arithmetics00_2Faults_Fault1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_arithmetics00_2Faults_Fault1_value'))

    def test_AFW_arithmetics00_2Faults_Fault2_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_arithmetics00_2Faults_Fault2_comparison'))

    def test_AFW_arithmetics00_2Faults_Fault2_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_arithmetics00_2Faults_Fault2_dependency'))

    def test_AFW_arithmetics00_2Faults_Fault2_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_arithmetics00_2Faults_Fault2_value'))

    def test_AFW_arithmetics00_2Faults_Fault3_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_arithmetics00_2Faults_Fault3_comparison'))

    def test_AFW_arithmetics00_2Faults_Fault3_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_arithmetics00_2Faults_Fault3_dependency'))

    def test_AFW_arithmetics00_2Faults_Fault3_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_arithmetics00_2Faults_Fault3_value'))

    def test_AFW_arithmetics00_3Faults_Fault1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_arithmetics00_3Faults_Fault1_comparison'))

    def test_AFW_arithmetics00_3Faults_Fault1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_arithmetics00_3Faults_Fault1_dependency'))

    def test_AFW_arithmetics00_3Faults_Fault1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_arithmetics00_3Faults_Fault1_value'))

    def test_AFW_arithmetics01_1Faults_Fault1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_arithmetics01_1Faults_Fault1_comparison'))

    def test_AFW_arithmetics01_1Faults_Fault1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_arithmetics01_1Faults_Fault1_dependency'))

    def test_AFW_arithmetics01_1Faults_Fault1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_arithmetics01_1Faults_Fault1_value'))

    def test_AFW_arithmetics01_1Faults_Fault2_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_arithmetics01_1Faults_Fault2_comparison'))

    def test_AFW_arithmetics01_1Faults_Fault2_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_arithmetics01_1Faults_Fault2_dependency'))

    def test_AFW_arithmetics01_1Faults_Fault2_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_arithmetics01_1Faults_Fault2_value'))

    def test_AFW_arithmetics01_1Faults_Fault3_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_arithmetics01_1Faults_Fault3_comparison'))

    def test_AFW_arithmetics01_1Faults_Fault3_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_arithmetics01_1Faults_Fault3_dependency'))

    def test_AFW_arithmetics01_1Faults_Fault3_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_arithmetics01_1Faults_Fault3_value'))

    def test_AFW_arithmetics01_2Faults_Fault1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_arithmetics01_2Faults_Fault1_comparison'))

    def test_AFW_arithmetics01_2Faults_Fault1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_arithmetics01_2Faults_Fault1_dependency'))

    def test_AFW_arithmetics01_2Faults_Fault1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_arithmetics01_2Faults_Fault1_value'))

    def test_AFW_arithmetics01_2Faults_Fault2_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_arithmetics01_2Faults_Fault2_comparison'))

    def test_AFW_arithmetics01_2Faults_Fault2_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_arithmetics01_2Faults_Fault2_dependency'))

    def test_AFW_arithmetics01_2Faults_Fault2_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_arithmetics01_2Faults_Fault2_value'))

    def test_AFW_arithmetics01_2Faults_Fault3_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_arithmetics01_2Faults_Fault3_comparison'))

    def test_AFW_arithmetics01_2Faults_Fault3_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_arithmetics01_2Faults_Fault3_dependency'))

    def test_AFW_arithmetics01_2Faults_Fault3_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_arithmetics01_2Faults_Fault3_value'))

    def test_AFW_arithmetics01_3Faults_Fault1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_arithmetics01_3Faults_Fault1_comparison'))

    def test_AFW_arithmetics01_3Faults_Fault1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_arithmetics01_3Faults_Fault1_dependency'))

    def test_AFW_arithmetics01_3Faults_Fault1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_arithmetics01_3Faults_Fault1_value'))

    def test_AFW_arithmetics02_1Faults_Fault1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_arithmetics02_1Faults_Fault1_comparison'))

    def test_AFW_arithmetics02_1Faults_Fault1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_arithmetics02_1Faults_Fault1_dependency'))

    def test_AFW_arithmetics02_1Faults_Fault1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_arithmetics02_1Faults_Fault1_value'))

    def test_AFW_arithmetics02_1Faults_Fault2_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_arithmetics02_1Faults_Fault2_comparison'))

    def test_AFW_arithmetics02_1Faults_Fault2_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_arithmetics02_1Faults_Fault2_dependency'))

    def test_AFW_arithmetics02_1Faults_Fault2_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_arithmetics02_1Faults_Fault2_value'))

    def test_AFW_arithmetics02_1Faults_Fault3_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_arithmetics02_1Faults_Fault3_comparison'))

    def test_AFW_arithmetics02_1Faults_Fault3_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_arithmetics02_1Faults_Fault3_dependency'))

    def test_AFW_arithmetics02_1Faults_Fault3_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_arithmetics02_1Faults_Fault3_value'))

    def test_AFW_arithmetics02_2Faults_Fault1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_arithmetics02_2Faults_Fault1_comparison'))

    def test_AFW_arithmetics02_2Faults_Fault1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_arithmetics02_2Faults_Fault1_dependency'))

    def test_AFW_arithmetics02_2Faults_Fault1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_arithmetics02_2Faults_Fault1_value'))

    def test_AFW_arithmetics02_2Faults_Fault2_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_arithmetics02_2Faults_Fault2_comparison'))

    def test_AFW_arithmetics02_2Faults_Fault2_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_arithmetics02_2Faults_Fault2_dependency'))

    def test_AFW_arithmetics02_2Faults_Fault2_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_arithmetics02_2Faults_Fault2_value'))

    def test_AFW_arithmetics02_2Faults_Fault3_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_arithmetics02_2Faults_Fault3_comparison'))

    def test_AFW_arithmetics02_2Faults_Fault3_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_arithmetics02_2Faults_Fault3_dependency'))

    def test_AFW_arithmetics02_2Faults_Fault3_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_arithmetics02_2Faults_Fault3_value'))

    def test_AFW_arithmetics02_3Faults_Fault1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_arithmetics02_3Faults_Fault1_comparison'))

    def test_AFW_arithmetics02_3Faults_Fault1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_arithmetics02_3Faults_Fault1_dependency'))

    def test_AFW_arithmetics02_3Faults_Fault1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_arithmetics02_3Faults_Fault1_value'))

    def test_AFW_arithmetics03_1Faults_Fault1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_arithmetics03_1Faults_Fault1_comparison'))

    def test_AFW_arithmetics03_1Faults_Fault1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_arithmetics03_1Faults_Fault1_dependency'))

    def test_AFW_arithmetics03_1Faults_Fault1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_arithmetics03_1Faults_Fault1_value'))

    def test_AFW_arithmetics03_1Faults_Fault2_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_arithmetics03_1Faults_Fault2_comparison'))

    def test_AFW_arithmetics03_1Faults_Fault2_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_arithmetics03_1Faults_Fault2_dependency'))

    def test_AFW_arithmetics03_1Faults_Fault2_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_arithmetics03_1Faults_Fault2_value'))

    def test_AFW_arithmetics03_1Faults_Fault3_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_arithmetics03_1Faults_Fault3_comparison'))

    def test_AFW_arithmetics03_1Faults_Fault3_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_arithmetics03_1Faults_Fault3_dependency'))

    def test_AFW_arithmetics03_1Faults_Fault3_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_arithmetics03_1Faults_Fault3_value'))

    def test_AFW_arithmetics03_2Faults_Fault1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_arithmetics03_2Faults_Fault1_comparison'))

    def test_AFW_arithmetics03_2Faults_Fault1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_arithmetics03_2Faults_Fault1_dependency'))

    def test_AFW_arithmetics03_2Faults_Fault1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_arithmetics03_2Faults_Fault1_value'))

    def test_AFW_arithmetics03_2Faults_Fault2_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_arithmetics03_2Faults_Fault2_comparison'))

    def test_AFW_arithmetics03_2Faults_Fault2_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_arithmetics03_2Faults_Fault2_dependency'))

    def test_AFW_arithmetics03_2Faults_Fault2_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_arithmetics03_2Faults_Fault2_value'))

    def test_AFW_arithmetics03_2Faults_Fault3_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_arithmetics03_2Faults_Fault3_comparison'))

    def test_AFW_arithmetics03_2Faults_Fault3_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_arithmetics03_2Faults_Fault3_dependency'))

    def test_AFW_arithmetics03_2Faults_Fault3_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_arithmetics03_2Faults_Fault3_value'))

    def test_AFW_arithmetics03_3Faults_Fault1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_arithmetics03_3Faults_Fault1_comparison'))

    def test_AFW_arithmetics03_3Faults_Fault1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_arithmetics03_3Faults_Fault1_dependency'))

    def test_AFW_arithmetics03_3Faults_Fault1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_arithmetics03_3Faults_Fault1_value'))

    def test_AFW_arithmetics04_1Faults_Fault1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_arithmetics04_1Faults_Fault1_comparison'))

    def test_AFW_arithmetics04_1Faults_Fault1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_arithmetics04_1Faults_Fault1_dependency'))

    def test_AFW_arithmetics04_1Faults_Fault1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_arithmetics04_1Faults_Fault1_value'))

    def test_AFW_arithmetics04_1Faults_Fault2_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_arithmetics04_1Faults_Fault2_comparison'))

    def test_AFW_arithmetics04_1Faults_Fault2_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_arithmetics04_1Faults_Fault2_dependency'))

    def test_AFW_arithmetics04_1Faults_Fault2_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_arithmetics04_1Faults_Fault2_value'))

    def test_AFW_arithmetics04_1Faults_Fault3_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_arithmetics04_1Faults_Fault3_comparison'))

    def test_AFW_arithmetics04_1Faults_Fault3_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_arithmetics04_1Faults_Fault3_dependency'))

    def test_AFW_arithmetics04_1Faults_Fault3_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_arithmetics04_1Faults_Fault3_value'))

    def test_AFW_arithmetics04_1Faults_Fault4_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_arithmetics04_1Faults_Fault4_comparison'))

    def test_AFW_arithmetics04_1Faults_Fault4_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_arithmetics04_1Faults_Fault4_dependency'))

    def test_AFW_arithmetics04_1Faults_Fault4_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_arithmetics04_1Faults_Fault4_value'))

    def test_AFW_arithmetics04_1Faults_Fault5_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_arithmetics04_1Faults_Fault5_comparison'))

    def test_AFW_arithmetics04_1Faults_Fault5_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_arithmetics04_1Faults_Fault5_dependency'))

    def test_AFW_arithmetics04_1Faults_Fault5_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_arithmetics04_1Faults_Fault5_value'))

    def test_AFW_arithmetics04_2Faults_Fault1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_arithmetics04_2Faults_Fault1_comparison'))

    def test_AFW_arithmetics04_2Faults_Fault1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_arithmetics04_2Faults_Fault1_dependency'))

    def test_AFW_arithmetics04_2Faults_Fault1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_arithmetics04_2Faults_Fault1_value'))

    def test_AFW_arithmetics04_2Faults_Fault2_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_arithmetics04_2Faults_Fault2_comparison'))

    def test_AFW_arithmetics04_2Faults_Fault2_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_arithmetics04_2Faults_Fault2_dependency'))

    def test_AFW_arithmetics04_2Faults_Fault2_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_arithmetics04_2Faults_Fault2_value'))

    def test_AFW_arithmetics04_2Faults_Fault3_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_arithmetics04_2Faults_Fault3_comparison'))

    def test_AFW_arithmetics04_2Faults_Fault3_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_arithmetics04_2Faults_Fault3_dependency'))

    def test_AFW_arithmetics04_2Faults_Fault3_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_arithmetics04_2Faults_Fault3_value'))

    def test_AFW_arithmetics04_3Faults_Fault1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_arithmetics04_3Faults_Fault1_comparison'))

    def test_AFW_arithmetics04_3Faults_Fault1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_arithmetics04_3Faults_Fault1_dependency'))

    def test_AFW_arithmetics04_3Faults_Fault1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_arithmetics04_3Faults_Fault1_value'))

    def test_AFW_austrian_league_1Faults_Fault1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_austrian_league_1Faults_Fault1_comparison'))

    def test_AFW_austrian_league_1Faults_Fault1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_austrian_league_1Faults_Fault1_dependency'))

    def test_AFW_austrian_league_1Faults_Fault1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_austrian_league_1Faults_Fault1_value'))

    def test_AFW_austrian_league_1Faults_Fault2_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_austrian_league_1Faults_Fault2_comparison'))

    def test_AFW_austrian_league_1Faults_Fault2_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_austrian_league_1Faults_Fault2_dependency'))

    def test_AFW_austrian_league_1Faults_Fault2_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_austrian_league_1Faults_Fault2_value'))

    def test_AFW_austrian_league_1Faults_Fault3_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_austrian_league_1Faults_Fault3_comparison'))

    def test_AFW_austrian_league_1Faults_Fault3_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_austrian_league_1Faults_Fault3_dependency'))

    def test_AFW_austrian_league_1Faults_Fault3_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_austrian_league_1Faults_Fault3_value'))

    def test_AFW_austrian_league_1Faults_Fault4_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_austrian_league_1Faults_Fault4_comparison'))

    def test_AFW_austrian_league_1Faults_Fault4_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_austrian_league_1Faults_Fault4_dependency'))

    def test_AFW_austrian_league_1Faults_Fault4_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_austrian_league_1Faults_Fault4_value'))

    def test_AFW_austrian_league_2Faults_Fault1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_austrian_league_2Faults_Fault1_comparison'))

    def test_AFW_austrian_league_2Faults_Fault1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_austrian_league_2Faults_Fault1_dependency'))

    def test_AFW_austrian_league_2Faults_Fault1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_austrian_league_2Faults_Fault1_value'))

    def test_AFW_austrian_league_2Faults_Fault2_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_austrian_league_2Faults_Fault2_comparison'))

    def test_AFW_austrian_league_2Faults_Fault2_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_austrian_league_2Faults_Fault2_dependency'))

    def test_AFW_austrian_league_2Faults_Fault2_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_austrian_league_2Faults_Fault2_value'))

    def test_AFW_austrian_league_2Faults_Fault3_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_austrian_league_2Faults_Fault3_comparison'))

    def test_AFW_austrian_league_2Faults_Fault3_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_austrian_league_2Faults_Fault3_dependency'))

    def test_AFW_austrian_league_2Faults_Fault3_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_austrian_league_2Faults_Fault3_value'))

    def test_AFW_austrian_league_3Faults_Fault1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_austrian_league_3Faults_Fault1_comparison'))

    def test_AFW_austrian_league_3Faults_Fault1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_austrian_league_3Faults_Fault1_dependency'))

    def test_AFW_austrian_league_3Faults_Fault1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_austrian_league_3Faults_Fault1_value'))

    def test_AFW_bank_account_1Faults_Fault1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_bank_account_1Faults_Fault1_comparison'))

    def test_AFW_bank_account_1Faults_Fault1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_bank_account_1Faults_Fault1_dependency'))

    def test_AFW_bank_account_1Faults_Fault1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_bank_account_1Faults_Fault1_value'))

    def test_AFW_bank_account_1Faults_Fault2_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_bank_account_1Faults_Fault2_comparison'))

    def test_AFW_bank_account_1Faults_Fault2_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_bank_account_1Faults_Fault2_dependency'))

    def test_AFW_bank_account_1Faults_Fault2_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_bank_account_1Faults_Fault2_value'))

    def test_AFW_bank_account_1Faults_Fault3_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_bank_account_1Faults_Fault3_comparison'))

    def test_AFW_bank_account_1Faults_Fault3_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_bank_account_1Faults_Fault3_dependency'))

    def test_AFW_bank_account_1Faults_Fault3_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_bank_account_1Faults_Fault3_value'))

    def test_AFW_bank_account_1Faults_Fault4_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_bank_account_1Faults_Fault4_comparison'))

    def test_AFW_bank_account_1Faults_Fault4_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_bank_account_1Faults_Fault4_dependency'))

    def test_AFW_bank_account_1Faults_Fault4_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_bank_account_1Faults_Fault4_value'))

    def test_AFW_bank_account_1Faults_Fault5_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_bank_account_1Faults_Fault5_comparison'))

    def test_AFW_bank_account_1Faults_Fault5_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_bank_account_1Faults_Fault5_dependency'))

    def test_AFW_bank_account_1Faults_Fault5_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_bank_account_1Faults_Fault5_value'))

    def test_AFW_bank_account_2Faults_Fault1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_bank_account_2Faults_Fault1_comparison'))

    def test_AFW_bank_account_2Faults_Fault1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_bank_account_2Faults_Fault1_dependency'))

    def test_AFW_bank_account_2Faults_Fault1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_bank_account_2Faults_Fault1_value'))

    def test_AFW_bank_account_2Faults_Fault2_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_bank_account_2Faults_Fault2_comparison'))

    def test_AFW_bank_account_2Faults_Fault2_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_bank_account_2Faults_Fault2_dependency'))

    def test_AFW_bank_account_2Faults_Fault2_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_bank_account_2Faults_Fault2_value'))

    def test_AFW_bank_account_2Faults_Fault3_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_bank_account_2Faults_Fault3_comparison'))

    def test_AFW_bank_account_2Faults_Fault3_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_bank_account_2Faults_Fault3_dependency'))

    def test_AFW_bank_account_2Faults_Fault3_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_bank_account_2Faults_Fault3_value'))

    def test_AFW_bank_account_3Faults_Fault1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_bank_account_3Faults_Fault1_comparison'))

    def test_AFW_bank_account_3Faults_Fault1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_bank_account_3Faults_Fault1_dependency'))

    def test_AFW_bank_account_3Faults_Fault1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_bank_account_3Faults_Fault1_value'))

    def test_AFW_birthdays_1Faults_Fault1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_birthdays_1Faults_Fault1_comparison'))

    def test_AFW_birthdays_1Faults_Fault1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_birthdays_1Faults_Fault1_dependency'))

    def test_AFW_birthdays_1Faults_Fault1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_birthdays_1Faults_Fault1_value'))

    def test_AFW_birthdays_1Faults_Fault2_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_birthdays_1Faults_Fault2_comparison'))

    def test_AFW_birthdays_1Faults_Fault2_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_birthdays_1Faults_Fault2_dependency'))

    def test_AFW_birthdays_1Faults_Fault2_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_birthdays_1Faults_Fault2_value'))

    def test_AFW_birthdays_1Faults_Fault3_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_birthdays_1Faults_Fault3_comparison'))

    def test_AFW_birthdays_1Faults_Fault3_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_birthdays_1Faults_Fault3_dependency'))

    def test_AFW_birthdays_1Faults_Fault3_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_birthdays_1Faults_Fault3_value'))

    def test_AFW_birthdays_1Faults_Fault4_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_birthdays_1Faults_Fault4_comparison'))

    def test_AFW_birthdays_1Faults_Fault4_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_birthdays_1Faults_Fault4_dependency'))

    def test_AFW_birthdays_1Faults_Fault4_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_birthdays_1Faults_Fault4_value'))

    def test_AFW_birthdays_1Faults_Fault5_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_birthdays_1Faults_Fault5_comparison'))

    def test_AFW_birthdays_1Faults_Fault5_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_birthdays_1Faults_Fault5_dependency'))

    def test_AFW_birthdays_1Faults_Fault5_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_birthdays_1Faults_Fault5_value'))

    def test_AFW_birthdays_2Faults_Fault1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_birthdays_2Faults_Fault1_comparison'))

    def test_AFW_birthdays_2Faults_Fault1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_birthdays_2Faults_Fault1_dependency'))

    def test_AFW_birthdays_2Faults_Fault1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_birthdays_2Faults_Fault1_value'))

    def test_AFW_birthdays_2Faults_Fault2_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_birthdays_2Faults_Fault2_comparison'))

    def test_AFW_birthdays_2Faults_Fault2_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_birthdays_2Faults_Fault2_dependency'))

    def test_AFW_birthdays_2Faults_Fault2_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_birthdays_2Faults_Fault2_value'))

    def test_AFW_birthdays_2Faults_Fault3_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_birthdays_2Faults_Fault3_comparison'))

    def test_AFW_birthdays_2Faults_Fault3_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_birthdays_2Faults_Fault3_dependency'))

    def test_AFW_birthdays_2Faults_Fault3_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_birthdays_2Faults_Fault3_value'))

    def test_AFW_birthdays_3Faults_Fault1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_birthdays_3Faults_Fault1_comparison'))

    def test_AFW_birthdays_3Faults_Fault1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_birthdays_3Faults_Fault1_dependency'))

    def test_AFW_birthdays_3Faults_Fault1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_birthdays_3Faults_Fault1_value'))

    def test_AFW_book_recommendation_1Faults_Fault1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_book_recommendation_1Faults_Fault1_comparison'))

    def test_AFW_book_recommendation_1Faults_Fault1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_book_recommendation_1Faults_Fault1_dependency'))

    def test_AFW_book_recommendation_1Faults_Fault1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_book_recommendation_1Faults_Fault1_value'))

    def test_AFW_book_recommendation_1Faults_Fault2_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_book_recommendation_1Faults_Fault2_comparison'))

    def test_AFW_book_recommendation_1Faults_Fault2_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_book_recommendation_1Faults_Fault2_dependency'))

    def test_AFW_book_recommendation_1Faults_Fault2_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_book_recommendation_1Faults_Fault2_value'))

    def test_AFW_book_recommendation_1Faults_Fault3_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_book_recommendation_1Faults_Fault3_comparison'))

    def test_AFW_book_recommendation_1Faults_Fault3_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_book_recommendation_1Faults_Fault3_dependency'))

    def test_AFW_book_recommendation_1Faults_Fault3_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_book_recommendation_1Faults_Fault3_value'))

    def test_AFW_book_recommendation_1Faults_Fault4_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_book_recommendation_1Faults_Fault4_comparison'))

    def test_AFW_book_recommendation_1Faults_Fault4_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_book_recommendation_1Faults_Fault4_dependency'))

    def test_AFW_book_recommendation_1Faults_Fault4_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_book_recommendation_1Faults_Fault4_value'))

    def test_AFW_book_recommendation_2Faults_Fault1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_book_recommendation_2Faults_Fault1_comparison'))

    def test_AFW_book_recommendation_2Faults_Fault1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_book_recommendation_2Faults_Fault1_dependency'))

    def test_AFW_book_recommendation_2Faults_Fault1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_book_recommendation_2Faults_Fault1_value'))

    def test_AFW_book_recommendation_2Faults_Fault2_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_book_recommendation_2Faults_Fault2_comparison'))

    def test_AFW_book_recommendation_2Faults_Fault2_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_book_recommendation_2Faults_Fault2_dependency'))

    def test_AFW_book_recommendation_2Faults_Fault2_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_book_recommendation_2Faults_Fault2_value'))

    def test_AFW_book_recommendation_2Faults_Fault3_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_book_recommendation_2Faults_Fault3_comparison'))

    def test_AFW_book_recommendation_2Faults_Fault3_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_book_recommendation_2Faults_Fault3_dependency'))

    def test_AFW_book_recommendation_2Faults_Fault3_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_book_recommendation_2Faults_Fault3_value'))

    def test_AFW_book_recommendation_3Faults_Fault1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_book_recommendation_3Faults_Fault1_comparison'))

    def test_AFW_book_recommendation_3Faults_Fault1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_book_recommendation_3Faults_Fault1_dependency'))

    def test_AFW_book_recommendation_3Faults_Fault1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_book_recommendation_3Faults_Fault1_value'))

    def test_AFW_cake_1Faults_Fault1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_cake_1Faults_Fault1_comparison'))

    def test_AFW_cake_1Faults_Fault1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_cake_1Faults_Fault1_dependency'))

    def test_AFW_cake_1Faults_Fault1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_cake_1Faults_Fault1_value'))

    def test_AFW_cake_2Faults_Fault1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_cake_2Faults_Fault1_comparison'))

    def test_AFW_cake_2Faults_Fault1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_cake_2Faults_Fault1_dependency'))

    def test_AFW_cake_2Faults_Fault1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_cake_2Faults_Fault1_value'))

    def test_AFW_cake_2Faults_Fault2_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_cake_2Faults_Fault2_comparison'))

    def test_AFW_cake_2Faults_Fault2_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_cake_2Faults_Fault2_dependency'))

    def test_AFW_cake_2Faults_Fault2_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_cake_2Faults_Fault2_value'))

    def test_AFW_cake_2Faults_Fault3_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_cake_2Faults_Fault3_comparison'))

    def test_AFW_cake_2Faults_Fault3_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_cake_2Faults_Fault3_dependency'))

    def test_AFW_cake_2Faults_Fault3_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_cake_2Faults_Fault3_value'))

    def test_AFW_cake_3Faults_Fault1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_cake_3Faults_Fault1_comparison'))

    def test_AFW_cake_3Faults_Fault1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_cake_3Faults_Fault1_dependency'))

    def test_AFW_cake_3Faults_Fault1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_cake_3Faults_Fault1_value'))

    def test_AFW_computer_shopping_1Faults_Fault1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_computer_shopping_1Faults_Fault1_comparison'))

    def test_AFW_computer_shopping_1Faults_Fault1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_computer_shopping_1Faults_Fault1_dependency'))

    def test_AFW_computer_shopping_1Faults_Fault1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_computer_shopping_1Faults_Fault1_value'))

    def test_AFW_computer_shopping_1Faults_Fault2_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_computer_shopping_1Faults_Fault2_comparison'))

    def test_AFW_computer_shopping_1Faults_Fault2_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_computer_shopping_1Faults_Fault2_dependency'))

    def test_AFW_computer_shopping_1Faults_Fault2_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_computer_shopping_1Faults_Fault2_value'))

    def test_AFW_computer_shopping_2Faults_Fault1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_computer_shopping_2Faults_Fault1_comparison'))

    def test_AFW_computer_shopping_2Faults_Fault1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_computer_shopping_2Faults_Fault1_dependency'))

    def test_AFW_computer_shopping_2Faults_Fault1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_computer_shopping_2Faults_Fault1_value'))

    def test_AFW_computer_shopping_2Faults_Fault2_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_computer_shopping_2Faults_Fault2_comparison'))

    def test_AFW_computer_shopping_2Faults_Fault2_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_computer_shopping_2Faults_Fault2_dependency'))

    def test_AFW_computer_shopping_2Faults_Fault2_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_computer_shopping_2Faults_Fault2_value'))

    def test_AFW_computer_shopping_2Faults_Fault3_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_computer_shopping_2Faults_Fault3_comparison'))

    def test_AFW_computer_shopping_2Faults_Fault3_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_computer_shopping_2Faults_Fault3_dependency'))

    def test_AFW_computer_shopping_2Faults_Fault3_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_computer_shopping_2Faults_Fault3_value'))

    def test_AFW_computer_shopping_3Faults_Fault1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_computer_shopping_3Faults_Fault1_comparison'))

    def test_AFW_computer_shopping_3Faults_Fault1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_computer_shopping_3Faults_Fault1_dependency'))

    def test_AFW_computer_shopping_3Faults_Fault1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_computer_shopping_3Faults_Fault1_value'))

    def test_AFW_conditionals01_1Faults_Fault1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_conditionals01_1Faults_Fault1_comparison'))

    def test_AFW_conditionals01_1Faults_Fault1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_conditionals01_1Faults_Fault1_dependency'))

    def test_AFW_conditionals01_1Faults_Fault1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_conditionals01_1Faults_Fault1_value'))

    def test_AFW_conditionals01_1Faults_Fault2_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_conditionals01_1Faults_Fault2_comparison'))

    def test_AFW_conditionals01_1Faults_Fault2_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_conditionals01_1Faults_Fault2_dependency'))

    def test_AFW_conditionals01_1Faults_Fault2_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_conditionals01_1Faults_Fault2_value'))

    def test_AFW_conditionals01_1Faults_Fault3_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_conditionals01_1Faults_Fault3_comparison'))

    def test_AFW_conditionals01_1Faults_Fault3_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_conditionals01_1Faults_Fault3_dependency'))

    def test_AFW_conditionals01_1Faults_Fault3_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_conditionals01_1Faults_Fault3_value'))

    def test_AFW_conditionals01_2Faults_Fault1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_conditionals01_2Faults_Fault1_comparison'))

    def test_AFW_conditionals01_2Faults_Fault1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_conditionals01_2Faults_Fault1_dependency'))

    def test_AFW_conditionals01_2Faults_Fault1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_conditionals01_2Faults_Fault1_value'))

    def test_AFW_conditionals01_2Faults_Fault2_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_conditionals01_2Faults_Fault2_comparison'))

    def test_AFW_conditionals01_2Faults_Fault2_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_conditionals01_2Faults_Fault2_dependency'))

    def test_AFW_conditionals01_2Faults_Fault2_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_conditionals01_2Faults_Fault2_value'))

    def test_AFW_conditionals01_2Faults_Fault3_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_conditionals01_2Faults_Fault3_comparison'))

    def test_AFW_conditionals01_2Faults_Fault3_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_conditionals01_2Faults_Fault3_dependency'))

    def test_AFW_conditionals01_2Faults_Fault3_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_conditionals01_2Faults_Fault3_value'))

    def test_AFW_conditionals01_3Faults_Fault1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_conditionals01_3Faults_Fault1_comparison'))

    def test_AFW_conditionals01_3Faults_Fault1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_conditionals01_3Faults_Fault1_dependency'))

    def test_AFW_conditionals01_3Faults_Fault1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_conditionals01_3Faults_Fault1_value'))

    def test_AFW_conditionals02_1Faults_Fault1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_conditionals02_1Faults_Fault1_comparison'))

    def test_AFW_conditionals02_1Faults_Fault1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_conditionals02_1Faults_Fault1_dependency'))

    def test_AFW_conditionals02_1Faults_Fault1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_conditionals02_1Faults_Fault1_value'))

    def test_AFW_conditionals02_1Faults_Fault2_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_conditionals02_1Faults_Fault2_comparison'))

    def test_AFW_conditionals02_1Faults_Fault2_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_conditionals02_1Faults_Fault2_dependency'))

    def test_AFW_conditionals02_1Faults_Fault2_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_conditionals02_1Faults_Fault2_value'))

    def test_AFW_conditionals02_1Faults_Fault3_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_conditionals02_1Faults_Fault3_comparison'))

    def test_AFW_conditionals02_1Faults_Fault3_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_conditionals02_1Faults_Fault3_dependency'))

    def test_AFW_conditionals02_1Faults_Fault3_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_conditionals02_1Faults_Fault3_value'))

    def test_AFW_conditionals02_2Faults_Fault1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_conditionals02_2Faults_Fault1_comparison'))

    def test_AFW_conditionals02_2Faults_Fault1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_conditionals02_2Faults_Fault1_dependency'))

    def test_AFW_conditionals02_2Faults_Fault1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_conditionals02_2Faults_Fault1_value'))

    def test_AFW_conditionals02_2Faults_Fault2_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_conditionals02_2Faults_Fault2_comparison'))

    def test_AFW_conditionals02_2Faults_Fault2_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_conditionals02_2Faults_Fault2_dependency'))

    def test_AFW_conditionals02_2Faults_Fault2_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_conditionals02_2Faults_Fault2_value'))

    def test_AFW_conditionals02_2Faults_Fault3_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_conditionals02_2Faults_Fault3_comparison'))

    def test_AFW_conditionals02_2Faults_Fault3_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_conditionals02_2Faults_Fault3_dependency'))

    def test_AFW_conditionals02_2Faults_Fault3_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_conditionals02_2Faults_Fault3_value'))

    def test_AFW_conditionals02_3Faults_Fault1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_conditionals02_3Faults_Fault1_comparison'))

    def test_AFW_conditionals02_3Faults_Fault1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_conditionals02_3Faults_Fault1_dependency'))

    def test_AFW_conditionals02_3Faults_Fault1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_conditionals02_3Faults_Fault1_value'))

    def test_AFW_dice_rolling_1Faults_Fault1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_dice_rolling_1Faults_Fault1_comparison'))

    def test_AFW_dice_rolling_1Faults_Fault1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_dice_rolling_1Faults_Fault1_dependency'))

    def test_AFW_dice_rolling_1Faults_Fault1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_dice_rolling_1Faults_Fault1_value'))

    def test_AFW_dice_rolling_2Faults_Fault1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_dice_rolling_2Faults_Fault1_comparison'))

    def test_AFW_dice_rolling_2Faults_Fault1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_dice_rolling_2Faults_Fault1_dependency'))

    def test_AFW_dice_rolling_2Faults_Fault1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_dice_rolling_2Faults_Fault1_value'))

    def test_AFW_dice_rolling_2Faults_Fault2_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_dice_rolling_2Faults_Fault2_comparison'))

    def test_AFW_dice_rolling_2Faults_Fault2_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_dice_rolling_2Faults_Fault2_dependency'))

    def test_AFW_dice_rolling_2Faults_Fault2_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_dice_rolling_2Faults_Fault2_value'))

    def test_AFW_dice_rolling_2Faults_Fault3_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_dice_rolling_2Faults_Fault3_comparison'))

    def test_AFW_dice_rolling_2Faults_Fault3_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_dice_rolling_2Faults_Fault3_dependency'))

    def test_AFW_dice_rolling_2Faults_Fault3_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_dice_rolling_2Faults_Fault3_value'))

    def test_AFW_dice_rolling_3Faults_Fault1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_dice_rolling_3Faults_Fault1_comparison'))

    def test_AFW_dice_rolling_3Faults_Fault1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_dice_rolling_3Faults_Fault1_dependency'))

    def test_AFW_dice_rolling_3Faults_Fault1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_dice_rolling_3Faults_Fault1_value'))

    def test_AFW_energy_1Faults_Fault2_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_energy_1Faults_Fault2_comparison'))

    def test_AFW_energy_1Faults_Fault2_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_energy_1Faults_Fault2_dependency'))

    def test_AFW_energy_1Faults_Fault2_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_energy_1Faults_Fault2_value'))

    def test_AFW_energy_1Faults_Fault3_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_energy_1Faults_Fault3_comparison'))

    def test_AFW_energy_1Faults_Fault3_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_energy_1Faults_Fault3_dependency'))

    def test_AFW_energy_1Faults_Fault3_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_energy_1Faults_Fault3_value'))

    def test_AFW_energy_1Faults_Fault5_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_energy_1Faults_Fault5_comparison'))

    def test_AFW_energy_1Faults_Fault5_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_energy_1Faults_Fault5_dependency'))

    def test_AFW_energy_1Faults_Fault5_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_energy_1Faults_Fault5_value'))

    def test_AFW_energy_2Faults_Fault2_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_energy_2Faults_Fault2_comparison'))

    def test_AFW_energy_2Faults_Fault2_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_energy_2Faults_Fault2_dependency'))

    def test_AFW_energy_2Faults_Fault2_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_energy_2Faults_Fault2_value'))

    def test_AFW_euclidean_algorithm_1Faults_Fault1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_euclidean_algorithm_1Faults_Fault1_comparison'))

    def test_AFW_euclidean_algorithm_1Faults_Fault1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_euclidean_algorithm_1Faults_Fault1_dependency'))

    def test_AFW_euclidean_algorithm_1Faults_Fault1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_euclidean_algorithm_1Faults_Fault1_value'))

    def test_AFW_euclidean_algorithm_1Faults_Fault2_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_euclidean_algorithm_1Faults_Fault2_comparison'))

    def test_AFW_euclidean_algorithm_1Faults_Fault2_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_euclidean_algorithm_1Faults_Fault2_dependency'))

    def test_AFW_euclidean_algorithm_1Faults_Fault2_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_euclidean_algorithm_1Faults_Fault2_value'))

    def test_AFW_euclidean_algorithm_1Faults_Fault3_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_euclidean_algorithm_1Faults_Fault3_comparison'))

    def test_AFW_euclidean_algorithm_1Faults_Fault3_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_euclidean_algorithm_1Faults_Fault3_dependency'))

    def test_AFW_euclidean_algorithm_1Faults_Fault3_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_euclidean_algorithm_1Faults_Fault3_value'))

    def test_AFW_euclidean_algorithm_2Faults_Fault1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_euclidean_algorithm_2Faults_Fault1_comparison'))

    def test_AFW_euclidean_algorithm_2Faults_Fault1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_euclidean_algorithm_2Faults_Fault1_dependency'))

    def test_AFW_euclidean_algorithm_2Faults_Fault1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_euclidean_algorithm_2Faults_Fault1_value'))

    def test_AFW_euclidean_algorithm_2Faults_Fault2_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_euclidean_algorithm_2Faults_Fault2_comparison'))

    def test_AFW_euclidean_algorithm_2Faults_Fault2_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_euclidean_algorithm_2Faults_Fault2_dependency'))

    def test_AFW_euclidean_algorithm_2Faults_Fault2_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_euclidean_algorithm_2Faults_Fault2_value'))

    def test_AFW_euclidean_algorithm_2Faults_Fault3_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_euclidean_algorithm_2Faults_Fault3_comparison'))

    def test_AFW_euclidean_algorithm_2Faults_Fault3_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_euclidean_algorithm_2Faults_Fault3_dependency'))

    def test_AFW_euclidean_algorithm_2Faults_Fault3_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_euclidean_algorithm_2Faults_Fault3_value'))

    def test_AFW_euclidean_algorithm_3Faults_Fault1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_euclidean_algorithm_3Faults_Fault1_comparison'))

    def test_AFW_euclidean_algorithm_3Faults_Fault1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_euclidean_algorithm_3Faults_Fault1_dependency'))

    def test_AFW_euclidean_algorithm_3Faults_Fault1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_euclidean_algorithm_3Faults_Fault1_value'))

    def test_AFW_euclidean_algorithm_small_1Faults_Fault1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_euclidean_algorithm_small_1Faults_Fault1_comparison'))

    def test_AFW_euclidean_algorithm_small_1Faults_Fault1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_euclidean_algorithm_small_1Faults_Fault1_dependency'))

    def test_AFW_euclidean_algorithm_small_1Faults_Fault1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_euclidean_algorithm_small_1Faults_Fault1_value'))

    def test_AFW_euclidean_algorithm_small_1Faults_Fault2_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_euclidean_algorithm_small_1Faults_Fault2_comparison'))

    def test_AFW_euclidean_algorithm_small_1Faults_Fault2_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_euclidean_algorithm_small_1Faults_Fault2_dependency'))

    def test_AFW_euclidean_algorithm_small_1Faults_Fault2_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_euclidean_algorithm_small_1Faults_Fault2_value'))

    def test_AFW_euclidean_algorithm_small_1Faults_Fault3_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_euclidean_algorithm_small_1Faults_Fault3_comparison'))

    def test_AFW_euclidean_algorithm_small_1Faults_Fault3_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_euclidean_algorithm_small_1Faults_Fault3_dependency'))

    def test_AFW_euclidean_algorithm_small_1Faults_Fault3_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_euclidean_algorithm_small_1Faults_Fault3_value'))

    def test_AFW_euclidean_algorithm_small_2Faults_Fault1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_euclidean_algorithm_small_2Faults_Fault1_comparison'))

    def test_AFW_euclidean_algorithm_small_2Faults_Fault1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_euclidean_algorithm_small_2Faults_Fault1_dependency'))

    def test_AFW_euclidean_algorithm_small_2Faults_Fault1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_euclidean_algorithm_small_2Faults_Fault1_value'))

    def test_AFW_euclidean_algorithm_small_2Faults_Fault2_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_euclidean_algorithm_small_2Faults_Fault2_comparison'))

    def test_AFW_euclidean_algorithm_small_2Faults_Fault2_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_euclidean_algorithm_small_2Faults_Fault2_dependency'))

    def test_AFW_euclidean_algorithm_small_2Faults_Fault2_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_euclidean_algorithm_small_2Faults_Fault2_value'))

    def test_AFW_euclidean_algorithm_small_2Faults_Fault3_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_euclidean_algorithm_small_2Faults_Fault3_comparison'))

    def test_AFW_euclidean_algorithm_small_2Faults_Fault3_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_euclidean_algorithm_small_2Faults_Fault3_dependency'))

    def test_AFW_euclidean_algorithm_small_2Faults_Fault3_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_euclidean_algorithm_small_2Faults_Fault3_value'))

    def test_AFW_euclidean_algorithm_small_3Faults_Fault1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_euclidean_algorithm_small_3Faults_Fault1_comparison'))

    def test_AFW_euclidean_algorithm_small_3Faults_Fault1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_euclidean_algorithm_small_3Faults_Fault1_dependency'))

    def test_AFW_euclidean_algorithm_small_3Faults_Fault1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_euclidean_algorithm_small_3Faults_Fault1_value'))

    def test_AFW_fibonacci_1Faults_Fault1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_fibonacci_1Faults_Fault1_comparison'))

    def test_AFW_fibonacci_1Faults_Fault1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_fibonacci_1Faults_Fault1_dependency'))

    def test_AFW_fibonacci_1Faults_Fault1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_fibonacci_1Faults_Fault1_value'))

    def test_AFW_fibonacci_1Faults_Fault2_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_fibonacci_1Faults_Fault2_comparison'))

    def test_AFW_fibonacci_1Faults_Fault2_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_fibonacci_1Faults_Fault2_dependency'))

    def test_AFW_fibonacci_1Faults_Fault2_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_fibonacci_1Faults_Fault2_value'))

    def test_AFW_fibonacci_1Faults_Fault3_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_fibonacci_1Faults_Fault3_comparison'))

    def test_AFW_fibonacci_1Faults_Fault3_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_fibonacci_1Faults_Fault3_dependency'))

    def test_AFW_fibonacci_1Faults_Fault3_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_fibonacci_1Faults_Fault3_value'))

    def test_AFW_fibonacci_2Faults_Fault1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_fibonacci_2Faults_Fault1_comparison'))

    def test_AFW_fibonacci_2Faults_Fault1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_fibonacci_2Faults_Fault1_dependency'))

    def test_AFW_fibonacci_2Faults_Fault1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_fibonacci_2Faults_Fault1_value'))

    def test_AFW_fibonacci_2Faults_Fault2_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_fibonacci_2Faults_Fault2_comparison'))

    def test_AFW_fibonacci_2Faults_Fault2_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_fibonacci_2Faults_Fault2_dependency'))

    def test_AFW_fibonacci_2Faults_Fault2_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_fibonacci_2Faults_Fault2_value'))

    def test_AFW_fibonacci_2Faults_Fault3_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_fibonacci_2Faults_Fault3_comparison'))

    def test_AFW_fibonacci_2Faults_Fault3_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_fibonacci_2Faults_Fault3_dependency'))

    def test_AFW_fibonacci_2Faults_Fault3_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_fibonacci_2Faults_Fault3_value'))

    def test_AFW_fibonacci_3Faults_Fault1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_fibonacci_3Faults_Fault1_comparison'))

    def test_AFW_fibonacci_3Faults_Fault1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_fibonacci_3Faults_Fault1_dependency'))

    def test_AFW_fibonacci_3Faults_Fault1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_fibonacci_3Faults_Fault1_value'))

    def test_AFW_matrix_1Faults_Fault1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_matrix_1Faults_Fault1_comparison'))

    def test_AFW_matrix_1Faults_Fault1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_matrix_1Faults_Fault1_dependency'))

    def test_AFW_matrix_1Faults_Fault1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_matrix_1Faults_Fault1_value'))

    def test_AFW_matrix_1Faults_Fault2_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_matrix_1Faults_Fault2_comparison'))

    def test_AFW_matrix_1Faults_Fault2_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_matrix_1Faults_Fault2_dependency'))

    def test_AFW_matrix_1Faults_Fault2_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_matrix_1Faults_Fault2_value'))

    def test_AFW_matrix_2Faults_Fault1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_matrix_2Faults_Fault1_comparison'))

    def test_AFW_matrix_2Faults_Fault1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_matrix_2Faults_Fault1_dependency'))

    def test_AFW_matrix_2Faults_Fault1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_matrix_2Faults_Fault1_value'))

    def test_AFW_matrix_2Faults_Fault2_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_matrix_2Faults_Fault2_comparison'))

    def test_AFW_matrix_2Faults_Fault2_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_matrix_2Faults_Fault2_dependency'))

    def test_AFW_matrix_2Faults_Fault2_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_matrix_2Faults_Fault2_value'))

    def test_AFW_matrix_2Faults_Fault3_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_matrix_2Faults_Fault3_comparison'))

    def test_AFW_matrix_2Faults_Fault3_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_matrix_2Faults_Fault3_dependency'))

    def test_AFW_matrix_2Faults_Fault3_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_matrix_2Faults_Fault3_value'))

    def test_AFW_matrix_3Faults_Fault1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_matrix_3Faults_Fault1_comparison'))

    def test_AFW_matrix_3Faults_Fault1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_matrix_3Faults_Fault1_dependency'))

    def test_AFW_matrix_3Faults_Fault1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_matrix_3Faults_Fault1_value'))

    def test_AFW_oscars2012_1Faults_Fault3_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_oscars2012_1Faults_Fault3_comparison'))

    def test_AFW_oscars2012_1Faults_Fault3_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_oscars2012_1Faults_Fault3_dependency'))

    def test_AFW_oscars2012_1Faults_Fault3_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_oscars2012_1Faults_Fault3_value'))

    def test_AFW_oscars2012_1Faults_Fault4_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_oscars2012_1Faults_Fault4_comparison'))

    def test_AFW_oscars2012_1Faults_Fault4_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_oscars2012_1Faults_Fault4_dependency'))

    def test_AFW_oscars2012_1Faults_Fault4_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_oscars2012_1Faults_Fault4_value'))

    def test_AFW_oscars2012_1Faults_Fault5_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_oscars2012_1Faults_Fault5_comparison'))

    def test_AFW_oscars2012_1Faults_Fault5_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_oscars2012_1Faults_Fault5_dependency'))

    def test_AFW_oscars2012_1Faults_Fault5_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_oscars2012_1Faults_Fault5_value'))

    def test_AFW_oscars2012_2Faults_Fault2_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_oscars2012_2Faults_Fault2_comparison'))

    def test_AFW_oscars2012_2Faults_Fault2_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_oscars2012_2Faults_Fault2_dependency'))

    def test_AFW_oscars2012_2Faults_Fault2_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_oscars2012_2Faults_Fault2_value'))

    def test_AFW_parabola_1Faults_Fault1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_parabola_1Faults_Fault1_comparison'))

    def test_AFW_parabola_1Faults_Fault1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_parabola_1Faults_Fault1_dependency'))

    def test_AFW_parabola_1Faults_Fault1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_parabola_1Faults_Fault1_value'))

    def test_AFW_parabola_2Faults_Fault1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_parabola_2Faults_Fault1_comparison'))

    def test_AFW_parabola_2Faults_Fault1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_parabola_2Faults_Fault1_dependency'))

    def test_AFW_parabola_2Faults_Fault1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_parabola_2Faults_Fault1_value'))

    def test_AFW_parabola_2Faults_Fault2_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_parabola_2Faults_Fault2_comparison'))

    def test_AFW_parabola_2Faults_Fault2_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_parabola_2Faults_Fault2_dependency'))

    def test_AFW_parabola_2Faults_Fault2_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_parabola_2Faults_Fault2_value'))

    def test_AFW_parabola_2Faults_Fault3_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_parabola_2Faults_Fault3_comparison'))

    def test_AFW_parabola_2Faults_Fault3_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_parabola_2Faults_Fault3_dependency'))

    def test_AFW_parabola_2Faults_Fault3_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_parabola_2Faults_Fault3_value'))

    def test_AFW_parabola_3Faults_Fault1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_parabola_3Faults_Fault1_comparison'))

    def test_AFW_parabola_3Faults_Fault1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_parabola_3Faults_Fault1_dependency'))

    def test_AFW_parabola_3Faults_Fault1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_parabola_3Faults_Fault1_value'))

    def test_AFW_prom_calculator_1Faults_Fault1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_prom_calculator_1Faults_Fault1_comparison'))

    def test_AFW_prom_calculator_1Faults_Fault1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_prom_calculator_1Faults_Fault1_dependency'))

    def test_AFW_prom_calculator_1Faults_Fault1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_prom_calculator_1Faults_Fault1_value'))

    def test_AFW_prom_calculator_2Faults_Fault1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_prom_calculator_2Faults_Fault1_comparison'))

    def test_AFW_prom_calculator_2Faults_Fault1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_prom_calculator_2Faults_Fault1_dependency'))

    def test_AFW_prom_calculator_2Faults_Fault1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_prom_calculator_2Faults_Fault1_value'))

    def test_AFW_prom_calculator_2Faults_Fault2_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_prom_calculator_2Faults_Fault2_comparison'))

    def test_AFW_prom_calculator_2Faults_Fault2_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_prom_calculator_2Faults_Fault2_dependency'))

    def test_AFW_prom_calculator_2Faults_Fault2_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_prom_calculator_2Faults_Fault2_value'))

    def test_AFW_prom_calculator_2Faults_Fault3_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_prom_calculator_2Faults_Fault3_comparison'))

    def test_AFW_prom_calculator_2Faults_Fault3_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_prom_calculator_2Faults_Fault3_dependency'))

    def test_AFW_prom_calculator_2Faults_Fault3_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_prom_calculator_2Faults_Fault3_value'))

    def test_AFW_prom_calculator_3Faults_Fault1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_prom_calculator_3Faults_Fault1_comparison'))

    def test_AFW_prom_calculator_3Faults_Fault1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_prom_calculator_3Faults_Fault1_dependency'))

    def test_AFW_prom_calculator_3Faults_Fault1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_prom_calculator_3Faults_Fault1_value'))

    def test_AFW_ranking_1Faults_Fault1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_ranking_1Faults_Fault1_comparison'))

    def test_AFW_ranking_1Faults_Fault1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_ranking_1Faults_Fault1_dependency'))

    def test_AFW_ranking_1Faults_Fault1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_ranking_1Faults_Fault1_value'))

    def test_AFW_ranking_1Faults_Fault2_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_ranking_1Faults_Fault2_comparison'))

    def test_AFW_ranking_1Faults_Fault2_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_ranking_1Faults_Fault2_dependency'))

    def test_AFW_ranking_1Faults_Fault2_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_ranking_1Faults_Fault2_value'))

    def test_AFW_ranking_2Faults_Fault1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_ranking_2Faults_Fault1_comparison'))

    def test_AFW_ranking_2Faults_Fault1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_ranking_2Faults_Fault1_dependency'))

    def test_AFW_ranking_2Faults_Fault1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_ranking_2Faults_Fault1_value'))

    def test_AFW_ranking_2Faults_Fault2_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_ranking_2Faults_Fault2_comparison'))

    def test_AFW_ranking_2Faults_Fault2_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_ranking_2Faults_Fault2_dependency'))

    def test_AFW_ranking_2Faults_Fault2_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_ranking_2Faults_Fault2_value'))

    def test_AFW_ranking_2Faults_Fault3_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_ranking_2Faults_Fault3_comparison'))

    def test_AFW_ranking_2Faults_Fault3_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_ranking_2Faults_Fault3_dependency'))

    def test_AFW_ranking_2Faults_Fault3_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_ranking_2Faults_Fault3_value'))

    def test_AFW_ranking_3Faults_Fault1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_ranking_3Faults_Fault1_comparison'))

    def test_AFW_ranking_3Faults_Fault1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_ranking_3Faults_Fault1_dependency'))

    def test_AFW_ranking_3Faults_Fault1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_ranking_3Faults_Fault1_value'))

    def test_AFW_shares_1Faults_Fault1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_shares_1Faults_Fault1_comparison'))

    def test_AFW_shares_1Faults_Fault1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_shares_1Faults_Fault1_dependency'))

    def test_AFW_shares_1Faults_Fault1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_shares_1Faults_Fault1_value'))

    def test_AFW_shares_1Faults_Fault2_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_shares_1Faults_Fault2_comparison'))

    def test_AFW_shares_1Faults_Fault2_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_shares_1Faults_Fault2_dependency'))

    def test_AFW_shares_1Faults_Fault2_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_shares_1Faults_Fault2_value'))

    def test_AFW_shares_1Faults_Fault3_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_shares_1Faults_Fault3_comparison'))

    def test_AFW_shares_1Faults_Fault3_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_shares_1Faults_Fault3_dependency'))

    def test_AFW_shares_1Faults_Fault3_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_shares_1Faults_Fault3_value'))

    def test_AFW_shares_1Faults_Fault4_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_shares_1Faults_Fault4_comparison'))

    def test_AFW_shares_1Faults_Fault4_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_shares_1Faults_Fault4_dependency'))

    def test_AFW_shares_1Faults_Fault4_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_shares_1Faults_Fault4_value'))

    def test_AFW_shares_1Faults_Fault5_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_shares_1Faults_Fault5_comparison'))

    def test_AFW_shares_1Faults_Fault5_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_shares_1Faults_Fault5_dependency'))

    def test_AFW_shares_1Faults_Fault5_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_shares_1Faults_Fault5_value'))

    def test_AFW_shares_2Faults_Fault1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_shares_2Faults_Fault1_comparison'))

    def test_AFW_shares_2Faults_Fault1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_shares_2Faults_Fault1_dependency'))

    def test_AFW_shares_2Faults_Fault1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_shares_2Faults_Fault1_value'))

    def test_AFW_shares_2Faults_Fault2_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_shares_2Faults_Fault2_comparison'))

    def test_AFW_shares_2Faults_Fault2_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_shares_2Faults_Fault2_dependency'))

    def test_AFW_shares_2Faults_Fault2_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_shares_2Faults_Fault2_value'))

    def test_AFW_shares_2Faults_Fault3_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_shares_2Faults_Fault3_comparison'))

    def test_AFW_shares_2Faults_Fault3_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_shares_2Faults_Fault3_dependency'))

    def test_AFW_shares_2Faults_Fault3_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_shares_2Faults_Fault3_value'))

    def test_AFW_shares_3Faults_Fault1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_shares_3Faults_Fault1_comparison'))

    def test_AFW_shares_3Faults_Fault1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_shares_3Faults_Fault1_dependency'))

    def test_AFW_shares_3Faults_Fault1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_shares_3Faults_Fault1_value'))

    def test_AFW_shopping_bedroom1_1Faults_Fault1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_shopping_bedroom1_1Faults_Fault1_comparison'))

    def test_AFW_shopping_bedroom1_1Faults_Fault1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_shopping_bedroom1_1Faults_Fault1_dependency'))

    def test_AFW_shopping_bedroom1_1Faults_Fault1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_shopping_bedroom1_1Faults_Fault1_value'))

    def test_AFW_shopping_bedroom1_1Faults_Fault2_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_shopping_bedroom1_1Faults_Fault2_comparison'))

    def test_AFW_shopping_bedroom1_1Faults_Fault2_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_shopping_bedroom1_1Faults_Fault2_dependency'))

    def test_AFW_shopping_bedroom1_1Faults_Fault2_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_shopping_bedroom1_1Faults_Fault2_value'))

    def test_AFW_shopping_bedroom1_2Faults_Fault1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_shopping_bedroom1_2Faults_Fault1_comparison'))

    def test_AFW_shopping_bedroom1_2Faults_Fault1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_shopping_bedroom1_2Faults_Fault1_dependency'))

    def test_AFW_shopping_bedroom1_2Faults_Fault1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_shopping_bedroom1_2Faults_Fault1_value'))

    def test_AFW_shopping_bedroom1_2Faults_Fault2_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_shopping_bedroom1_2Faults_Fault2_comparison'))

    def test_AFW_shopping_bedroom1_2Faults_Fault2_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_shopping_bedroom1_2Faults_Fault2_dependency'))

    def test_AFW_shopping_bedroom1_2Faults_Fault2_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_shopping_bedroom1_2Faults_Fault2_value'))

    def test_AFW_shopping_bedroom1_2Faults_Fault3_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_shopping_bedroom1_2Faults_Fault3_comparison'))

    def test_AFW_shopping_bedroom1_2Faults_Fault3_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_shopping_bedroom1_2Faults_Fault3_dependency'))

    def test_AFW_shopping_bedroom1_2Faults_Fault3_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_shopping_bedroom1_2Faults_Fault3_value'))

    def test_AFW_shopping_bedroom1_3Faults_Fault1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_shopping_bedroom1_3Faults_Fault1_comparison'))

    def test_AFW_shopping_bedroom1_3Faults_Fault1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_shopping_bedroom1_3Faults_Fault1_dependency'))

    def test_AFW_shopping_bedroom1_3Faults_Fault1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_shopping_bedroom1_3Faults_Fault1_value'))

    def test_AFW_shopping_bedroom2_1Faults_Fault1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_shopping_bedroom2_1Faults_Fault1_comparison'))

    def test_AFW_shopping_bedroom2_1Faults_Fault1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_shopping_bedroom2_1Faults_Fault1_dependency'))

    def test_AFW_shopping_bedroom2_1Faults_Fault1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_shopping_bedroom2_1Faults_Fault1_value'))

    def test_AFW_shopping_bedroom2_1Faults_Fault2_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_shopping_bedroom2_1Faults_Fault2_comparison'))

    def test_AFW_shopping_bedroom2_1Faults_Fault2_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_shopping_bedroom2_1Faults_Fault2_dependency'))

    def test_AFW_shopping_bedroom2_1Faults_Fault2_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_shopping_bedroom2_1Faults_Fault2_value'))

    def test_AFW_shopping_bedroom2_1Faults_Fault3_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_shopping_bedroom2_1Faults_Fault3_comparison'))

    def test_AFW_shopping_bedroom2_1Faults_Fault3_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_shopping_bedroom2_1Faults_Fault3_dependency'))

    def test_AFW_shopping_bedroom2_1Faults_Fault3_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_shopping_bedroom2_1Faults_Fault3_value'))

    def test_AFW_shopping_bedroom2_1Faults_Fault4_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_shopping_bedroom2_1Faults_Fault4_comparison'))

    def test_AFW_shopping_bedroom2_1Faults_Fault4_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_shopping_bedroom2_1Faults_Fault4_dependency'))

    def test_AFW_shopping_bedroom2_1Faults_Fault4_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_shopping_bedroom2_1Faults_Fault4_value'))

    def test_AFW_shopping_bedroom2_1Faults_Fault5_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_shopping_bedroom2_1Faults_Fault5_comparison'))

    def test_AFW_shopping_bedroom2_1Faults_Fault5_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_shopping_bedroom2_1Faults_Fault5_dependency'))

    def test_AFW_shopping_bedroom2_1Faults_Fault5_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_shopping_bedroom2_1Faults_Fault5_value'))

    def test_AFW_shopping_bedroom2_2Faults_Fault1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_shopping_bedroom2_2Faults_Fault1_comparison'))

    def test_AFW_shopping_bedroom2_2Faults_Fault1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_shopping_bedroom2_2Faults_Fault1_dependency'))

    def test_AFW_shopping_bedroom2_2Faults_Fault1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_shopping_bedroom2_2Faults_Fault1_value'))

    def test_AFW_shopping_bedroom2_2Faults_Fault2_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_shopping_bedroom2_2Faults_Fault2_comparison'))

    def test_AFW_shopping_bedroom2_2Faults_Fault2_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_shopping_bedroom2_2Faults_Fault2_dependency'))

    def test_AFW_shopping_bedroom2_2Faults_Fault2_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_shopping_bedroom2_2Faults_Fault2_value'))

    def test_AFW_shopping_bedroom2_2Faults_Fault3_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_shopping_bedroom2_2Faults_Fault3_comparison'))

    def test_AFW_shopping_bedroom2_2Faults_Fault3_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_shopping_bedroom2_2Faults_Fault3_dependency'))

    def test_AFW_shopping_bedroom2_2Faults_Fault3_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_shopping_bedroom2_2Faults_Fault3_value'))

    def test_AFW_shopping_bedroom2_3Faults_Fault1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_shopping_bedroom2_3Faults_Fault1_comparison'))

    def test_AFW_shopping_bedroom2_3Faults_Fault1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_shopping_bedroom2_3Faults_Fault1_dependency'))

    def test_AFW_shopping_bedroom2_3Faults_Fault1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_shopping_bedroom2_3Faults_Fault1_value'))

    def test_AFW_training_1Faults_Fault1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_training_1Faults_Fault1_comparison'))

    def test_AFW_training_1Faults_Fault1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_training_1Faults_Fault1_dependency'))

    def test_AFW_training_1Faults_Fault1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_training_1Faults_Fault1_value'))

    def test_AFW_training_1Faults_Fault2_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_training_1Faults_Fault2_comparison'))

    def test_AFW_training_1Faults_Fault2_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_training_1Faults_Fault2_dependency'))

    def test_AFW_training_1Faults_Fault2_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_training_1Faults_Fault2_value'))

    def test_AFW_training_1Faults_Fault3_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_training_1Faults_Fault3_comparison'))

    def test_AFW_training_1Faults_Fault3_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_training_1Faults_Fault3_dependency'))

    def test_AFW_training_1Faults_Fault3_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_training_1Faults_Fault3_value'))

    def test_AFW_training_1Faults_Fault4_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_training_1Faults_Fault4_comparison'))

    def test_AFW_training_1Faults_Fault4_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_training_1Faults_Fault4_dependency'))

    def test_AFW_training_1Faults_Fault4_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_training_1Faults_Fault4_value'))

    def test_AFW_training_1Faults_Fault5_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_training_1Faults_Fault5_comparison'))

    def test_AFW_training_1Faults_Fault5_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_training_1Faults_Fault5_dependency'))

    def test_AFW_training_1Faults_Fault5_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_training_1Faults_Fault5_value'))

    def test_AFW_training_2Faults_Fault1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_training_2Faults_Fault1_comparison'))

    def test_AFW_training_2Faults_Fault1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_training_2Faults_Fault1_dependency'))

    def test_AFW_training_2Faults_Fault1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_training_2Faults_Fault1_value'))

    def test_AFW_training_2Faults_Fault2_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_training_2Faults_Fault2_comparison'))

    def test_AFW_training_2Faults_Fault2_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_training_2Faults_Fault2_dependency'))

    def test_AFW_training_2Faults_Fault2_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_training_2Faults_Fault2_value'))

    def test_AFW_training_2Faults_Fault3_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_training_2Faults_Fault3_comparison'))

    def test_AFW_training_2Faults_Fault3_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_training_2Faults_Fault3_dependency'))

    def test_AFW_training_2Faults_Fault3_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_training_2Faults_Fault3_value'))

    def test_AFW_training_3Faults_Fault1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_training_3Faults_Fault1_comparison'))

    def test_AFW_training_3Faults_Fault1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_training_3Faults_Fault1_dependency'))

    def test_AFW_training_3Faults_Fault1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_training_3Faults_Fault1_value'))

    def test_AFW_weather_1Faults_Fault1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_weather_1Faults_Fault1_comparison'))

    def test_AFW_weather_1Faults_Fault1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_weather_1Faults_Fault1_dependency'))

    def test_AFW_weather_1Faults_Fault1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_weather_1Faults_Fault1_value'))

    def test_AFW_weather_1Faults_Fault2_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_weather_1Faults_Fault2_comparison'))

    def test_AFW_weather_1Faults_Fault2_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_weather_1Faults_Fault2_dependency'))

    def test_AFW_weather_1Faults_Fault2_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_weather_1Faults_Fault2_value'))

    def test_AFW_weather_1Faults_Fault3_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_weather_1Faults_Fault3_comparison'))

    def test_AFW_weather_1Faults_Fault3_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_weather_1Faults_Fault3_dependency'))

    def test_AFW_weather_1Faults_Fault3_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_weather_1Faults_Fault3_value'))

    def test_AFW_weather_1Faults_Fault4_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_weather_1Faults_Fault4_comparison'))

    def test_AFW_weather_1Faults_Fault4_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_weather_1Faults_Fault4_dependency'))

    def test_AFW_weather_1Faults_Fault4_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_weather_1Faults_Fault4_value'))

    def test_AFW_weather_2Faults_Fault1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_weather_2Faults_Fault1_comparison'))

    def test_AFW_weather_2Faults_Fault1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_weather_2Faults_Fault1_dependency'))

    def test_AFW_weather_2Faults_Fault1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_weather_2Faults_Fault1_value'))

    def test_AFW_weather_2Faults_Fault2_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_weather_2Faults_Fault2_comparison'))

    def test_AFW_weather_2Faults_Fault2_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_weather_2Faults_Fault2_dependency'))

    def test_AFW_weather_2Faults_Fault2_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_weather_2Faults_Fault2_value'))

    def test_AFW_weather_2Faults_Fault3_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_weather_2Faults_Fault3_comparison'))

    def test_AFW_weather_2Faults_Fault3_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_weather_2Faults_Fault3_dependency'))

    def test_AFW_weather_2Faults_Fault3_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_weather_2Faults_Fault3_value'))

    def test_AFW_weather_3Faults_Fault1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_weather_3Faults_Fault1_comparison'))

    def test_AFW_weather_3Faults_Fault1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_weather_3Faults_Fault1_dependency'))

    def test_AFW_weather_3Faults_Fault1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_weather_3Faults_Fault1_value'))

    def test_AFW_wimbledon2012_1Faults_Fault1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_wimbledon2012_1Faults_Fault1_comparison'))

    def test_AFW_wimbledon2012_1Faults_Fault1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_wimbledon2012_1Faults_Fault1_dependency'))

    def test_AFW_wimbledon2012_1Faults_Fault1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_wimbledon2012_1Faults_Fault1_value'))

    def test_AFW_wimbledon2012_1Faults_Fault2_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_wimbledon2012_1Faults_Fault2_comparison'))

    def test_AFW_wimbledon2012_1Faults_Fault2_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_wimbledon2012_1Faults_Fault2_dependency'))

    def test_AFW_wimbledon2012_1Faults_Fault2_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_wimbledon2012_1Faults_Fault2_value'))

    def test_AFW_wimbledon2012_1Faults_Fault3_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_wimbledon2012_1Faults_Fault3_comparison'))

    def test_AFW_wimbledon2012_1Faults_Fault3_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_wimbledon2012_1Faults_Fault3_dependency'))

    def test_AFW_wimbledon2012_1Faults_Fault3_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_wimbledon2012_1Faults_Fault3_value'))

    def test_AFW_wimbledon2012_2Faults_Fault1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_wimbledon2012_2Faults_Fault1_comparison'))

    def test_AFW_wimbledon2012_2Faults_Fault1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_wimbledon2012_2Faults_Fault1_dependency'))

    def test_AFW_wimbledon2012_2Faults_Fault1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_wimbledon2012_2Faults_Fault1_value'))

    def test_AFW_wimbledon2012_2Faults_Fault2_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_wimbledon2012_2Faults_Fault2_comparison'))

    def test_AFW_wimbledon2012_2Faults_Fault2_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_wimbledon2012_2Faults_Fault2_dependency'))

    def test_AFW_wimbledon2012_2Faults_Fault2_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_wimbledon2012_2Faults_Fault2_value'))

    def test_AFW_wimbledon2012_2Faults_Fault3_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_wimbledon2012_2Faults_Fault3_comparison'))

    def test_AFW_wimbledon2012_2Faults_Fault3_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_wimbledon2012_2Faults_Fault3_dependency'))

    def test_AFW_wimbledon2012_2Faults_Fault3_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_wimbledon2012_2Faults_Fault3_value'))

    def test_AFW_wimbledon2012_3Faults_Fault1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_wimbledon2012_3Faults_Fault1_comparison'))

    def test_AFW_wimbledon2012_3Faults_Fault1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_wimbledon2012_3Faults_Fault1_dependency'))

    def test_AFW_wimbledon2012_3Faults_Fault1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/AFW_wimbledon2012_3Faults_Fault1_value'))

    def test_circuit10_10_1FAULTS_FAULTVERSION1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_10_1FAULTS_FAULTVERSION1_comparison'))

    def test_circuit10_10_1FAULTS_FAULTVERSION1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_10_1FAULTS_FAULTVERSION1_dependency'))

    def test_circuit10_10_1FAULTS_FAULTVERSION1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_10_1FAULTS_FAULTVERSION1_value'))

    def test_circuit10_10_1FAULTS_FAULTVERSION2_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_10_1FAULTS_FAULTVERSION2_comparison'))

    def test_circuit10_10_1FAULTS_FAULTVERSION2_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_10_1FAULTS_FAULTVERSION2_dependency'))

    def test_circuit10_10_1FAULTS_FAULTVERSION2_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_10_1FAULTS_FAULTVERSION2_value'))

    def test_circuit10_10_1FAULTS_FAULTVERSION3_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_10_1FAULTS_FAULTVERSION3_comparison'))

    def test_circuit10_10_1FAULTS_FAULTVERSION3_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_10_1FAULTS_FAULTVERSION3_dependency'))

    def test_circuit10_10_1FAULTS_FAULTVERSION3_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_10_1FAULTS_FAULTVERSION3_value'))

    def test_circuit10_10_1FAULTS_FAULTVERSION4_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_10_1FAULTS_FAULTVERSION4_comparison'))

    def test_circuit10_10_1FAULTS_FAULTVERSION4_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_10_1FAULTS_FAULTVERSION4_dependency'))

    def test_circuit10_10_1FAULTS_FAULTVERSION4_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_10_1FAULTS_FAULTVERSION4_value'))

    def test_circuit10_10_1FAULTS_FAULTVERSION5_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_10_1FAULTS_FAULTVERSION5_comparison'))

    def test_circuit10_10_1FAULTS_FAULTVERSION5_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_10_1FAULTS_FAULTVERSION5_dependency'))

    def test_circuit10_10_1FAULTS_FAULTVERSION5_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_10_1FAULTS_FAULTVERSION5_value'))

    def test_circuit10_10_2FAULTS_FAULTVERSION1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_10_2FAULTS_FAULTVERSION1_comparison'))

    def test_circuit10_10_2FAULTS_FAULTVERSION1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_10_2FAULTS_FAULTVERSION1_dependency'))

    def test_circuit10_10_2FAULTS_FAULTVERSION1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_10_2FAULTS_FAULTVERSION1_value'))

    def test_circuit10_10_2FAULTS_FAULTVERSION2_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_10_2FAULTS_FAULTVERSION2_comparison'))

    def test_circuit10_10_2FAULTS_FAULTVERSION2_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_10_2FAULTS_FAULTVERSION2_dependency'))

    def test_circuit10_10_2FAULTS_FAULTVERSION2_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_10_2FAULTS_FAULTVERSION2_value'))

    def test_circuit10_10_2FAULTS_FAULTVERSION3_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_10_2FAULTS_FAULTVERSION3_comparison'))

    def test_circuit10_10_2FAULTS_FAULTVERSION3_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_10_2FAULTS_FAULTVERSION3_dependency'))

    def test_circuit10_10_2FAULTS_FAULTVERSION3_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_10_2FAULTS_FAULTVERSION3_value'))

    def test_circuit10_10_2FAULTS_FAULTVERSION4_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_10_2FAULTS_FAULTVERSION4_comparison'))

    def test_circuit10_10_2FAULTS_FAULTVERSION4_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_10_2FAULTS_FAULTVERSION4_dependency'))

    def test_circuit10_10_2FAULTS_FAULTVERSION4_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_10_2FAULTS_FAULTVERSION4_value'))

    def test_circuit10_10_2FAULTS_FAULTVERSION5_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_10_2FAULTS_FAULTVERSION5_comparison'))

    def test_circuit10_10_2FAULTS_FAULTVERSION5_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_10_2FAULTS_FAULTVERSION5_dependency'))

    def test_circuit10_10_2FAULTS_FAULTVERSION5_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_10_2FAULTS_FAULTVERSION5_value'))

    def test_circuit10_10_3FAULTS_FAULTVERSION1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_10_3FAULTS_FAULTVERSION1_comparison'))

    def test_circuit10_10_3FAULTS_FAULTVERSION1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_10_3FAULTS_FAULTVERSION1_dependency'))

    def test_circuit10_10_3FAULTS_FAULTVERSION1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_10_3FAULTS_FAULTVERSION1_value'))

    def test_circuit10_10_3FAULTS_FAULTVERSION2_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_10_3FAULTS_FAULTVERSION2_comparison'))

    def test_circuit10_10_3FAULTS_FAULTVERSION2_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_10_3FAULTS_FAULTVERSION2_dependency'))

    def test_circuit10_10_3FAULTS_FAULTVERSION2_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_10_3FAULTS_FAULTVERSION2_value'))

    def test_circuit10_10_3FAULTS_FAULTVERSION3_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_10_3FAULTS_FAULTVERSION3_comparison'))

    def test_circuit10_10_3FAULTS_FAULTVERSION3_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_10_3FAULTS_FAULTVERSION3_dependency'))

    def test_circuit10_10_3FAULTS_FAULTVERSION3_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_10_3FAULTS_FAULTVERSION3_value'))

    def test_circuit10_10_3FAULTS_FAULTVERSION4_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_10_3FAULTS_FAULTVERSION4_comparison'))

    def test_circuit10_10_3FAULTS_FAULTVERSION4_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_10_3FAULTS_FAULTVERSION4_dependency'))

    def test_circuit10_10_3FAULTS_FAULTVERSION4_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_10_3FAULTS_FAULTVERSION4_value'))

    def test_circuit10_10_3FAULTS_FAULTVERSION5_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_10_3FAULTS_FAULTVERSION5_comparison'))

    def test_circuit10_10_3FAULTS_FAULTVERSION5_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_10_3FAULTS_FAULTVERSION5_dependency'))

    def test_circuit10_10_3FAULTS_FAULTVERSION5_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_10_3FAULTS_FAULTVERSION5_value'))

    def test_circuit10_15_1FAULTS_FAULTVERSION1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_15_1FAULTS_FAULTVERSION1_comparison'))

    def test_circuit10_15_1FAULTS_FAULTVERSION1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_15_1FAULTS_FAULTVERSION1_dependency'))

    def test_circuit10_15_1FAULTS_FAULTVERSION1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_15_1FAULTS_FAULTVERSION1_value'))

    def test_circuit10_15_1FAULTS_FAULTVERSION2_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_15_1FAULTS_FAULTVERSION2_comparison'))

    def test_circuit10_15_1FAULTS_FAULTVERSION2_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_15_1FAULTS_FAULTVERSION2_dependency'))

    def test_circuit10_15_1FAULTS_FAULTVERSION2_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_15_1FAULTS_FAULTVERSION2_value'))

    def test_circuit10_15_1FAULTS_FAULTVERSION3_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_15_1FAULTS_FAULTVERSION3_comparison'))

    def test_circuit10_15_1FAULTS_FAULTVERSION3_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_15_1FAULTS_FAULTVERSION3_dependency'))

    def test_circuit10_15_1FAULTS_FAULTVERSION3_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_15_1FAULTS_FAULTVERSION3_value'))

    def test_circuit10_15_1FAULTS_FAULTVERSION4_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_15_1FAULTS_FAULTVERSION4_comparison'))

    def test_circuit10_15_1FAULTS_FAULTVERSION4_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_15_1FAULTS_FAULTVERSION4_dependency'))

    def test_circuit10_15_1FAULTS_FAULTVERSION4_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_15_1FAULTS_FAULTVERSION4_value'))

    def test_circuit10_15_2FAULTS_FAULTVERSION3_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_15_2FAULTS_FAULTVERSION3_comparison'))

    def test_circuit10_15_2FAULTS_FAULTVERSION3_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_15_2FAULTS_FAULTVERSION3_dependency'))

    def test_circuit10_15_2FAULTS_FAULTVERSION3_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_15_2FAULTS_FAULTVERSION3_value'))

    def test_circuit10_15_2FAULTS_FAULTVERSION4_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_15_2FAULTS_FAULTVERSION4_comparison'))

    def test_circuit10_15_2FAULTS_FAULTVERSION4_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_15_2FAULTS_FAULTVERSION4_dependency'))

    def test_circuit10_15_2FAULTS_FAULTVERSION4_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_15_2FAULTS_FAULTVERSION4_value'))

    def test_circuit10_15_2FAULTS_FAULTVERSION5_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_15_2FAULTS_FAULTVERSION5_comparison'))

    def test_circuit10_15_2FAULTS_FAULTVERSION5_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_15_2FAULTS_FAULTVERSION5_dependency'))

    def test_circuit10_15_2FAULTS_FAULTVERSION5_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_15_2FAULTS_FAULTVERSION5_value'))

    def test_circuit10_15_3FAULTS_FAULTVERSION1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_15_3FAULTS_FAULTVERSION1_comparison'))

    def test_circuit10_15_3FAULTS_FAULTVERSION1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_15_3FAULTS_FAULTVERSION1_dependency'))

    def test_circuit10_15_3FAULTS_FAULTVERSION1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_15_3FAULTS_FAULTVERSION1_value'))

    def test_circuit10_15_3FAULTS_FAULTVERSION2_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_15_3FAULTS_FAULTVERSION2_comparison'))

    def test_circuit10_15_3FAULTS_FAULTVERSION2_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_15_3FAULTS_FAULTVERSION2_dependency'))

    def test_circuit10_15_3FAULTS_FAULTVERSION2_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_15_3FAULTS_FAULTVERSION2_value'))

    def test_circuit10_15_3FAULTS_FAULTVERSION3_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_15_3FAULTS_FAULTVERSION3_comparison'))

    def test_circuit10_15_3FAULTS_FAULTVERSION3_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_15_3FAULTS_FAULTVERSION3_dependency'))

    def test_circuit10_15_3FAULTS_FAULTVERSION3_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_15_3FAULTS_FAULTVERSION3_value'))

    def test_circuit10_15_3FAULTS_FAULTVERSION4_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_15_3FAULTS_FAULTVERSION4_comparison'))

    def test_circuit10_15_3FAULTS_FAULTVERSION4_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_15_3FAULTS_FAULTVERSION4_dependency'))

    def test_circuit10_15_3FAULTS_FAULTVERSION4_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_15_3FAULTS_FAULTVERSION4_value'))

    def test_circuit10_15_3FAULTS_FAULTVERSION5_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_15_3FAULTS_FAULTVERSION5_comparison'))

    def test_circuit10_15_3FAULTS_FAULTVERSION5_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_15_3FAULTS_FAULTVERSION5_dependency'))

    def test_circuit10_15_3FAULTS_FAULTVERSION5_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_15_3FAULTS_FAULTVERSION5_value'))

    def test_circuit10_2_1FAULTS_FAULTVERSION1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_2_1FAULTS_FAULTVERSION1_comparison'))

    def test_circuit10_2_1FAULTS_FAULTVERSION1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_2_1FAULTS_FAULTVERSION1_dependency'))

    def test_circuit10_2_1FAULTS_FAULTVERSION1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_2_1FAULTS_FAULTVERSION1_value'))

    def test_circuit10_2_1FAULTS_FAULTVERSION2_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_2_1FAULTS_FAULTVERSION2_comparison'))

    def test_circuit10_2_1FAULTS_FAULTVERSION2_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_2_1FAULTS_FAULTVERSION2_dependency'))

    def test_circuit10_2_1FAULTS_FAULTVERSION2_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_2_1FAULTS_FAULTVERSION2_value'))

    def test_circuit10_2_1FAULTS_FAULTVERSION3_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_2_1FAULTS_FAULTVERSION3_comparison'))

    def test_circuit10_2_1FAULTS_FAULTVERSION3_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_2_1FAULTS_FAULTVERSION3_dependency'))

    def test_circuit10_2_1FAULTS_FAULTVERSION3_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_2_1FAULTS_FAULTVERSION3_value'))

    def test_circuit10_2_1FAULTS_FAULTVERSION4_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_2_1FAULTS_FAULTVERSION4_comparison'))

    def test_circuit10_2_1FAULTS_FAULTVERSION4_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_2_1FAULTS_FAULTVERSION4_dependency'))

    def test_circuit10_2_1FAULTS_FAULTVERSION4_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_2_1FAULTS_FAULTVERSION4_value'))

    def test_circuit10_2_1FAULTS_FAULTVERSION5_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_2_1FAULTS_FAULTVERSION5_comparison'))

    def test_circuit10_2_1FAULTS_FAULTVERSION5_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_2_1FAULTS_FAULTVERSION5_dependency'))

    def test_circuit10_2_1FAULTS_FAULTVERSION5_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_2_1FAULTS_FAULTVERSION5_value'))

    def test_circuit10_2_2FAULTS_FAULTVERSION1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_2_2FAULTS_FAULTVERSION1_comparison'))

    def test_circuit10_2_2FAULTS_FAULTVERSION1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_2_2FAULTS_FAULTVERSION1_dependency'))

    def test_circuit10_2_2FAULTS_FAULTVERSION1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_2_2FAULTS_FAULTVERSION1_value'))

    def test_circuit10_2_2FAULTS_FAULTVERSION2_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_2_2FAULTS_FAULTVERSION2_comparison'))

    def test_circuit10_2_2FAULTS_FAULTVERSION2_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_2_2FAULTS_FAULTVERSION2_dependency'))

    def test_circuit10_2_2FAULTS_FAULTVERSION2_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_2_2FAULTS_FAULTVERSION2_value'))

    def test_circuit10_2_2FAULTS_FAULTVERSION3_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_2_2FAULTS_FAULTVERSION3_comparison'))

    def test_circuit10_2_2FAULTS_FAULTVERSION3_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_2_2FAULTS_FAULTVERSION3_dependency'))

    def test_circuit10_2_2FAULTS_FAULTVERSION3_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_2_2FAULTS_FAULTVERSION3_value'))

    def test_circuit10_2_2FAULTS_FAULTVERSION4_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_2_2FAULTS_FAULTVERSION4_comparison'))

    def test_circuit10_2_2FAULTS_FAULTVERSION4_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_2_2FAULTS_FAULTVERSION4_dependency'))

    def test_circuit10_2_2FAULTS_FAULTVERSION4_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_2_2FAULTS_FAULTVERSION4_value'))

    def test_circuit10_2_2FAULTS_FAULTVERSION5_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_2_2FAULTS_FAULTVERSION5_comparison'))

    def test_circuit10_2_2FAULTS_FAULTVERSION5_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_2_2FAULTS_FAULTVERSION5_dependency'))

    def test_circuit10_2_2FAULTS_FAULTVERSION5_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_2_2FAULTS_FAULTVERSION5_value'))

    def test_circuit10_2_3FAULTS_FAULTVERSION1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_2_3FAULTS_FAULTVERSION1_comparison'))

    def test_circuit10_2_3FAULTS_FAULTVERSION1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_2_3FAULTS_FAULTVERSION1_dependency'))

    def test_circuit10_2_3FAULTS_FAULTVERSION1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_2_3FAULTS_FAULTVERSION1_value'))

    def test_circuit10_2_3FAULTS_FAULTVERSION2_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_2_3FAULTS_FAULTVERSION2_comparison'))

    def test_circuit10_2_3FAULTS_FAULTVERSION2_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_2_3FAULTS_FAULTVERSION2_dependency'))

    def test_circuit10_2_3FAULTS_FAULTVERSION2_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_2_3FAULTS_FAULTVERSION2_value'))

    def test_circuit10_2_3FAULTS_FAULTVERSION3_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_2_3FAULTS_FAULTVERSION3_comparison'))

    def test_circuit10_2_3FAULTS_FAULTVERSION3_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_2_3FAULTS_FAULTVERSION3_dependency'))

    def test_circuit10_2_3FAULTS_FAULTVERSION3_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_2_3FAULTS_FAULTVERSION3_value'))

    def test_circuit10_2_3FAULTS_FAULTVERSION4_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_2_3FAULTS_FAULTVERSION4_comparison'))

    def test_circuit10_2_3FAULTS_FAULTVERSION4_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_2_3FAULTS_FAULTVERSION4_dependency'))

    def test_circuit10_2_3FAULTS_FAULTVERSION4_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_2_3FAULTS_FAULTVERSION4_value'))

    def test_circuit10_2_3FAULTS_FAULTVERSION5_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_2_3FAULTS_FAULTVERSION5_comparison'))

    def test_circuit10_2_3FAULTS_FAULTVERSION5_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_2_3FAULTS_FAULTVERSION5_dependency'))

    def test_circuit10_2_3FAULTS_FAULTVERSION5_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit10_2_3FAULTS_FAULTVERSION5_value'))

    def test_circuit15_10_1FAULTS_FAULTVERSION1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_10_1FAULTS_FAULTVERSION1_comparison'))

    def test_circuit15_10_1FAULTS_FAULTVERSION1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_10_1FAULTS_FAULTVERSION1_dependency'))

    def test_circuit15_10_1FAULTS_FAULTVERSION1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_10_1FAULTS_FAULTVERSION1_value'))

    def test_circuit15_10_1FAULTS_FAULTVERSION2_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_10_1FAULTS_FAULTVERSION2_comparison'))

    def test_circuit15_10_1FAULTS_FAULTVERSION2_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_10_1FAULTS_FAULTVERSION2_dependency'))

    def test_circuit15_10_1FAULTS_FAULTVERSION2_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_10_1FAULTS_FAULTVERSION2_value'))

    def test_circuit15_10_1FAULTS_FAULTVERSION3_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_10_1FAULTS_FAULTVERSION3_comparison'))

    def test_circuit15_10_1FAULTS_FAULTVERSION3_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_10_1FAULTS_FAULTVERSION3_dependency'))

    def test_circuit15_10_1FAULTS_FAULTVERSION3_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_10_1FAULTS_FAULTVERSION3_value'))

    def test_circuit15_10_1FAULTS_FAULTVERSION4_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_10_1FAULTS_FAULTVERSION4_comparison'))

    def test_circuit15_10_1FAULTS_FAULTVERSION4_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_10_1FAULTS_FAULTVERSION4_dependency'))

    def test_circuit15_10_1FAULTS_FAULTVERSION4_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_10_1FAULTS_FAULTVERSION4_value'))

    def test_circuit15_10_1FAULTS_FAULTVERSION5_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_10_1FAULTS_FAULTVERSION5_comparison'))

    def test_circuit15_10_1FAULTS_FAULTVERSION5_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_10_1FAULTS_FAULTVERSION5_dependency'))

    def test_circuit15_10_1FAULTS_FAULTVERSION5_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_10_1FAULTS_FAULTVERSION5_value'))

    def test_circuit15_10_2FAULTS_FAULTVERSION1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_10_2FAULTS_FAULTVERSION1_comparison'))

    def test_circuit15_10_2FAULTS_FAULTVERSION1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_10_2FAULTS_FAULTVERSION1_dependency'))

    def test_circuit15_10_2FAULTS_FAULTVERSION1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_10_2FAULTS_FAULTVERSION1_value'))

    def test_circuit15_10_2FAULTS_FAULTVERSION2_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_10_2FAULTS_FAULTVERSION2_comparison'))

    def test_circuit15_10_2FAULTS_FAULTVERSION2_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_10_2FAULTS_FAULTVERSION2_dependency'))

    def test_circuit15_10_2FAULTS_FAULTVERSION2_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_10_2FAULTS_FAULTVERSION2_value'))

    def test_circuit15_10_2FAULTS_FAULTVERSION3_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_10_2FAULTS_FAULTVERSION3_comparison'))

    def test_circuit15_10_2FAULTS_FAULTVERSION3_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_10_2FAULTS_FAULTVERSION3_dependency'))

    def test_circuit15_10_2FAULTS_FAULTVERSION3_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_10_2FAULTS_FAULTVERSION3_value'))

    def test_circuit15_10_2FAULTS_FAULTVERSION4_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_10_2FAULTS_FAULTVERSION4_comparison'))

    def test_circuit15_10_2FAULTS_FAULTVERSION4_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_10_2FAULTS_FAULTVERSION4_dependency'))

    def test_circuit15_10_2FAULTS_FAULTVERSION4_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_10_2FAULTS_FAULTVERSION4_value'))

    def test_circuit15_10_2FAULTS_FAULTVERSION5_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_10_2FAULTS_FAULTVERSION5_comparison'))

    def test_circuit15_10_2FAULTS_FAULTVERSION5_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_10_2FAULTS_FAULTVERSION5_dependency'))

    def test_circuit15_10_2FAULTS_FAULTVERSION5_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_10_2FAULTS_FAULTVERSION5_value'))

    def test_circuit15_10_3FAULTS_FAULTVERSION1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_10_3FAULTS_FAULTVERSION1_comparison'))

    def test_circuit15_10_3FAULTS_FAULTVERSION1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_10_3FAULTS_FAULTVERSION1_dependency'))

    def test_circuit15_10_3FAULTS_FAULTVERSION1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_10_3FAULTS_FAULTVERSION1_value'))

    def test_circuit15_10_3FAULTS_FAULTVERSION2_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_10_3FAULTS_FAULTVERSION2_comparison'))

    def test_circuit15_10_3FAULTS_FAULTVERSION2_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_10_3FAULTS_FAULTVERSION2_dependency'))

    def test_circuit15_10_3FAULTS_FAULTVERSION2_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_10_3FAULTS_FAULTVERSION2_value'))

    def test_circuit15_10_3FAULTS_FAULTVERSION3_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_10_3FAULTS_FAULTVERSION3_comparison'))

    def test_circuit15_10_3FAULTS_FAULTVERSION3_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_10_3FAULTS_FAULTVERSION3_dependency'))

    def test_circuit15_10_3FAULTS_FAULTVERSION3_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_10_3FAULTS_FAULTVERSION3_value'))

    def test_circuit15_10_3FAULTS_FAULTVERSION4_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_10_3FAULTS_FAULTVERSION4_comparison'))

    def test_circuit15_10_3FAULTS_FAULTVERSION4_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_10_3FAULTS_FAULTVERSION4_dependency'))

    def test_circuit15_10_3FAULTS_FAULTVERSION4_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_10_3FAULTS_FAULTVERSION4_value'))

    def test_circuit15_10_3FAULTS_FAULTVERSION5_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_10_3FAULTS_FAULTVERSION5_comparison'))

    def test_circuit15_10_3FAULTS_FAULTVERSION5_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_10_3FAULTS_FAULTVERSION5_dependency'))

    def test_circuit15_10_3FAULTS_FAULTVERSION5_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_10_3FAULTS_FAULTVERSION5_value'))

    def test_circuit15_20_1FAULTS_FAULTVERSION1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_20_1FAULTS_FAULTVERSION1_comparison'))

    def test_circuit15_20_1FAULTS_FAULTVERSION1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_20_1FAULTS_FAULTVERSION1_dependency'))

    def test_circuit15_20_1FAULTS_FAULTVERSION1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_20_1FAULTS_FAULTVERSION1_value'))

    def test_circuit15_20_1FAULTS_FAULTVERSION2_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_20_1FAULTS_FAULTVERSION2_comparison'))

    def test_circuit15_20_1FAULTS_FAULTVERSION2_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_20_1FAULTS_FAULTVERSION2_dependency'))

    def test_circuit15_20_1FAULTS_FAULTVERSION2_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_20_1FAULTS_FAULTVERSION2_value'))

    def test_circuit15_20_1FAULTS_FAULTVERSION3_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_20_1FAULTS_FAULTVERSION3_comparison'))

    def test_circuit15_20_1FAULTS_FAULTVERSION3_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_20_1FAULTS_FAULTVERSION3_dependency'))

    def test_circuit15_20_1FAULTS_FAULTVERSION3_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_20_1FAULTS_FAULTVERSION3_value'))

    def test_circuit15_20_1FAULTS_FAULTVERSION4_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_20_1FAULTS_FAULTVERSION4_comparison'))

    def test_circuit15_20_1FAULTS_FAULTVERSION4_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_20_1FAULTS_FAULTVERSION4_dependency'))

    def test_circuit15_20_1FAULTS_FAULTVERSION4_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_20_1FAULTS_FAULTVERSION4_value'))

    def test_circuit15_20_1FAULTS_FAULTVERSION5_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_20_1FAULTS_FAULTVERSION5_comparison'))

    def test_circuit15_20_1FAULTS_FAULTVERSION5_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_20_1FAULTS_FAULTVERSION5_dependency'))

    def test_circuit15_20_1FAULTS_FAULTVERSION5_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_20_1FAULTS_FAULTVERSION5_value'))

    def test_circuit15_20_2FAULTS_FAULTVERSION1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_20_2FAULTS_FAULTVERSION1_comparison'))

    def test_circuit15_20_2FAULTS_FAULTVERSION1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_20_2FAULTS_FAULTVERSION1_dependency'))

    def test_circuit15_20_2FAULTS_FAULTVERSION1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_20_2FAULTS_FAULTVERSION1_value'))

    def test_circuit15_20_2FAULTS_FAULTVERSION2_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_20_2FAULTS_FAULTVERSION2_comparison'))

    def test_circuit15_20_2FAULTS_FAULTVERSION2_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_20_2FAULTS_FAULTVERSION2_dependency'))

    def test_circuit15_20_2FAULTS_FAULTVERSION2_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_20_2FAULTS_FAULTVERSION2_value'))

    def test_circuit15_20_2FAULTS_FAULTVERSION3_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_20_2FAULTS_FAULTVERSION3_comparison'))

    def test_circuit15_20_2FAULTS_FAULTVERSION3_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_20_2FAULTS_FAULTVERSION3_dependency'))

    def test_circuit15_20_2FAULTS_FAULTVERSION3_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_20_2FAULTS_FAULTVERSION3_value'))

    def test_circuit15_20_2FAULTS_FAULTVERSION4_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_20_2FAULTS_FAULTVERSION4_comparison'))

    def test_circuit15_20_2FAULTS_FAULTVERSION4_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_20_2FAULTS_FAULTVERSION4_dependency'))

    def test_circuit15_20_2FAULTS_FAULTVERSION4_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_20_2FAULTS_FAULTVERSION4_value'))

    def test_circuit15_20_2FAULTS_FAULTVERSION5_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_20_2FAULTS_FAULTVERSION5_comparison'))

    def test_circuit15_20_2FAULTS_FAULTVERSION5_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_20_2FAULTS_FAULTVERSION5_dependency'))

    def test_circuit15_20_2FAULTS_FAULTVERSION5_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_20_2FAULTS_FAULTVERSION5_value'))

    def test_circuit15_20_3FAULTS_FAULTVERSION1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_20_3FAULTS_FAULTVERSION1_comparison'))

    def test_circuit15_20_3FAULTS_FAULTVERSION1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_20_3FAULTS_FAULTVERSION1_dependency'))

    def test_circuit15_20_3FAULTS_FAULTVERSION1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_20_3FAULTS_FAULTVERSION1_value'))

    def test_circuit15_20_3FAULTS_FAULTVERSION2_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_20_3FAULTS_FAULTVERSION2_comparison'))

    def test_circuit15_20_3FAULTS_FAULTVERSION2_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_20_3FAULTS_FAULTVERSION2_dependency'))

    def test_circuit15_20_3FAULTS_FAULTVERSION2_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_20_3FAULTS_FAULTVERSION2_value'))

    def test_circuit15_20_3FAULTS_FAULTVERSION3_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_20_3FAULTS_FAULTVERSION3_comparison'))

    def test_circuit15_20_3FAULTS_FAULTVERSION3_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_20_3FAULTS_FAULTVERSION3_dependency'))

    def test_circuit15_20_3FAULTS_FAULTVERSION3_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_20_3FAULTS_FAULTVERSION3_value'))

    def test_circuit15_20_3FAULTS_FAULTVERSION4_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_20_3FAULTS_FAULTVERSION4_comparison'))

    def test_circuit15_20_3FAULTS_FAULTVERSION4_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_20_3FAULTS_FAULTVERSION4_dependency'))

    def test_circuit15_20_3FAULTS_FAULTVERSION4_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_20_3FAULTS_FAULTVERSION4_value'))

    def test_circuit15_20_3FAULTS_FAULTVERSION5_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_20_3FAULTS_FAULTVERSION5_comparison'))

    def test_circuit15_20_3FAULTS_FAULTVERSION5_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_20_3FAULTS_FAULTVERSION5_dependency'))

    def test_circuit15_20_3FAULTS_FAULTVERSION5_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_20_3FAULTS_FAULTVERSION5_value'))

    def test_circuit15_2_1FAULTS_FAULTVERSION1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_2_1FAULTS_FAULTVERSION1_comparison'))

    def test_circuit15_2_1FAULTS_FAULTVERSION1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_2_1FAULTS_FAULTVERSION1_dependency'))

    def test_circuit15_2_1FAULTS_FAULTVERSION1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_2_1FAULTS_FAULTVERSION1_value'))

    def test_circuit15_2_1FAULTS_FAULTVERSION2_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_2_1FAULTS_FAULTVERSION2_comparison'))

    def test_circuit15_2_1FAULTS_FAULTVERSION2_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_2_1FAULTS_FAULTVERSION2_dependency'))

    def test_circuit15_2_1FAULTS_FAULTVERSION2_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_2_1FAULTS_FAULTVERSION2_value'))

    def test_circuit15_2_1FAULTS_FAULTVERSION3_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_2_1FAULTS_FAULTVERSION3_comparison'))

    def test_circuit15_2_1FAULTS_FAULTVERSION3_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_2_1FAULTS_FAULTVERSION3_dependency'))

    def test_circuit15_2_1FAULTS_FAULTVERSION3_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_2_1FAULTS_FAULTVERSION3_value'))

    def test_circuit15_2_1FAULTS_FAULTVERSION4_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_2_1FAULTS_FAULTVERSION4_comparison'))

    def test_circuit15_2_1FAULTS_FAULTVERSION4_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_2_1FAULTS_FAULTVERSION4_dependency'))

    def test_circuit15_2_1FAULTS_FAULTVERSION4_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_2_1FAULTS_FAULTVERSION4_value'))

    def test_circuit15_2_1FAULTS_FAULTVERSION5_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_2_1FAULTS_FAULTVERSION5_comparison'))

    def test_circuit15_2_1FAULTS_FAULTVERSION5_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_2_1FAULTS_FAULTVERSION5_dependency'))

    def test_circuit15_2_1FAULTS_FAULTVERSION5_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_2_1FAULTS_FAULTVERSION5_value'))

    def test_circuit15_2_2FAULTS_FAULTVERSION1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_2_2FAULTS_FAULTVERSION1_comparison'))

    def test_circuit15_2_2FAULTS_FAULTVERSION1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_2_2FAULTS_FAULTVERSION1_dependency'))

    def test_circuit15_2_2FAULTS_FAULTVERSION1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_2_2FAULTS_FAULTVERSION1_value'))

    def test_circuit15_2_2FAULTS_FAULTVERSION2_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_2_2FAULTS_FAULTVERSION2_comparison'))

    def test_circuit15_2_2FAULTS_FAULTVERSION2_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_2_2FAULTS_FAULTVERSION2_dependency'))

    def test_circuit15_2_2FAULTS_FAULTVERSION2_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_2_2FAULTS_FAULTVERSION2_value'))

    def test_circuit15_2_2FAULTS_FAULTVERSION3_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_2_2FAULTS_FAULTVERSION3_comparison'))

    def test_circuit15_2_2FAULTS_FAULTVERSION3_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_2_2FAULTS_FAULTVERSION3_dependency'))

    def test_circuit15_2_2FAULTS_FAULTVERSION3_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_2_2FAULTS_FAULTVERSION3_value'))

    def test_circuit15_2_2FAULTS_FAULTVERSION4_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_2_2FAULTS_FAULTVERSION4_comparison'))

    def test_circuit15_2_2FAULTS_FAULTVERSION4_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_2_2FAULTS_FAULTVERSION4_dependency'))

    def test_circuit15_2_2FAULTS_FAULTVERSION4_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_2_2FAULTS_FAULTVERSION4_value'))

    def test_circuit15_2_2FAULTS_FAULTVERSION5_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_2_2FAULTS_FAULTVERSION5_comparison'))

    def test_circuit15_2_2FAULTS_FAULTVERSION5_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_2_2FAULTS_FAULTVERSION5_dependency'))

    def test_circuit15_2_2FAULTS_FAULTVERSION5_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_2_2FAULTS_FAULTVERSION5_value'))

    def test_circuit15_2_3FAULTS_FAULTVERSION1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_2_3FAULTS_FAULTVERSION1_comparison'))

    def test_circuit15_2_3FAULTS_FAULTVERSION1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_2_3FAULTS_FAULTVERSION1_dependency'))

    def test_circuit15_2_3FAULTS_FAULTVERSION1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_2_3FAULTS_FAULTVERSION1_value'))

    def test_circuit15_2_3FAULTS_FAULTVERSION2_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_2_3FAULTS_FAULTVERSION2_comparison'))

    def test_circuit15_2_3FAULTS_FAULTVERSION2_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_2_3FAULTS_FAULTVERSION2_dependency'))

    def test_circuit15_2_3FAULTS_FAULTVERSION2_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_2_3FAULTS_FAULTVERSION2_value'))

    def test_circuit15_2_3FAULTS_FAULTVERSION3_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_2_3FAULTS_FAULTVERSION3_comparison'))

    def test_circuit15_2_3FAULTS_FAULTVERSION3_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_2_3FAULTS_FAULTVERSION3_dependency'))

    def test_circuit15_2_3FAULTS_FAULTVERSION3_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_2_3FAULTS_FAULTVERSION3_value'))

    def test_circuit15_2_3FAULTS_FAULTVERSION4_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_2_3FAULTS_FAULTVERSION4_comparison'))

    def test_circuit15_2_3FAULTS_FAULTVERSION4_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_2_3FAULTS_FAULTVERSION4_dependency'))

    def test_circuit15_2_3FAULTS_FAULTVERSION4_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_2_3FAULTS_FAULTVERSION4_value'))

    def test_circuit15_2_3FAULTS_FAULTVERSION5_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_2_3FAULTS_FAULTVERSION5_comparison'))

    def test_circuit15_2_3FAULTS_FAULTVERSION5_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_2_3FAULTS_FAULTVERSION5_dependency'))

    def test_circuit15_2_3FAULTS_FAULTVERSION5_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit15_2_3FAULTS_FAULTVERSION5_value'))

    def test_circuit20_15_1FAULTS_FAULTVERSION1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_15_1FAULTS_FAULTVERSION1_comparison'))

    def test_circuit20_15_1FAULTS_FAULTVERSION1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_15_1FAULTS_FAULTVERSION1_dependency'))

    def test_circuit20_15_1FAULTS_FAULTVERSION1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_15_1FAULTS_FAULTVERSION1_value'))

    def test_circuit20_15_1FAULTS_FAULTVERSION2_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_15_1FAULTS_FAULTVERSION2_comparison'))

    def test_circuit20_15_1FAULTS_FAULTVERSION2_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_15_1FAULTS_FAULTVERSION2_dependency'))

    def test_circuit20_15_1FAULTS_FAULTVERSION2_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_15_1FAULTS_FAULTVERSION2_value'))

    def test_circuit20_15_1FAULTS_FAULTVERSION3_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_15_1FAULTS_FAULTVERSION3_comparison'))

    def test_circuit20_15_1FAULTS_FAULTVERSION3_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_15_1FAULTS_FAULTVERSION3_dependency'))

    def test_circuit20_15_1FAULTS_FAULTVERSION3_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_15_1FAULTS_FAULTVERSION3_value'))

    def test_circuit20_15_1FAULTS_FAULTVERSION4_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_15_1FAULTS_FAULTVERSION4_comparison'))

    def test_circuit20_15_1FAULTS_FAULTVERSION4_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_15_1FAULTS_FAULTVERSION4_dependency'))

    def test_circuit20_15_1FAULTS_FAULTVERSION4_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_15_1FAULTS_FAULTVERSION4_value'))

    def test_circuit20_15_1FAULTS_FAULTVERSION5_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_15_1FAULTS_FAULTVERSION5_comparison'))

    def test_circuit20_15_1FAULTS_FAULTVERSION5_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_15_1FAULTS_FAULTVERSION5_dependency'))

    def test_circuit20_15_1FAULTS_FAULTVERSION5_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_15_1FAULTS_FAULTVERSION5_value'))

    def test_circuit20_15_2FAULTS_FAULTVERSION1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_15_2FAULTS_FAULTVERSION1_comparison'))

    def test_circuit20_15_2FAULTS_FAULTVERSION1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_15_2FAULTS_FAULTVERSION1_dependency'))

    def test_circuit20_15_2FAULTS_FAULTVERSION1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_15_2FAULTS_FAULTVERSION1_value'))

    def test_circuit20_15_2FAULTS_FAULTVERSION2_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_15_2FAULTS_FAULTVERSION2_comparison'))

    def test_circuit20_15_2FAULTS_FAULTVERSION2_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_15_2FAULTS_FAULTVERSION2_dependency'))

    def test_circuit20_15_2FAULTS_FAULTVERSION2_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_15_2FAULTS_FAULTVERSION2_value'))

    def test_circuit20_15_2FAULTS_FAULTVERSION3_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_15_2FAULTS_FAULTVERSION3_comparison'))

    def test_circuit20_15_2FAULTS_FAULTVERSION3_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_15_2FAULTS_FAULTVERSION3_dependency'))

    def test_circuit20_15_2FAULTS_FAULTVERSION3_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_15_2FAULTS_FAULTVERSION3_value'))

    def test_circuit20_15_2FAULTS_FAULTVERSION4_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_15_2FAULTS_FAULTVERSION4_comparison'))

    def test_circuit20_15_2FAULTS_FAULTVERSION4_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_15_2FAULTS_FAULTVERSION4_dependency'))

    def test_circuit20_15_2FAULTS_FAULTVERSION4_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_15_2FAULTS_FAULTVERSION4_value'))

    def test_circuit20_15_2FAULTS_FAULTVERSION5_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_15_2FAULTS_FAULTVERSION5_comparison'))

    def test_circuit20_15_2FAULTS_FAULTVERSION5_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_15_2FAULTS_FAULTVERSION5_dependency'))

    def test_circuit20_15_2FAULTS_FAULTVERSION5_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_15_2FAULTS_FAULTVERSION5_value'))

    def test_circuit20_15_3FAULTS_FAULTVERSION1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_15_3FAULTS_FAULTVERSION1_comparison'))

    def test_circuit20_15_3FAULTS_FAULTVERSION1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_15_3FAULTS_FAULTVERSION1_dependency'))

    def test_circuit20_15_3FAULTS_FAULTVERSION1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_15_3FAULTS_FAULTVERSION1_value'))

    def test_circuit20_15_3FAULTS_FAULTVERSION2_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_15_3FAULTS_FAULTVERSION2_comparison'))

    def test_circuit20_15_3FAULTS_FAULTVERSION2_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_15_3FAULTS_FAULTVERSION2_dependency'))

    def test_circuit20_15_3FAULTS_FAULTVERSION2_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_15_3FAULTS_FAULTVERSION2_value'))

    def test_circuit20_15_3FAULTS_FAULTVERSION3_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_15_3FAULTS_FAULTVERSION3_comparison'))

    def test_circuit20_15_3FAULTS_FAULTVERSION3_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_15_3FAULTS_FAULTVERSION3_dependency'))

    def test_circuit20_15_3FAULTS_FAULTVERSION3_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_15_3FAULTS_FAULTVERSION3_value'))

    def test_circuit20_15_3FAULTS_FAULTVERSION4_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_15_3FAULTS_FAULTVERSION4_comparison'))

    def test_circuit20_15_3FAULTS_FAULTVERSION4_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_15_3FAULTS_FAULTVERSION4_dependency'))

    def test_circuit20_15_3FAULTS_FAULTVERSION4_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_15_3FAULTS_FAULTVERSION4_value'))

    def test_circuit20_15_3FAULTS_FAULTVERSION5_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_15_3FAULTS_FAULTVERSION5_comparison'))

    def test_circuit20_15_3FAULTS_FAULTVERSION5_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_15_3FAULTS_FAULTVERSION5_dependency'))

    def test_circuit20_15_3FAULTS_FAULTVERSION5_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_15_3FAULTS_FAULTVERSION5_value'))

    def test_circuit20_20_1FAULTS_FAULTVERSION1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_20_1FAULTS_FAULTVERSION1_comparison'))

    def test_circuit20_20_1FAULTS_FAULTVERSION1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_20_1FAULTS_FAULTVERSION1_dependency'))

    def test_circuit20_20_1FAULTS_FAULTVERSION1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_20_1FAULTS_FAULTVERSION1_value'))

    def test_circuit20_20_1FAULTS_FAULTVERSION2_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_20_1FAULTS_FAULTVERSION2_comparison'))

    def test_circuit20_20_1FAULTS_FAULTVERSION2_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_20_1FAULTS_FAULTVERSION2_dependency'))

    def test_circuit20_20_1FAULTS_FAULTVERSION2_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_20_1FAULTS_FAULTVERSION2_value'))

    def test_circuit20_20_1FAULTS_FAULTVERSION3_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_20_1FAULTS_FAULTVERSION3_comparison'))

    def test_circuit20_20_1FAULTS_FAULTVERSION3_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_20_1FAULTS_FAULTVERSION3_dependency'))

    def test_circuit20_20_1FAULTS_FAULTVERSION3_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_20_1FAULTS_FAULTVERSION3_value'))

    def test_circuit20_20_1FAULTS_FAULTVERSION4_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_20_1FAULTS_FAULTVERSION4_comparison'))

    def test_circuit20_20_1FAULTS_FAULTVERSION4_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_20_1FAULTS_FAULTVERSION4_dependency'))

    def test_circuit20_20_1FAULTS_FAULTVERSION4_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_20_1FAULTS_FAULTVERSION4_value'))

    def test_circuit20_20_1FAULTS_FAULTVERSION5_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_20_1FAULTS_FAULTVERSION5_comparison'))

    def test_circuit20_20_1FAULTS_FAULTVERSION5_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_20_1FAULTS_FAULTVERSION5_dependency'))

    def test_circuit20_20_1FAULTS_FAULTVERSION5_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_20_1FAULTS_FAULTVERSION5_value'))

    def test_circuit20_20_2FAULTS_FAULTVERSION1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_20_2FAULTS_FAULTVERSION1_comparison'))

    def test_circuit20_20_2FAULTS_FAULTVERSION1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_20_2FAULTS_FAULTVERSION1_dependency'))

    def test_circuit20_20_2FAULTS_FAULTVERSION1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_20_2FAULTS_FAULTVERSION1_value'))

    def test_circuit20_20_2FAULTS_FAULTVERSION2_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_20_2FAULTS_FAULTVERSION2_comparison'))

    def test_circuit20_20_2FAULTS_FAULTVERSION2_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_20_2FAULTS_FAULTVERSION2_dependency'))

    def test_circuit20_20_2FAULTS_FAULTVERSION2_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_20_2FAULTS_FAULTVERSION2_value'))

    def test_circuit20_20_2FAULTS_FAULTVERSION3_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_20_2FAULTS_FAULTVERSION3_comparison'))

    def test_circuit20_20_2FAULTS_FAULTVERSION3_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_20_2FAULTS_FAULTVERSION3_dependency'))

    def test_circuit20_20_2FAULTS_FAULTVERSION3_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_20_2FAULTS_FAULTVERSION3_value'))

    def test_circuit20_20_2FAULTS_FAULTVERSION4_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_20_2FAULTS_FAULTVERSION4_comparison'))

    def test_circuit20_20_2FAULTS_FAULTVERSION4_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_20_2FAULTS_FAULTVERSION4_dependency'))

    def test_circuit20_20_2FAULTS_FAULTVERSION4_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_20_2FAULTS_FAULTVERSION4_value'))

    def test_circuit20_20_2FAULTS_FAULTVERSION5_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_20_2FAULTS_FAULTVERSION5_comparison'))

    def test_circuit20_20_2FAULTS_FAULTVERSION5_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_20_2FAULTS_FAULTVERSION5_dependency'))

    def test_circuit20_20_2FAULTS_FAULTVERSION5_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_20_2FAULTS_FAULTVERSION5_value'))

    def test_circuit20_20_3FAULTS_FAULTVERSION1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_20_3FAULTS_FAULTVERSION1_comparison'))

    def test_circuit20_20_3FAULTS_FAULTVERSION1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_20_3FAULTS_FAULTVERSION1_dependency'))

    def test_circuit20_20_3FAULTS_FAULTVERSION1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_20_3FAULTS_FAULTVERSION1_value'))

    def test_circuit20_20_3FAULTS_FAULTVERSION2_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_20_3FAULTS_FAULTVERSION2_comparison'))

    def test_circuit20_20_3FAULTS_FAULTVERSION2_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_20_3FAULTS_FAULTVERSION2_dependency'))

    def test_circuit20_20_3FAULTS_FAULTVERSION2_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_20_3FAULTS_FAULTVERSION2_value'))

    def test_circuit20_20_3FAULTS_FAULTVERSION3_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_20_3FAULTS_FAULTVERSION3_comparison'))

    def test_circuit20_20_3FAULTS_FAULTVERSION3_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_20_3FAULTS_FAULTVERSION3_dependency'))

    def test_circuit20_20_3FAULTS_FAULTVERSION3_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_20_3FAULTS_FAULTVERSION3_value'))

    def test_circuit20_20_3FAULTS_FAULTVERSION4_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_20_3FAULTS_FAULTVERSION4_comparison'))

    def test_circuit20_20_3FAULTS_FAULTVERSION4_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_20_3FAULTS_FAULTVERSION4_dependency'))

    def test_circuit20_20_3FAULTS_FAULTVERSION4_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_20_3FAULTS_FAULTVERSION4_value'))

    def test_circuit20_20_3FAULTS_FAULTVERSION5_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_20_3FAULTS_FAULTVERSION5_comparison'))

    def test_circuit20_20_3FAULTS_FAULTVERSION5_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_20_3FAULTS_FAULTVERSION5_dependency'))

    def test_circuit20_20_3FAULTS_FAULTVERSION5_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_20_3FAULTS_FAULTVERSION5_value'))

    def test_circuit20_2_1FAULTS_FAULTVERSION1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_2_1FAULTS_FAULTVERSION1_comparison'))

    def test_circuit20_2_1FAULTS_FAULTVERSION1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_2_1FAULTS_FAULTVERSION1_dependency'))

    def test_circuit20_2_1FAULTS_FAULTVERSION1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_2_1FAULTS_FAULTVERSION1_value'))

    def test_circuit20_2_1FAULTS_FAULTVERSION2_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_2_1FAULTS_FAULTVERSION2_comparison'))

    def test_circuit20_2_1FAULTS_FAULTVERSION2_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_2_1FAULTS_FAULTVERSION2_dependency'))

    def test_circuit20_2_1FAULTS_FAULTVERSION2_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_2_1FAULTS_FAULTVERSION2_value'))

    def test_circuit20_2_1FAULTS_FAULTVERSION3_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_2_1FAULTS_FAULTVERSION3_comparison'))

    def test_circuit20_2_1FAULTS_FAULTVERSION3_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_2_1FAULTS_FAULTVERSION3_dependency'))

    def test_circuit20_2_1FAULTS_FAULTVERSION3_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_2_1FAULTS_FAULTVERSION3_value'))

    def test_circuit20_2_1FAULTS_FAULTVERSION4_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_2_1FAULTS_FAULTVERSION4_comparison'))

    def test_circuit20_2_1FAULTS_FAULTVERSION4_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_2_1FAULTS_FAULTVERSION4_dependency'))

    def test_circuit20_2_1FAULTS_FAULTVERSION4_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_2_1FAULTS_FAULTVERSION4_value'))

    def test_circuit20_2_1FAULTS_FAULTVERSION5_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_2_1FAULTS_FAULTVERSION5_comparison'))

    def test_circuit20_2_1FAULTS_FAULTVERSION5_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_2_1FAULTS_FAULTVERSION5_dependency'))

    def test_circuit20_2_1FAULTS_FAULTVERSION5_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_2_1FAULTS_FAULTVERSION5_value'))

    def test_circuit20_2_2FAULTS_FAULTVERSION1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_2_2FAULTS_FAULTVERSION1_comparison'))

    def test_circuit20_2_2FAULTS_FAULTVERSION1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_2_2FAULTS_FAULTVERSION1_dependency'))

    def test_circuit20_2_2FAULTS_FAULTVERSION1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_2_2FAULTS_FAULTVERSION1_value'))

    def test_circuit20_2_2FAULTS_FAULTVERSION2_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_2_2FAULTS_FAULTVERSION2_comparison'))

    def test_circuit20_2_2FAULTS_FAULTVERSION2_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_2_2FAULTS_FAULTVERSION2_dependency'))

    def test_circuit20_2_2FAULTS_FAULTVERSION2_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_2_2FAULTS_FAULTVERSION2_value'))

    def test_circuit20_2_2FAULTS_FAULTVERSION3_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_2_2FAULTS_FAULTVERSION3_comparison'))

    def test_circuit20_2_2FAULTS_FAULTVERSION3_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_2_2FAULTS_FAULTVERSION3_dependency'))

    def test_circuit20_2_2FAULTS_FAULTVERSION3_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_2_2FAULTS_FAULTVERSION3_value'))

    def test_circuit20_2_2FAULTS_FAULTVERSION4_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_2_2FAULTS_FAULTVERSION4_comparison'))

    def test_circuit20_2_2FAULTS_FAULTVERSION4_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_2_2FAULTS_FAULTVERSION4_dependency'))

    def test_circuit20_2_2FAULTS_FAULTVERSION4_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_2_2FAULTS_FAULTVERSION4_value'))

    def test_circuit20_2_2FAULTS_FAULTVERSION5_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_2_2FAULTS_FAULTVERSION5_comparison'))

    def test_circuit20_2_2FAULTS_FAULTVERSION5_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_2_2FAULTS_FAULTVERSION5_dependency'))

    def test_circuit20_2_2FAULTS_FAULTVERSION5_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_2_2FAULTS_FAULTVERSION5_value'))

    def test_circuit20_2_3FAULTS_FAULTVERSION1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_2_3FAULTS_FAULTVERSION1_comparison'))

    def test_circuit20_2_3FAULTS_FAULTVERSION1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_2_3FAULTS_FAULTVERSION1_dependency'))

    def test_circuit20_2_3FAULTS_FAULTVERSION1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_2_3FAULTS_FAULTVERSION1_value'))

    def test_circuit20_2_3FAULTS_FAULTVERSION2_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_2_3FAULTS_FAULTVERSION2_comparison'))

    def test_circuit20_2_3FAULTS_FAULTVERSION2_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_2_3FAULTS_FAULTVERSION2_dependency'))

    def test_circuit20_2_3FAULTS_FAULTVERSION2_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_2_3FAULTS_FAULTVERSION2_value'))

    def test_circuit20_2_3FAULTS_FAULTVERSION3_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_2_3FAULTS_FAULTVERSION3_comparison'))

    def test_circuit20_2_3FAULTS_FAULTVERSION3_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_2_3FAULTS_FAULTVERSION3_dependency'))

    def test_circuit20_2_3FAULTS_FAULTVERSION3_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_2_3FAULTS_FAULTVERSION3_value'))

    def test_circuit20_2_3FAULTS_FAULTVERSION4_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_2_3FAULTS_FAULTVERSION4_comparison'))

    def test_circuit20_2_3FAULTS_FAULTVERSION4_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_2_3FAULTS_FAULTVERSION4_dependency'))

    def test_circuit20_2_3FAULTS_FAULTVERSION4_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_2_3FAULTS_FAULTVERSION4_value'))

    def test_circuit20_2_3FAULTS_FAULTVERSION5_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_2_3FAULTS_FAULTVERSION5_comparison'))

    def test_circuit20_2_3FAULTS_FAULTVERSION5_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_2_3FAULTS_FAULTVERSION5_dependency'))

    def test_circuit20_2_3FAULTS_FAULTVERSION5_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit20_2_3FAULTS_FAULTVERSION5_value'))

    def test_circuit2_10_1FAULTS_FAULTVERSION1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_10_1FAULTS_FAULTVERSION1_comparison'))

    def test_circuit2_10_1FAULTS_FAULTVERSION1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_10_1FAULTS_FAULTVERSION1_dependency'))

    def test_circuit2_10_1FAULTS_FAULTVERSION1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_10_1FAULTS_FAULTVERSION1_value'))

    def test_circuit2_10_1FAULTS_FAULTVERSION2_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_10_1FAULTS_FAULTVERSION2_comparison'))

    def test_circuit2_10_1FAULTS_FAULTVERSION2_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_10_1FAULTS_FAULTVERSION2_dependency'))

    def test_circuit2_10_1FAULTS_FAULTVERSION2_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_10_1FAULTS_FAULTVERSION2_value'))

    def test_circuit2_10_1FAULTS_FAULTVERSION3_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_10_1FAULTS_FAULTVERSION3_comparison'))

    def test_circuit2_10_1FAULTS_FAULTVERSION3_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_10_1FAULTS_FAULTVERSION3_dependency'))

    def test_circuit2_10_1FAULTS_FAULTVERSION3_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_10_1FAULTS_FAULTVERSION3_value'))

    def test_circuit2_10_1FAULTS_FAULTVERSION4_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_10_1FAULTS_FAULTVERSION4_comparison'))

    def test_circuit2_10_1FAULTS_FAULTVERSION4_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_10_1FAULTS_FAULTVERSION4_dependency'))

    def test_circuit2_10_1FAULTS_FAULTVERSION4_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_10_1FAULTS_FAULTVERSION4_value'))

    def test_circuit2_10_1FAULTS_FAULTVERSION5_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_10_1FAULTS_FAULTVERSION5_comparison'))

    def test_circuit2_10_1FAULTS_FAULTVERSION5_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_10_1FAULTS_FAULTVERSION5_dependency'))

    def test_circuit2_10_1FAULTS_FAULTVERSION5_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_10_1FAULTS_FAULTVERSION5_value'))

    def test_circuit2_10_2FAULTS_FAULTVERSION1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_10_2FAULTS_FAULTVERSION1_comparison'))

    def test_circuit2_10_2FAULTS_FAULTVERSION1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_10_2FAULTS_FAULTVERSION1_dependency'))

    def test_circuit2_10_2FAULTS_FAULTVERSION1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_10_2FAULTS_FAULTVERSION1_value'))

    def test_circuit2_10_2FAULTS_FAULTVERSION2_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_10_2FAULTS_FAULTVERSION2_comparison'))

    def test_circuit2_10_2FAULTS_FAULTVERSION2_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_10_2FAULTS_FAULTVERSION2_dependency'))

    def test_circuit2_10_2FAULTS_FAULTVERSION2_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_10_2FAULTS_FAULTVERSION2_value'))

    def test_circuit2_10_2FAULTS_FAULTVERSION3_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_10_2FAULTS_FAULTVERSION3_comparison'))

    def test_circuit2_10_2FAULTS_FAULTVERSION3_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_10_2FAULTS_FAULTVERSION3_dependency'))

    def test_circuit2_10_2FAULTS_FAULTVERSION3_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_10_2FAULTS_FAULTVERSION3_value'))

    def test_circuit2_10_2FAULTS_FAULTVERSION4_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_10_2FAULTS_FAULTVERSION4_comparison'))

    def test_circuit2_10_2FAULTS_FAULTVERSION4_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_10_2FAULTS_FAULTVERSION4_dependency'))

    def test_circuit2_10_2FAULTS_FAULTVERSION4_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_10_2FAULTS_FAULTVERSION4_value'))

    def test_circuit2_10_2FAULTS_FAULTVERSION5_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_10_2FAULTS_FAULTVERSION5_comparison'))

    def test_circuit2_10_2FAULTS_FAULTVERSION5_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_10_2FAULTS_FAULTVERSION5_dependency'))

    def test_circuit2_10_2FAULTS_FAULTVERSION5_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_10_2FAULTS_FAULTVERSION5_value'))

    def test_circuit2_10_3FAULTS_FAULTVERSION1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_10_3FAULTS_FAULTVERSION1_comparison'))

    def test_circuit2_10_3FAULTS_FAULTVERSION1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_10_3FAULTS_FAULTVERSION1_dependency'))

    def test_circuit2_10_3FAULTS_FAULTVERSION1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_10_3FAULTS_FAULTVERSION1_value'))

    def test_circuit2_10_3FAULTS_FAULTVERSION2_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_10_3FAULTS_FAULTVERSION2_comparison'))

    def test_circuit2_10_3FAULTS_FAULTVERSION2_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_10_3FAULTS_FAULTVERSION2_dependency'))

    def test_circuit2_10_3FAULTS_FAULTVERSION2_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_10_3FAULTS_FAULTVERSION2_value'))

    def test_circuit2_10_3FAULTS_FAULTVERSION3_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_10_3FAULTS_FAULTVERSION3_comparison'))

    def test_circuit2_10_3FAULTS_FAULTVERSION3_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_10_3FAULTS_FAULTVERSION3_dependency'))

    def test_circuit2_10_3FAULTS_FAULTVERSION3_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_10_3FAULTS_FAULTVERSION3_value'))

    def test_circuit2_10_3FAULTS_FAULTVERSION4_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_10_3FAULTS_FAULTVERSION4_comparison'))

    def test_circuit2_10_3FAULTS_FAULTVERSION4_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_10_3FAULTS_FAULTVERSION4_dependency'))

    def test_circuit2_10_3FAULTS_FAULTVERSION4_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_10_3FAULTS_FAULTVERSION4_value'))

    def test_circuit2_10_3FAULTS_FAULTVERSION5_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_10_3FAULTS_FAULTVERSION5_comparison'))

    def test_circuit2_10_3FAULTS_FAULTVERSION5_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_10_3FAULTS_FAULTVERSION5_dependency'))

    def test_circuit2_10_3FAULTS_FAULTVERSION5_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_10_3FAULTS_FAULTVERSION5_value'))

    def test_circuit2_15_1FAULTS_FAULTVERSION1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_15_1FAULTS_FAULTVERSION1_comparison'))

    def test_circuit2_15_1FAULTS_FAULTVERSION1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_15_1FAULTS_FAULTVERSION1_dependency'))

    def test_circuit2_15_1FAULTS_FAULTVERSION1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_15_1FAULTS_FAULTVERSION1_value'))

    def test_circuit2_15_1FAULTS_FAULTVERSION2_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_15_1FAULTS_FAULTVERSION2_comparison'))

    def test_circuit2_15_1FAULTS_FAULTVERSION2_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_15_1FAULTS_FAULTVERSION2_dependency'))

    def test_circuit2_15_1FAULTS_FAULTVERSION2_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_15_1FAULTS_FAULTVERSION2_value'))

    def test_circuit2_15_1FAULTS_FAULTVERSION3_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_15_1FAULTS_FAULTVERSION3_comparison'))

    def test_circuit2_15_1FAULTS_FAULTVERSION3_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_15_1FAULTS_FAULTVERSION3_dependency'))

    def test_circuit2_15_1FAULTS_FAULTVERSION3_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_15_1FAULTS_FAULTVERSION3_value'))

    def test_circuit2_15_1FAULTS_FAULTVERSION4_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_15_1FAULTS_FAULTVERSION4_comparison'))

    def test_circuit2_15_1FAULTS_FAULTVERSION4_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_15_1FAULTS_FAULTVERSION4_dependency'))

    def test_circuit2_15_1FAULTS_FAULTVERSION4_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_15_1FAULTS_FAULTVERSION4_value'))

    def test_circuit2_15_1FAULTS_FAULTVERSION5_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_15_1FAULTS_FAULTVERSION5_comparison'))

    def test_circuit2_15_1FAULTS_FAULTVERSION5_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_15_1FAULTS_FAULTVERSION5_dependency'))

    def test_circuit2_15_1FAULTS_FAULTVERSION5_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_15_1FAULTS_FAULTVERSION5_value'))

    def test_circuit2_15_2FAULTS_FAULTVERSION1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_15_2FAULTS_FAULTVERSION1_comparison'))

    def test_circuit2_15_2FAULTS_FAULTVERSION1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_15_2FAULTS_FAULTVERSION1_dependency'))

    def test_circuit2_15_2FAULTS_FAULTVERSION1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_15_2FAULTS_FAULTVERSION1_value'))

    def test_circuit2_15_2FAULTS_FAULTVERSION2_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_15_2FAULTS_FAULTVERSION2_comparison'))

    def test_circuit2_15_2FAULTS_FAULTVERSION2_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_15_2FAULTS_FAULTVERSION2_dependency'))

    def test_circuit2_15_2FAULTS_FAULTVERSION2_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_15_2FAULTS_FAULTVERSION2_value'))

    def test_circuit2_15_2FAULTS_FAULTVERSION3_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_15_2FAULTS_FAULTVERSION3_comparison'))

    def test_circuit2_15_2FAULTS_FAULTVERSION3_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_15_2FAULTS_FAULTVERSION3_dependency'))

    def test_circuit2_15_2FAULTS_FAULTVERSION3_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_15_2FAULTS_FAULTVERSION3_value'))

    def test_circuit2_15_2FAULTS_FAULTVERSION4_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_15_2FAULTS_FAULTVERSION4_comparison'))

    def test_circuit2_15_2FAULTS_FAULTVERSION4_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_15_2FAULTS_FAULTVERSION4_dependency'))

    def test_circuit2_15_2FAULTS_FAULTVERSION4_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_15_2FAULTS_FAULTVERSION4_value'))

    def test_circuit2_15_2FAULTS_FAULTVERSION5_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_15_2FAULTS_FAULTVERSION5_comparison'))

    def test_circuit2_15_2FAULTS_FAULTVERSION5_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_15_2FAULTS_FAULTVERSION5_dependency'))

    def test_circuit2_15_2FAULTS_FAULTVERSION5_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_15_2FAULTS_FAULTVERSION5_value'))

    def test_circuit2_15_3FAULTS_FAULTVERSION1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_15_3FAULTS_FAULTVERSION1_comparison'))

    def test_circuit2_15_3FAULTS_FAULTVERSION1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_15_3FAULTS_FAULTVERSION1_dependency'))

    def test_circuit2_15_3FAULTS_FAULTVERSION1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_15_3FAULTS_FAULTVERSION1_value'))

    def test_circuit2_15_3FAULTS_FAULTVERSION2_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_15_3FAULTS_FAULTVERSION2_comparison'))

    def test_circuit2_15_3FAULTS_FAULTVERSION2_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_15_3FAULTS_FAULTVERSION2_dependency'))

    def test_circuit2_15_3FAULTS_FAULTVERSION2_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_15_3FAULTS_FAULTVERSION2_value'))

    def test_circuit2_15_3FAULTS_FAULTVERSION3_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_15_3FAULTS_FAULTVERSION3_comparison'))

    def test_circuit2_15_3FAULTS_FAULTVERSION3_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_15_3FAULTS_FAULTVERSION3_dependency'))

    def test_circuit2_15_3FAULTS_FAULTVERSION3_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_15_3FAULTS_FAULTVERSION3_value'))

    def test_circuit2_15_3FAULTS_FAULTVERSION4_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_15_3FAULTS_FAULTVERSION4_comparison'))

    def test_circuit2_15_3FAULTS_FAULTVERSION4_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_15_3FAULTS_FAULTVERSION4_dependency'))

    def test_circuit2_15_3FAULTS_FAULTVERSION4_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_15_3FAULTS_FAULTVERSION4_value'))

    def test_circuit2_15_3FAULTS_FAULTVERSION5_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_15_3FAULTS_FAULTVERSION5_comparison'))

    def test_circuit2_15_3FAULTS_FAULTVERSION5_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_15_3FAULTS_FAULTVERSION5_dependency'))

    def test_circuit2_15_3FAULTS_FAULTVERSION5_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_15_3FAULTS_FAULTVERSION5_value'))

    def test_circuit2_20_1FAULTS_FAULTVERSION1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_20_1FAULTS_FAULTVERSION1_comparison'))

    def test_circuit2_20_1FAULTS_FAULTVERSION1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_20_1FAULTS_FAULTVERSION1_dependency'))

    def test_circuit2_20_1FAULTS_FAULTVERSION1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_20_1FAULTS_FAULTVERSION1_value'))

    def test_circuit2_20_1FAULTS_FAULTVERSION2_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_20_1FAULTS_FAULTVERSION2_comparison'))

    def test_circuit2_20_1FAULTS_FAULTVERSION2_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_20_1FAULTS_FAULTVERSION2_dependency'))

    def test_circuit2_20_1FAULTS_FAULTVERSION2_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_20_1FAULTS_FAULTVERSION2_value'))

    def test_circuit2_20_1FAULTS_FAULTVERSION3_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_20_1FAULTS_FAULTVERSION3_comparison'))

    def test_circuit2_20_1FAULTS_FAULTVERSION3_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_20_1FAULTS_FAULTVERSION3_dependency'))

    def test_circuit2_20_1FAULTS_FAULTVERSION3_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_20_1FAULTS_FAULTVERSION3_value'))

    def test_circuit2_20_1FAULTS_FAULTVERSION4_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_20_1FAULTS_FAULTVERSION4_comparison'))

    def test_circuit2_20_1FAULTS_FAULTVERSION4_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_20_1FAULTS_FAULTVERSION4_dependency'))

    def test_circuit2_20_1FAULTS_FAULTVERSION4_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_20_1FAULTS_FAULTVERSION4_value'))

    def test_circuit2_20_1FAULTS_FAULTVERSION5_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_20_1FAULTS_FAULTVERSION5_comparison'))

    def test_circuit2_20_1FAULTS_FAULTVERSION5_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_20_1FAULTS_FAULTVERSION5_dependency'))

    def test_circuit2_20_1FAULTS_FAULTVERSION5_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_20_1FAULTS_FAULTVERSION5_value'))

    def test_circuit2_20_2FAULTS_FAULTVERSION1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_20_2FAULTS_FAULTVERSION1_comparison'))

    def test_circuit2_20_2FAULTS_FAULTVERSION1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_20_2FAULTS_FAULTVERSION1_dependency'))

    def test_circuit2_20_2FAULTS_FAULTVERSION1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_20_2FAULTS_FAULTVERSION1_value'))

    def test_circuit2_20_2FAULTS_FAULTVERSION2_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_20_2FAULTS_FAULTVERSION2_comparison'))

    def test_circuit2_20_2FAULTS_FAULTVERSION2_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_20_2FAULTS_FAULTVERSION2_dependency'))

    def test_circuit2_20_2FAULTS_FAULTVERSION2_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_20_2FAULTS_FAULTVERSION2_value'))

    def test_circuit2_20_2FAULTS_FAULTVERSION3_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_20_2FAULTS_FAULTVERSION3_comparison'))

    def test_circuit2_20_2FAULTS_FAULTVERSION3_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_20_2FAULTS_FAULTVERSION3_dependency'))

    def test_circuit2_20_2FAULTS_FAULTVERSION3_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_20_2FAULTS_FAULTVERSION3_value'))

    def test_circuit2_20_2FAULTS_FAULTVERSION4_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_20_2FAULTS_FAULTVERSION4_comparison'))

    def test_circuit2_20_2FAULTS_FAULTVERSION4_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_20_2FAULTS_FAULTVERSION4_dependency'))

    def test_circuit2_20_2FAULTS_FAULTVERSION4_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_20_2FAULTS_FAULTVERSION4_value'))

    def test_circuit2_20_2FAULTS_FAULTVERSION5_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_20_2FAULTS_FAULTVERSION5_comparison'))

    def test_circuit2_20_2FAULTS_FAULTVERSION5_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_20_2FAULTS_FAULTVERSION5_dependency'))

    def test_circuit2_20_2FAULTS_FAULTVERSION5_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_20_2FAULTS_FAULTVERSION5_value'))

    def test_circuit2_20_3FAULTS_FAULTVERSION1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_20_3FAULTS_FAULTVERSION1_comparison'))

    def test_circuit2_20_3FAULTS_FAULTVERSION1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_20_3FAULTS_FAULTVERSION1_dependency'))

    def test_circuit2_20_3FAULTS_FAULTVERSION1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_20_3FAULTS_FAULTVERSION1_value'))

    def test_circuit2_20_3FAULTS_FAULTVERSION2_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_20_3FAULTS_FAULTVERSION2_comparison'))

    def test_circuit2_20_3FAULTS_FAULTVERSION2_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_20_3FAULTS_FAULTVERSION2_dependency'))

    def test_circuit2_20_3FAULTS_FAULTVERSION2_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_20_3FAULTS_FAULTVERSION2_value'))

    def test_circuit2_20_3FAULTS_FAULTVERSION3_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_20_3FAULTS_FAULTVERSION3_comparison'))

    def test_circuit2_20_3FAULTS_FAULTVERSION3_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_20_3FAULTS_FAULTVERSION3_dependency'))

    def test_circuit2_20_3FAULTS_FAULTVERSION3_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_20_3FAULTS_FAULTVERSION3_value'))

    def test_circuit2_20_3FAULTS_FAULTVERSION4_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_20_3FAULTS_FAULTVERSION4_comparison'))

    def test_circuit2_20_3FAULTS_FAULTVERSION4_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_20_3FAULTS_FAULTVERSION4_dependency'))

    def test_circuit2_20_3FAULTS_FAULTVERSION4_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_20_3FAULTS_FAULTVERSION4_value'))

    def test_circuit2_20_3FAULTS_FAULTVERSION5_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_20_3FAULTS_FAULTVERSION5_comparison'))

    def test_circuit2_20_3FAULTS_FAULTVERSION5_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_20_3FAULTS_FAULTVERSION5_dependency'))

    def test_circuit2_20_3FAULTS_FAULTVERSION5_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_20_3FAULTS_FAULTVERSION5_value'))

    def test_circuit2_2_1FAULTS_FAULTVERSION1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_2_1FAULTS_FAULTVERSION1_comparison'))

    def test_circuit2_2_1FAULTS_FAULTVERSION1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_2_1FAULTS_FAULTVERSION1_dependency'))

    def test_circuit2_2_1FAULTS_FAULTVERSION1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_2_1FAULTS_FAULTVERSION1_value'))

    def test_circuit2_2_1FAULTS_FAULTVERSION2_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_2_1FAULTS_FAULTVERSION2_comparison'))

    def test_circuit2_2_1FAULTS_FAULTVERSION2_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_2_1FAULTS_FAULTVERSION2_dependency'))

    def test_circuit2_2_1FAULTS_FAULTVERSION2_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_2_1FAULTS_FAULTVERSION2_value'))

    def test_circuit2_2_1FAULTS_FAULTVERSION3_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_2_1FAULTS_FAULTVERSION3_comparison'))

    def test_circuit2_2_1FAULTS_FAULTVERSION3_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_2_1FAULTS_FAULTVERSION3_dependency'))

    def test_circuit2_2_1FAULTS_FAULTVERSION3_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_2_1FAULTS_FAULTVERSION3_value'))

    def test_circuit2_2_1FAULTS_FAULTVERSION4_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_2_1FAULTS_FAULTVERSION4_comparison'))

    def test_circuit2_2_1FAULTS_FAULTVERSION4_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_2_1FAULTS_FAULTVERSION4_dependency'))

    def test_circuit2_2_1FAULTS_FAULTVERSION4_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_2_1FAULTS_FAULTVERSION4_value'))

    def test_circuit2_2_1FAULTS_FAULTVERSION5_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_2_1FAULTS_FAULTVERSION5_comparison'))

    def test_circuit2_2_1FAULTS_FAULTVERSION5_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_2_1FAULTS_FAULTVERSION5_dependency'))

    def test_circuit2_2_1FAULTS_FAULTVERSION5_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_2_1FAULTS_FAULTVERSION5_value'))

    def test_circuit2_2_2FAULTS_FAULTVERSION1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_2_2FAULTS_FAULTVERSION1_comparison'))

    def test_circuit2_2_2FAULTS_FAULTVERSION1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_2_2FAULTS_FAULTVERSION1_dependency'))

    def test_circuit2_2_2FAULTS_FAULTVERSION1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_2_2FAULTS_FAULTVERSION1_value'))

    def test_circuit2_2_2FAULTS_FAULTVERSION2_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_2_2FAULTS_FAULTVERSION2_comparison'))

    def test_circuit2_2_2FAULTS_FAULTVERSION2_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_2_2FAULTS_FAULTVERSION2_dependency'))

    def test_circuit2_2_2FAULTS_FAULTVERSION2_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_2_2FAULTS_FAULTVERSION2_value'))

    def test_circuit2_2_2FAULTS_FAULTVERSION3_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_2_2FAULTS_FAULTVERSION3_comparison'))

    def test_circuit2_2_2FAULTS_FAULTVERSION3_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_2_2FAULTS_FAULTVERSION3_dependency'))

    def test_circuit2_2_2FAULTS_FAULTVERSION3_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_2_2FAULTS_FAULTVERSION3_value'))

    def test_circuit2_2_2FAULTS_FAULTVERSION4_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_2_2FAULTS_FAULTVERSION4_comparison'))

    def test_circuit2_2_2FAULTS_FAULTVERSION4_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_2_2FAULTS_FAULTVERSION4_dependency'))

    def test_circuit2_2_2FAULTS_FAULTVERSION4_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_2_2FAULTS_FAULTVERSION4_value'))

    def test_circuit2_2_2FAULTS_FAULTVERSION5_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_2_2FAULTS_FAULTVERSION5_comparison'))

    def test_circuit2_2_2FAULTS_FAULTVERSION5_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_2_2FAULTS_FAULTVERSION5_dependency'))

    def test_circuit2_2_2FAULTS_FAULTVERSION5_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_2_2FAULTS_FAULTVERSION5_value'))

    def test_circuit2_2_3FAULTS_FAULTVERSION1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_2_3FAULTS_FAULTVERSION1_comparison'))

    def test_circuit2_2_3FAULTS_FAULTVERSION1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_2_3FAULTS_FAULTVERSION1_dependency'))

    def test_circuit2_2_3FAULTS_FAULTVERSION1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_2_3FAULTS_FAULTVERSION1_value'))

    def test_circuit2_2_3FAULTS_FAULTVERSION2_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_2_3FAULTS_FAULTVERSION2_comparison'))

    def test_circuit2_2_3FAULTS_FAULTVERSION2_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_2_3FAULTS_FAULTVERSION2_dependency'))

    def test_circuit2_2_3FAULTS_FAULTVERSION2_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_2_3FAULTS_FAULTVERSION2_value'))

    def test_circuit2_2_3FAULTS_FAULTVERSION3_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_2_3FAULTS_FAULTVERSION3_comparison'))

    def test_circuit2_2_3FAULTS_FAULTVERSION3_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_2_3FAULTS_FAULTVERSION3_dependency'))

    def test_circuit2_2_3FAULTS_FAULTVERSION3_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_2_3FAULTS_FAULTVERSION3_value'))

    def test_circuit2_2_3FAULTS_FAULTVERSION4_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_2_3FAULTS_FAULTVERSION4_comparison'))

    def test_circuit2_2_3FAULTS_FAULTVERSION4_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_2_3FAULTS_FAULTVERSION4_dependency'))

    def test_circuit2_2_3FAULTS_FAULTVERSION4_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_2_3FAULTS_FAULTVERSION4_value'))

    def test_circuit2_2_3FAULTS_FAULTVERSION5_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_2_3FAULTS_FAULTVERSION5_comparison'))

    def test_circuit2_2_3FAULTS_FAULTVERSION5_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_2_3FAULTS_FAULTVERSION5_dependency'))

    def test_circuit2_2_3FAULTS_FAULTVERSION5_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_2_3FAULTS_FAULTVERSION5_value'))

    def test_circuit2_3_1FAULTS_FAULTVERSION1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_3_1FAULTS_FAULTVERSION1_comparison'))

    def test_circuit2_3_1FAULTS_FAULTVERSION1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_3_1FAULTS_FAULTVERSION1_dependency'))

    def test_circuit2_3_1FAULTS_FAULTVERSION1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_3_1FAULTS_FAULTVERSION1_value'))

    def test_circuit2_3_1FAULTS_FAULTVERSION2_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_3_1FAULTS_FAULTVERSION2_comparison'))

    def test_circuit2_3_1FAULTS_FAULTVERSION2_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_3_1FAULTS_FAULTVERSION2_dependency'))

    def test_circuit2_3_1FAULTS_FAULTVERSION2_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_3_1FAULTS_FAULTVERSION2_value'))

    def test_circuit2_3_1FAULTS_FAULTVERSION3_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_3_1FAULTS_FAULTVERSION3_comparison'))

    def test_circuit2_3_1FAULTS_FAULTVERSION3_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_3_1FAULTS_FAULTVERSION3_dependency'))

    def test_circuit2_3_1FAULTS_FAULTVERSION3_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_3_1FAULTS_FAULTVERSION3_value'))

    def test_circuit2_3_1FAULTS_FAULTVERSION4_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_3_1FAULTS_FAULTVERSION4_comparison'))

    def test_circuit2_3_1FAULTS_FAULTVERSION4_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_3_1FAULTS_FAULTVERSION4_dependency'))

    def test_circuit2_3_1FAULTS_FAULTVERSION4_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_3_1FAULTS_FAULTVERSION4_value'))

    def test_circuit2_3_1FAULTS_FAULTVERSION5_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_3_1FAULTS_FAULTVERSION5_comparison'))

    def test_circuit2_3_1FAULTS_FAULTVERSION5_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_3_1FAULTS_FAULTVERSION5_dependency'))

    def test_circuit2_3_1FAULTS_FAULTVERSION5_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_3_1FAULTS_FAULTVERSION5_value'))

    def test_circuit2_3_2FAULTS_FAULTVERSION1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_3_2FAULTS_FAULTVERSION1_comparison'))

    def test_circuit2_3_2FAULTS_FAULTVERSION1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_3_2FAULTS_FAULTVERSION1_dependency'))

    def test_circuit2_3_2FAULTS_FAULTVERSION1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_3_2FAULTS_FAULTVERSION1_value'))

    def test_circuit2_3_2FAULTS_FAULTVERSION2_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_3_2FAULTS_FAULTVERSION2_comparison'))

    def test_circuit2_3_2FAULTS_FAULTVERSION2_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_3_2FAULTS_FAULTVERSION2_dependency'))

    def test_circuit2_3_2FAULTS_FAULTVERSION2_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_3_2FAULTS_FAULTVERSION2_value'))

    def test_circuit2_3_2FAULTS_FAULTVERSION3_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_3_2FAULTS_FAULTVERSION3_comparison'))

    def test_circuit2_3_2FAULTS_FAULTVERSION3_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_3_2FAULTS_FAULTVERSION3_dependency'))

    def test_circuit2_3_2FAULTS_FAULTVERSION3_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_3_2FAULTS_FAULTVERSION3_value'))

    def test_circuit2_3_2FAULTS_FAULTVERSION4_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_3_2FAULTS_FAULTVERSION4_comparison'))

    def test_circuit2_3_2FAULTS_FAULTVERSION4_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_3_2FAULTS_FAULTVERSION4_dependency'))

    def test_circuit2_3_2FAULTS_FAULTVERSION4_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_3_2FAULTS_FAULTVERSION4_value'))

    def test_circuit2_3_2FAULTS_FAULTVERSION5_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_3_2FAULTS_FAULTVERSION5_comparison'))

    def test_circuit2_3_2FAULTS_FAULTVERSION5_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_3_2FAULTS_FAULTVERSION5_dependency'))

    def test_circuit2_3_2FAULTS_FAULTVERSION5_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_3_2FAULTS_FAULTVERSION5_value'))

    def test_circuit2_3_3FAULTS_FAULTVERSION1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_3_3FAULTS_FAULTVERSION1_comparison'))

    def test_circuit2_3_3FAULTS_FAULTVERSION1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_3_3FAULTS_FAULTVERSION1_dependency'))

    def test_circuit2_3_3FAULTS_FAULTVERSION1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_3_3FAULTS_FAULTVERSION1_value'))

    def test_circuit2_3_3FAULTS_FAULTVERSION2_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_3_3FAULTS_FAULTVERSION2_comparison'))

    def test_circuit2_3_3FAULTS_FAULTVERSION2_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_3_3FAULTS_FAULTVERSION2_dependency'))

    def test_circuit2_3_3FAULTS_FAULTVERSION2_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_3_3FAULTS_FAULTVERSION2_value'))

    def test_circuit2_3_3FAULTS_FAULTVERSION3_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_3_3FAULTS_FAULTVERSION3_comparison'))

    def test_circuit2_3_3FAULTS_FAULTVERSION3_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_3_3FAULTS_FAULTVERSION3_dependency'))

    def test_circuit2_3_3FAULTS_FAULTVERSION3_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_3_3FAULTS_FAULTVERSION3_value'))

    def test_circuit2_3_3FAULTS_FAULTVERSION4_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_3_3FAULTS_FAULTVERSION4_comparison'))

    def test_circuit2_3_3FAULTS_FAULTVERSION4_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_3_3FAULTS_FAULTVERSION4_dependency'))

    def test_circuit2_3_3FAULTS_FAULTVERSION4_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_3_3FAULTS_FAULTVERSION4_value'))

    def test_circuit2_3_3FAULTS_FAULTVERSION5_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_3_3FAULTS_FAULTVERSION5_comparison'))

    def test_circuit2_3_3FAULTS_FAULTVERSION5_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_3_3FAULTS_FAULTVERSION5_dependency'))

    def test_circuit2_3_3FAULTS_FAULTVERSION5_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit2_3_3FAULTS_FAULTVERSION5_value'))

    def test_circuit3_2_1FAULTS_FAULTVERSION1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit3_2_1FAULTS_FAULTVERSION1_comparison'))

    def test_circuit3_2_1FAULTS_FAULTVERSION1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit3_2_1FAULTS_FAULTVERSION1_dependency'))

    def test_circuit3_2_1FAULTS_FAULTVERSION1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit3_2_1FAULTS_FAULTVERSION1_value'))

    def test_circuit3_2_1FAULTS_FAULTVERSION2_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit3_2_1FAULTS_FAULTVERSION2_comparison'))

    def test_circuit3_2_1FAULTS_FAULTVERSION2_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit3_2_1FAULTS_FAULTVERSION2_dependency'))

    def test_circuit3_2_1FAULTS_FAULTVERSION2_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit3_2_1FAULTS_FAULTVERSION2_value'))

    def test_circuit3_2_1FAULTS_FAULTVERSION3_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit3_2_1FAULTS_FAULTVERSION3_comparison'))

    def test_circuit3_2_1FAULTS_FAULTVERSION3_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit3_2_1FAULTS_FAULTVERSION3_dependency'))

    def test_circuit3_2_1FAULTS_FAULTVERSION3_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit3_2_1FAULTS_FAULTVERSION3_value'))

    def test_circuit3_2_1FAULTS_FAULTVERSION4_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit3_2_1FAULTS_FAULTVERSION4_comparison'))

    def test_circuit3_2_1FAULTS_FAULTVERSION4_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit3_2_1FAULTS_FAULTVERSION4_dependency'))

    def test_circuit3_2_1FAULTS_FAULTVERSION4_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit3_2_1FAULTS_FAULTVERSION4_value'))

    def test_circuit3_2_1FAULTS_FAULTVERSION5_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit3_2_1FAULTS_FAULTVERSION5_comparison'))

    def test_circuit3_2_1FAULTS_FAULTVERSION5_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit3_2_1FAULTS_FAULTVERSION5_dependency'))

    def test_circuit3_2_1FAULTS_FAULTVERSION5_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit3_2_1FAULTS_FAULTVERSION5_value'))

    def test_circuit3_2_2FAULTS_FAULTVERSION1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit3_2_2FAULTS_FAULTVERSION1_comparison'))

    def test_circuit3_2_2FAULTS_FAULTVERSION1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit3_2_2FAULTS_FAULTVERSION1_dependency'))

    def test_circuit3_2_2FAULTS_FAULTVERSION1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit3_2_2FAULTS_FAULTVERSION1_value'))

    def test_circuit3_2_2FAULTS_FAULTVERSION2_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit3_2_2FAULTS_FAULTVERSION2_comparison'))

    def test_circuit3_2_2FAULTS_FAULTVERSION2_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit3_2_2FAULTS_FAULTVERSION2_dependency'))

    def test_circuit3_2_2FAULTS_FAULTVERSION2_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit3_2_2FAULTS_FAULTVERSION2_value'))

    def test_circuit3_2_2FAULTS_FAULTVERSION3_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit3_2_2FAULTS_FAULTVERSION3_comparison'))

    def test_circuit3_2_2FAULTS_FAULTVERSION3_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit3_2_2FAULTS_FAULTVERSION3_dependency'))

    def test_circuit3_2_2FAULTS_FAULTVERSION3_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit3_2_2FAULTS_FAULTVERSION3_value'))

    def test_circuit3_2_2FAULTS_FAULTVERSION4_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit3_2_2FAULTS_FAULTVERSION4_comparison'))

    def test_circuit3_2_2FAULTS_FAULTVERSION4_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit3_2_2FAULTS_FAULTVERSION4_dependency'))

    def test_circuit3_2_2FAULTS_FAULTVERSION4_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit3_2_2FAULTS_FAULTVERSION4_value'))

    def test_circuit3_2_2FAULTS_FAULTVERSION5_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit3_2_2FAULTS_FAULTVERSION5_comparison'))

    def test_circuit3_2_2FAULTS_FAULTVERSION5_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit3_2_2FAULTS_FAULTVERSION5_dependency'))

    def test_circuit3_2_2FAULTS_FAULTVERSION5_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit3_2_2FAULTS_FAULTVERSION5_value'))

    def test_circuit3_2_3FAULTS_FAULTVERSION1_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit3_2_3FAULTS_FAULTVERSION1_comparison'))

    def test_circuit3_2_3FAULTS_FAULTVERSION1_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit3_2_3FAULTS_FAULTVERSION1_dependency'))

    def test_circuit3_2_3FAULTS_FAULTVERSION1_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit3_2_3FAULTS_FAULTVERSION1_value'))

    def test_circuit3_2_3FAULTS_FAULTVERSION2_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit3_2_3FAULTS_FAULTVERSION2_comparison'))

    def test_circuit3_2_3FAULTS_FAULTVERSION2_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit3_2_3FAULTS_FAULTVERSION2_dependency'))

    def test_circuit3_2_3FAULTS_FAULTVERSION2_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit3_2_3FAULTS_FAULTVERSION2_value'))

    def test_circuit3_2_3FAULTS_FAULTVERSION3_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit3_2_3FAULTS_FAULTVERSION3_comparison'))

    def test_circuit3_2_3FAULTS_FAULTVERSION3_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit3_2_3FAULTS_FAULTVERSION3_dependency'))

    def test_circuit3_2_3FAULTS_FAULTVERSION3_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit3_2_3FAULTS_FAULTVERSION3_value'))

    def test_circuit3_2_3FAULTS_FAULTVERSION4_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit3_2_3FAULTS_FAULTVERSION4_comparison'))

    def test_circuit3_2_3FAULTS_FAULTVERSION4_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit3_2_3FAULTS_FAULTVERSION4_dependency'))

    def test_circuit3_2_3FAULTS_FAULTVERSION4_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit3_2_3FAULTS_FAULTVERSION4_value'))

    def test_circuit3_2_3FAULTS_FAULTVERSION5_comparison(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit3_2_3FAULTS_FAULTVERSION5_comparison'))

    def test_circuit3_2_3FAULTS_FAULTVERSION5_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit3_2_3FAULTS_FAULTVERSION5_dependency'))

    def test_circuit3_2_3FAULTS_FAULTVERSION5_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/circuit3_2_3FAULTS_FAULTVERSION5_value'))

    def test_if_test_dependency(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/if-test_dependency'))

    def test_if_test_value(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/spreadsheet_examples/if-test_value'))

