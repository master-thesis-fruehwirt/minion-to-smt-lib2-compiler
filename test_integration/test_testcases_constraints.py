from unittest import TestCase

from test_integration.interface.integrationtest_interface import IntegrationtestInterface


class Testtestcases_constraints(TestCase):

    def setUp(self) -> None:
        self._interface = IntegrationtestInterface()

    def test_test_abs(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_constraints/test_abs'))

    def test_test_alldiff(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_constraints/test_alldiff'))

    def test_test_alldiffmatrix(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_constraints/test_alldiffmatrix'))

    def test_test_difference(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_constraints/test_difference'))

    def test_test_diseq(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_constraints/test_diseq'))

    def test_test_div(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_constraints/test_div'))

    def test_test_element1(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_constraints/test_element1'))

    def test_test_element2(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_constraints/test_element2'))

    def test_test_element_one1(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_constraints/test_element_one1'))

    def test_test_element_one2(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_constraints/test_element_one2'))

    def test_test_eq1(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_constraints/test_eq1'))

    def test_test_eq2(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_constraints/test_eq2'))

    def test_test_eq3(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_constraints/test_eq3'))

    def test_test_eq4(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_constraints/test_eq4'))

    def test_test_gacalldiff(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_constraints/test_gacalldiff'))

    def test_test_hamming1(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_constraints/test_hamming1'))

    def test_test_hamming2(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_constraints/test_hamming2'))

    def test_test_hamming3(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_constraints/test_hamming3'))

    def test_test_ineq(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_constraints/test_ineq'))

    def test_test_lexleq1(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_constraints/test_lexleq1'))

    def test_test_lexleq2(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_constraints/test_lexleq2'))

    def test_test_lexless1(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_constraints/test_lexless1'))

    def test_test_lexless2(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_constraints/test_lexless2'))

    def test_test_litsumgeq(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_constraints/test_litsumgeq'))

    def test_test_max(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_constraints/test_max'))

    def test_test_min(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_constraints/test_min'))

    def test_test_minuseq(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_constraints/test_minuseq'))

    def test_test_modulo(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_constraints/test_modulo'))

    def test_test_negativetable1(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_constraints/test_negativetable1'))

    def test_test_negativetable2(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_constraints/test_negativetable2'))

    def test_test_negativetable3(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_constraints/test_negativetable3'))

    def test_test_occurrence(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_constraints/test_occurrence'))

    def test_test_occurrencegeq(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_constraints/test_occurrencegeq'))

    def test_test_occurrenceleq(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_constraints/test_occurrenceleq'))

    def test_test_pow(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_constraints/test_pow'))

    def test_test_product1(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_constraints/test_product1'))

    def test_test_product2(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_constraints/test_product2'))

    def test_test_reify(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_constraints/test_reify'))

    def test_test_reifyimply(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_constraints/test_reifyimply'))

    def test_test_sumeq(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_constraints/test_sumeq'))

    def test_test_sumgeq(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_constraints/test_sumgeq'))

    def test_test_sumleq(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_constraints/test_sumleq'))

    def test_test_table1(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_constraints/test_table1'))

    def test_test_table2(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_constraints/test_table2'))

    def test_test_table3(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_constraints/test_table3'))

    def test_test_w_inintervalset(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_constraints/test_w_inintervalset'))

    def test_test_w_inrange1(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_constraints/test_w_inrange1'))

    def test_test_w_inrange2(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_constraints/test_w_inrange2'))

    def test_test_w_inset(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_constraints/test_w_inset'))

    def test_test_w_literal(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_constraints/test_w_literal'))

    def test_test_w_notinrange(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_constraints/test_w_notinrange'))

    def test_test_w_notinset(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_constraints/test_w_notinset'))

    def test_test_w_notliteral(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_constraints/test_w_notliteral'))

    def test_test_watched_and(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_constraints/test_watched_and'))

    def test_test_watched_or(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_constraints/test_watched_or'))

    def test_test_watchelement1(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_constraints/test_watchelement1'))

    def test_test_watchelement2(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_constraints/test_watchelement2'))

    def test_test_watchelement3(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_constraints/test_watchelement3'))

    def test_test_watchelement_one(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_constraints/test_watchelement_one'))

    def test_test_watchelement_undefzero1(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_constraints/test_watchelement_undefzero1'))

    def test_test_watchelement_undefzero2(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_constraints/test_watchelement_undefzero2'))

    def test_test_watchless(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_constraints/test_watchless'))

    def test_test_watchsumeq(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_constraints/test_watchsumeq'))

    def test_test_watchsumgeq(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_constraints/test_watchsumgeq'))

    def test_test_watchsumleq(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_constraints/test_watchsumleq'))

    def test_test_watchvecneq1(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_constraints/test_watchvecneq1'))

    def test_test_watchvecneq2(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_constraints/test_watchvecneq2'))

    def test_test_weightedsumeq(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_constraints/test_weightedsumeq'))

    def test_test_weightedsumgeq(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_constraints/test_weightedsumgeq'))

    def test_test_weightedsumleq(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_constraints/test_weightedsumleq'))

