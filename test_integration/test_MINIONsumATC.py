from unittest import TestCase

from test_integration.interface.integrationtest_interface import IntegrationtestInterface


class TestMINIONsumATC(TestCase):

    def setUp(self) -> None:
        self._interface = IntegrationtestInterface()

    def test_2MINIONSumATC(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/minion_java_examples/MINIONsumATC/2MINIONSumATC'))

    def test_2MINIONSumATC_V1(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/minion_java_examples/MINIONsumATC/2MINIONSumATC_V1'))

    def test_2MINIONSumATC_V2(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/minion_java_examples/MINIONsumATC/2MINIONSumATC_V2'))

    def test_2MINIONSumATC_V3(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/minion_java_examples/MINIONsumATC/2MINIONSumATC_V3'))

    def test_2MINIONSumATC_V4(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/minion_java_examples/MINIONsumATC/2MINIONSumATC_V4'))

    def test_4MINIONSumATC(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/minion_java_examples/MINIONsumATC/4MINIONSumATC'))

    def test_4MINIONSumATC_V1(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/minion_java_examples/MINIONsumATC/4MINIONSumATC_V1'))

    def test_4MINIONSumATC_V2(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/minion_java_examples/MINIONsumATC/4MINIONSumATC_V2'))

    def test_4MINIONSumATC_V3(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/minion_java_examples/MINIONsumATC/4MINIONSumATC_V3'))

    def test_4MINIONSumATC_V4(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/minion_java_examples/MINIONsumATC/4MINIONSumATC_V4'))

    def test_7MINIONSumATC(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/minion_java_examples/MINIONsumATC/7MINIONSumATC'))

    def test_7MINIONSumATC_V1(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/minion_java_examples/MINIONsumATC/7MINIONSumATC_V1'))

    def test_7MINIONSumATC_V2(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/minion_java_examples/MINIONsumATC/7MINIONSumATC_V2'))

    def test_7MINIONSumATC_V3(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/minion_java_examples/MINIONsumATC/7MINIONSumATC_V3'))

    def test_7MINIONSumATC_V4(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/minion_java_examples/MINIONsumATC/7MINIONSumATC_V4'))

