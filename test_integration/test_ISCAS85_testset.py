from unittest import TestCase

from test_integration.interface.integrationtest_interface import IntegrationtestInterface


class TestISCAS85_testset(TestCase):

    def setUp(self) -> None:
        self._interface = IntegrationtestInterface()

    def test_c880_tc_3_30(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c880_tc_3_30'))

    def test_c880_tc_3_25(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c880_tc_3_25'))

    def test_c880_tc_3_24(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c880_tc_3_24'))

    def test_c880_tc_2_29(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c880_tc_2_29'))

    def test_c880_tc_2_27(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c880_tc_2_27'))

    def test_c880_tc_2_24(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c880_tc_2_24'))

    def test_c880_tc_2_22(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c880_tc_2_22'))

    def test_c880_tc_1_27(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c880_tc_1_27'))

    def test_c880_tc_1_23(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c880_tc_1_23'))

    def test_c5315_tc_1_78(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c5315_tc_1_78'))

    def test_c5315_tc_1_74(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c5315_tc_1_74'))

    def test_c5315_tc_1_73(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c5315_tc_1_73'))

    def test_c499_tc_2_17(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c499_tc_2_17'))

    def test_c499_tc_2_15(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c499_tc_2_15'))

    def test_c3540_tc_1_62(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c3540_tc_1_62'))

    def test_c2670_tc_3_59(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c2670_tc_3_59'))

    def test_c2670_tc_3_55(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c2670_tc_3_55'))

    def test_c2670_tc_3_52(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c2670_tc_3_52'))

    def test_c2670_tc_2_58(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c2670_tc_2_58'))

    def test_c2670_tc_2_53(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c2670_tc_2_53'))

    def test_c1908_tc_3_45(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c1908_tc_3_45'))

    def test_c1908_tc_3_41(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c1908_tc_3_41'))

    def test_c1908_tc_2_43(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c1908_tc_2_43'))

    def test_c1355_tc_3_40(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c1355_tc_3_40'))

