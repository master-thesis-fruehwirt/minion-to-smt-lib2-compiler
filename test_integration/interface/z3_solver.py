import os
from pathlib import Path

from src.arguments.const import Const
from src.arguments.list_access import ListAccess
from src.constraints.constraint_eq import Constraint_eq
from src.mapper.type import Type
from test_integration.interface.solver import Solver


class Z3Solver(Solver):

    def __init__(self):
        super().__init__()

    def zip_var_assignment(self, minion_vars, solution):
        zipped_assignment = []
        last_index = 0
        for var in minion_vars:
            if len(var.get_dimensions()) > 1:
                index = var.get_dimensions()[1]
                zipped_assignment.append((var, solution[last_index:last_index+index]))
                last_index += index
            elif len(var.get_dimensions()) <= 1:
                zipped_assignment.append((var, solution[last_index:last_index+1]))
                last_index += 1
        return zipped_assignment

    def create_additional_constraints(self, zipped_solutions):
        constraints = []
        for var, assignment in zipped_solutions:
            if len(assignment) == 1:
                if not var.get_dimensions():
                    constraints.append(Constraint_eq("eq", var, Const(int(assignment[0][0]))).to_smtlib2().to_code() + "\n")
                else:
                    for i, ass_i in enumerate(assignment[0]):
                        constraints.append(Constraint_eq("eq", Const(int(ass_i)),
                                                         ListAccess(var, i)).to_smtlib2().to_code() + "\n")
            else:
                for i, ass_i in enumerate(assignment):
                    for j, ass_j in enumerate(ass_i):
                        if var.get_type() == Type.TYPE_TABLE:
                            la = ListAccess(ListAccess(var, i), j)
                        else:
                            la = ListAccess(var, i, j)

                        constraints.append(Constraint_eq("eq", Const(int(ass_j)),
                                                         la).to_smtlib2().to_code() + "\n")
        return constraints

    def inject_z3_multiple_solutions(self, minion_vars, minion_solutions, filename, print_z3_output=False, keep_temp_files=False):
        print(f"Starting Z3 injections: {len(minion_solutions)}")
        result = None

        for count, solution in enumerate(minion_solutions):
            if result is None:
                result = True

            zipped_assignments = self.zip_var_assignment(minion_vars, solution)
            constraints = self.create_additional_constraints(zipped_assignments)

            with open(f"{filename}.smtlib2") as file:
                lines = file.readlines()

            new_code = []
            for i in range(len(lines)):
                if "(check-sat)" in lines[i]:
                    new_code = lines[:i] + constraints + lines[i:]

            new_filename = f"{filename}_modified_{count}.smtlib2"
            with open(new_filename, 'w') as file:
                file.writelines(new_code)

            check = self._run_solver(new_filename, print_z3_output, count, "sat")

            if not keep_temp_files:
                os.remove(new_filename)
            print(f"{filename}: {count + 1}/{len(minion_solutions)}: {check}")
            result = result and check

        if len(minion_solutions) == 0 and result is None:
            new_filename = f"{filename}.smtlib2"
            check = self._run_solver(new_filename, print_z3_output, 0, "unsat")
            print(f"{filename}: 0/{len(minion_solutions)}: {check}")
            result = check

        return result

    def _run_solver(self, filename, print_z3_output, count, search_str):
        root = Path(__file__).parent.parent.parent
        full_path = str(root) + "/solver/z3"
        output = self.execute_solver([full_path, filename], 180)

        if print_z3_output:
            output_file = f"{filename}_modified_{count}.out"
            with open(output_file, 'w') as file:
                file.writelines(output)

        lines = [o.strip() for o in output.splitlines()]
        if len(lines) == 0:
            return True
        return lines[0] == search_str  # 'sat' or 'unsat'
