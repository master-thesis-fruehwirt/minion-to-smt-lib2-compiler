import subprocess


class Solver:

    def __init__(self):
        pass

    def execute_solver(self, cmd, timeout=0):
        pipe = subprocess.Popen(cmd, stdout=subprocess.PIPE)
        try:
            if timeout > 0:
                out, _ = pipe.communicate(timeout=timeout)
            else:
                out, _ = pipe.communicate()
            return out.decode("utf-8")
        except:
            pipe.kill()
        return ""
