import os
import sys
from datetime import datetime
from pathlib import Path

from src.controller import Controller
from test_integration.interface.minion_solver import MinionSolver
from test_integration.interface.z3_solver import Z3Solver


class IntegrationtestInterface:

    def __init__(self):
        self._minion_solver = MinionSolver()
        self._z3_solver = Z3Solver()

    def _compile(self, filename):
        sys.argv = ["", f"{filename}.minion", "print-minion"]
        controller = Controller()
        data = controller.read_file()
        controller.compile(data)
        return controller.get_var_decls()

    def _get_filenames(self, path):
        l = os.listdir(path)
        files = [filename.split('.')[0] for filename in l if filename.endswith(".minion")]
        return files

    def run_dir(self, path, exceptions=None, print_z3_output=False, keep_temp_files=False):
        if exceptions is None:
            exceptions = []
        root = Path(__file__).parent.parent
        full_path = str(root) + "/" + path
        files = self._get_filenames(full_path)
        files = [file for file in files if file not in exceptions]

        result = True
        overall_count = 0
        for file in files:
            filename = f"../{path}/{file}"
            print(f"Testing: {path}/{file}")
            variables = self._compile(filename)
            solutions = self._minion_solver.execute_minion_allsols(filename)
            overall_count += len(solutions)
            result = self._z3_solver.inject_z3_multiple_solutions(variables, solutions, filename, print_z3_output, keep_temp_files) and result
        return result

    def print_time(self, t1, t2, text):
        t = t2 - t1
        print(f"{text}: {t}")

    def run_file(self, path, print_z3_output=False, keep_temp_files=False):
        filename = f"../{path}"
        start_time = datetime.now()
        variables = self._compile(filename)
        compile_time = datetime.now()
        solutions = self._minion_solver.execute_minion_allsols(filename)
        minion_time = datetime.now()
        result = self._z3_solver.inject_z3_multiple_solutions(variables, solutions, filename, print_z3_output, keep_temp_files)
        z3_time = datetime.now()
        self.print_time(start_time, compile_time, "cmp")
        self.print_time(compile_time, minion_time, "min")
        self.print_time(minion_time, z3_time, "sol")
        return result

    def cleanup_dir(self, path):
        root = Path(__file__).parent.parent.parent
        full_path = str(root) + "/" + path
        l = os.listdir(full_path)
        files = [filename for filename in l if filename.endswith(".out") or filename.endswith(".smtlib2")]
        for file in files:
            filename = f"../{path}/{file}"
            os.remove(filename)

    def run_minion_only(self, path):
        full_path = f"../{path}"
        output = self._minion_solver.run_minion(full_path)
        print(output)
        return True

    def run_compile_only(self, path, print_z3_output=False, keep_temp_files=False):
        filename = f"../{path}"

        variables = self._compile(filename)

        return self._z3_solver.inject_z3_multiple_solutions(variables, [], filename, print_z3_output, keep_temp_files)