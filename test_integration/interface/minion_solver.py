from pathlib import Path

from test_integration.interface.solver import Solver


class MinionSolver(Solver):

    def __init__(self):
        super().__init__()

    def run_minion(self, filename):
        filename = f"{filename}.minion"
        root = Path(__file__).parent.parent.parent
        full_path = str(root) + "/solver/minion"
        return self.execute_solver([full_path, "-findallsols", "-sollimit", "5", "-timelimit", "90", filename])

    def execute_minion_allsols(self, filename):
        output = self.run_minion(filename)

        minion_output = [o.strip() for o in output.splitlines()]
        results = [r.split(" ") for r in minion_output]

        solutions = []
        solution = []
        for line in results:
            if "Sol:" in line:
                solution.append(line[1:])
            elif len(line) == 1 and line[0] == '':
                if solution:
                    solutions.append(solution)
                solution = []

        return solutions
