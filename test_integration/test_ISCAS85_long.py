from unittest import TestCase

from test_integration.interface.integrationtest_interface import IntegrationtestInterface


class TestISCAS85_long(TestCase):

    def setUp(self) -> None:
        self._interface = IntegrationtestInterface()

    def test_c3540_tc_2_61(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/ISCAS85_long/c3540_tc_2_61'))

    def test_c3540_tc_2_62(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/ISCAS85_long/c3540_tc_2_62'))

    def test_c3540_tc_2_63(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/ISCAS85_long/c3540_tc_2_63'))

    def test_c3540_tc_2_64(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/ISCAS85_long/c3540_tc_2_64'))

    def test_c3540_tc_2_65(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/ISCAS85_long/c3540_tc_2_65'))

    def test_c3540_tc_2_66(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/ISCAS85_long/c3540_tc_2_66'))

    def test_c3540_tc_2_67(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/ISCAS85_long/c3540_tc_2_67'))

    def test_c3540_tc_2_68(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/ISCAS85_long/c3540_tc_2_68'))

    def test_c3540_tc_2_69(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/ISCAS85_long/c3540_tc_2_69'))

    def test_c3540_tc_2_70(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/ISCAS85_long/c3540_tc_2_70'))

    def test_c3540_tc_3_61(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/ISCAS85_long/c3540_tc_3_61'))

    def test_c3540_tc_3_62(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/ISCAS85_long/c3540_tc_3_62'))

    def test_c3540_tc_3_63(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/ISCAS85_long/c3540_tc_3_63'))

    def test_c3540_tc_3_64(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/ISCAS85_long/c3540_tc_3_64'))

    def test_c3540_tc_3_65(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/ISCAS85_long/c3540_tc_3_65'))

    def test_c3540_tc_3_66(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/ISCAS85_long/c3540_tc_3_66'))

    def test_c3540_tc_3_67(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/ISCAS85_long/c3540_tc_3_67'))

    def test_c3540_tc_3_68(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/ISCAS85_long/c3540_tc_3_68'))

    def test_c3540_tc_3_69(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/ISCAS85_long/c3540_tc_3_69'))

    def test_c3540_tc_3_70(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/ISCAS85_long/c3540_tc_3_70'))

    def test_c5315_tc_2_71(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/ISCAS85_long/c5315_tc_2_71'))

    def test_c5315_tc_2_72(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/ISCAS85_long/c5315_tc_2_72'))

    def test_c5315_tc_2_73(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/ISCAS85_long/c5315_tc_2_73'))

    def test_c5315_tc_2_74(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/ISCAS85_long/c5315_tc_2_74'))

    def test_c5315_tc_2_75(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/ISCAS85_long/c5315_tc_2_75'))

    def test_c5315_tc_2_76(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/ISCAS85_long/c5315_tc_2_76'))

    def test_c5315_tc_2_77(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/ISCAS85_long/c5315_tc_2_77'))

    def test_c5315_tc_2_78(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/ISCAS85_long/c5315_tc_2_78'))

    def test_c5315_tc_2_79(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/ISCAS85_long/c5315_tc_2_79'))

    def test_c5315_tc_2_80(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/ISCAS85_long/c5315_tc_2_80'))

    def test_c5315_tc_3_71(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/ISCAS85_long/c5315_tc_3_71'))

    def test_c5315_tc_3_72(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/ISCAS85_long/c5315_tc_3_72'))

    def test_c5315_tc_3_73(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/ISCAS85_long/c5315_tc_3_73'))

    def test_c5315_tc_3_74(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/ISCAS85_long/c5315_tc_3_74'))

    def test_c5315_tc_3_75(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/ISCAS85_long/c5315_tc_3_75'))

    def test_c5315_tc_3_76(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/ISCAS85_long/c5315_tc_3_76'))

    def test_c5315_tc_3_77(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/ISCAS85_long/c5315_tc_3_77'))

    def test_c5315_tc_3_78(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/ISCAS85_long/c5315_tc_3_78'))

    def test_c5315_tc_3_79(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/ISCAS85_long/c5315_tc_3_79'))

    def test_c5315_tc_3_80(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/ISCAS85_long/c5315_tc_3_80'))

    def test_c6288_tc_1_81(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/ISCAS85_long/c6288_tc_1_81'))

    def test_c6288_tc_1_82(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/ISCAS85_long/c6288_tc_1_82'))

    def test_c6288_tc_1_83(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/ISCAS85_long/c6288_tc_1_83'))

    def test_c6288_tc_1_84(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/ISCAS85_long/c6288_tc_1_84'))

    def test_c6288_tc_1_85(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/ISCAS85_long/c6288_tc_1_85'))

    def test_c6288_tc_1_86(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/ISCAS85_long/c6288_tc_1_86'))

    def test_c6288_tc_1_87(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/ISCAS85_long/c6288_tc_1_87'))

    def test_c6288_tc_1_88(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/ISCAS85_long/c6288_tc_1_88'))

    def test_c6288_tc_1_89(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/ISCAS85_long/c6288_tc_1_89'))

    def test_c6288_tc_1_90(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/ISCAS85_long/c6288_tc_1_90'))

    def test_c6288_tc_2_81(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/ISCAS85_long/c6288_tc_2_81'))

    def test_c6288_tc_2_82(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/ISCAS85_long/c6288_tc_2_82'))

    def test_c6288_tc_2_83(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/ISCAS85_long/c6288_tc_2_83'))

    def test_c6288_tc_2_84(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/ISCAS85_long/c6288_tc_2_84'))

    def test_c6288_tc_2_85(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/ISCAS85_long/c6288_tc_2_85'))

    def test_c6288_tc_2_86(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/ISCAS85_long/c6288_tc_2_86'))

    def test_c6288_tc_2_87(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/ISCAS85_long/c6288_tc_2_87'))

    def test_c6288_tc_2_88(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/ISCAS85_long/c6288_tc_2_88'))

    def test_c6288_tc_2_89(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/ISCAS85_long/c6288_tc_2_89'))

    def test_c6288_tc_2_90(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/ISCAS85_long/c6288_tc_2_90'))

    def test_c6288_tc_3_81(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/ISCAS85_long/c6288_tc_3_81'))

    def test_c6288_tc_3_82(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/ISCAS85_long/c6288_tc_3_82'))

    def test_c6288_tc_3_83(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/ISCAS85_long/c6288_tc_3_83'))

    def test_c6288_tc_3_84(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/ISCAS85_long/c6288_tc_3_84'))

    def test_c6288_tc_3_85(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/ISCAS85_long/c6288_tc_3_85'))

    def test_c6288_tc_3_86(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/ISCAS85_long/c6288_tc_3_86'))

    def test_c6288_tc_3_87(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/ISCAS85_long/c6288_tc_3_87'))

    def test_c6288_tc_3_88(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/ISCAS85_long/c6288_tc_3_88'))

    def test_c6288_tc_3_89(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/ISCAS85_long/c6288_tc_3_89'))

    def test_c6288_tc_3_90(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/ISCAS85_long/c6288_tc_3_90'))

    def test_c7552_tc_1_100(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/ISCAS85_long/c7552_tc_1_100'))

    def test_c7552_tc_1_91(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/ISCAS85_long/c7552_tc_1_91'))

    def test_c7552_tc_1_92(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/ISCAS85_long/c7552_tc_1_92'))

    def test_c7552_tc_1_93(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/ISCAS85_long/c7552_tc_1_93'))

    def test_c7552_tc_1_94(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/ISCAS85_long/c7552_tc_1_94'))

    def test_c7552_tc_1_95(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/ISCAS85_long/c7552_tc_1_95'))

    def test_c7552_tc_1_96(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/ISCAS85_long/c7552_tc_1_96'))

    def test_c7552_tc_1_97(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/ISCAS85_long/c7552_tc_1_97'))

    def test_c7552_tc_1_98(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/ISCAS85_long/c7552_tc_1_98'))

    def test_c7552_tc_1_99(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/ISCAS85_long/c7552_tc_1_99'))

    def test_c7552_tc_2_100(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/ISCAS85_long/c7552_tc_2_100'))

    def test_c7552_tc_2_91(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/ISCAS85_long/c7552_tc_2_91'))

    def test_c7552_tc_2_92(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/ISCAS85_long/c7552_tc_2_92'))

    def test_c7552_tc_2_93(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/ISCAS85_long/c7552_tc_2_93'))

    def test_c7552_tc_2_94(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/ISCAS85_long/c7552_tc_2_94'))

    def test_c7552_tc_2_95(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/ISCAS85_long/c7552_tc_2_95'))

    def test_c7552_tc_2_96(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/ISCAS85_long/c7552_tc_2_96'))

    def test_c7552_tc_2_97(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/ISCAS85_long/c7552_tc_2_97'))

    def test_c7552_tc_2_98(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/ISCAS85_long/c7552_tc_2_98'))

    def test_c7552_tc_2_99(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/ISCAS85_long/c7552_tc_2_99'))

    def test_c7552_tc_3_100(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/ISCAS85_long/c7552_tc_3_100'))

    def test_c7552_tc_3_91(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/ISCAS85_long/c7552_tc_3_91'))

    def test_c7552_tc_3_92(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/ISCAS85_long/c7552_tc_3_92'))

    def test_c7552_tc_3_93(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/ISCAS85_long/c7552_tc_3_93'))

    def test_c7552_tc_3_94(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/ISCAS85_long/c7552_tc_3_94'))

    def test_c7552_tc_3_95(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/ISCAS85_long/c7552_tc_3_95'))

    def test_c7552_tc_3_96(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/ISCAS85_long/c7552_tc_3_96'))

    def test_c7552_tc_3_97(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/ISCAS85_long/c7552_tc_3_97'))

    def test_c7552_tc_3_98(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/ISCAS85_long/c7552_tc_3_98'))

    def test_c7552_tc_3_99(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/ISCAS85_long/c7552_tc_3_99'))

