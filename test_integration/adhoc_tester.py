from unittest import TestCase

from test_integration.interface.integrationtest_interface import IntegrationtestInterface


class TestIntegration(TestCase):

    def setUp(self) -> None:
        self._interface = IntegrationtestInterface()

    def test_cleanup(self):
        self._interface.cleanup_dir("test_files/testcases_constraints")

    def test_minion_only(self):
        self.assertTrue(self._interface.run_minion_only("test_files/testcases_advanced/spreadsheet_examples/AFW_arithmetics00_1Faults_Fault2_value"))

    def test_compile_only(self):
        self.assertTrue(self._interface.run_compile_only("test_files/testcases_advanced/spreadsheet_examples/AFW_book_recommendation_1Faults_Fault1_comparison"))

    def test_adhoc(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_constraints/test_table1', True, True))

    def test_long(self):
        self.assertTrue(self._interface.run_file("test_files/testcases_advanced/ISCAS85/ISCAS85_long/c3540_tc_2_61", True, True))
