from unittest import TestCase

from test_integration.interface.integrationtest_interface import IntegrationtestInterface


class TestMINIONgcDATC(TestCase):

    def setUp(self) -> None:
        self._interface = IntegrationtestInterface()

    def test_2MINIONGcdATC(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/minion_java_examples/MINIONgcDATC/2MINIONGcdATC'))

    def test_2MINIONGcdATC_V1(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/minion_java_examples/MINIONgcDATC/2MINIONGcdATC_V1'))

    def test_2MINIONGcdATC_V2(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/minion_java_examples/MINIONgcDATC/2MINIONGcdATC_V2'))

    def test_2MINIONGcdATC_V3(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/minion_java_examples/MINIONgcDATC/2MINIONGcdATC_V3'))

    def test_2MINIONGcdATC_V4(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/minion_java_examples/MINIONgcDATC/2MINIONGcdATC_V4'))

    def test_4MINIONGcdATC(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/minion_java_examples/MINIONgcDATC/4MINIONGcdATC'))

    def test_4MINIONGcdATC_V1(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/minion_java_examples/MINIONgcDATC/4MINIONGcdATC_V1'))

    def test_4MINIONGcdATC_V2(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/minion_java_examples/MINIONgcDATC/4MINIONGcdATC_V2'))

    def test_4MINIONGcdATC_V3(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/minion_java_examples/MINIONgcDATC/4MINIONGcdATC_V3'))

    def test_4MINIONGcdATC_V4(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/minion_java_examples/MINIONgcDATC/4MINIONGcdATC_V4'))

    def test_7MINIONGcdATC(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/minion_java_examples/MINIONgcDATC/7MINIONGcdATC'))

    def test_7MINIONGcdATC_V1(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/minion_java_examples/MINIONgcDATC/7MINIONGcdATC_V1'))

    def test_7MINIONGcdATC_V2(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/minion_java_examples/MINIONgcDATC/7MINIONGcdATC_V2'))

    def test_7MINIONGcdATC_V3(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/minion_java_examples/MINIONgcDATC/7MINIONGcdATC_V3'))

    def test_7MINIONGcdATC_V4(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/minion_java_examples/MINIONgcDATC/7MINIONGcdATC_V4'))

