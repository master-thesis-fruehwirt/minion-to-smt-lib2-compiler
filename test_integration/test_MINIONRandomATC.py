from unittest import TestCase

from test_integration.interface.integrationtest_interface import IntegrationtestInterface


class TestMINIONRandomATC(TestCase):

    def setUp(self) -> None:
        self._interface = IntegrationtestInterface()

    def test_2MINIONRandomATC(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/minion_java_examples/MINIONRandomATC/2MINIONRandomATC'))

    def test_2MINIONRandomATC_V1(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/minion_java_examples/MINIONRandomATC/2MINIONRandomATC_V1'))

    def test_2MINIONRandomATC_V2(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/minion_java_examples/MINIONRandomATC/2MINIONRandomATC_V2'))

    def test_2MINIONRandomATC_V3(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/minion_java_examples/MINIONRandomATC/2MINIONRandomATC_V3'))

    def test_2MINIONRandomATC_V4(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/minion_java_examples/MINIONRandomATC/2MINIONRandomATC_V4'))

    def test_4MINIONRandomATC(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/minion_java_examples/MINIONRandomATC/4MINIONRandomATC'))

    def test_4MINIONRandomATC_V1(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/minion_java_examples/MINIONRandomATC/4MINIONRandomATC_V1'))

    def test_4MINIONRandomATC_V2(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/minion_java_examples/MINIONRandomATC/4MINIONRandomATC_V2'))

    def test_4MINIONRandomATC_V3(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/minion_java_examples/MINIONRandomATC/4MINIONRandomATC_V3'))

    def test_4MINIONRandomATC_V4(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/minion_java_examples/MINIONRandomATC/4MINIONRandomATC_V4'))

    def test_7MINIONRandomATC(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/minion_java_examples/MINIONRandomATC/7MINIONRandomATC'))

    def test_7MINIONRandomATC_V1(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/minion_java_examples/MINIONRandomATC/7MINIONRandomATC_V1'))

    def test_7MINIONRandomATC_V2(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/minion_java_examples/MINIONRandomATC/7MINIONRandomATC_V2'))

    def test_7MINIONRandomATC_V3(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/minion_java_examples/MINIONRandomATC/7MINIONRandomATC_V3'))

    def test_7MINIONRandomATC_V4(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/minion_java_examples/MINIONRandomATC/7MINIONRandomATC_V4'))

