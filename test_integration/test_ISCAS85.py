from unittest import TestCase

from test_integration.interface.integrationtest_interface import IntegrationtestInterface


class TestISCAS85(TestCase):

    def setUp(self) -> None:
        self._interface = IntegrationtestInterface()

    def test_c1355_tc_1_31(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c1355_tc_1_31'))

    def test_c1355_tc_1_32(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c1355_tc_1_32'))

    def test_c1355_tc_1_33(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c1355_tc_1_33'))

    def test_c1355_tc_1_34(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c1355_tc_1_34'))

    def test_c1355_tc_1_35(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c1355_tc_1_35'))

    def test_c1355_tc_1_36(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c1355_tc_1_36'))

    def test_c1355_tc_1_37(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c1355_tc_1_37'))

    def test_c1355_tc_1_38(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c1355_tc_1_38'))

    def test_c1355_tc_1_39(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c1355_tc_1_39'))

    def test_c1355_tc_1_40(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c1355_tc_1_40'))

    def test_c1355_tc_2_31(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c1355_tc_2_31'))

    def test_c1355_tc_2_32(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c1355_tc_2_32'))

    def test_c1355_tc_2_33(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c1355_tc_2_33'))

    def test_c1355_tc_2_34(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c1355_tc_2_34'))

    def test_c1355_tc_2_35(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c1355_tc_2_35'))

    def test_c1355_tc_2_36(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c1355_tc_2_36'))

    def test_c1355_tc_2_37(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c1355_tc_2_37'))

    def test_c1355_tc_2_38(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c1355_tc_2_38'))

    def test_c1355_tc_2_39(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c1355_tc_2_39'))

    def test_c1355_tc_2_40(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c1355_tc_2_40'))

    def test_c1355_tc_3_31(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c1355_tc_3_31'))

    def test_c1355_tc_3_32(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c1355_tc_3_32'))

    def test_c1355_tc_3_33(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c1355_tc_3_33'))

    def test_c1355_tc_3_34(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c1355_tc_3_34'))

    def test_c1355_tc_3_35(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c1355_tc_3_35'))

    def test_c1355_tc_3_36(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c1355_tc_3_36'))

    def test_c1355_tc_3_37(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c1355_tc_3_37'))

    def test_c1355_tc_3_38(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c1355_tc_3_38'))

    def test_c1355_tc_3_39(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c1355_tc_3_39'))

    def test_c1355_tc_3_40(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c1355_tc_3_40'))

    def test_c1908_tc_1_41(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c1908_tc_1_41'))

    def test_c1908_tc_1_42(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c1908_tc_1_42'))

    def test_c1908_tc_1_43(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c1908_tc_1_43'))

    def test_c1908_tc_1_44(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c1908_tc_1_44'))

    def test_c1908_tc_1_45(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c1908_tc_1_45'))

    def test_c1908_tc_1_46(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c1908_tc_1_46'))

    def test_c1908_tc_1_47(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c1908_tc_1_47'))

    def test_c1908_tc_1_48(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c1908_tc_1_48'))

    def test_c1908_tc_1_49(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c1908_tc_1_49'))

    def test_c1908_tc_1_50(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c1908_tc_1_50'))

    def test_c1908_tc_2_41(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c1908_tc_2_41'))

    def test_c1908_tc_2_42(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c1908_tc_2_42'))

    def test_c1908_tc_2_43(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c1908_tc_2_43'))

    def test_c1908_tc_2_44(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c1908_tc_2_44'))

    def test_c1908_tc_2_45(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c1908_tc_2_45'))

    def test_c1908_tc_2_46(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c1908_tc_2_46'))

    def test_c1908_tc_2_47(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c1908_tc_2_47'))

    def test_c1908_tc_2_48(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c1908_tc_2_48'))

    def test_c1908_tc_2_49(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c1908_tc_2_49'))

    def test_c1908_tc_2_50(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c1908_tc_2_50'))

    def test_c1908_tc_3_41(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c1908_tc_3_41'))

    def test_c1908_tc_3_42(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c1908_tc_3_42'))

    def test_c1908_tc_3_43(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c1908_tc_3_43'))

    def test_c1908_tc_3_44(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c1908_tc_3_44'))

    def test_c1908_tc_3_45(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c1908_tc_3_45'))

    def test_c1908_tc_3_46(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c1908_tc_3_46'))

    def test_c1908_tc_3_47(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c1908_tc_3_47'))

    def test_c1908_tc_3_48(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c1908_tc_3_48'))

    def test_c1908_tc_3_49(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c1908_tc_3_49'))

    def test_c1908_tc_3_50(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c1908_tc_3_50'))

    def test_c2670_tc_1_51(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c2670_tc_1_51'))

    def test_c2670_tc_1_52(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c2670_tc_1_52'))

    def test_c2670_tc_1_53(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c2670_tc_1_53'))

    def test_c2670_tc_1_54(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c2670_tc_1_54'))

    def test_c2670_tc_1_55(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c2670_tc_1_55'))

    def test_c2670_tc_1_56(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c2670_tc_1_56'))

    def test_c2670_tc_1_57(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c2670_tc_1_57'))

    def test_c2670_tc_1_58(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c2670_tc_1_58'))

    def test_c2670_tc_1_59(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c2670_tc_1_59'))

    def test_c2670_tc_1_60(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c2670_tc_1_60'))

    def test_c2670_tc_2_51(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c2670_tc_2_51'))

    def test_c2670_tc_2_52(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c2670_tc_2_52'))

    def test_c2670_tc_2_53(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c2670_tc_2_53'))

    def test_c2670_tc_2_54(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c2670_tc_2_54'))

    def test_c2670_tc_2_55(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c2670_tc_2_55'))

    def test_c2670_tc_2_56(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c2670_tc_2_56'))

    def test_c2670_tc_2_57(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c2670_tc_2_57'))

    def test_c2670_tc_2_58(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c2670_tc_2_58'))

    def test_c2670_tc_2_59(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c2670_tc_2_59'))

    def test_c2670_tc_2_60(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c2670_tc_2_60'))

    def test_c2670_tc_3_51(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c2670_tc_3_51'))

    def test_c2670_tc_3_52(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c2670_tc_3_52'))

    def test_c2670_tc_3_53(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c2670_tc_3_53'))

    def test_c2670_tc_3_54(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c2670_tc_3_54'))

    def test_c2670_tc_3_55(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c2670_tc_3_55'))

    def test_c2670_tc_3_56(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c2670_tc_3_56'))

    def test_c2670_tc_3_57(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c2670_tc_3_57'))

    def test_c2670_tc_3_58(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c2670_tc_3_58'))

    def test_c2670_tc_3_59(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c2670_tc_3_59'))

    def test_c2670_tc_3_60(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c2670_tc_3_60'))

    def test_c3540_tc_1_61(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c3540_tc_1_61'))

    def test_c3540_tc_1_62(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c3540_tc_1_62'))

    def test_c3540_tc_1_63(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c3540_tc_1_63'))

    def test_c3540_tc_1_64(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c3540_tc_1_64'))

    def test_c3540_tc_1_65(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c3540_tc_1_65'))

    def test_c3540_tc_1_66(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c3540_tc_1_66'))

    def test_c3540_tc_1_67(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c3540_tc_1_67'))

    def test_c3540_tc_1_68(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c3540_tc_1_68'))

    def test_c3540_tc_1_69(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c3540_tc_1_69'))

    def test_c3540_tc_1_70(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c3540_tc_1_70'))

    def test_c432_tc_1_1(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c432_tc_1_1'))

    def test_c432_tc_1_10(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c432_tc_1_10'))

    def test_c432_tc_1_2(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c432_tc_1_2'))

    def test_c432_tc_1_3(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c432_tc_1_3'))

    def test_c432_tc_1_4(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c432_tc_1_4'))

    def test_c432_tc_1_5(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c432_tc_1_5'))

    def test_c432_tc_1_6(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c432_tc_1_6'))

    def test_c432_tc_1_7(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c432_tc_1_7'))

    def test_c432_tc_1_8(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c432_tc_1_8'))

    def test_c432_tc_1_9(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c432_tc_1_9'))

    def test_c432_tc_2_1(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c432_tc_2_1'))

    def test_c432_tc_2_10(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c432_tc_2_10'))

    def test_c432_tc_2_2(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c432_tc_2_2'))

    def test_c432_tc_2_3(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c432_tc_2_3'))

    def test_c432_tc_2_4(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c432_tc_2_4'))

    def test_c432_tc_2_5(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c432_tc_2_5'))

    def test_c432_tc_2_6(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c432_tc_2_6'))

    def test_c432_tc_2_7(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c432_tc_2_7'))

    def test_c432_tc_2_8(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c432_tc_2_8'))

    def test_c432_tc_2_9(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c432_tc_2_9'))

    def test_c432_tc_3_1(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c432_tc_3_1'))

    def test_c432_tc_3_10(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c432_tc_3_10'))

    def test_c432_tc_3_2(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c432_tc_3_2'))

    def test_c432_tc_3_3(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c432_tc_3_3'))

    def test_c432_tc_3_4(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c432_tc_3_4'))

    def test_c432_tc_3_5(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c432_tc_3_5'))

    def test_c432_tc_3_6(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c432_tc_3_6'))

    def test_c432_tc_3_7(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c432_tc_3_7'))

    def test_c432_tc_3_8(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c432_tc_3_8'))

    def test_c432_tc_3_9(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c432_tc_3_9'))

    def test_c499_tc_1_11(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c499_tc_1_11'))

    def test_c499_tc_1_12(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c499_tc_1_12'))

    def test_c499_tc_1_13(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c499_tc_1_13'))

    def test_c499_tc_1_14(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c499_tc_1_14'))

    def test_c499_tc_1_15(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c499_tc_1_15'))

    def test_c499_tc_1_16(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c499_tc_1_16'))

    def test_c499_tc_1_17(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c499_tc_1_17'))

    def test_c499_tc_1_18(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c499_tc_1_18'))

    def test_c499_tc_1_19(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c499_tc_1_19'))

    def test_c499_tc_1_20(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c499_tc_1_20'))

    def test_c499_tc_2_11(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c499_tc_2_11'))

    def test_c499_tc_2_12(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c499_tc_2_12'))

    def test_c499_tc_2_13(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c499_tc_2_13'))

    def test_c499_tc_2_14(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c499_tc_2_14'))

    def test_c499_tc_2_15(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c499_tc_2_15'))

    def test_c499_tc_2_16(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c499_tc_2_16'))

    def test_c499_tc_2_17(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c499_tc_2_17'))

    def test_c499_tc_2_18(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c499_tc_2_18'))

    def test_c499_tc_2_19(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c499_tc_2_19'))

    def test_c499_tc_2_20(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c499_tc_2_20'))

    def test_c499_tc_3_11(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c499_tc_3_11'))

    def test_c499_tc_3_12(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c499_tc_3_12'))

    def test_c499_tc_3_13(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c499_tc_3_13'))

    def test_c499_tc_3_14(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c499_tc_3_14'))

    def test_c499_tc_3_15(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c499_tc_3_15'))

    def test_c499_tc_3_16(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c499_tc_3_16'))

    def test_c499_tc_3_17(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c499_tc_3_17'))

    def test_c499_tc_3_18(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c499_tc_3_18'))

    def test_c499_tc_3_19(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c499_tc_3_19'))

    def test_c499_tc_3_20(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c499_tc_3_20'))

    def test_c5315_tc_1_71(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c5315_tc_1_71'))

    def test_c5315_tc_1_72(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c5315_tc_1_72'))

    def test_c5315_tc_1_73(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c5315_tc_1_73'))

    def test_c5315_tc_1_74(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c5315_tc_1_74'))

    def test_c5315_tc_1_75(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c5315_tc_1_75'))

    def test_c5315_tc_1_76(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c5315_tc_1_76'))

    def test_c5315_tc_1_77(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c5315_tc_1_77'))

    def test_c5315_tc_1_78(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c5315_tc_1_78'))

    def test_c5315_tc_1_79(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c5315_tc_1_79'))

    def test_c5315_tc_1_80(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c5315_tc_1_80'))

    def test_c880_tc_1_21(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c880_tc_1_21'))

    def test_c880_tc_1_22(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c880_tc_1_22'))

    def test_c880_tc_1_23(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c880_tc_1_23'))

    def test_c880_tc_1_24(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c880_tc_1_24'))

    def test_c880_tc_1_25(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c880_tc_1_25'))

    def test_c880_tc_1_26(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c880_tc_1_26'))

    def test_c880_tc_1_27(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c880_tc_1_27'))

    def test_c880_tc_1_28(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c880_tc_1_28'))

    def test_c880_tc_1_29(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c880_tc_1_29'))

    def test_c880_tc_1_30(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c880_tc_1_30'))

    def test_c880_tc_2_21(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c880_tc_2_21'))

    def test_c880_tc_2_22(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c880_tc_2_22'))

    def test_c880_tc_2_23(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c880_tc_2_23'))

    def test_c880_tc_2_24(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c880_tc_2_24'))

    def test_c880_tc_2_25(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c880_tc_2_25'))

    def test_c880_tc_2_26(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c880_tc_2_26'))

    def test_c880_tc_2_27(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c880_tc_2_27'))

    def test_c880_tc_2_28(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c880_tc_2_28'))

    def test_c880_tc_2_29(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c880_tc_2_29'))

    def test_c880_tc_2_30(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c880_tc_2_30'))

    def test_c880_tc_3_21(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c880_tc_3_21'))

    def test_c880_tc_3_22(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c880_tc_3_22'))

    def test_c880_tc_3_23(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c880_tc_3_23'))

    def test_c880_tc_3_24(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c880_tc_3_24'))

    def test_c880_tc_3_25(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c880_tc_3_25'))

    def test_c880_tc_3_26(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c880_tc_3_26'))

    def test_c880_tc_3_27(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c880_tc_3_27'))

    def test_c880_tc_3_28(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c880_tc_3_28'))

    def test_c880_tc_3_29(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c880_tc_3_29'))

    def test_c880_tc_3_30(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c880_tc_3_30'))

