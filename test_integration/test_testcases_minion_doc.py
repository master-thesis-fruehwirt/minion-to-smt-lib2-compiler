from unittest import TestCase

from test_integration.interface.integrationtest_interface import IntegrationtestInterface


class Testtestcases_minion_doc(TestCase):

    def setUp(self) -> None:
        self._interface = IntegrationtestInterface()

    def test_EightNumberPuzzle(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_minion_doc/EightNumberPuzzle'))

    def test_FarmersProblem(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_minion_doc/FarmersProblem'))

    def test_K4P2GracefulGraph(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_minion_doc/K4P2GracefulGraph'))

    def test_NQueensBool(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_minion_doc/NQueensBool'))

    def test_NQueensColumn(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_minion_doc/NQueensColumn'))

    def test_SENDMOREMONEY(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_minion_doc/SENDMOREMONEY'))

    def test_zebra(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_minion_doc/zebra'))

