from unittest import TestCase

from test_integration.interface.integrationtest_interface import IntegrationtestInterface


class TestISCAS85_long_testset(TestCase):

    def setUp(self) -> None:
        self._interface = IntegrationtestInterface()

    def test_c5315_tc_1_71(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c5315_tc_1_71'))

    def test_c5315_tc_1_72(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c5315_tc_1_72'))

    def test_c5315_tc_1_73(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c5315_tc_1_73'))

    def test_c5315_tc_1_74(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/c5315_tc_1_74'))

    def test_c3540_tc_2_61(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/ISCAS85_long/c3540_tc_2_61'))

    def test_c3540_tc_2_63(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/ISCAS85_long/c3540_tc_2_63'))

    def test_c3540_tc_3_61(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/ISCAS85_long/c3540_tc_3_61'))

    def test_c3540_tc_3_63(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/ISCAS85_long/c3540_tc_3_63'))

    def test_c3540_tc_3_66(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/ISCAS85_long/c3540_tc_3_66'))

    def test_c5315_tc_2_71(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/ISCAS85_long/c5315_tc_2_71'))

    def test_c5315_tc_2_72(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/ISCAS85_long/c5315_tc_2_72'))

    def test_c5315_tc_2_73(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/ISCAS85_long/c5315_tc_2_73'))

    def test_c5315_tc_2_74(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/ISCAS85_long/c5315_tc_2_74'))

    def test_c5315_tc_2_75(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/ISCAS85_long/c5315_tc_2_75'))

    def test_c5315_tc_2_78(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/ISCAS85_long/c5315_tc_2_78'))

    def test_c5315_tc_2_79(self):
        self.assertTrue(self._interface.run_file('test_files/testcases_advanced/ISCAS85/ISCAS85_long/c5315_tc_2_79'))

